package de.ninenations;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import de.ninenations.core.NN;
import de.ninenations.util.YLog;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {
	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        config.useImmersiveMode = true;
        config.hideStatusBar = true;

		initialize(NN.init(new AndroidPlattform(this)), config);
	}

}

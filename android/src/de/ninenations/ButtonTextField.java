/**
 * 
 */
package de.ninenations;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.ITextfield;
import de.ninenations.ui.elements.YRandomField;
import de.tomgrill.gdxdialogs.core.GDXDialogsSystem;
import de.tomgrill.gdxdialogs.core.dialogs.GDXTextPrompt;
import de.tomgrill.gdxdialogs.core.listener.TextPromptListener;

/**
 * @author sven
 *
 */
public class ButtonTextField extends VisTextButton implements ITextfield {

	/**
	 * @param text
	 */
	public ButtonTextField(final YRandomField base) {
		super("");

		addCaptureListener(new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				showDialog(base);

			}
		});
	}

	private void showDialog(final YRandomField base) {
		GDXTextPrompt textPrompt = GDXDialogsSystem.getDialogManager().newDialog(GDXTextPrompt.class);

		textPrompt.setTitle("The name");
		textPrompt.setMessage("Please write the name for " + base.getText());

		textPrompt.setCancelButtonLabel("Cancel");
		textPrompt.setConfirmButtonLabel("Save name");

		textPrompt.setTextPromptListener(new TextPromptListener() {

			@Override
			public void confirm(String text) {
				base.saveText(text);
				setText(text);
			}

			@Override
			public void cancel() {
				// handle input cancel
			}
		});

		textPrompt.build().show();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.elements.ITextfield#getText()
	 */
	@Override
	public String getText() {
		return getLabel().getText().toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.elements.ITextfield#getActor()
	 */
	@Override
	public Actor getActor() {
		return this;
	}

}

/**
 * 
 */
package de.ninenations;

import java.net.URL;
import java.net.URLClassLoader;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.kotcrab.vis.ui.util.dialog.InputDialogListener;

import de.ninenations.core.BasePlattform;
import de.ninenations.game.S;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.mod.IMod;
import de.ninenations.mod.ModInfo;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.ITextfield;
import de.ninenations.ui.elements.YRandomField;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.YError;
import de.tomgrill.gdxdialogs.core.GDXDialogs;
import de.tomgrill.gdxdialogs.core.GDXDialogsSystem;
import de.tomgrill.gdxdialogs.core.dialogs.GDXButtonDialog;
import de.tomgrill.gdxdialogs.core.dialogs.GDXTextPrompt;
import de.tomgrill.gdxdialogs.core.listener.ButtonClickListener;
import de.tomgrill.gdxdialogs.core.listener.TextPromptListener;

/**
 * @author sven
 *
 */
public class AndroidPlattform extends BasePlattform {
	private static GDXDialogs dialogs;
	private AndroidApplication app;

	public AndroidPlattform(AndroidApplication app) {
		this.app = app;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.IPlattform#showErrorDialog(java.lang.String)
	 */
	@Override
	public void showErrorDialog(String message) {
		// TODO native dialog
		Gdx.app.error("Error", message);

		// show default?
		if (dialogs == null) {
			dialogs = GDXDialogsSystem.install();
		}

		GDXButtonDialog bDialog = dialogs.newDialog(GDXButtonDialog.class);
		// bDialog.setTitle("Buy a item");
		bDialog.setMessage(message);

		bDialog.setClickListener(new ButtonClickListener() {

			@Override
			public void click(int button) {
				// handle button click here
				// TODO
				Gdx.app.exit();
			}
		});

		bDialog.addButton("Ok");

		bDialog.build().show();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.IPlattform#inputDialog(com.badlogic.gdx.scenes.scene2d.Stage,
	 * java.lang.String, java.lang.String, java.lang.String,
	 * com.kotcrab.vis.ui.util.dialog.InputDialogListener)
	 */
	public void inputDialog(Stage stage, String title, String desc, String def, final InputDialogListener listener) {

		// show default?
		if (dialogs == null) {
			dialogs = GDXDialogsSystem.install();
		}

		GDXTextPrompt textPrompt = dialogs.newDialog(GDXTextPrompt.class);
		textPrompt.setTitle(title);
		textPrompt.setMessage(desc);
		if (def != null) {
			textPrompt.setValue(def);
			System.out.println(def);
		}

		// TODO translate it
		textPrompt.setCancelButtonLabel("Cancel");
		textPrompt.setConfirmButtonLabel("Ok");

		textPrompt.setTextPromptListener(new TextPromptListener() {

			@Override
			public void confirm(String text) {
				listener.finished(text);
			}

			@Override
			public void cancel() {
				listener.canceled();
			}
		});

		textPrompt.build().show();
	}

	@Override
	public void addInputs(final MapScreen mapScreen, InputMultiplexer multiplexer) {
		// final QMapMap map = mapScreen.getMap();

		multiplexer.addProcessor(new GestureDetector(new GestureDetector.GestureListener() {

			// protected int lastPointer;

			private int zoom;

			@Override
			public boolean zoom(float initialDistance, float distance) {

				// if (initial ==initialDistance)
				// return true;
				//
				// initial = initialDistance;
				//
				// //TODO test
				// if (distance > 400)
				// S.map().zoomIn();
				// else
				// S.map().zoomOut();
				//
				// YLog.log("zoom", initialDistance, distance);

				// System.out.println("zoom:" + initialDistance + " " + distance);
				return false;// true;
			}

			@Override
			public boolean touchDown(float x, float y, int pointer, int button) {
				return false;
			}

			@Override
			public boolean tap(float x, float y, int count, int button) {
				return false;
			}

			@Override
			public void pinchStop() {
				// need zoom?
				if (zoom == 0) {
					return;
				}

				if (zoom == 1) {
					S.map().zoomIn();
				} else {
					S.map().zoomOut();
				}

				zoom = 0;

			}

			@Override
			public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
				// can run?
				if (MapScreen.get().getInputs().hasActiveAction()) {
					return false;
				}

				// TODO debug it
				if (initialPointer1.x > pointer1.x) {
					zoom = 1;
				} else {
					zoom = -1;
				}

				return true;
			}

			@Override
			public boolean panStop(float x, float y, int pointer, int button) {
				// TODO
				/*
				 * System.out.println("panstop:" + x + " " + y + " " + pointer + " " + button);
				 * 
				 * if (lastPointer != pointer) { if (lastPointer == 1) { map.zoomIn(); } else {
				 * map.zoomOut(); } lastPointer = pointer; } // TODO Auto-generated method stub
				 */
				return false;
			}

			@Override
			public boolean pan(float x, float y, float deltaX, float deltaY) {
				// can run?
				if (MapScreen.get().getInputs().hasActiveAction()) {
					return false;
				}
				S.map().moveMapView(-deltaX * S.map().getCamera().zoom, deltaY * S.map().getCamera().zoom);
				return true;
			}

			@Override
			public boolean longPress(float x, float y) {
				return false;
			}

			@Override
			public boolean fling(float velocityX, float velocityY, int button) {
				// TODO Auto-generated method stub
				return false;
			}
		}));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.IPlattform#formatDate(long)
	 */
	@Override
	public String formatDate(long date) {
		return new SimpleDateFormat().format(new Date(date));
	}

	@Override
	public String getID() {
		return "android";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.base.IPlattform#startUpCode()
	 */
	@Override
	public void startUpCode() {
		GDXDialogsSystem.install();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.yaams.base.IPlattform#getAnimatedLabel(java.lang.String)
	 */
	@Override
	public Label getAnimatedLabel(String message) {
		// TypingLabel t = new TypingLabel(message, VisUI.getSkin());
		// t.setWrap(true);
		// return t;
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.yaams.base.IPlattform#getPlayerDefaultName()
	 */
	@Override
	public String getPlayerDefaultName() {
		return System.getProperties().getProperty("user.name", "Player");
	}

	@Override
	public String getSystemErrorInfo() {

		Object osv = "??", os = "??", java = "??";
		try {
			osv = System.getProperties().getProperty("os.version", "" + Gdx.app.getVersion());
			os = System.getProperties().getProperty("os.name", Gdx.app.getType().toString());
		} catch (Exception e1) {
			Gdx.app.error("Errorscreen", "Can not get OS", e1);
		}

		try {
			java = System.getProperties().getProperty("java.version", "?");
		} catch (Exception e1) {
			Gdx.app.error("Errorscreen", "Can not get java", e1);
		}

		// generate string
		return "OS:" + os + " " + osv + ", Java:" + java;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.yaams.base.IPlattform#showSystemSavePath(de.yaams.ui.YTable,
	 * com.badlogic.gdx.files.FileHandle)
	 */
	@Override
	public void showSystemSavePath(YTable table, FileHandle file) {
		table.addL("Absolut", file.file().getAbsolutePath());
	}

	public AndroidApplication getApp() {
		return app;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.ANDROID);
	}

	@Override
	public String getName() {
		return "Android";
	}

	@Override
	public ITextfield createTextField(YRandomField base) {
		return new ButtonTextField(base);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#loadMod(de.ninenations.mod.ModInfo)
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "deprecation" })
	public IMod loadMod(ModInfo info) {
		try {
			FileHandle jar = info.getFolder().child(info.getType() + ".jar");
			URL[] urls = { jar.file().toURL() };

			URLClassLoader child = new URLClassLoader(urls, this.getClass().getClassLoader());
			Class classToLoad = Class.forName(info.getClassName(), true, child);
			child.close();
			return (IMod) classToLoad.newInstance();
		} catch (Exception e) {
			YError.error(e, false);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#getDayOfYear()
	 */
	@Override
	public int getDayOfYear() {
		return LocalDate.now().getDayOfYear();
	}
}

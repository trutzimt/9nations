<?xml version="1.0" encoding="UTF-8"?>
<tileset name="autotiles" tilewidth="32" tileheight="32" tilecount="768" columns="32">
 <image source="autotiles.png" width="1024" height="768"/>
 <tile id="10">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="12">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="13">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="44">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="45">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="46">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="53">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="74">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="75">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="76">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="77">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="78">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="79">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="80">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="81">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="82">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="83">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="84">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="85">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="104">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="105">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="106">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="107">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="108">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="109">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="110">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="111">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="112">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="113">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="114">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="115">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="116">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="117">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="136">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="137">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="138">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="139">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="140">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="141">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="142">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="143">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="144">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="145">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="146">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="147">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="148">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="149">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="168">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="169">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="170">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="171">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="172">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="173">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="174">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="175">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="176">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="177">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="178">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="179">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="180">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="181">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="200">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="201">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="202">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="203">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="204">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="205">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="206">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="207">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="208">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="209">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="210">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="211">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="212">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="213">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="232">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="233">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="234">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="235">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="236">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="237">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="238">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="239">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="240">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="241">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="242">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="243">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="244">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="245">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="264">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="265">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="266">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="267">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="268">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="269">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="270">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="271">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="272">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="273">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="274">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="275">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="276">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="277">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="296">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="297">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="298">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="299">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="300">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="301">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="302">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="303">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="328">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="329">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="330">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="331">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="332">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="333">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="334">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="335">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="360">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="361">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="362">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="363">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="364">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="365">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="366">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="367">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="488">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="489">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="492">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="493">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="520">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="521">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="524">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="525">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="552">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="553">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="556">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="557">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="584">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="585">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="586">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="587">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="616">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="617">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="618">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="619">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="648">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="649">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="650">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="651">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="680">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="681">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="682">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="683">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="684">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="685">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="712">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="713">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="714">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="715">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="716">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="717">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="744">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="745">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="746">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="747">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="748">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="749">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
</tileset>

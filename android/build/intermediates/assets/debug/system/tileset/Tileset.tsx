<?xml version="1.0" encoding="UTF-8"?>
<tileset name="Tileset" tilewidth="32" tileheight="32" tilecount="2048" columns="32">
 <image source="tilemap.png" width="1024" height="2048"/>
 <terraintypes>
  <terrain name="Wiesenwald" tile="574"/>
  <terrain name="Gras" tile="385"/>
 </terraintypes>
 <tile id="8">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="9">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="10">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="11">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="14">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="15">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="16">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="17">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="18">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="19">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="20">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="21">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="22">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="23">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="24">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="25">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="26">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="27">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="28">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="29">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="30">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="31">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="34">
  <properties>
   <property name="build" value="rpalast3"/>
  </properties>
 </tile>
 <tile id="40">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="41">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="42">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="43">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="46">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="47">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="48">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="49">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="50">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="51">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="52">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="53">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="54">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="55">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="56">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="57">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="58">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="59">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="60">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="61">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="62">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="63">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="66">
  <properties>
   <property name="build" value="rpalast5"/>
  </properties>
 </tile>
 <tile id="72">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="73">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="74">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="75">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="78">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="79">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="80">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="81">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="82">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="83">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="84">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="85">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="86">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="87">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="88">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="89">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="90">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="91">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="92">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="93">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="94">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="95">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="101">
  <properties>
   <property name="build" value="ntownhall2"/>
  </properties>
 </tile>
 <tile id="102">
  <properties>
   <property name="build" value="ntownhall3"/>
  </properties>
 </tile>
 <tile id="112">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="113">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="116">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="117">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="118">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="119">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="120">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="121">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="122">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="123">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="124">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="125">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="126">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="127">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="132">
  <properties>
   <property name="build" value="rberry"/>
  </properties>
 </tile>
 <tile id="144">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="145">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="148">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="149">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="150">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="151">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="152">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="153">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="154">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="155">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="156">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="157">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="158">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="159">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="162">
  <properties>
   <property name="build" value="nquarry2"/>
  </properties>
 </tile>
 <tile id="176">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="177">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="180">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="181">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
 <tile id="182">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="183">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="184">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="185">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="186">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="187">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="188">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="189">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="190">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="191">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="194">
  <properties>
   <property name="build" value="rleaf"/>
  </properties>
 </tile>
 <tile id="199">
  <properties>
   <property name="build" value="nhouse"/>
  </properties>
 </tile>
 <tile id="208">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="209">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="212">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="213">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="214">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="215">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="240">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="241">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="244">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="245">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="246">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="247">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="272">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="273">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="276">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="277">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="278">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="279">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="288">
  <properties>
   <property name="build" value="ntownhall"/>
  </properties>
 </tile>
 <tile id="289">
  <properties>
   <property name="build" value="nhouse2"/>
  </properties>
 </tile>
 <tile id="294">
  <properties>
   <property name="build" value="ntownhall4"/>
  </properties>
 </tile>
 <tile id="308">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="309">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="340">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="341">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="352">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="353">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="354">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="372">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="373">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="384">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="385" terrain="1,1,1,1"/>
 <tile id="386">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="387">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="388">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="393">
  <properties>
   <property name="build" value="nwall"/>
  </properties>
 </tile>
 <tile id="395">
  <properties>
   <property name="build" value="ntemple"/>
  </properties>
 </tile>
 <tile id="400">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="401">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="402">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="403">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="404">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="405">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="406">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="407">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="408" terrain="0,0,0,0">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="409" terrain="1,0,0,0">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="410" terrain="0,1,0,0">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="411">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="412" terrain="0,0,0,1">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="413">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="414">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="415">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="416">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="417">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="418">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="419">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="420">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="432">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="433">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="434">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="435">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="436">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="437">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="438">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="439">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="440" terrain="0,0,1,0">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="441">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="442">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="443">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="444">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="445">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="446">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="447">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="464">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="465">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="466">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="467">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="468">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="469">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="470">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="471">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="472" terrain="1,0,1,0">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="473">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="474">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="475">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="476" terrain="1,1,0,0">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="477">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="478">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="479">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="492">
  <properties>
   <property name="build" value="nstreet"/>
  </properties>
 </tile>
 <tile id="496">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="497">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="498">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="499">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="500">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="501">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="502">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="503">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="504" terrain="0,1,0,1">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="505">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="506">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="507">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="508" terrain="0,0,1,1">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="509">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="510">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="511">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="528">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="529">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="530">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="531">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="532">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="533">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="534">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="535">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="536">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="537">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="538" terrain="1,1,1,0">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="539">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="540" terrain="1,1,0,1">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="541">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="542" terrain="0,1,1,1">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="543">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="545">
  <properties>
   <property name="build" value="ntemple2"/>
  </properties>
 </tile>
 <tile id="547">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="548">
  <properties>
   <property name="build" value="ncircus"/>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="549">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="560">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="561">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="562">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="563">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="564">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="565">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="566">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="567">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="568" terrain="1,0,1,1">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="569">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="570">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="571">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="572">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="573">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="574">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="575">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="610">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="611">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="612">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="613">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="640">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="641">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="642">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="643">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="644">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="645">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="672">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="673">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="674">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="675">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="676">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="677">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="704">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="705">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="706">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="707">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="708">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="709">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="743">
  <properties>
   <property name="build" value="rplank"/>
  </properties>
 </tile>
 <tile id="775">
  <properties>
   <property name="build" value="nsawmill"/>
  </properties>
 </tile>
 <tile id="788">
  <properties>
   <property name="build" value="nbrickfactory"/>
  </properties>
 </tile>
 <tile id="789">
  <properties>
   <property name="build" value="nbarracks"/>
  </properties>
 </tile>
 <tile id="819">
  <properties>
   <property name="build" value="rrope"/>
  </properties>
 </tile>
 <tile id="851">
  <properties>
   <property name="build" value="nbridge"/>
  </properties>
 </tile>
 <tile id="862">
  <properties>
   <property name="build" value="ncastle"/>
  </properties>
 </tile>
 <tile id="889">
  <properties>
   <property name="build" value="nmine"/>
  </properties>
 </tile>
 <tile id="911">
  <properties>
   <property name="build" value="nwoodtower"/>
  </properties>
 </tile>
 <tile id="912">
  <properties>
   <property name="build" value="ngoldpalace"/>
  </properties>
 </tile>
 <tile id="932">
  <properties>
   <property name="build" value="ndockyard"/>
  </properties>
 </tile>
 <tile id="958">
  <properties>
   <property name="build" value="rwapple"/>
  </properties>
 </tile>
 <tile id="959">
  <properties>
   <property name="build" value="rpalast"/>
  </properties>
 </tile>
 <tile id="964">
  <properties>
   <property name="build" value="nmarket"/>
  </properties>
 </tile>
 <tile id="999">
  <properties>
   <property name="build" value="nlibrary"/>
  </properties>
 </tile>
 <tile id="1114">
  <properties>
   <property name="build" value="rsling"/>
  </properties>
 </tile>
 <tile id="1147">
  <properties>
   <property name="build" value="nfisher"/>
  </properties>
 </tile>
 <tile id="1176">
  <properties>
   <property name="build" value="nworkshop"/>
  </properties>
 </tile>
 <tile id="1180">
  <properties>
   <property name="build" value="nresearch"/>
  </properties>
 </tile>
 <tile id="1210">
  <properties>
   <property name="build" value="narmoury"/>
  </properties>
 </tile>
 <tile id="1316">
  <properties>
   <property name="build" value="rhouse"/>
  </properties>
 </tile>
 <tile id="1462">
  <properties>
   <property name="build" value="ralchemy"/>
  </properties>
 </tile>
 <tile id="1494">
  <properties>
   <property name="build" value="nlogger2"/>
  </properties>
 </tile>
 <tile id="1495">
  <properties>
   <property name="build" value="nquarry"/>
  </properties>
 </tile>
 <tile id="1526">
  <properties>
   <property name="build" value="nhunter"/>
  </properties>
 </tile>
 <tile id="1527">
  <properties>
   <property name="build" value="nlogger"/>
  </properties>
 </tile>
 <tile id="1654">
  <properties>
   <property name="build" value="nstreet,rstreet"/>
  </properties>
 </tile>
 <tile id="1664">
  <properties>
   <property name="build" value="nfield"/>
  </properties>
 </tile>
 <tile id="1697">
  <properties>
   <property name="build" value="nfarm"/>
  </properties>
 </tile>
 <tile id="1761">
  <properties>
   <property name="build" value="nfarm2"/>
  </properties>
 </tile>
 <tile id="1762">
  <properties>
   <property name="build" value="nstorage"/>
  </properties>
 </tile>
 <tile id="1838">
  <properties>
   <property name="build" value="rwood"/>
  </properties>
 </tile>
 <tile id="1871">
  <properties>
   <property name="build" value="rtrain"/>
  </properties>
 </tile>
 <tile id="1900">
  <properties>
   <property name="rresearch" value=""/>
  </properties>
 </tile>
 <tile id="1906">
  <properties>
   <property name="build" value="nwall"/>
  </properties>
 </tile>
 <tile id="1910">
  <properties>
   <property name="build" value="nstreet2"/>
  </properties>
 </tile>
</tileset>

Created by Sven. The source code is licensed under the Apache 2 License.
bitbucket.org/trutzimt/ninenations/overview

Libraries
=========
libgdx (1.9.8)
gdx-dialogs-core (1.3.0)
libgdx-utils-box2d (0.13.4)
vis-ui (1.4.1)
gdxgamesvcs (0.2.3)

Software
========
Eclipse
Installer install4j by ej-technologies: ej-technologies.com/products/install4j

Scripts
=======
http://www.dungeoneering.net/d100-list-fantasy-town-names/
http://listofrandomnames.com/index.cfm?textarea

Graphic & Sounds
================
by den_yes from https://opengameart.org/content/explosion-animation-1
system/animations/explosion.png

by snowdrop from WTactics.org / opengameart.org/users/snowdrop
system/face/*

by Avery from forums.rpgmakerweb.com/index.php?threads/avys-icon-workshop-new-stuff-atm-no-more-requests-please.21623/
Gamejolt icon from gamejolt.com/about
System icons by bogo-d from www.deviantart.com/bogo-d/art/Project-Icons-v-2-1-8-155863784
Crown by DJ Viandox from fixicon.com
by The Infamous Bon Bon from forums.rpgmakerweb.com/index.php?threads/bon-bons-mv-resources-new-1-19-16-two-new-titles-for-school-based-games.49721/
system/icons/iconsheet.png (edited)

by DJ Viandox from fixicon.com
system/logo/*

by yd from opengameart.org/content/aleonas-tales-some-ui
system/menu/menuBackground.jpg
system/menu/menuLoading.jpg (edited)

by artisticdude from opengameart.org/content/rpg-sound-pack
system/sound/interface2.mp3
system/sound/interface5.mp3
system/sound/interface6.mp3
system/sound/random3.mp3
system/sound/sword-unsheathe2.mp3
system/sound/swing3.mp3
system/sound/coin2.mp3
system/sound/bite-small3.mp3

by Kenney from kenney.nl/assets/ui-audio
system/sound/click3.mp3
system/sound/metalPot3.mp3

by Kenney from kenney.nl/assets/rpg-audio
system/sound/bookFlip1.mp3
system/sound/bookClose.mp3

by spuispuin from opengameart.org/content/won-orchestral-winning-jingle
system/sound/Won_.mp3

by qubodup from opengameart.org/content/3-background-crash-explosion-bang-sounds
system/sound/qubodup-BangMid.mp3

by Wyrmheart from opengameart.org/content/rpg-game-ui
system/skin/*

by Sir Twist from opengameart.org/content/window-frame-1
by Lamoot from opengameart.org/content/rpg-gui-construction-kit-v10
system/skin/simple.png

by The Inquisitor from downloads.rpg-palace.com/details.php?id=103
by Candacis from forums.rpgmakerweb.com/index.php?threads/candacis-resources.19694/
by Avery from forums.rpgmakerweb.com/index.php?threads/averys-ace-stuff.26173/
by Nekura from forums.rpgmakerweb.com/index.php?threads/nekura.38957/
by Champion Of Darkness from forums.rpgmakerweb.com/index.php?threads/champion-of-darkness-world-map-tiles-v-1-7.44668/
by Lunarea from forums.rpgmakerweb.com/index.php?threads/lunas-tiles.84/
by Paladin Ramos from forums.rpgmakerweb.com/index.php?threads/overworld-farm-tile-v0-2-graveyard-v0-1-aquaduct-v0-1.25878/
by ?? from www25.atwiki.jp/mack_material/pages/13.html
by Eos from eosbox.hiroimon.com/loft_mate.html
by Pixel Art World from yms.main.jp/page-msets/worldmap1.html
system/tileset/tilemap.png (edited)

by Pixel Art World from yms.main.jp/page-msets/worldmap1.html
system/tileset/autotiles.png
system/tileset/autotilesMaker.png (edited)

by mintk from mintk.blog79.fc2.com/blog-entry-260.html
by candacis from forums.rpgmakerweb.com/index.php?threads/candacis-resources.19694/
by kazzadors from forums.rpgmakerweb.com/index.php?threads/kazzadors-fantasy-sprites.683/
by momope8 from momope8.blog67.fc2.com/category16-1.html
system/unit/units1.png

by aweryn from vxresource.wordpress.com/2010/02/27/new-characters-faces/
system/unit/newchar05-5.png 

by Dust Collector from www.gdunlimited.net/forums/user/501-dust-collector/?tab=gallery
system/unit/gallery_501_57_6911.png

by saada from www.gdunlimited.net/forums/user/25428-saada/?tab=gallery
system/unit/gallery_25428_10_0.png
system/unit/gallery_25428_10_1188.png

by momope8 from momope8.blog67.fc2.com/category16-1.html
system/unit/vx_chara04_a_2a5.png
system/unit/vx_chara04_a_2a6.png
system/unit/vx_chara04_a_2a7.png
system/unit/vxchara08_ab2.png
system/unit/vxchara08_ab.png
system/unit/vxchara08_a.png
system/unit/vx_chara06_a.png
system/unit/vx_chara06_b.png

by Kotcrab from github.com/kotcrab/vis-ui-contrib/tree/master/skins
system/x1/*
system/x2/*

by tob from RTP-Tunes
music/*

Content from 64 folder the same credits, but rescaled.
<?xml version="1.0" encoding="UTF-8"?>
<tileset name="autotilesMaker" tilewidth="32" tileheight="32" tilecount="7" columns="7">
 <image source="autotilesMaker.png" width="224" height="32"/>
 <tile id="0">
  <properties>
   <property name="region" value="default"/>
  </properties>
 </tile>
 <tile id="1">
  <properties>
   <property name="region" value="dark"/>
  </properties>
 </tile>
 <tile id="2">
  <properties>
   <property name="region" value="forest"/>
  </properties>
 </tile>
 <tile id="3">
  <properties>
   <property name="region" value="grass"/>
  </properties>
 </tile>
 <tile id="4">
  <properties>
   <property name="region" value="water"/>
  </properties>
 </tile>
 <tile id="5">
  <properties>
   <property name="region" value="mountain"/>
  </properties>
 </tile>
 <tile id="6">
  <properties>
   <property name="region" value="hill"/>
  </properties>
 </tile>
</tileset>

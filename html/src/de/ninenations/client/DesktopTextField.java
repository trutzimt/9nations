/**
 * 
 */
package de.ninenations.client;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kotcrab.vis.ui.widget.VisTextField;

import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.ITextfield;
import de.ninenations.ui.elements.YRandomField;

/**
 * @author sven
 *
 */
public class DesktopTextField extends VisTextField implements ITextfield {

	/**
	 * 
	 */
	public DesktopTextField(final YRandomField base) {
		addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				base.saveText(getText());

			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.elements.ITextfield#getActor()
	 */
	@Override
	public Actor getActor() {
		return this;
	}

}

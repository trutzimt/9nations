/**
 * 
 */
package de.ninenations.client;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.ninenations.core.BasePlattform;
import de.ninenations.core.NN;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.menu.MainMenuScreen;
import de.ninenations.menu.MenuWindow.MWI;
import de.ninenations.mod.IMod;
import de.ninenations.mod.ModInfo;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.ITextfield;
import de.ninenations.ui.elements.YRandomField;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class HtmlPlattform extends BasePlattform {

	private final String last;

	/**
	 * 
	 */
	public HtmlPlattform() {
		last = "\n If you like 9Nations, download the complete version from 9nations.de/download.";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#showErrorDialog(java.lang.String)
	 */
	@Override
	public void showErrorDialog(String message) {
		// TODO
		System.out.println(message);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.IPlattform#formatDate(long)
	 */
	@Override
	public String formatDate(long date) {
		return new Date(date).toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#startUpCode()
	 */
	@Override
	public void startUpCode() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#getAnimatedLabel(java.lang.String)
	 */
	@Override
	public Label getAnimatedLabel(String message) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#getPlayerDefaultName()
	 */
	@Override
	public String getPlayerDefaultName() {
		return "Online player";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#getSystemErrorInfo()
	 */
	@Override
	public String getSystemErrorInfo() {
		return "browser";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#showSystemSavePath(de.ninenations.ui.
	 * elements.YTable, com.badlogic.gdx.files.FileHandle)
	 */
	@Override
	public void showSystemSavePath(YTable table, FileHandle file) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.yaams.base.IPlattform#checkMenu(java.util.List)
	 */
	@Override
	public void checkMenu(List<MWI> items) {
		// remove items
		items.removeAll(Arrays.asList(MWI.LOAD, MWI.SAVE, MWI.GAME_QUIT, MWI.PROGRAM_QUIT));

		if (NN.screen() instanceof MapScreen) {
			items.add(MWI.MAINMENU);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.yaams.base.IPlattform#startupCheck()
	 */
	@Override
	public void startupCheck(MainMenuScreen screen) {
		Dialogs.showOKDialog(screen.getStage(), "Limited functions", "This is the online version of " + NSettings.NAME
				+ ". For technical reasons, not all functions, \nsuch as loading and saving are not supported. Big maps can be very slow." + last);

	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.HTML);
	}

	@Override
	public String getName() {
		return "Browser";
	}

	@Override
	public String getID() {
		return "html";
	}

	@Override
	public ITextfield createTextField(YRandomField base) {
		return new DesktopTextField(base);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#loadMod(de.ninenations.mod.ModInfo)
	 */
	@Override
	public IMod loadMod(ModInfo info) {
		Dialogs.showOKDialog(NN.screen().getStage(), "Limited functions",
				"Mod support is limited in this online version, so mod " + info.getName() + " can not be loaded." + last);
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#getDayOfYear()
	 */
	@Override
	public int getDayOfYear() {
		// TODO can calculate?
		return 0;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#getFolderContent(java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public FileHandle[] getFolderContent(String folder, String extension) {
		// TODO use exentsion

		// files
		String[] names = Gdx.files.internal(folder + "/list.9nhtml").readString().split(",");
		FileHandle[] f = new FileHandle[names.length];

		// add it
		for (int i = 0; i < names.length; i++) {
			f[i] = Gdx.files.internal(folder + "/" + names[i]);
		}
		return f;
	}

}

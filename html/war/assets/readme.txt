Hello! 
Nice to meet you, i'm Sven and develop 9 Nations. It is a tuned-based strategy game. You have to explore the area und develop your town.

This version of the game is in alpha state. This means that you can encounter bugs, crashes, incomplete and unpolished features, etc.
It also means the game only contains one nation and a small part of the planned features. Also except balancing issuses.

9 Nations is open source, that mean you can help, support, join or develop it also. There is something for everyone: Graphics, Programming, Game Design, Balancing, Community Management and much more. 

If you have found any mistakes, bugs oder you have questions, just click the feedback button. I'm happy to hear from you. To understand 9 Nations, select Scenario and than the tutorial.

What's new?
* New Window decoration on some windows + save
* Destroy animation
* Street upgrade for North
* New Fight calculation (with damage)
* Clearer interface
* New Feedback option

* test 64er

Bug fixes
* More stable mod selection
* GUI scaled better on small screens
* overscrolling on map disabled
* Random event system calculation fix

You can find the old changelogs on the homepage.

Thanks for your interest :)
Greetings Sven
/**
 *
 */
package de.ninenations.menu;

import java.util.ArrayList;
import java.util.Arrays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.ninenations.core.NN;
import de.ninenations.data.LexikonWindow;
import de.ninenations.endlessgame.EndlessGameWindow;
import de.ninenations.feedback.FeedbackWindow;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.saveload.SaveLoadManagement;
import de.ninenations.saveload.SaveLoadWindow;
import de.ninenations.scenarios.CampaignMgmt;
import de.ninenations.scenarios.LevelWindow;
import de.ninenations.screen.IScreen;
import de.ninenations.stats.StatsWindow;
import de.ninenations.ui.NDialog;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.NTextButton;
import de.ninenations.ui.newWindow.NMessageDialog;
import de.ninenations.ui.newWindow.NWindow;
import de.ninenations.util.NSettings;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class MenuWindow extends NWindow {

	public enum MWI {
		SCENARIO, ENDLESS, LOAD, OPTIONS, STATISTIC, ABOUT, FEEDBACK, PROGRAM_QUIT, SAVE, DEBUG, GAME_QUIT, MAINMENU, LEXICON
	}

	public static final String ID = "main";

	/**
	 * 
	 */
	public MenuWindow(IScreen screen) {
		super(screen instanceof MainMenuScreen ? "mainmenu" : ID, "9 Nations", YIcons.LOGO, screen instanceof MapScreen);

		final Stage stage = screen.getStage();

		ArrayList<MWI> items = createMenu(screen);

		// build the menu
		for (MWI m : items) {
			switch (m) {
			case SCENARIO:
				a(new NTextButton("Scenarios") {

					@Override
					public void perform() {
						YSounds.pClick();
						stage.addActor(new LevelWindow());

					}
				});
				break;
			case ENDLESS:
				a(new NTextButton("Endless Game") {

					@Override
					public void perform() {
						YSounds.pClick();
						stage.addActor(new EndlessGameWindow());

					}
				});
				break;
			case DEBUG:
				a(new NTextButton("Debug Window") {

					@Override
					public void perform() {
						YSounds.pClick();
						stage.addActor(new DebugWindow());
						close();
					}
				});
				break;
			case STATISTIC:
				a(new NTextButton("Statistic") {

					@Override
					public void perform() {
						YSounds.pClick();
						stage.addActor(new StatsWindow());
					}
				});
				break;
			case LOAD:
				a(new NTextButton("Load") {

					@Override
					public void perform() {
						YSounds.pClick();
						stage.addActor(new SaveLoadWindow(true, false));
					}
				});
				break;
			case SAVE:
				a(new NTextButton("Save") {

					@Override
					public void perform() {
						YSounds.pClick();
						stage.addActor(new SaveLoadWindow(false, false));
					}
				});
				break;
			case OPTIONS:
				a(new NTextButton("Options") {

					@Override
					public void perform() {
						YSounds.pClick();
						NN.windows().open(OptionsWindow.ID);
					}
				});
				break;
			case ABOUT:
				a(new NTextButton("about 9 Nations") {

					@Override
					public void perform() {
						YSounds.pClick();
						stage.addActor(new CreditsWindow());

					}
				});
				break;
			case FEEDBACK:
				a(new NTextButton("Send Feedback") {

					@Override
					public void perform() {
						YSounds.pClick();
						NN.windows().open(FeedbackWindow.ID);

					}
				});
				break;
			case LEXICON:
				a(new NTextButton("Lexicon", true) {

					@Override
					public void perform() {
						stage.addActor(new LexikonWindow());
						close();

					}
				});
				break;
			case PROGRAM_QUIT:
				a(new NTextButton("Quit", true) {

					@Override
					public void perform() {
						Gdx.app.exit();

					}
				});
				break;
			case GAME_QUIT:
				a(new NTextButton("Quit", true) {

					@Override
					public void perform() {
						showExitMenu(stage);
					}
				});
				break;
			case MAINMENU:
				a(new NTextButton("To Main Menu", true) {

					@Override
					public void perform() {

						NDialog d = new NDialog("You really want to quit?", "");
						d.addCancel();
						d.add("Main Menu", new YChangeListener() {

							@Override
							public void changedY(Actor actor) {
								NN.get().switchScreen(new MainMenuScreen(false));
							}
						});
						d.show(stage);

					}
				});
				break;
			}
		}

		pack();
		// setSize(getWidth()+40, getHeight()+);

		setCenterY();
		if (screen instanceof MainMenuScreen) {
			setRight(3);
		} else {
			setCenterX();
		}

		loadPos();

		fadeIn();

	}

	/**
	 * @param screen
	 * @return
	 */
	private ArrayList<MWI> createMenu(IScreen screen) {
		ArrayList<MWI> items = new ArrayList<>();
		if (screen instanceof MainMenuScreen) {
			if (NN.nData().getUnits().size > 1) {
				if (CampaignMgmt.getCampaignIds().toArray().size >= 1) {
					items.add(MWI.SCENARIO);
				}
				items.add(MWI.ENDLESS);
			}

			if (!SaveLoadManagement.getSaves().isEmpty()) {
				items.add(MWI.LOAD);
			}

			items.addAll(Arrays.asList(MWI.OPTIONS, MWI.STATISTIC, MWI.ABOUT, MWI.FEEDBACK, MWI.PROGRAM_QUIT));
		} else if (screen instanceof MapScreen) {
			// load it?
			if (S.isActive(ScenConf.LOADSAVE)) {
				if (!SaveLoadManagement.getSaves().isEmpty()) {
					items.add(MWI.LOAD);
				}

				items.add(MWI.SAVE);
			}

			items.addAll(Arrays.asList(MWI.LEXICON, MWI.STATISTIC, MWI.ABOUT, MWI.FEEDBACK, MWI.OPTIONS));

			if (NSettings.isDebug()) {
				items.add(MWI.DEBUG);
			}

			items.add(MWI.GAME_QUIT);
		} else {
			items.addAll(Arrays.asList(MWI.ABOUT, MWI.FEEDBACK, MWI.MAINMENU));
		}

		NN.plattform().checkMenu(items);
		return items;
	}

	/**
	 * Button add helper
	 * 
	 * @param t
	 */
	private void a(final NTextButton t) {
		add(t).growX().row();
	}

	/**
	 * @param stage
	 */
	private void showExitMenu(final Stage stage) {

		final NMessageDialog d = new NMessageDialog("exitgamefullmenu", "You really want to quit?", YIcons.LOGO, "", null);

		d.addButton(new NTextButton("Main Menu", true) {

			@Override
			public void perform() {
				NN.get().switchScreen(new MainMenuScreen(false));
			}
		});

		d.addButton(new NTextButton("Exit Game", true) {

			@Override
			public void perform() {
				Gdx.app.exit();
			}
		});

		if (S.isActive(ScenConf.LOADSAVE)) {
			d.addButton(new NTextButton("Save & Exit", true) {

				@Override
				public void perform() {
					stage.addActor(new SaveLoadWindow(false, true));
					d.close();
				}
			});
		}
		d.addClose();
		d.setModal(true);
		d.build(stage);
	}
}

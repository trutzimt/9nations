/**
 * 
 */
package de.ninenations.menu;

import de.ninenations.core.NN;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.data.ress.MarketWindow;
import de.ninenations.game.S;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player.RessType;
import de.ninenations.screen.LoaderScreen;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.NTextButton;
import de.ninenations.ui.newWindow.NWindow;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class DebugWindow extends NWindow {

	public final static String ID = "debug";

	/**
	 * @param title
	 */
	public DebugWindow() {
		super(ID, "Debug general", YIcons.DEBUG);

		add(new NTextButton("Reread nData", true) {

			@Override
			public void perform() {
				S.nData().generate();

			}
		});

		add(new NTextButton("Will lose", true) {

			@Override
			public void perform() {
				S.actPlayer().setWillLose(true);

			}
		}).row();

		add(new NTextButton("Will win", true) {

			@Override
			public void perform() {
				S.actPlayer().setWillWin(true);

			}
		});

		add(new NTextButton("Market Window", true) {

			@Override
			public void perform() {
				MapScreen.get().getStage().addActor(new MarketWindow(S.town().getTownsByPlayer(S.actPlayer()).first()));

			}
		}).row();

		add(new NTextButton("+500 Gold", true) {

			@Override
			public void perform() {
				S.town().getTownsByPlayer(S.actPlayer()).first().addRess(BaseRess.GOLD, 500, RessType.GIFT);

			}
		});

		add(new NTextButton("+100 Wealth", true) {

			@Override
			public void perform() {
				S.town().getTownsByPlayer(S.actPlayer()).first().addRess(BaseRess.WEALTH, 100, RessType.GIFT);

			}
		}).row();

		add(new NTextButton("research all", true) {

			@Override
			public void perform() {
				for (String r : S.nData().getResearch()) {
					S.actPlayer().getResearch().setResearch(r, true);
				}

			}
		});

		add(new NTextButton("Reset all", true) {

			@Override
			public void perform() {
				NSettings.getPref().clear();
				NSettings.getPref().flush();
				NN.get().switchScreen(new LoaderScreen("Reset"));

			}
		}).row();

		pack();
		loadPos();
	}

}

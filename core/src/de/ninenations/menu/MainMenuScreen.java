/**
 *
 */
package de.ninenations.menu;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.core.NN;
import de.ninenations.feedback.FeedbackWindow;
import de.ninenations.game.GameStartScreen;
import de.ninenations.saveload.SaveLoadManagement;
import de.ninenations.saveload.YSaveInfo;
import de.ninenations.scenarios.CampaignMgmt;
import de.ninenations.screen.BaseMenuScreen;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.NTextButton;
import de.ninenations.ui.newWindow.NMessageDialog;
import de.ninenations.util.NSettings;
import de.ninenations.util.NSettingsHelper;
import de.ninenations.util.YMusic;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class MainMenuScreen extends BaseMenuScreen {

	/**
	 * Create it
	 *
	 * @param qossire
	 */
	public MainMenuScreen(boolean firstStart) {
		super("menuBackground");
		YMusic.mainmenu();

		stage.addActor(new MenuWindow(this));
		stage.addActor(new VisLabel(NN.plattform().getName() + " V" + NSettings.VERSION + (NSettings.isDebug() ? "-DEBUG" : "")));
		// stage.getRoot().getColor().a = 0;
		// stage.getRoot().addAction(Actions.fadeIn(1));

		if (firstStart) {
			startupCheck();
		}
	}

	/**
	 * First run
	 */
	protected void startupCheck() {
		NN.plattform().startupCheck(this);

		// has units?
		if (NN.nData().getUnits().size == 1) {
			Dialogs.showOKDialog(stage, "Invalid installation", "Your installation looks invalid. " + NSettings.NAME + " could not load any units. \n"
					+ "Please check the content of the folder system/config or reinstall the game.");
		}

		// first run?
		if (NSettings.getPref().getFloat("firstRun", 0f) != (float) NSettings.VERSION) {
			NMessageDialog q = new NMessageDialog("welcome", "Welcome to " + NSettings.NAME + " V" + NSettings.VERSION, YIcons.LOGO,
					CreditsWindow.getWelcomeMsg(), new YChangeListener() {

						@Override
						public void changedY(Actor actor) {
							NSettings.getPref().putFloat("firstRun", (float) NSettings.VERSION);
							NSettings.save();

							NSettingsHelper.checkforUpdates(false, stage);
						}
					});
			q.addButton(new NTextButton("Send Feedback", true) {

				@Override
				public void perform() {
					NN.windows().open(FeedbackWindow.ID);

				}
			});
			q.addButton(new NTextButton("Start Tutorial") {

				@Override
				public void perform() {
					YSounds.play(YSounds.STARTGAME);
					NN.get().switchScreen(new GameStartScreen(CampaignMgmt.getCampaign("tut").getScenario("1")));
				}
			});
			q.addClose();
			q.build(stage);

			return;
		} else {
			NSettingsHelper.checkforUpdates(false, stage);
		}

		// has a temp file?
		if (SaveLoadManagement.exist("temp")) {
			YSaveInfo q = SaveLoadManagement.loadConfig("temp");

			final NMessageDialog d = new NMessageDialog("loadtemp", "Load last game?", YIcons.FILE, q.getDesc(), null);
			d.addButton(new NTextButton("Delete and Close", true) {

				@Override
				public void perform() {
					SaveLoadManagement.delete("temp");
					d.close();

				}
			});

			d.addButton(new NTextButton("Load save game") {

				@Override
				public void perform() {
					YSounds.play(YSounds.STARTGAME);
					SaveLoadManagement.load("temp");

				}
			});

			if (NSettings.isDebug()) {
				d.addClose();
			}
			d.setModal(true);
			d.build(stage);

			return;
		}

		// has a crash file?
		if (SaveLoadManagement.exist("crash")) {
			YSaveInfo q = SaveLoadManagement.loadConfig("crash");

			final NMessageDialog d = new NMessageDialog("loadcrash", "Load save game, before it crashed?", YIcons.FILE, q.getDesc(), null);
			d.addButton(new NTextButton("Delete and Close", true) {

				@Override
				public void perform() {
					SaveLoadManagement.delete("temp");
					d.close();

				}
			});

			d.addButton(new NTextButton("Load save game") {

				@Override
				public void perform() {
					YSounds.play(YSounds.STARTGAME);
					SaveLoadManagement.load("temp");

				}
			});

			if (NSettings.isDebug()) {
				d.addClose();
			}
			d.setModal(true);
			d.build(stage);

		}
	}

}

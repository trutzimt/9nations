/**
 * 
 */
package de.ninenations.menu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisSlider;

import de.ninenations.core.NN;
import de.ninenations.mod.ModTab;
import de.ninenations.screen.LoaderScreen;
import de.ninenations.stats.gameservice.GameServiceTab;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.NTextButton;
import de.ninenations.ui.elements.YRandomField;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.newWindow.NTab;
import de.ninenations.ui.newWindow.NTabWindow;
import de.ninenations.util.NGenerator;
import de.ninenations.util.NSettings;
import de.ninenations.util.NSettingsHelper;
import de.ninenations.util.YMusic;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class OptionsWindow extends NTabWindow {

	public static final String ID = "opts";

	/**
	 * @param name
	 */
	public OptionsWindow() {
		super(ID, "Options", YIcons.MAINMENU);

		addTab(new GameTab());
		addTab(new GraphicTab());
		addTab(new AudioTab());
		// add it?
		if (NN.get().getAchievements().getGameService().size > 0) {
			addTab(new GameServiceTab());
		}
		addTab(new ModTab());
		addTab(new UpdateTab());

		buildIt();
	}

	class GameTab extends NTab {

		public GameTab() {
			super("game", NSettings.NAME, YIcons.LOGO);
		}

		@Override
		public Table getContentTable() {
			YTable t = new YTable();

			final YRandomField name = new YRandomField(NSettings.getUserName()) {

				@Override
				public void saveText(String text) {
					NSettings.getPref().putString("playername", text);
				}

				@Override
				protected String getRndText() {
					return NGenerator.getName(NN.random().nextBoolean());
				}
			};
			t.addL("Player Name", name);

			final VisSlider not = new VisSlider(1, 100, 2, false);
			not.setValue(NSettings.getNoticationDuration());
			not.addListener(new YChangeListener(false) {

				@Override
				public void changedY(Actor actor) {
					NSettings.getPref().putInteger(NSettings.NOTICATIONDURATION, (int) not.getValue());
					YSounds.pClick();
				}
			});
			t.addL("Notication duration", not);

			// add debug
			if (NSettings.isDebug()) {
				VisCheckBox box = new VisCheckBox("Debug Mode", NSettings.isDebug());
				box.addCaptureListener(new ChangeListener() {

					@Override
					public void changed(ChangeEvent event, Actor actor) {
						YSounds.pClick();
						NSettings.getPref().putBoolean("dev", ((VisCheckBox) actor).isChecked());

					}
				});
				t.addL("Debug", box);
			}

			t.addL(null, NSettingsHelper.generateOptionsBox("Show disabled game options", NSettings.SHOWDISABLED, NSettings.isShowDisabled(), null));

			NN.plattform().addOptionsGame(t);

			return t;
		}

	}

	class GraphicTab extends NTab {

		public GraphicTab() {
			super("graphic", "Graphic", 245);
		}

		@Override
		public Table getContentTable() {
			YTable t = new YTable();

			// add gui scale
			if (NN.get().getScreen() instanceof MainMenuScreen) {
				final boolean bS = Gdx.files.internal("64").exists();
				t.addL(null, NSettingsHelper.generateOptionsBox("Bigger Gui" + (bS ? "" : " light") + "\n (The game will be restarted, after change)",
						"biggerGui", NSettings.isBiggerGui(), new YChangeListener() {

							@Override
							public void changedY(Actor actor) {
								NSettings.getPref().putInteger("guiScale", bS && ((VisCheckBox) actor).isChecked() ? 64 : 32);
								NSettings.save();
								NN.get().switchScreen(new LoaderScreen("Loading 9 Nations: "));

							}
						}));
			} else {
				t.addL(null, "Some settings only avaible in the main menu");
			}

			t.addL(null, NSettingsHelper.generateOptionsBox("Cleaner GUI Interface", NSettings.GUIDESIGN, NSettings.isCleanerGuiDesign(), null));

			NN.plattform().addOptionsGraphic(t);

			return t;
		}

	}

	class AudioTab extends NTab {

		public AudioTab() {
			super("audio", "Audio", 199);
		}

		@Override
		public Table getContentTable() {
			YTable t = new YTable();

			final VisSlider sound = new VisSlider(0, 1, 0.1f, false);
			sound.setValue(NSettings.getSoundVolume());
			sound.addListener(new YChangeListener(false) {

				@Override
				public void changedY(Actor actor) {
					if (NSettings.getSoundVolume() != sound.getValue()) {
						NSettings.getPref().putFloat("sound", sound.getValue());
						YSounds.pClick();
					}
				}
			});
			t.addL("Sound", sound);

			// add music?
			if (NN.music() != null) {
				final VisSlider music = new VisSlider(0, 1, 0.1f, false);
				music.setValue(NSettings.getMusicVolume());
				music.addListener(new YChangeListener(false) {

					@Override
					public void changedY(Actor actor) {
						if (NSettings.getMusicVolume() != music.getValue()) {
							NSettings.getPref().putFloat("music", music.getValue());
							YMusic.updateVol();
							YMusic.clickForOptions();
						}
					}
				});
				t.addL("Music", music);
				if (NSettings.isDebug()) {
					t.addL("Playing", NN.music().getActSong());
				}
			}

			NN.plattform().addOptionsAudio(t);

			return t;
		}

	}

	class UpdateTab extends NTab {

		public UpdateTab() {
			super("network", "Network", 409);
		}

		@Override
		public Table getContentTable() {
			YTable t = new YTable();

			t.addL("Your Version", NSettings.NAME + " V" + NSettings.VERSION);

			t.addL(null, NSettingsHelper.generateOptionsBox("Enable Update check", NSettings.OENABLEUPDATECHECK, NSettings.isEnableUpdateCheck(), null));
			t.addL(null, new NTextButton("Manual update check", true) {

				@Override
				public void perform() {
					NSettingsHelper.checkforUpdates(true, NN.screen().getStage());

				}
			});

			NN.plattform().addOptionsUpdate(t);

			return t;
		}

	}

}

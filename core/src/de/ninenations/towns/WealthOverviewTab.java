package de.ninenations.towns;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.ninenations.data.ress.BaseRess;
import de.ninenations.game.S;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.NTable;
import de.ninenations.ui.elements.YProgressBar;
import de.ninenations.ui.newWindow.NTab;

// add happiness
// TODO
public class WealthOverviewTab extends NTab {

	public static final String ID = "wealthoverview";

	public WealthOverviewTab() {
		super(ID, S.nData().getR(BaseRess.WEALTH).getName(), YIcons.WEALTH);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getContentTable()
	 */
	@Override
	public Table getContentTable() {
		NTable cont = new NTable();

		int wealth = 0;

		for (Town t : S.town().getTownsByPlayer(S.actPlayer())) {
			cont.addL(t.getName(), new YProgressBar(t.getRess(BaseRess.WEALTH), 100));
			wealth += t.getRess(BaseRess.WEALTH);
		}

		cont.addSep().colspan(2);
		cont.addL("Total", new YProgressBar(wealth, 100 * S.town().getTownsByPlayerCount(S.actPlayer())));

		// float mod = map.getActivePlayer().getWealthMultiplicator();
		// if (mod != 0) {
		// cont.add(new VisLabel(QConfig.t("town.wealth.reward")));
		// cont.add(map.getQossire().getPlattform().getAnimatedLabel(QConfig.t("town.wealth.move",
		// QStrings.procent(mod))));
		//
		// }

		return cont;
	}

}
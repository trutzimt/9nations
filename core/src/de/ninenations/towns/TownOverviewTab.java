/**
 * 
 */
package de.ninenations.towns;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.ninenations.game.S;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.NSplitTab;
import de.ninenations.ui.elements.NTextButton;

/**
 * @author sven
 *
 */
public class TownOverviewTab extends NSplitTab<Town> {

	public static final String ID = "townoverview";

	private VisTextButton details;

	public TownOverviewTab() {
		super(ID, "Towns", YIcons.TOWN, "At the moment you have no towns. Found one.");

		// add button
		details = new NTextButton("") {

			@Override
			public void perform() {
				details();

			}

		};
		buttonBar.add(details);

		reset();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#rebuild()
	 */
	@Override
	protected void rebuild() {
		resetElements();
		super.rebuild();
	}

	public void resetElements() {
		elements.clear();
		// add all elements
		for (final Town town : MapScreen.get().getData().getTowns().getTownsByPlayer(S.actPlayer())) {
			addElement(town);
		}
	}

	/**
	 * Reset the status
	 */
	@Override
	protected void reset() {
		super.reset();
		active = null;
		details.setDisabled(true);
		details.setText("Select a town first");

	}

	/**
	 * Place the selected artwork
	 */
	@Override
	protected void doubleClickElement(Button btn) {
		details();
	}

	/**
	 * Buy the selected artwork
	 */
	protected void details() {
		MapScreen.get().getStage().addActor(new TownWindow(active));
	}

	@Override
	protected void clickElement(Button btn) {
		active = (Town) btn.getUserObject();
		details.setText("Show Details for " + active.getName());
		details.setDisabled(false);
	}
}
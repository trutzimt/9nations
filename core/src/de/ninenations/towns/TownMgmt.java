package de.ninenations.towns;

import com.badlogic.gdx.utils.Array;

import de.ninenations.core.BaseMgmtMgmt;
import de.ninenations.game.map.NMapBuilding;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;

/**
 * 
 */
public class TownMgmt extends BaseMgmtMgmt<Town> {

	private static final long serialVersionUID = -3952383246612932362L;

	/**
	 * Default constructor
	 */
	public TownMgmt() {

	}

	/**
	 * @param x
	 * @param y
	 */
	public Town add(String name, Player p, int x, int y) {
		Town t = new Town(name, counter++, p.getId(), x, y);
		elements.add(t);

		// first town from the player?
		if (getTownsByPlayerCount(p) == 1) {
			// move all units
			for (NOnMapObject obj : MapScreen.get().getData().getOnMap().getOnMapObjByPlayer(p)) {
				obj.setTown(t.getId());
			}
		}

		return t;

	}

	public void init() {}

	/**
	 * Get all Towns form this player
	 * 
	 * @param player
	 * @return
	 */
	public Array<Town> getTownsByPlayer(Player player) {
		Array<Town> a = new Array<>();
		for (Town t : elements) {
			if (t.getPlayer() == player) {
				a.add(t);
			}
		}

		return a;

	}

	/**
	 * Get all Towns form this player
	 * 
	 * @param player
	 * @return
	 */
	public int getTownsByPlayerCount(Player player) {
		int a = 0;
		for (Town t : elements) {
			if (t.getPlayer() == player) {
				a++;
			}
		}

		return a;

	}

	/**
	 * Get the town
	 *
	 * @param map
	 * @param player
	 * @param x
	 * @param y
	 * @param nextField,
	 *            only on this or the next field?
	 * @return null, if not exist
	 */
	public Town getNearstTown(Player player, int x, int y, boolean nextField) {

		// on the next field and part of the player?
		for (NMapBuilding b : new NMapBuilding[] { MapScreen.get().getData().getBuilding(x, y), MapScreen.get().getData().getBuilding(x - 1, y),
				MapScreen.get().getData().getBuilding(x, y + 1), MapScreen.get().getData().getBuilding(x + 1, y),
				MapScreen.get().getData().getBuilding(x, y - 1) }) {
			if (b != null && b.getPlayer() == player) {
				return b.getTown();
			}
		}

		// look only for the field?
		if (nextField) {
			return null;
		}

		// get towns
		Array<Town> towns = getTownsByPlayer(player);

		// has a town?
		if (towns.size == 0) {
			return null;
		}

		if (towns.size == 1) {
			return towns.get(0);
		}

		Town t = null;
		// find nearst town
		int diff = 9999999;
		for (Town pT : towns) {
			int d = Math.abs(pT.getX() - x) + Math.abs(pT.getY() - y);
			// Town is near?
			if (d < diff) {
				diff = d;
				t = pT;
			}
		}

		return t;
	}

}
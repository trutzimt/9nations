package de.ninenations.towns;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisSlider;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.ninenations.data.nations.BaseNation;
import de.ninenations.data.nations.BaseNation.NationRess;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.game.S;
import de.ninenations.ui.elements.YProgressBar;
import de.ninenations.util.YSounds;

public class WealthTab extends Tab {

	private Town town;
	private int baseWealth;
	private VisLabel baseWealthLabel;
	private Array<VisSlider> sliders;

	public WealthTab(Town town) {
		super(false, false);
		this.town = town;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getTabTitle()
	 */
	@Override
	public String getTabTitle() {
		return S.nData().getR(BaseRess.WEALTH).getName();
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getContentTable()
	 */
	@Override
	public Table getContentTable() {
		BaseNation nation = S.actPlayer().getNation();
		VisTable cont = new VisTable();
		baseWealthLabel = new VisLabel("");

		// add pop
		cont.add(new VisLabel("Population"));
		cont.add(new YProgressBar(town.getRess(BaseRess.WORKER), town.getRess(BaseRess.WORKERMAX))).growX();
		cont.add().row();

		cont.addSeparator().colspan(3);

		// add diff
		baseWealth = 0;
		for (String type : S.nData().getRess()) {
			int level = (int) nation.getRessData(type, NationRess.TOWNLEVEL);
			if (level != 0 && town.getLevel() >= level) {
				baseWealth += addHapp(cont, type);
			}
		}

		if (baseWealth > 0) {
			cont.addSeparator().colspan(3);
		}

		// add changer
		sliders = new Array<>();
		for (String type : S.nData().getRess()) {
			float levelChange = (float) nation.getRessData(type, NationRess.TOWNLEVELCHANGEABLE);
			if (levelChange != 0) {
				addChanger(cont, S.nData().getR(type));
			}
		}

		cont.addSeparator().colspan(3);

		// add wealth
		BaseRess r = S.nData().getR(BaseRess.WEALTH);
		cont.add(new VisLabel(r.getName()));
		cont.add(new YProgressBar(town.getRess(BaseRess.WEALTH), 100)).growX();
		updateWeathLabel();
		cont.add(baseWealthLabel).row();

		float mod = town.getWealthMultiplicator();
		if (mod != 0) {
			cont.add(new VisLabel("Reward"));
			cont.add(new VisLabel("Build/Train time " + procent(mod * -1) + ", AP/HP " + procent(mod))).colspan(2);

		}

		return cont;
	}

	private int addHapp(VisTable cont, String ress) {
		BaseRess r = S.nData().getR(ress);
		cont.add(new VisLabel(r.getName()));
		cont.add(new YProgressBar(town.getRess(ress), town.getRess(BaseRess.WORKERMAX)));

		// add calc
		int val = town.getWealth(ress);
		cont.add(new VisLabel(plus(val))).row();

		return val;
	}

	private void addChanger(VisTable cont, final BaseRess ress) {
		cont.add(new VisLabel(ress.getName()));

		final VisLabel erg = new VisLabel();
		final VisLabel desc = new VisLabel();
		final VisSlider slider = new VisSlider(-5, 5, 1, false);
		slider.setValue(town.getTownLevelChangeable().get(ress.getType(), 0));
		sliders.add(slider);
		// slider.setValue(QSettings.getSoundVolume());
		slider.addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				YSounds.pClick();
				town.getTownLevelChangeable().put(ress.getType(), (int) slider.getValue());
				updateChanger(slider, ress, erg, desc);
			}
		});

		cont.add(slider).growX();

		// add calc
		updateChanger(slider, ress, erg, desc);
		cont.add(erg).row();
		cont.add();
		cont.add(desc);
		cont.add().row();

		// return town.getHappiness(ress);
	}

	private void updateChanger(VisSlider slider, BaseRess ress, VisLabel erg, VisLabel desc) {
		erg.setText(plus((int) slider.getValue()));

		int cost = town.getWealthRess(ress.getType());
		desc.setText((cost > 0 ? "Cost " : "Get ") + ress.getName() + " " + (cost > 0 ? cost : cost * -1));
		updateWeathLabel();
	}

	/**
	*
	*/
	private void updateWeathLabel() {
		int wealth = baseWealth;
		for (VisSlider slider : sliders) {
			wealth += slider.getValue();
		}
		baseWealthLabel.setText(plus(wealth));
	}

	/**
	 * Add a plus sign, if the value positive
	 *
	 * @param val
	 * @return
	 */
	private String plus(int val) {
		return val > 0 ? "+" + val : Integer.toString(val);
	}

	/**
	 * Add a plus sign, if the value positive
	 *
	 * @param val
	 * @return
	 */
	public String procent(float val) {
		return plus(Math.round(val * 100)) + "%";
	}

}
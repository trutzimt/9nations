/**
 *
 */
package de.ninenations.towns;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.elements.YSplitTab;
import de.ninenations.ui.window.YTabWindow;

/**
 * @author sven
 *
 */
public class TownWindow extends YTabWindow {

	protected Town town;

	/**
	 * @param title
	 */
	public TownWindow(final Town town) {
		super(town.getName());

		this.town = town;

		// add overview
		tabbedPane.add(new TownOverviewTab());

		// is not owner?
		if (town.getPlayer() != S.actPlayer()) {
			buildIt();
			return;
		}

		// add happiness
		if (S.isActive(ScenConf.WEALTH)) {
			tabbedPane.add(new WealthTab(town));
		}

		tabbedPane.add(new NOnMapOverview(EMapData.BUILDING));
		tabbedPane.add(new NOnMapOverview(EMapData.UNIT));

		addTitleIcon(town.getIcon());
		buildIt();

	}

	class TownOverviewTab extends Tab {

		public TownOverviewTab() {
			super(false, false);
		}

		@Override
		public String getTabTitle() {
			return "Overview";
		}

		@Override
		public Table getContentTable() {
			return town.getInfoPanel().createScrollPaneInTable();
		}

	}

	class NOnMapOverview extends YSplitTab<NOnMapObject> {

		public NOnMapOverview(EMapData type) {
			super(type == EMapData.BUILDING ? "Buildings" : "Units", "The town has no " + (type == EMapData.BUILDING ? "buildings" : "units"));

			// add it
			for (NOnMapObject obj : MapScreen.get().getData().getOnMap().getOnMapObjByTown(town, type)) {
				addElement(obj);
			}

		}

		@Override
		protected void doubleClickElement(Button btn) {}

	}

}

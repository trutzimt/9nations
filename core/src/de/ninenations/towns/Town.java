package de.ninenations.towns;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Keys;

import de.ninenations.core.BaseMgmtObject;
import de.ninenations.data.nations.BaseNation;
import de.ninenations.data.nations.BaseNation.NationRess;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.player.Player.RessType;
import de.ninenations.stats.Stats;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.NRessLabel;
import de.ninenations.ui.elements.YProgressBar;
import de.ninenations.ui.elements.YRandomField;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.window.YNotificationSaver;
import de.ninenations.util.NGenerator;
import de.ninenations.util.YError;

/**
 * 
 */
public class Town extends BaseMgmtObject {

	private int level;
	private int maxRange, maxBuildings, maxStorage;
	private int x, y;

	private int pid; // player/owner id

	// TODO calc method
	private int actStorage;

	/**
	 * 
	 */
	private ObjectMap<String, Integer> ress;
	private ObjectMap<String, Integer> townLevelChangeable;

	private Stats stats;

	/**
	 * Default constructor
	 */
	@SuppressWarnings("unused")
	private Town() {
		super();
	}

	/**
	 * Default constructor
	 */
	public Town(String name, int tid, int pid, int x, int y) {
		super(null, tid, name);

		this.pid = pid;

		setLevel(1);

		ress = new ObjectMap<>();
		townLevelChangeable = new ObjectMap<>();

		this.x = x;
		this.y = y;

		maxStorage = 100;

		if (S.isActive(ScenConf.WEALTH)) {
			ress.put(BaseRess.WEALTH, 50);
		}

		stats = new Stats();
	}

	/**
	 * @param type
	 * @param baseid
	 */
	@Deprecated
	public void addRess(String type, int count) {
		addRess(type, count, RessType.GIFT);
	}

	/**
	 * @param type
	 * @param baseid
	 */
	public void addRess(String type, int count, RessType art) {
		// YLog.log2("add ress", name, type, count);

		// multiple count?
		int val = getPlayer().getRessMultiplicator(art, type);
		if (val != 0) {
			count += count * val / 100.0f;
		}

		// set
		if (ress.containsKey(type)) {
			ress.put(type, ress.get(type, 0) + count);
		} else {
			ress.put(type, count);
		}

		BaseRess r = S.nData().getR(type);
		// check limit
		actStorage += count * r.getStorageWeight();
		if (actStorage > maxStorage) {
			int diff = (int) (actStorage * r.getStorageWeight() - maxStorage);
			ress.put(type, ress.get(type) - diff);
			actStorage = maxStorage;
			getPlayer().addInfo(new YNotificationSaver(name + " has a full storage. You lose " + diff + "x " + r.getName(), r.getIcon(), YIcons.RESSOURCE));
		}

		if (ress.get(type) < 0) {
			YError.error(new IllegalArgumentException("Ress " + type + " is negativ (" + ress.get(type) + ")"), false);
		}
	}

	/**
	 * @param type
	 */
	public int getRess(String type) {
		return ress.get(type, 0);
	}

	/**
	 * @param type
	 */
	public Keys<String> getRessTypes() {
		return ress.keys();
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.TOWN);
	}

	@Override
	public YTable getInfoPanel() {
		YTable t = new YTable();
		// is not owner?
		if (getPlayer() != S.actPlayer()) {
			t.addL("Name", name);
			t.addL("Player", getPlayer().getName());
			return t;
		}

		t.addL("Name", new YRandomField(name) {

			@Override
			public void saveText(String text) {
				name = text;

			}

			@Override
			protected String getRndText() {
				return NGenerator.getTown();
			}
		});
		t.addL("Level", getLevelName());
		t.addL("Range", maxRange + " Fields");
		t.addL("Population", new YProgressBar(getRess(BaseRess.WORKER), getRess(BaseRess.WORKERMAX)));
		t.addL("Storage", new YProgressBar(actStorage, maxStorage));
		t.addL("Buildings", new YProgressBar(getActBuildingCount(), maxBuildings));

		// add wealth
		if (S.isActive(ScenConf.WEALTH)) {
			t.addL("Wealth", new YProgressBar(getRess(BaseRess.WEALTH), 100));
		}

		t.addL("Ress", new NRessLabel(NRessLabel.generateRessLabel(ress)));

		return t;
	}

	/**
	 * @return
	 */
	public int getActBuildingCount() {
		return S.onMap().countOnMapObjByTownType(this, EMapData.BUILDING, null);
	}

	/**
	 * @return the pid
	 */
	public Player getPlayer() {
		return MapScreen.get().getData().getPlayers().get(pid);
	}

	@Override
	public void nextRound() {

		// check storage
		if (actStorage * 1.1 >= maxStorage) {
			getPlayer().addInfo(new YNotificationSaver("Town " + name + " has almost a full storage.", YIcons.RESSOURCE));
		}

		updateWealth();

		// update stat
		stats.add(Stats.GOLD, getRess(BaseRess.GOLD));
		stats.nextRound();

	}

	@Override
	public void afterLoad() {}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @return the level
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * @return the actStorage
	 */
	public int getActStorage() {
		return actStorage;
	}

	/**
	 * @return the maxStorage
	 */
	public int getMaxStorage() {
		return maxStorage;
	}

	public String getLevelName() {
		return getLevelName(level);
	}

	/**
	 * Get the name
	 * 
	 * @param level
	 * @return
	 */
	public static String getLevelName(int level) {
		switch (level) {
		case 1:
			return "Village";
		case 2:
			return "Bigger Village";
		case 3:
			return "Town";
		case 4:
			return "Bigger Town";
		default:
			return level + " ??";
		}
	}

	/**
	 * @param level
	 *            the level to set
	 */
	public void setLevel(int level) {
		this.level = level;

		maxBuildings = level * 15;
		maxRange = 5 * level;
	}

	/**
	 * @param maxStorage
	 *            the maxStorage to set
	 */
	public void setMaxStorage(int maxStorage) {

		// TODO remove to much ress
		this.maxStorage = maxStorage;
	}

	/**
	 * Calculate the wealth
	 *
	 * @param type
	 * @return
	 */
	public int getWealth(String type) {
		int diff = Math.round(getRess(type) / (float) getRess(BaseRess.WORKERMAX) * 10) - 10;
		if (diff < 0) {
			return diff < -5 ? -5 : diff;
		}

		return diff > 5 ? 5 : diff;
	}

	/**
	 * Update the wealth for the town
	 */
	public void updateWealth() {
		if (!S.isActive(ScenConf.WEALTH)) {
			return;
		}

		int baseWealth = 0;

		// TODO fix
		if (name.equals("nature")) {
			return;
		}

		BaseNation nation = getPlayer().getNation();
		for (String type : S.nData().getRess()) {
			int rlevel = (int) nation.getRessData(type, NationRess.TOWNLEVEL);
			float levelChange = (float) nation.getRessData(type, NationRess.TOWNLEVELCHANGEABLE);
			if (rlevel != 0 && level >= rlevel) {
				baseWealth += getWealth(type);
			}
			if (levelChange != 0) {
				// TODO fix negativ elements
				int c = getWealthRess(type);
				if (getRess(type) >= c) {
					addRess(type, -getWealthRess(type));
				} else {
					townLevelChangeable.put(type, -5);
					BaseRess br = S.nData().getR(type);
					getPlayer().addInfo(new YNotificationSaver("Missing " + c + "x " + br.getName() + " for consuming in " + getName(), br.getiID()));

				}
				baseWealth += townLevelChangeable.get(type, 0);

			}
		}

		// noti?
		if (0 > baseWealth) {
			getPlayer().addInfo(new YNotificationSaver("You losing wealth in " + getName() + ".", S.nData().getR(BaseRess.WEALTH).getiID()));
		}

		// add it?
		int s = getRess(BaseRess.WEALTH);
		if (s + baseWealth < 0) {
			addRess(BaseRess.WEALTH, -s);
		} else if (s + baseWealth > 100) {
			addRess(BaseRess.WEALTH, 100 - s);
		} else {
			addRess(BaseRess.WEALTH, baseWealth);
		}

		if (getRess(BaseRess.WEALTH) < 25) {
			getPlayer().addInfo(new YNotificationSaver("Your wealth in " + getName() + " is very low.", S.nData().getR(BaseRess.WEALTH).getiID()));

		}

		stats.add(Stats.WEALTH, getRess(BaseRess.WEALTH));

	}

	/**
	 * Calculate the ress, based on the wealth
	 *
	 * @param type
	 * @return
	 */
	public int getWealthRess(String type) {
		float fac = (float) getPlayer().getNation().getRessData(type, NationRess.TOWNLEVELCHANGEABLE);
		int cost = 0;
		if (fac > 0) {
			cost = (int) (getRess(BaseRess.WORKERMAX) * fac * (townLevelChangeable.get(type, 0) + 5) / 5);
		} else {
			cost = (int) (getRess(BaseRess.WORKERMAX) * fac * townLevelChangeable.get(type, 0) / 5) * -1;
		}

		return cost;
	}

	/**
	 * @return the townLevelChangeable
	 */
	public ObjectMap<String, Integer> getTownLevelChangeable() {
		return townLevelChangeable;
	}

	/**
	 * Get the wealth town multiplicator
	 *
	 * @return - > negativ + > positiov
	 */
	public float getWealthMultiplicator() {
		if (!S.isActive(ScenConf.WEALTH)) {
			return 0;
		}

		float wealth = (float) getRess(BaseRess.WEALTH) / 100;

		if (wealth >= 0.75f) {
			return wealth - 0.75f;
		}
		if (wealth < 0.5f) {
			return (0.5f - wealth) * -1;
		}

		return 0;
	}

	/**
	 * @return the maxRange
	 */
	public int getMaxRange() {
		return maxRange;
	}

	/**
	 * @return the maxBuildings
	 */
	public int getMaxBuildingCount() {
		return maxBuildings;
	}

	// TODO public void checkForAbandonedBuildings() {
	// for (NOnMapObject b : S.onMap().getOnMapObjByPlayer(S.players().get(0),
	// EMapData.BUILDING)) {
	//
	// }
	// }

	/**
	 * Destroy this town
	 */
	public void destroy() {

		// remove town form all buildings
		for (NOnMapObject b : S.onMap().getOnMapObjByTown(this, EMapData.BUILDING)) {
			b.setTown(-1);
			b.setPlayer(1);
		}

		// remove townhall
		S.build(x, y).destroy();

		// remove town form all units
		Array<NOnMapObject> a = S.onMap().getOnMapObjByTown(this, EMapData.UNIT);
		for (NOnMapObject b : a) {
			b.setTown(-1);
		}

		// remove town
		S.town().remove(id);

		// player has a town left?
		if (S.town().getTownsByPlayerCount(getPlayer()) > 0) {
			// put all units in this town
			Town t = S.town().getNearstTown(getPlayer(), x, y, false);
			for (NOnMapObject b : a) {
				b.setTown(t.getId());
			}
			getPlayer().addInfo(new YNotificationSaver("All units from " + getName() + " no are part of " + t.getName(), YIcons.DISEMBARK));
		}

		// TODO check if buildings can be part of another town

	}

	/**
	 * @param pid
	 *            the pid to set
	 */
	public void setPid(int pid) {
		this.pid = pid;
	}

	/**
	 * @return the stats
	 */
	public Stats getStats() {
		return stats;
	}

	/**
	 * @return the maxBuildings
	 */
	public int getMaxBuildings() {
		return maxBuildings;
	}

	/**
	 * @param maxBuildings
	 *            the maxBuildings to set
	 */
	public void setMaxBuildings(int maxBuildings) {
		this.maxBuildings = maxBuildings;
	}

	/**
	 * @param maxRange
	 *            the maxRange to set
	 */
	public void setMaxRange(int maxRange) {
		this.maxRange = maxRange;
	}

}
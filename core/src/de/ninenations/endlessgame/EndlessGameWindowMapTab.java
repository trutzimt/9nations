/**
 * 
 */
package de.ninenations.endlessgame;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.ObjectMap;

import de.ninenations.core.NN;
import de.ninenations.endlessgame.EndlessGameWindowMapTab.MapInfo;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.NTextButton;
import de.ninenations.ui.elements.YSplitTab;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.elements.YTextButton;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class EndlessGameWindowMapTab extends YSplitTab<MapInfo> {

	private ObjectMap<String, String> settings;
	private NTextButton setMap;

	public EndlessGameWindowMapTab(ObjectMap<String, String> settings) {
		super("Map", "No map found");

		this.settings = settings;

		setMap = new NTextButton("Set Map") {

			@Override
			public void perform() {
				doubleClickElement(null);

			}
		};
		setMap.setDisabled(true);
		buttonBar.addActor(setMap);

		for (FileHandle file : NN.plattform().getFolderContent("map", ".tmx")) {
			addElement(new MapInfo(file));
		}
	}

	/**
	 * First click on the list element
	 * 
	 * @param btn
	 */
	@Override
	protected void clickElement(Button btn) {
		setMap.setDisabled(false);
		setMap.setText("Set " + active.getName() + " as map");
	}

	@Override
	protected void doubleClickElement(Button btn) {
		YSounds.pClick();
		settings.put("map", active.getName());

	}

	public class MapInfo extends BaseDisplay {

		private FileHandle map;
		private int width, height;
		private TiledMap tiledMap;

		public MapInfo(FileHandle map) {
			super(null, map.nameWithoutExtension());
			this.map = map;

		}

		@Override
		public Image getIcon() {
			return YIcons.getIconI(YIcons.MAP);
		}

		@Override
		public YTable getInfoPanel() {
			// read data
			if (tiledMap == null) {

				// TODO performar
				if (!NN.asset().isLoaded(map.toString(), TiledMap.class)) {
					NN.asset().load(map.toString(), TiledMap.class);
					NN.asset().finishLoading();
				}

				tiledMap = NN.asset().get(map.toString(), TiledMap.class);
				TiledMapTileLayer t = (TiledMapTileLayer) tiledMap.getLayers().get(0);
				width = t.getWidth();
				height = t.getHeight();
			}

			YTable t = new YTable();
			t.addL("Name", name);
			t.addL("Size", width + "x" + height + " fields");
			t.addL(null, new YTextButton("Preview", true) {

				@Override
				public void perform() {
					NN.screen().getStage().addActor(new EndlessMapPreview(tiledMap, name));

				}
			});
			return t;
		}

	}
}

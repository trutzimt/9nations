package de.ninenations.endlessgame;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YWindow;

public class EndlessMapPreview extends YWindow {

	public EndlessMapPreview(TiledMap tiledMap, String name) {
		super("Preview of " + name);

		TiledMapTileLayer t = (TiledMapTileLayer) tiledMap.getLayers().get(0);
		int width = t.getWidth();
		int height = t.getHeight();

		// build all layers
		Image img[][] = new Image[width][height];
		for (int z = 0; z < tiledMap.getLayers().getCount(); z++) {
			t = (TiledMapTileLayer) tiledMap.getLayers().get(z);
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					if (t.getCell(x, y) != null) {
						img[x][y] = new Image(t.getCell(x, y).getTile().getTextureRegion());
					}
				}
			}
		}

		// add all layers
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (img[x][y] != null) {
					add(img[x][y]);
				}
			}
			row();
		}

		// set size
		pack();
		setWidth(Math.min(getWidth(), Gdx.graphics.getWidth()));
		setHeight(Math.min(getHeight(), Gdx.graphics.getHeight()));

		addTitleIcon(YIcons.getIconI(YIcons.MAP));
	}

}

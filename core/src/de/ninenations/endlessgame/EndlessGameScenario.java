/**
 *
 */
package de.ninenations.endlessgame;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

import de.ninenations.actions.req.ReqExplored;
import de.ninenations.actions.req.ReqPlayer;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.data.terrain.BaseTerrain;
import de.ninenations.data.terrain.BaseTerrain.NMove;
import de.ninenations.endlessgame.EndlessGameWindow.PlayerE;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.quests.QuestGenerator;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.CampaignMgmt;
import de.ninenations.scenarios.ScenarioHelper;
import de.ninenations.util.NGenerator;
import de.ninenations.util.YError;
import de.ninenations.util.YLog;

/**
 * @author sven
 *
 */
public class EndlessGameScenario extends BaseScenario {

	public static final String ID = "endlessgame";

	private ObjectMap<String, String> settings;
	private Array<PlayerE> players;

	/**
	 */
	public EndlessGameScenario(ObjectMap<ScenConf, Boolean> confSettings, ObjectMap<String, String> settings, Array<PlayerE> players) {
		super(ID, "Endless game", "map/" + settings.get("map") + ".tmx");

		camp = CampaignMgmt.getCampaign("single");

		this.settings = settings;
		this.players = players;

		// save the settings
		for (ScenConf c : confSettings.keys()) {
			conf.put(c, confSettings.get(c));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.trutz.qossire.scenario.BaseScenario#firstRound(at.trutz.qossire.player.
	 * PlayerManagement)
	 */
	@Override
	public void firstStart() {

		// change human to hotseat?
		int human = -1;

		for (int i = 0; i < players.size; i++) {
			if (players.get(i).getType().equals("human")) {
				if (human == -1) {
					human = i;
				} else {
					players.get(i).setType("hotseat");
					players.get(human).setType("hotseat");
				}
			}
		}

		// create the player
		players: for (PlayerE e : players) {
			Player p = MapScreen.get().getData().getPlayers().add(e.getType(), e.getName(), e.getNation());
			String leader = p.getNation().getLeader();

			// add quests
			if ("1".equals(settings.get("quest.gold", "0"))) {
				p.addQuest(QuestGenerator.winRess(BaseRess.GOLD, 1000));
			}

			if ("1".equals(settings.get("quest.explore", "0"))) {
				p.addQuest(QuestGenerator.win().addReq(new ReqExplored(false, (int) (S.width() * S.height() * 0.95))));
			}

			if ("1".equals(settings.get("quest.alone", "0"))) {
				p.addQuest(QuestGenerator.win().addReq(new ReqPlayer(true, 1)));
			}

			if ("1".equals(settings.get("quest.king", "0"))) {
				p.addQuest(QuestGenerator.loseUnit(leader));
			}

			// set start position
			// TODO better way
			for (int i = 0; i < 1000; i++) {
				int x = NGenerator.getIntBetween(0, S.width());
				int y = NGenerator.getIntBetween(0, S.height());

				// find a good terrain?
				BaseTerrain t = S.map().getTerrain(x, y);
				YLog.log(t.getName());
				if (t.canPass(NMove.WALK) && t.getCost(NMove.WALK) <= 5) {
					ScenarioHelper.unit(leader, p, null, x, y);
					continue players;
				}
			}

			YError.error(new IllegalArgumentException("Can not find a free place to start"), false);

		}

		// find a new spot

	}

}

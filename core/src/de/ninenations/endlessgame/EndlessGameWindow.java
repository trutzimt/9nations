/**
 *
 */
package de.ninenations.endlessgame;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisSelectBox;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.ninenations.core.NN;
import de.ninenations.game.GameStartScreen;
import de.ninenations.game.ScenConf;
import de.ninenations.ui.elements.YRandomField;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.elements.YTextButton;
import de.ninenations.ui.window.YTabWindow;
import de.ninenations.util.NGenerator;
import de.ninenations.util.NSettings;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class EndlessGameWindow extends YTabWindow {

	private ObjectMap<ScenConf, Boolean> settingsConf;
	private ObjectMap<String, String> settings;
	private String seed;
	private Array<PlayerE> players;

	/**
	 * @param title
	 */
	public EndlessGameWindow() {
		super("Endless game");

		seed = Long.toString(NN.random().nextLong());

		settingsConf = new ObjectMap<>();
		for (ScenConf c : ScenConf.values()) {
			settingsConf.put(c, true);
		}
		settings = new ObjectMap<>();
		players = new Array<>();

		tabbedPane.add(new EndlessGameWindowMapTab(settings));
		tabbedPane.add(new SettingsTab());
		tabbedPane.add(new PlayerTab());

		buildIt();

	}

	public void addStartButton(VisTable table) {
		table.addSeparator();
		table.add(new YTextButton("Start game") {

			@Override
			public void perform() {

				// check it
				if (players.size == 0) {
					YSounds.pBuzzer();
					Dialogs.showOKDialog(getStage(), getTitleLabel().getText().toString(), "Please add at least one player.");
					return;
				}

				// check map
				if (!settings.containsKey("map")) {
					YSounds.pBuzzer();
					Dialogs.showOKDialog(getStage(), getTitleLabel().getText().toString(), "Please select a map first.");
					return;
				}

				// check it
				boolean human = false;
				for (PlayerE e : players) {
					if (NN.nData().getK(e.getType()).isHuman()) {
						human = true;
						break;
					}
				}

				if (!human) {
					YSounds.pBuzzer();
					Dialogs.showOKDialog(getStage(), getTitleLabel().getText().toString(), "Please add at least one human player.");
					return;
				}

				// check it
				YSounds.play(YSounds.STARTGAME);

				EndlessGameScenario e = new EndlessGameScenario(settingsConf, settings, players);
				e.setRandomSeed(Long.parseLong(seed));

				NN.get().switchScreen(new GameStartScreen(e));

			}
		});
	}

	public class SettingsTab extends Tab {
		public SettingsTab() {
			super(false, false);
		}

		@Override
		public String getTabTitle() {
			return "Game Settings";
		}

		@Override
		public Table getContentTable() {
			YTable table = new YTable();
			table.addH("Game Settings");

			// add settings
			addC(table, ScenConf.FOG, "Enable fog of war");
			addC(table, ScenConf.RESEARCH, "Enable research");
			addC(table, ScenConf.RANDOMEVENTS, "Enable random events");
			// addC(table, ScenConf.TOWNS, "Enable Towns");
			// addC(table, ScenConf.WEATHER, "Enable Weather system");
			addC(table, ScenConf.WEALTH, "Enable wealth system");

			table.addH("Options to win");
			addS(table, "quest.gold", "Collect 1000 Gold");
			addS(table, "quest.explore", "Explore the whole map");
			addS(table, "quest.alone", "Be the last active player");

			table.addH("Options to lose");
			addS(table, "quest.king", "Lose your leader");

			table.addH("Advanced settings");
			table.addL("Seed", new YRandomField(seed) {

				@Override
				public void saveText(String text) {
					seed = text;

				}

				@Override
				protected String getRndText() {
					return Long.toString(NN.random().nextLong());
				}
			});

			addStartButton(table);

			return table.createScrollPaneInTable();
		}

		/**
		 * Helper add
		 *
		 * @param table
		 * @param label
		 * @param a
		 */
		protected void addC(YTable table, final ScenConf conf, String title) {
			final VisCheckBox c = new VisCheckBox(title);

			c.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					settingsConf.put(conf, c.isChecked());
				}
			});
			// start value
			c.setChecked(settingsConf.get(conf, true));

			table.addL(null, c);
		}

		/**
		 * Helper add
		 *
		 * @param table
		 * @param label
		 * @param a
		 */
		protected void addS(YTable table, final String key, String title) {
			final VisCheckBox c = new VisCheckBox(title);

			c.addListener(new ChangeListener() {
				@Override
				public void changed(ChangeEvent event, Actor actor) {
					settings.put(key, c.isChecked() ? "1" : "0");
				}
			});
			// start value
			c.setChecked(settings.get(key, "0").equals("1"));

			table.addL(null, c);
		}

	}

	public class PlayerTab extends Tab {

		private VisTable playerTable;
		private VisTable table;
		private Array<String> typs, nations, playerTypes;

		public PlayerTab() {
			super(false, false);

			players = new Array<>();
			addPlayer();
			playerTable = new VisTable();

			// build typs
			typs = new Array<>();
			playerTypes = new Array<>();
			for (String ki : NN.nData().getKIs()) {
				if (!NN.nData().getK(ki).isSelectable()) {
					continue;
				}

				typs.add(NN.nData().getK(ki).getName());
				playerTypes.add(ki);
			}

			// build nations
			nations = new Array<>();
			for (String ki : NN.nData().getNations()) {
				nations.add(NN.nData().getN(ki).getName());
			}

			table = new VisTable();
			table.add(playerTable).grow().row();
			rebuildTable();
			table.add(new YTextButton("Add new player") {

				@Override
				public void perform() {

					addPlayer();
					YSounds.pClick();
					rebuildTable();

				}
			}).row();

			addStartButton(table);
		}

		private void rebuildTable() {
			playerTable.reset();

			if (players.size == 0) {
				playerTable.add(new VisLabel("Please add a player first"));
				return;
			}

			playerTable.add(new VisLabel("Name"));
			playerTable.add(new VisLabel("Typ"));
			playerTable.add(new VisLabel("Nation"));
			playerTable.add(new VisLabel("Option")).row();
			playerTable.addSeparator().colspan(4).row();

			for (final PlayerE p : players) {
				playerTable.add(new YRandomField(p.getName()) {

					@Override
					public void saveText(String text) {
						p.setName(text);
					}

					@Override
					protected String getRndText() {
						return NGenerator.getName(NN.random().nextBoolean());
					}
				});

				// add player type/ki
				VisSelectBox<String> box = new VisSelectBox<>();
				box.setItems(typs);
				box.addCaptureListener(new ChangeListener() {

					@SuppressWarnings("unchecked")
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						p.setType(playerTypes.get(((VisSelectBox<String>) actor).getSelectedIndex()));

					}
				});
				box.setSelected(NN.nData().getK(p.getType()).getName());
				playerTable.add(box);

				// add nation
				VisSelectBox<String> n2 = new VisSelectBox<>();
				n2.setItems(nations);
				n2.addCaptureListener(new ChangeListener() {

					@SuppressWarnings("unchecked")
					@Override
					public void changed(ChangeEvent event, Actor actor) {
						p.setNation(NN.nData().getNations().get(((VisSelectBox<String>) actor).getSelectedIndex()));
					}
				});
				n2.setSelected(NN.nData().getN(p.getNation()).getName());
				playerTable.add(n2);

				YTextButton d = new YTextButton("Delete") {

					@Override
					public void perform() {
						YSounds.pBuzzer();
						players.removeValue(p, true);
						rebuildTable();

					}
				};
				d.setDisabled(players.size < 2);

				playerTable.add(d).row();
			}
		}

		@Override
		public String getTabTitle() {
			return "Player";
		}

		@Override
		public Table getContentTable() {
			return table;
		}

		/**
		 * @return
		 */
		private void addPlayer() {
			PlayerE p = new PlayerE();
			p.setName(players.size == 0 ? NSettings.getUserName() : NGenerator.getName(NN.random().nextBoolean()));
			p.setType("human");// TODO nData.getKIs().get(0));
			p.setNation(NN.nData().getNations().get(0));
			players.add(p);
		}

	}

	public class PlayerE {
		private String name, type, nation;

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @return the nation
		 */
		public String getNation() {
			return nation;
		}

		/**
		 * @param name
		 *            the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @param type
		 *            the type to set
		 */
		public void setType(String type) {
			this.type = type;
		}

		/**
		 * @param nation
		 *            the nation to set
		 */
		public void setNation(String nation) {
			this.nation = nation;
		}
	}
}

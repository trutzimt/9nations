package de.ninenations.player;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.core.BaseMgmtMgmt;
import de.ninenations.data.ki.BasePlayerType;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.saveload.SaveLoadManagement;
import de.ninenations.ui.UiHelper;
import de.ninenations.util.NSettings;

/**
 * 
 */
public class PlayerMgmt extends BaseMgmtMgmt<Player> {

	private static final long serialVersionUID = 6312157606695268699L;
	private int actPlayer;

	/**
	 * Default constructor
	 */
	public PlayerMgmt() {
		super();

		actPlayer = 0;
	}

	/**
	 * Add a new player
	 * 
	 * @param type
	 * @param name
	 * @param nation
	 * @return
	 */
	public Player add(String type, String name, String nation) {
		Player p = new Player(type, name, counter++, nation);

		elements.add(p);

		return p;
	}

	/**
	 * Go to the next player or round
	 *
	 */
	public void nextPlayer() {

		if (actPlayer >= 0) {
			Player p = getActPlayer();
			p.performAtRoundEnd();
			// save?
			if (NSettings.isAutosave() && S.isActive(ScenConf.LOADSAVE) && p.getPlayerType().isHuman()) {
				SaveLoadManagement.save("Autosave " + p.getName(), false);
			}
		}

		// go to the next player
		actPlayer++;
		// next round?
		if (actPlayer >= elements.size) {
			MapScreen.get().getData().nextRound();
		}
		Player p = getActPlayer();
		p.nextRound();

		// check type
		BasePlayerType ki = p.getPlayerType();
		if (!ki.isHuman()) {
			// show it
			Image black = UiHelper.createBlackImg(false);
			MapScreen.get().getStage().addActor(black);

			VisLabel label = new VisLabel("Player " + p.getName() + " is playing.");
			label.setFillParent(true);
			label.setAlignment(Align.center);
			MapScreen.get().getStage().addActor(label);

			// perform
			ki.run(p);

			// remove it
			black.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
			label.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));

			nextPlayer();
			return;
		}

		// show the next player
		MapScreen.get().getMap().nextPlayer();
		MapScreen.get().getGui().nextPlayer();

		p.performAtRoundBegin();
		ki.run(p);
	}

	/**
	 * Remove the object
	 * 
	 * @param id
	 * @return
	 */
	@Override
	public void remove(int id) {
		// act player?
		if (get(id) == getActPlayer()) {
			nextPlayer();
			super.remove(id);
			if (actPlayer > 0) {
				actPlayer--;
			}
			return;
		}

		// remove it
		super.remove(id);
		actPlayer--;
	}

	/**
	 * Go to the next round
	 *
	 */
	@Override
	public void nextRound() {
		actPlayer = 0;
	}

	/**
	 * Get the act player
	 * 
	 * @return
	 */
	public Player getActPlayer() {
		return elements.get(actPlayer);
	}

	/**
	 * Init the nature player
	 */
	public void init() {
		add("nature", "Nature", null);
	}

	/**
	 * Start the game
	 */
	public void start() {
		actPlayer = -1;
		nextPlayer();
	}

}
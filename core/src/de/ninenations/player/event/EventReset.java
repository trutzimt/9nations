package de.ninenations.player.event;

import com.badlogic.gdx.scenes.scene2d.Actor;

import de.ninenations.data.events.BaseEvent;
import de.ninenations.game.S;
import de.ninenations.ui.actions.YChangeListener;

public class EventReset extends YChangeListener {

	private String eventID;
	private int playerID;

	/**
	 * @param eventID
	 * @param playerID
	 */
	public EventReset(String eventID, int playerID) {
		super();
		this.eventID = eventID;
		this.playerID = playerID;
	}

	@SuppressWarnings("unused")
	private EventReset() {}

	@Override
	public void changedY(Actor actor) {
		BaseEvent e = S.nData().getEv(eventID);
		e.performEnd(S.players().get(playerID), null, -1, -1);

	}

}

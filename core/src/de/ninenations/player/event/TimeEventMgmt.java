package de.ninenations.player.event;

import java.io.Serializable;

import com.badlogic.gdx.utils.SnapshotArray;

import de.ninenations.player.Player;

public class TimeEventMgmt implements Serializable {

	private static final long serialVersionUID = 7901540057464122826L;

	private SnapshotArray<TimeEvent> array;

	public TimeEventMgmt() {
		array = new SnapshotArray<>();
	}

	public TimeEvent add(TimeEvent event) {
		array.add(event);
		return event;
	}

	/**
	 * Cycle on all arrays
	 * 
	 * @param player
	 */
	public void nextRound(Player player) {
		if (array.size == 0) {
			return;
		}

		Object[] items = array.begin();
		for (int i = 0, n = array.size; i < n; i++) {
			TimeEvent item = (TimeEvent) items[i];
			if (item.nextRound(player)) {
				array.removeIndex(i);
			}
		}
		array.end();
	}

	/**
	 * @return the array
	 */
	public SnapshotArray<TimeEvent> getArrayRaw() {
		return array;
	}

}

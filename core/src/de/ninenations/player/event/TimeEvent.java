/**
 * 
 */
package de.ninenations.player.event;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.player.Player;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YTable;

/**
 * @author sven
 *
 */
public class TimeEvent extends BaseDisplay {

	private int wait, icon;
	private YChangeListener action;

	@SuppressWarnings("unused")
	private TimeEvent() {}

	/**
	 * 
	 * @param name
	 * @param icon
	 * @param wait
	 * @param action
	 */
	public TimeEvent(String name, int icon, int wait, YChangeListener action) {
		super(null, name);

		this.wait = wait;
		this.icon = icon;
		this.action = action;
	}

	/**
	 * Need to wait?
	 * 
	 * @param player
	 * @return true > finish, false > wait
	 */
	public boolean nextRound(Player player) {
		wait--;

		if (wait > 0) {
			return false;
		}

		action.changed(null, null);
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(icon);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getInfoPanel()
	 */
	@Override
	public YTable getInfoPanel() {
		YTable t = super.getInfoPanel();
		t.addL("Time", wait + " rounds");
		return t;
	}
}

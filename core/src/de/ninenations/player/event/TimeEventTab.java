/**
 * 
 */
package de.ninenations.player.event;

import com.badlogic.gdx.scenes.scene2d.ui.Button;

import de.ninenations.game.S;
import de.ninenations.ui.elements.NSplitTab;

/**
 * @author sven
 *
 */
public class TimeEventTab extends NSplitTab<TimeEvent> {

	public static final String ID = "timeevent";

	/**
	 * @param title
	 * @param error
	 */
	public TimeEventTab() {
		super(ID, "Events", S.round().getSeason().getLogo(), "Their are no events at the moment");

		for (TimeEvent t : S.actPlayer().getTimeEvents().getArrayRaw()) {
			addElement(t);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.ui.elements.YSplitTab#doubleClickElement(com.badlogic.gdx.
	 * scenes.scene2d.ui.Button)
	 */
	@Override
	protected void doubleClickElement(Button btn) {}

}

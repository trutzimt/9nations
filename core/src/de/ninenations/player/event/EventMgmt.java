package de.ninenations.player.event;

import de.ninenations.data.events.BaseEvent;
import de.ninenations.game.S;
import de.ninenations.player.Player;
import de.ninenations.ui.window.YNotificationSaver;

//TODO implement
/**
 * 
 */
public class EventMgmt implements java.io.Serializable {

	private static final long serialVersionUID = 1526432914074588609L;

	private int wait;

	private transient BaseEvent event;
	private String eventID;

	/**
	 * Default constructor
	 */
	public EventMgmt() {
		wait = -1;
	}

	public void nextRound(Player player) {
		// new start?
		if (wait == -1) {
			calcNewWaitTime();
		}

		// wait?
		if (wait > 0) {
			wait--;
			return;
		}

		// find next event?
		if (eventID == null) {
			event = S.nData().getRandEv();
			eventID = event.getType();
			// YLog.log("new event", event.getName());
		}

		// check req
		if (!getEvent().check(player, null, -1, -1)) {
			return;
		}

		// perform
		event.performStart(player, null, -1, -1);

		// inform
		player.addInfo(new YNotificationSaver(event.getDesc(), event.getIconID()));

		// save reset?
		if (event.getLength() > 0) {
			player.getTimeEvents().add(new TimeEvent(event.getName(), event.getIconID(), event.getLength(), new EventReset(eventID, player.getId())));
		}

		// reset
		eventID = null;
		calcNewWaitTime();
	}

	/**
	 * 
	 */
	private void calcNewWaitTime() {
		wait = S.r().nextInt(12) + 3;
	}

	/**
	 * @return the event
	 */
	public BaseEvent getEvent() {
		if (event == null) {
			event = S.nData().getEv(eventID);
		}
		return event;
	}

}
package de.ninenations.player;

import com.badlogic.gdx.scenes.scene2d.ui.Button;

import de.ninenations.game.S;
import de.ninenations.ui.elements.NSplitTab;

public class PlayersOverviewTab extends NSplitTab<Player> {

	public static final String ID = "playeroverview";

	public PlayersOverviewTab() {
		super(ID, "Players", 192, "No players");

		// add players
		for (Player p : S.players().getRawAccess()) {
			if (p.getPlayerType().isSelectable()) {
				addElement(p);
			}
		}
	}

	@Override
	protected void doubleClickElement(Button btn) {}

}

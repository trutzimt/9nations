package de.ninenations.player;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

import de.ninenations.core.BaseMgmtObject;
import de.ninenations.core.NArray;
import de.ninenations.core.NN;
import de.ninenations.core.NSimpleObjectMap;
import de.ninenations.data.ki.BasePlayerType;
import de.ninenations.data.nations.BaseNation;
import de.ninenations.game.GameStartScreen;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.menu.MainMenuScreen;
import de.ninenations.player.event.EventMgmt;
import de.ninenations.player.event.TimeEventMgmt;
import de.ninenations.quests.BaseQuest;
import de.ninenations.quests.BaseQuestItem;
import de.ninenations.research.ResearchMgmt;
import de.ninenations.saveload.SaveLoadWindow;
import de.ninenations.scenarios.BaseCampaign;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.stats.Stats;
import de.ninenations.towns.Town;
import de.ninenations.ui.NDialog;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YProgressBar;
import de.ninenations.ui.elements.YRandomField;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.window.YNotificationSaver;
import de.ninenations.ui.window.YTextDialogSaver;
import de.ninenations.util.NGenerator;
import de.ninenations.util.YError;
import de.ninenations.util.YLog;
import de.ninenations.util.YMusic;

/**
 * 
 */
public class Player extends BaseMgmtObject implements Serializable {

	private static final long serialVersionUID = -8685162918974263604L;

	private NArray<YNotificationSaver> notifications;
	private NArray<YTextDialogSaver> messages;

	/**
	 * Which Nation is the player?
	 */
	private String pnation;

	/**
	 * 
	 */
	private ResearchMgmt research;
	private NArray<BaseQuest> quests;
	private EventMgmt events;
	private TimeEventMgmt timeEvents;
	private Stats stats;

	private int[] features;

	/**
	 * 
	 */
	private int maxTowns;

	/**
	 * - Fog
	 */
	private int[][][] mapData;

	/**
	 * Beziehung zu den Göttern
	 */
	// private ObjectMap<String, Integer> gods;

	private transient Image[][] fogImg;

	/**
	 * Remember active unit
	 */
	private int activeUnit;

	private boolean willLose, willWin;

	private transient BasePlayerType playerType;
	private transient BaseNation playerNation;

	/**
	 * Modificator for the ress, e.g changed by research or events
	 *
	 */
	private NSimpleObjectMap<String, Integer> ressMultiplicator;

	/**
	 * Modificator for the ress, e.g changed by research or events
	 *
	 */
	private NSimpleObjectMap<String, Integer> otherMultiplicator;

	public enum RessType {
		PRODUCE, CONSUME, BUILD, GIFT, TRADE
	}

	/**
	 * Only for loading!
	 */
	@SuppressWarnings("unused")
	private Player() {
		super();
	}

	public Player(String type, String name, int pid, String nation) {
		super(type, pid, name);
		// research;

		pnation = nation;
		research = new ResearchMgmt(pid);
		quests = new NArray<>();
		mapData = new int[EMapData.values().length][S.map().getWidth()][S.map().getHeight()];
		notifications = new NArray<>();
		messages = new NArray<>();
		stats = new Stats();
		maxTowns = 1;
		features = new int[Feature.values().length];
		ressMultiplicator = new NSimpleObjectMap<>();
		otherMultiplicator = new NSimpleObjectMap<>();
		events = new EventMgmt();
		timeEvents = new TimeEventMgmt();

		buildFog();
	}

	/**
	 * Set this feature
	 * 
	 * @param feature
	 * @param val,
	 *            1=enabled, 0=disabled
	 */
	public void setFeature(Feature f, int val) {
		features[f.ordinal()] = val;
	}

	/**
	 * get this feature
	 * 
	 * @param feature
	 * @return val, 1=enabled, 0=disabled
	 */
	public int getFeature(Feature f) {
		return features[f.ordinal()];
	}

	/**
	 * Get this feature
	 * 
	 * @param f
	 * @return
	 */
	public boolean isFeature(Feature f) {
		return features[f.ordinal()] == 1;
	}

	/**
	 * Add a new notification for the player
	 * 
	 * @param text
	 * @param icon
	 */
	@Deprecated
	public void addInfo(String text, Image icon) {
		addInfo(text, icon, id);
	}

	/**
	 * Add a new notification for the player
	 * 
	 * @param text
	 * @param icon
	 */
	public void addInfo(YNotificationSaver info) {
		YLog.log2("Info", name, info.getText());

		// show it?
		if (!getPlayerType().isHuman()) {
			return;
		}

		// add to show
		notifications.add(info);

		if (S.actPlayer() == this) {
			MapScreen.get().getGui().addToast(info);
		}
	}

	/**
	 * Add a new notification for the player
	 * 
	 * @param text
	 * @param icon
	 */
	@Deprecated
	public YNotificationSaver addInfo(String text, Image icon, int id) {
		YNotificationSaver info = new YNotificationSaver(text, icon, id);
		addInfo(info);
		return info;
	}

	/**
	 * Add a new notification for the player
	 * 
	 * @param text
	 * @param icon
	 */
	public void addMessage(YTextDialogSaver... mess) {
		YLog.log("Message", mess[0].getHeader(), mess[0].getMessage());

		// active player?
		if (S.actPlayer() == this) {
			for (YTextDialogSaver m : mess) {
				MapScreen.get().getGui().getDialog().add(m);
			}
			MapScreen.get().getGui().getDialog().show();
			return;
		}

		// add for later
		for (YTextDialogSaver m : mess) {
			messages.add(m);
		}

	}

	/**
	 * Get the map data
	 * 
	 * @param type
	 * @param x
	 * @param y
	 * @return
	 */
	public int getMapData(EMapData type, int x, int y) {
		// outer border?
		if (!MapScreen.get().getMap().isValide(x, y)) {
			return 0;
		}

		return mapData[type.typ][x][y];
	}

	/**
	 * Set the map data
	 * 
	 * @param type
	 * @param x
	 * @param y
	 * @return
	 */
	public void setMapData(EMapData type, int x, int y, int value) {

		mapData[type.typ][x][y] = value;

		// is fog?
		if (type == EMapData.ISVISIBLE && fogImg != null) {
			if (value == 1) {
				if (fogImg[x][y] != null) {
					fogImg[x][y].addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
					fogImg[x][y] = null;
				}
			} else {
				Image b = YIcons.getBuildO(0);
				b.setX(x * 32);
				b.setY(y * 32);
				b.getColor().a = 0f;
				b.addAction(Actions.fadeIn(1));
				fogImg[x][y] = b;
				S.map().getStage().addActor(b);
			}
		}
	}

	/**
	 * Check with menu will shown for this player
	 * 
	 * @return
	 */
	public Array<String> getVisibleMenu() {
		Array<String> a = new Array<>();
		a.add("overview");
		a.add("minimap");

		if (quests.size >= 1) {
			a.add("quest");
		}

		if (isFeature(Feature.RESEARCH)) {
			a.add("research");
		}
		a.add("main");

		return a;
	}

	private void buildFog() {
		// build fog?
		if (!S.isActive(ScenConf.FOG)) {
			// reset fog
			for (int x = 0; x < S.width(); x++) {
				for (int y = 0; y < S.height(); y++) {
					mapData[EMapData.ISVISIBLE.typ][x][y] = 1;
				}
			}

			return;
		}

		fogImg = new Image[S.map().getWidth()][S.height()];

		Image i = YIcons.getBuildO(0);

		// build fog
		for (int x = 0; x < fogImg.length; x++) {
			for (int y = 0; y < fogImg[0].length; y++) {
				// need a fog?
				if (getMapData(EMapData.ISVISIBLE, x, y) == 1) {
					continue;
				}

				// build it
				Image b = new Image(i.getDrawable());
				b.setX(x * 32);
				b.setY(y * 32);
				b.setScale(1.05f);
				b.setVisible(false);
				fogImg[x][y] = b;
				S.map().getStage().addActor(b);

			}
		}
	}

	// private Drawable getFogImg(int x, int y) {
	//
	// }

	/**
	 * 
	 */
	public void performAtRoundBegin() {
		YLog.log(name, "start", MapScreen.get().getData().getRound().getRoundString());

		// perform quests
		try {
			for (BaseQuest q : quests) {
				q.performAtRoundBegin(this);
			}
		} catch (Exception e) {
			YError.error(e, false);
		}

		// show infos?
		if (notifications.size > 0) {
			for (YNotificationSaver s : notifications) {
				MapScreen.get().getGui().addToast(s);
			}
		}

		// show unit
		activeUnit = -1;
		showNextUnit();

		checkWinLose();

		NN.get().getAchievements().check(this);

		// perform on mapobjects
		for (NOnMapObject obj : S.onMap().getOnMapObjByPlayer(this)) {
			obj.performAtPlayerRoundBegin();
		}

		// build fog
		if (fogImg != null) {
			for (Image[] element : fogImg) {
				for (int y = 0; y < fogImg[0].length; y++) {
					// need a fog?
					if (element[y] == null) {
						continue;
					}

					element[y].setZIndex(999);
					element[y].setVisible(true);
				}
			}
		}

		// show messages?
		if (messages.size > 0) {
			for (YTextDialogSaver s : messages) {
				MapScreen.get().getGui().getDialog().add(s);
			}
			MapScreen.get().getGui().getDialog().checkToShow();
			messages.clear();
		}

	}

	/**
	 * 
	 */
	public void performAtRoundEnd() {
		// YLog.log(name, "end", MapScreen.get().getData().getRound().getRoundString());

		notifications.clear();

		// perform on mapobjects
		for (NOnMapObject obj : S.onMap().getOnMapObjByPlayer(this)) {
			obj.performAtPlayerRoundEnd();
		}

		// build fog
		if (fogImg != null) {
			for (Image[] element : fogImg) {
				for (int y = 0; y < fogImg[0].length; y++) {
					// need a fog?
					if (element[y] == null) {
						continue;
					}

					element[y].setVisible(false);
				}
			}
		}

	}

	@Override
	public Image getIcon() {
		return getNation().getIcon();
	}

	@Override
	public YTable getInfoPanel() {
		YTable t = new YTable();
		// another player?
		if (S.actPlayer() != this) {
			t.addL("Name", name);
		} else {
			t.addL("Name", new YRandomField(name) {

				@Override
				public void saveText(String text) {
					name = text;

				}

				@Override
				protected String getRndText() {
					return NGenerator.getName(S.r().nextBoolean());
				}
			});
		}

		t.addL("Nation", getNation().getName());
		t.addL("Points", stats.get(Stats.POINTS) + "");

		if (S.actPlayer() != this) {
			t.addL("Status", "You are at war");
			return t;
		}

		t.addL("Towns", new YProgressBar(S.town().getTownsByPlayerCount(this), maxTowns));

		return t;
	}

	/**
	 * @return the quests
	 */
	public Array<BaseQuest> getQuests() {
		return quests;
	}

	@Override
	public void nextRound() {
		research.nextRound();
		stats.add(Stats.RESEARCH, research.getFinishResearchCount());
		stats.nextRound();
		stats.add(Stats.POINTS, stats.getYesterday(Stats.POINTS));
		timeEvents.nextRound(this);
		if (S.isActive(ScenConf.RANDOMEVENTS)) {
			events.nextRound(this);
		}
	}

	@Override
	public void afterLoad() {
		buildFog();

	}

	/**
	 * Cout from all towns the ress
	 * 
	 * @param type
	 * @return
	 */
	public int getRess(String type) {
		int c = 0;

		for (Town t : MapScreen.get().getData().getTowns().getTownsByPlayer(this)) {
			c += t.getRess(type);
		}

		return c;
	}

	/**
	 * @return the research
	 */
	public ResearchMgmt getResearch() {
		return research;
	}

	public void addQuest(BaseQuest q) {
		quests.add(q);
	}

	public void addQuest(BaseQuestItem q) {
		BaseQuest b = new BaseQuest("i" + quests.size, q.getName());
		b.addItem(q);
		quests.add(b);
	}

	/**
	 * Go to the next free unit
	 * 
	 * @return true > found it
	 */
	public boolean showNextUnit() {

		Array<NOnMapObject> o = MapScreen.get().getData().getOnMap().getOnMapObjByPlayer(this, EMapData.UNIT);

		for (int i = 0; i < o.size; i++) {
			NMapUnit unit = (NMapUnit) o.get((i + 1 + activeUnit) % o.size);

			// can use?
			if (unit.getAp() == 0) {
				continue;
			}

			S.map().setCenterMapView(unit.getX(), unit.getY());
			MapScreen.get().getGui().showBottomMenuFor(unit.getX(), unit.getY());
			activeUnit = i;
			return true;
		}

		// found nothing? show first unit
		if (o.size > 0) {
			NMapUnit unit = (NMapUnit) o.get((1 + activeUnit) % o.size);

			S.map().setCenterMapView(unit.getX(), unit.getY());
			MapScreen.get().getGui().showBottomMenuFor(unit.getX(), unit.getY());
			activeUnit++;
			return true;
		}

		return false;
	}

	/**
	 * @return the maxTowns
	 */
	public int getMaxTowns() {
		return maxTowns;
	}

	/**
	 * @param maxTowns
	 *            the maxTowns to set
	 */
	public void addMaxTowns(int maxTowns) {
		this.maxTowns += maxTowns;
	}

	public void destroy() {
		willLose = true;
	}

	public void checkWinLose() {

		// no town or units?
		if (S.town().getTownsByPlayerCount(this) == 0 && S.onMap().getOnMapObjByPlayer(this, EMapData.UNIT).size == 0) {
			willLose = true;
		}

		BaseScenario s = MapScreen.get().getData().getScenario();

		// has won?
		if (willWin) {
			// save it
			s.setWon();
			YMusic.winScenario();
			NN.get().getAchievements().addPoints(s.getName(), stats.get(Stats.POINTS), true);

			// next scenario?
			final StringBuilder next = new StringBuilder();
			final BaseCampaign c = s.getCamp();
			if (c.isCampaign()) {
				// find pos
				final int p = c.getScenarios().indexOf(s);
				if (p != -1 && p < c.getScenarios().size() - 1) {
					next.append(c.getScenarios().get(p + 1).getType());

				}
			}

			NDialog d = new NDialog("You have won!", "Do you want to continue playing?");
			d.add("Continue Playing", new YChangeListener() {

				@Override
				public void changedY(Actor actor) {
					willWin = false;
				}
			});
			d.add("Main Menu", new YChangeListener() {

				@Override
				public void changedY(Actor actor) {
					NN.get().switchScreen(new MainMenuScreen(false));
				}
			});
			if (next.length() > 0) {
				d.add("Next Scenario", new YChangeListener() {

					@Override
					public void changedY(Actor actor) {
						NN.get().switchScreen(new GameStartScreen(c.getScenario(next.toString())));
					}
				});
			}
			d.show(MapScreen.get().getStage());
			return;
		}

		// lose
		if (willLose) {
			YMusic.loseScenario();
			NN.get().getAchievements().addPoints(s.getName(), stats.get(Stats.POINTS), false);

			// another human player?
			int aH = 0;
			for (Player p : S.players().getRawAccess()) {
				if (p.getPlayerType().isHuman()) {
					aH++;
				}
			}

			NDialog d = new NDialog("Game ends", "You lost the game");
			if (aH == 1) {
				d.add("Main Menu", new YChangeListener() {

					@Override
					public void changedY(Actor actor) {
						NN.get().switchScreen(new MainMenuScreen(false));
					}
				});
				d.add("Load Game", new YChangeListener() {

					@Override
					public void changedY(Actor actor) {
						MapScreen.get().getStage().addActor(new SaveLoadWindow(true, false));
					}
				});
				d.add("Restart", new YChangeListener() {

					@Override
					public void changedY(Actor actor) {
						NN.get().switchScreen(new GameStartScreen(MapScreen.get().getData().getScenario()));
					}
				});
			} else

			{
				d.add("Close", new YChangeListener() {

					@Override
					public void changedY(Actor actor) {
						destroyIntern();
					}

				});
			}

			d.show(MapScreen.get().getStage());
		}
	}

	/**
	 * Destroy the player
	 */
	private void destroyIntern() {
		// has towns?
		for (Town t : S.town().getTownsByPlayer(this)) {
			t.setPid(1);
		}

		// has units?
		for (NOnMapObject o : S.onMap().getOnMapObjByPlayer(this)) {
			o.setPlayer(1);
		}

		S.players().remove(id);
	}

	/**
	 * @param willLose
	 *            the willLose to set
	 */
	public void setWillLose(boolean willLose) {
		this.willLose = willLose;
	}

	/**
	 * @param willWin
	 *            the willWin to set
	 */
	public void setWillWin(boolean willWin) {
		this.willWin = willWin;
	}

	public BasePlayerType getPlayerType() {
		if (playerType == null) {
			playerType = S.nData().getK(getType());
		}
		return playerType;
	}

	/**
	 * @return the stats
	 */
	public Stats getStats() {
		return stats;
	}

	/**
	 * @return the stats
	 */
	public BaseNation getNation() {
		if (playerNation == null) {
			playerNation = S.nData().getN(pnation);
		}
		return playerNation;
	}

	/**
	 * @return the ressMultiplicator
	 */
	public int getRessMultiplicator(RessType type, String ress) {
		return ressMultiplicator.get(type + "-" + ress, 0);
	}

	/**
	 * @param ressMultiplicator
	 *            the ressMultiplicator to set
	 */
	public void setRessMultiplicator(RessType type, String ress, int val) {
		if (val == 0) {
			ressMultiplicator.remove(type + "-" + ress);
			return;
		}

		ressMultiplicator.put(type + "-" + ress, val);
	}

	/**
	 * @return the ressMultiplicator
	 */
	public int getOtherMultiplicator(String type) {
		return otherMultiplicator.get(type, 0);
	}

	/**
	 * @param ressMultiplicator
	 *            the ressMultiplicator to set
	 */
	public void setOtherMultiplicator(String type, int val) {
		if (val == 0) {
			otherMultiplicator.remove(type);
			return;
		}

		otherMultiplicator.put(type, val);
	}

	/**
	 * @return the infos
	 */
	public NArray<YNotificationSaver> getNotifications() {
		return notifications;
	}

	/**
	 * @return the events
	 */
	public EventMgmt getEvents() {
		return events;
	}

	/**
	 * @return the timeEvents
	 */
	public TimeEventMgmt getTimeEvents() {
		return timeEvents;
	}

}
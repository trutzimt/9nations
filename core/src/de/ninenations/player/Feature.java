/**
 * 
 */
package de.ninenations.player;

import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public enum Feature {

	RESEARCH("Research", YIcons.RESEARCH);

	private final String text;
	private final int logo;

	private Feature(String text, int logo) {
		this.text = text;
		this.logo = logo;
	}

	/**
	 * @return the value
	 */
	public String get() {
		return text;
	}

	/**
	 * @return the id
	 */
	public int getLogo() {
		return logo;
	}
}

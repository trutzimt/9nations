package de.ninenations.player;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.VisTable;

import de.ninenations.core.NN;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.NTextButton;
import de.ninenations.ui.newWindow.NWindow;

public class PlayerMapPreview extends NWindow {

	public static final String ID = "mappreview";

	public PlayerMapPreview() {
		super(ID, "Minimap", YIcons.MAP);

		// find player size
		int startX = 0, startY = 0, width = S.width(), height = S.height();

		if (S.isActive(ScenConf.FOG)) {
			sX: for (int x = startX; x < width; x++) {
				for (int y = startY; y < height; y++) {
					if (S.actPlayer().getMapData(EMapData.ISVISIBLE, x, y) == 1) {
						startX = x;
						break sX;
					}
				}
			}
			sY: for (int y = startY; y < height; y++) {
				for (int x = startX; x < width; x++) {
					if (S.actPlayer().getMapData(EMapData.ISVISIBLE, x, y) == 1) {
						startY = y;
						break sY;
					}
				}
			}
			sW: for (int x = width; x > startX; x--) {
				for (int y = startY; y < height; y++) {
					if (S.actPlayer().getMapData(EMapData.ISVISIBLE, x, y) == 1) {
						width = x + 1;
						break sW;
					}
				}
			}
			sH: for (int y = height; y > startY; y--) {
				for (int x = startX; x < width; x++) {
					if (S.actPlayer().getMapData(EMapData.ISVISIBLE, x, y) == 1) {
						height = y + 1;
						break sH;
					}
				}
			}
		}

		// TODO dynamic
		TiledMap tiledMap = NN.asset().get(MapScreen.get().getData().getPS("map"), TiledMap.class);

		TiledMapTileLayer t = (TiledMapTileLayer) tiledMap.getLayers().get(0);

		// build all layers
		Image img[][] = new Image[width][height];

		// add fog?
		if (S.isActive(ScenConf.FOG)) {
			for (int x = startX; x < width; x++) {
				for (int y = startY; y < height; y++) {
					if (S.actPlayer().getMapData(EMapData.ISVISIBLE, x, y) == 0) {
						img[x][y] = YIcons.getBuildO(0);
					}
				}
			}
		}

		for (int z = tiledMap.getLayers().getCount() - 1; z >= 0; z--) {
			t = (TiledMapTileLayer) tiledMap.getLayers().get(z);
			for (int x = startX; x < width; x++) {
				for (int y = startY; y < height; y++) {
					if (img[x][y] == null && t.getCell(x, y) != null) {
						img[x][y] = new Image(t.getCell(x, y).getTile().getTextureRegion());
					}
				}
			}
		}

		// add all layers
		for (int y = height - 1; y >= startY; y--) {
			for (int x = startX; x < width; x++) {
				if (img[x][y] != null) {
					final int xx = x, yy = y;
					img[x][y].addCaptureListener(new YChangeListener(true) {

						@Override
						public void changedY(Actor actor) {
							S.map().setCenterMapView(xx, yy);

						}
					});
					add(img[x][y]);
				}
			}
			row();
		}

		// add buttons
		VisTable buttonBar = new VisTable(true);
		buttonBar.add(new NTextButton("Map zoom in", true) {

			@Override
			public void perform() {
				S.map().zoomIn();

			}
		});
		buttonBar.add(new NTextButton("Map zoom out", true) {

			@Override
			public void perform() {
				S.map().zoomOut();

			}
		});
		add(buttonBar).colspan(width - startX);

		// set size
		pack();
		setWidth(Math.min(getWidth(), Gdx.graphics.getWidth()));
		setHeight(Math.min(getHeight(), Gdx.graphics.getHeight()));
		loadPos();
	}

}

/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.data.terrain.BaseTerrain;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqRiver extends BaseReq {

	/**
	 * 
	 */
	private static final long serialVersionUID = 393050508580982407L;

	/**
	 * 
	 */
	public ReqRiver() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {

		return x > 0 && !S.map().getTerrainID(x - 1, y).equals(BaseTerrain.WATER) && S.map().getTerrainID(x, y).equals(BaseTerrain.WATER) && x + 1 < S.width()
				&& !S.map().getTerrainID(x + 1, y).equals(BaseTerrain.WATER)
				|| y > 0 && !S.map().getTerrainID(x, y - 1).equals(BaseTerrain.WATER) && S.map().getTerrainID(x, y).equals(BaseTerrain.WATER) && y + 1 < S.height()
						&& !S.map().getTerrainID(x, y + 1).equals(BaseTerrain.WATER);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return false;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return getDesc();
	}

	@Override
	public String getDesc() {
		return "Can only be build on a river";
	}
}

/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.NRound.NSeason;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqSeason extends BaseReq {

	private static final long serialVersionUID = 3691918536158620304L;

	private NSeason season;
	private boolean not;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqSeason() {}

	/**
	 * 
	 * @param unit
	 * @param building
	 * @param together
	 */
	public ReqSeason(String season, boolean not) {
		this(NSeason.valueOf(season.toUpperCase()), not);
	}

	/**
	 * 
	 * @param unit
	 * @param building
	 * @param together
	 */
	public ReqSeason(NSeason season, boolean not) {
		super();
		this.season = season;
		this.not = not;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		boolean s = MapScreen.get().getData().getRound().is(season);
		return not ? !s : s;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return false;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return getDesc();
	}

	@Override
	public String getDesc() {
		return "Needs " + (not ? "not " : "") + season.get();
	}

}

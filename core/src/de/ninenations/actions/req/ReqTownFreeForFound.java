/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqTownFreeForFound extends BaseReq {

	/**
	 * 
	 */
	private static final long serialVersionUID = 393050508580982407L;

	/**
	 * 
	 */
	public ReqTownFreeForFound() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		return MapScreen.get().getData().getTowns().getTownsByPlayerCount(player) < player.getMaxTowns();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return false;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return "Town limit reached, can not found another town.";
	}

	@Override
	public String getDesc() {
		return "Need the logistic for a new town.";
	}
}

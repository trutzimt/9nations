/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqEmptyArea extends ReqEmptyField {

	private static final long serialVersionUID = 8876404558713332415L;

	private int area;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqEmptyArea() {}

	/**
	 * 
	 * @param unit
	 * @param building
	 * @param together
	 */
	public ReqEmptyArea(int area, boolean unit, boolean building, boolean together) {
		super(unit, building, together);
		this.area = area;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {

		// check area
		for (int xA = Math.max(0, x - area); xA < Math.min(S.width(), x + area); xA++) {
			for (int yA = Math.max(0, y - area); yA < Math.min(S.height(), y + area); yA++) {
				if (!super.checkReq(player, onMap, xA, yA)) {
					return false;
				}
			}
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc() {
		return "Needs an empty area (" + area + " fields)";
	}

}

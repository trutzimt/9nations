/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.scenarios.CampaignMgmt;

/**
 * @author sven
 *
 */
public class ReqScenarioFinish extends BaseReq {

	private static final long serialVersionUID = -5658077415567878644L;

	private String camp, scen;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqScenarioFinish() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqScenarioFinish(String camp, String scen) {
		this.camp = camp;
		this.scen = scen;
	}

	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		return CampaignMgmt.getCampaign(camp).getScenario(scen).isWon();
	}

	@Override
	public boolean isFinal() {
		return true;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return getDesc();
	}

	@Override
	public String getDesc() {
		return "Win scenario " + CampaignMgmt.getCampaign(camp).getScenario(scen).getName() + " from campaign " + CampaignMgmt.getCampaign(camp).getName();
	}

}

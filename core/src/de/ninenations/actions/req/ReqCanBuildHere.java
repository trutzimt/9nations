/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.data.buildingunitbase.DataObject;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqCanBuildHere extends BaseReq {

	private static final long serialVersionUID = 8876404558713332415L;

	private EMapData mapType;
	private String uType;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqCanBuildHere() {}

	/**
	 * 
	 * @param unit
	 * @param building
	 * @param together
	 */
	public ReqCanBuildHere(EMapData mapType, String uType) {

		this.mapType = mapType;
		this.uType = uType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		DataObject d = S.nData().get(mapType, uType);
		return d.check(player, onMap, x, y) == null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return false;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		DataObject d = S.nData().get(mapType, uType);
		return d.check(player, onMap, x, y);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc() {
		return "Can build it here";
	}

}

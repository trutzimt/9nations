/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqExploredField extends BaseReq {

	private static final long serialVersionUID = -5658077415567878644L;

	private int x, y;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqExploredField() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqExploredField(int x, int y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		return player.getMapData(EMapData.ISVISIBLE, this.x, this.y) == 1;
	}

	@Override
	public boolean isFinal() {
		return false;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return getDesc();
	}

	@Override
	public String getDesc() {
		return "Explore the field " + x + "," + y;
	}

}

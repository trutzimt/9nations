/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.TextHelper;

/**
 * @author sven
 *
 */
public class ReqTerrain extends BaseReq {

	/**
	 * 
	 */
	private static final long serialVersionUID = 393050508580982407L;

	protected String[] terrains;
	private boolean near;

	/**
	 * 
	 */
	protected ReqTerrain() {}

	/**
	 * 
	 * @param unit
	 * @param building
	 * @param together
	 */
	public ReqTerrain(boolean near, String... terrain) {
		super();
		terrains = terrain;
		this.near = near;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		boolean erg = false;
		for (String terrain : terrains) {
			if (near) {
				erg = S.valide(x - 1, y) && terrain.equals(MapScreen.get().getMap().getTerrainID(x - 1, y))
						|| S.valide(x + 1, y) && terrain.equals(MapScreen.get().getMap().getTerrainID(x + 1, y))
						|| S.valide(x, y - 1) && terrain.equals(MapScreen.get().getMap().getTerrainID(x, y - 1))
						|| S.valide(x, y + 1) && terrain.equals(MapScreen.get().getMap().getTerrainID(x, y + 1));
			} else {
				erg = terrain.equals(MapScreen.get().getMap().getTerrainID(x, y));
			}
			if (erg) {
				return true;
			}
		}
		return false;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return false;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return getDesc() + " and not " + S.nData().getT(MapScreen.get().getMap().getTerrainID(x, y)).getName();
	}

	@Override
	public String getDesc() {
		return "Needs " + (near ? "near " : "") + "the terrain " + TextHelper.getCommaSepA(terrains);
	}
}

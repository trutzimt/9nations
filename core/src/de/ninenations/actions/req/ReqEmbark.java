/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.action.ActionEmbark;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqEmbark extends ReqMinMax {

	private static final long serialVersionUID = -5658077415567878644L;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqEmbark() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqEmbark(boolean max, int count) {
		super(max, count, "embarked units");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.req.ReqMinMax#getActCount(de.ninenations.game.onmap.
	 * NOnMapObject, int, int)
	 */
	@Override
	public int getActCount(Player player, NOnMapObject onMap, int x, int y) {

		return ActionEmbark.getNextEmbarkID(onMap);
	}

}

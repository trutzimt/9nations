/**
 * 
 */
package de.ninenations.actions.req;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YProgressBar;
import de.ninenations.ui.elements.YTable;

/**
 * @author sven
 *
 */
public abstract class ReqMinMax extends BaseReq {

	private static final long serialVersionUID = 9150674654123447397L;

	protected boolean max;
	protected int count;
	protected String title;

	/**
	 * 
	 */
	protected ReqMinMax() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqMinMax(boolean max, int count, String title) {
		super();
		this.max = max;
		this.count = count;
		this.title = title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		if (max) {
			return getActCount(player, onMap, x, y) <= count;
		} else {
			return getActCount(player, onMap, x, y) >= count;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return false;
	}

	/**
	 * Return the act number of items
	 * 
	 * @return
	 */
	public abstract int getActCount(Player player, NOnMapObject onMap, int x, int y);

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return "Have " + (max ? "max. " : "min. ") + getActCount(player, onMap, x, y) + "/" + count + " " + title;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc() {
		return "Have " + (max ? "max. " : "min. ") + count + " " + title;
	}

	/**
	 * Add it to the table, if not final
	 * 
	 * @param table
	 * @param player
	 * @param onMap
	 * @param x
	 * @param y
	 */
	@Override
	public void addToTable(YTable table, Player player, NOnMapObject onMap, int x, int y) {
		// skip?
		if (isFinal()) {
			return;
		}

		Image l = YIcons.getIconI(checkReq(player, onMap, x, y) ? YIcons.RIGHT : YIcons.WRONG);

		if (onMap == null && player == null) {
			table.addI(l, getDesc());
			return;
		}

		table.addI(l, new YProgressBar(getActCount(player, onMap, x, y), count, getDesc(player, onMap, x, y)));
	}

}

/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqNoBuild extends BaseReq {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8980236435544176571L;

	/**
	 * 
	 */
	public ReqNoBuild() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return true;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return getDesc();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc() {
		return "Can not build directly";
	}
}

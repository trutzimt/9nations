/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.TextHelper;

/**
 * @author sven
 *
 */
public class ReqEmptyField extends BaseReq {

	private static final long serialVersionUID = 8876404558713332415L;
	protected boolean unit, building, together;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	protected ReqEmptyField() {}

	/**
	 * 
	 * @param unit
	 * @param building
	 * @param together
	 */
	public ReqEmptyField(boolean unit, boolean building, boolean together) {
		super();
		this.unit = unit;
		this.building = building;
		this.together = together;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		boolean b = MapScreen.get().getData().isBuilding(x, y);
		boolean u = MapScreen.get().getData().isUnit(x, y);

		if (together && unit && u && building && b) {
			return false;
		}

		return !(unit && u || building && b);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return false;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return getDesc() + " (" + TextHelper.getCommaSep(unit ? "no unit" : null, building ? "no building" : null) + ")";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc() {
		return "Needs an empty field";
	}

}

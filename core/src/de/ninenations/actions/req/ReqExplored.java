/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqExplored extends ReqMinMax {

	private static final long serialVersionUID = -5658077415567878644L;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqExplored() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqExplored(boolean max, int count) {
		super(max, count, "visible fields");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.req.ReqMinMax#getActCount(de.ninenations.game.onmap.
	 * NOnMapObject, int, int)
	 */
	@Override
	public int getActCount(Player player, NOnMapObject onMap, int x, int y) {
		int c = 0;

		for (int mx = 0; mx < S.map().getWidth(); mx++) {
			for (int my = 0; my < S.map().getHeight(); my++) {
				if (player.getMapData(EMapData.ISVISIBLE, mx, my) == 1) {
					c++;
				}
			}
		}

		return c;
	}

}

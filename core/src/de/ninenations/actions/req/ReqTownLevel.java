/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.towns.Town;

/**
 * @author sven
 *
 */
public class ReqTownLevel extends ReqMinMax {

	private static final long serialVersionUID = -5658077415567878644L;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqTownLevel() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqTownLevel(boolean max, int count) {
		super(max, count, "Town Level");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.req.ReqMinMax#getActCount(de.ninenations.game.onmap.
	 * NOnMapObject, int, int)
	 */
	@Override
	public int getActCount(Player player, NOnMapObject onMap, int x, int y) {
		if (x == -1 && y == -1) {
			// find any town
			int l = 0;
			for (Town t : S.town().getTownsByPlayer(player)) {
				if (t.getLevel() > l) {
					l = t.getLevel();
				}
			}
			return l;
		}

		return S.town().getNearstTown(player, x, y, false).getLevel();
	}

}

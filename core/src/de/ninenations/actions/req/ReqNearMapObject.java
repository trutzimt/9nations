/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqNearMapObject extends BaseReq {

	private static final long serialVersionUID = 8876404558713332415L;
	protected boolean unit;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqNearMapObject() {}

	/**
	 * 
	 * @param unit
	 * @param building
	 * @param together
	 */
	public ReqNearMapObject(boolean unit) {
		super();
		this.unit = unit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {

		if (unit) {
			return MapScreen.get().getData().isUnit(x, y) && S.unit(x, y) != onMap || MapScreen.get().getData().isUnit(x - 1, y)
					|| MapScreen.get().getData().isUnit(x + 1, y) || MapScreen.get().getData().isUnit(x, y - 1) || MapScreen.get().getData().isUnit(x, y + 1);
		}

		return MapScreen.get().getData().isBuilding(x, y) && S.build(x, y) != onMap || MapScreen.get().getData().isBuilding(x - 1, y)
				|| MapScreen.get().getData().isBuilding(x + 1, y) || MapScreen.get().getData().isBuilding(x, y - 1)
				|| MapScreen.get().getData().isBuilding(x, y + 1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return false;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return getDesc();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc() {
		return unit ? "Needs a unit nearby" : "Needs a building nearby";
	}

}

/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqPlayer extends ReqMinMax {

	private static final long serialVersionUID = -4367130691955836476L;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqPlayer() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqPlayer(boolean max, int count) {
		super(max, count, "Player");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.req.ReqMinMax#getActCount(de.ninenations.game.onmap.
	 * NOnMapObject, int, int)
	 */
	@Override
	public int getActCount(Player player, NOnMapObject onMap, int x, int y) {
		return MapScreen.get().getData().getPlayers().getSize() - 1;
	}

}

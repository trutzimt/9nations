/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqPlayerRess extends ReqMinMax {

	private static final long serialVersionUID = -5658077415567878644L;

	private String type;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqPlayerRess() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqPlayerRess(boolean max, String type, int count) {
		super(max, count, S.nData().getR(type).getName());

		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.req.ReqMinMax#getActCount(de.ninenations.game.onmap.
	 * NOnMapObject, int, int)
	 */
	@Override
	public int getActCount(Player player, NOnMapObject onMap, int x, int y) {
		return player.getRess(type);
	}

}

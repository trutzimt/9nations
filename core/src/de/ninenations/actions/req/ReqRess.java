/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqRess extends ReqMinMax {

	private static final long serialVersionUID = -5658077415567878644L;

	private String type;
	private boolean total;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqRess() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqRess(boolean max, String type, int count) {
		this(max, type, count, false);
	}

	/**
	 * 
	 * @param max
	 * @param type
	 * @param count
	 * @param total,
	 *            check for the player in total
	 */
	public ReqRess(boolean max, String type, int count, boolean total) {
		super(max, count, null);

		this.type = type;
		this.total = total;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		if (title == null) {
			title = S.nData().getR(type).getName();
		}
		return super.getDesc(player, onMap, x, y);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc() {
		if (title == null) {
			title = S.nData().getR(type).getName();
		}
		return super.getDesc();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.req.ReqMinMax#getActCount(de.ninenations.game.onmap.
	 * NOnMapObject, int, int)
	 */
	@Override
	public int getActCount(Player player, NOnMapObject onMap, int x, int y) {
		return total ? player.getRess(type) : onMap.getTown().getRess(type);
	}

}

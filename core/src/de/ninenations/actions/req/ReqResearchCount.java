/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqResearchCount extends ReqMinMax {

	private static final long serialVersionUID = -5658077415567878644L;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqResearchCount() {}

	/**
	 * 
	 * @param max
	 * @param type
	 * @param count
	 * @param total,
	 *            check for the player in total
	 */
	public ReqResearchCount(boolean max, int count) {
		super(max, count, "Research");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.req.ReqMinMax#getActCount(de.ninenations.game.onmap.
	 * NOnMapObject, int, int)
	 */
	@Override
	public int getActCount(Player player, NOnMapObject onMap, int x, int y) {
		return player.getResearch().getFinishResearchCount();
	}

}

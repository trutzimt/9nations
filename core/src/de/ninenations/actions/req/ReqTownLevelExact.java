/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.towns.Town;

/**
 * @author sven
 *
 */
public class ReqTownLevelExact extends ReqMinMax {

	private static final long serialVersionUID = -5658077415567878644L;

	private int level;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqTownLevelExact() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqTownLevelExact(boolean max, int level, int count) {
		super(max, count, Town.getLevelName(level));

		this.level = level;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.req.ReqMinMax#getActCount(de.ninenations.game.onmap.
	 * NOnMapObject, int, int)
	 */
	@Override
	public int getActCount(Player player, NOnMapObject onMap, int x, int y) {
		// find any town
		int l = 0;
		for (Town t : S.town().getTownsByPlayer(player)) {
			if (t.getLevel() == level) {
				l++;
			}
		}
		return l;
	}

}

/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.TextHelper;

/**
 * @author sven
 *
 */
public class ReqTerrainNot extends ReqTerrain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 393050508580982407L;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqTerrainNot() {}

	/**
	 * 
	 * @param unit
	 * @param building
	 * @param together
	 */
	public ReqTerrainNot(String... terrain) {
		super(false, terrain);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		for (String terrain : terrains) {
			if (!terrain.equals(MapScreen.get().getMap().getTerrainID(x, y))) {
				return true;
			}
		}
		return false;

	}

	@Override
	public String getDesc() {
		return "Needs not the terrain " + TextHelper.getCommaSepA(terrains);
	}
}

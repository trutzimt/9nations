/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.towns.Town;

/**
 * @author sven
 *
 */
public class ReqTownOwnInRadius extends BaseReq {

	/**
	 * 
	 */
	private static final long serialVersionUID = 393050508580982407L;

	/**
	 * 
	 */
	public ReqTownOwnInRadius() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		// check the town
		Town t = S.town().getNearstTown(player, x, y, false);
		if (t == null) {
			return false;
		}

		return Math.abs(t.getX() - x) + Math.abs(t.getY() - y) <= t.getMaxRange() && t.getActBuildingCount() < t.getMaxBuildingCount();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return false;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		Town t = S.town().getNearstTown(player, x, y, false);

		if (t == null) {
			return getDesc();
		}

		int diff = Math.abs(t.getX() - x) + Math.abs(t.getY() - y) - t.getMaxRange();
		if (diff > 0) {
			return diff + " fields to far away from " + t.getName();
		}

		return "Near " + t.getName() + " and the building limit is " + t.getActBuildingCount() + " from " + t.getMaxBuildingCount();
	}

	@Override
	public String getDesc() {
		return "Need to build near a town and need place in the town";
	}
}

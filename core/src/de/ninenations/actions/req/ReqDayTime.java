/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.NRound;
import de.ninenations.game.NRound.NDaytime;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.TextHelper;

/**
 * @author sven
 *
 */
public class ReqDayTime extends BaseReq {

	private static final long serialVersionUID = 4710442112284067548L;

	protected boolean morning, evening, night;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqDayTime() {}

	/**
	 * 
	 * @param morning
	 * @param evening
	 * @param night
	 */
	public ReqDayTime(String time) {
		this(NDaytime.valueOf(time.toUpperCase()));
	}

	/**
	 * 
	 * @param morning
	 * @param evening
	 * @param night
	 */
	public ReqDayTime(NDaytime time) {
		super();
		morning = time == NDaytime.MORNING;
		evening = time == NDaytime.EVENING;
		night = time == NDaytime.NIGHT;
	}

	/**
	 * 
	 * @param morning
	 * @param evening
	 * @param night
	 */
	public ReqDayTime(boolean morning, boolean evening, boolean night) {
		super();
		this.morning = morning;
		this.evening = evening;
		this.night = night;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		NRound r = MapScreen.get().getData().getRound();
		return morning && r.is(NDaytime.MORNING) || evening && r.is(NDaytime.EVENING) || night && r.is(NDaytime.NIGHT);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return false;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return getDesc();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc() {
		return "Needs "
				+ TextHelper.getCommaSep(morning ? NDaytime.MORNING.get() : null, evening ? NDaytime.EVENING.get() : null, night ? NDaytime.NIGHT.get() : null);
	}

}

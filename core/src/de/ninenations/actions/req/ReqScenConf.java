/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqScenConf extends BaseReq {

	private static final long serialVersionUID = -5658077415567878644L;

	private ScenConf conf;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqScenConf() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqScenConf(ScenConf conf) {
		this.conf = conf;
	}

	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {
		return S.isActive(conf);
	}

	@Override
	public boolean isFinal() {
		return true;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return getDesc();
	}

	@Override
	public String getDesc() {
		return "Disabled in this game";
	}

}

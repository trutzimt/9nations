/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqAp extends ReqMinMax {

	private static final long serialVersionUID = -5658077415567878644L;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqAp() {}

	/**
	 * @param max
	 * @param count
	 */
	public ReqAp(boolean max, int count) {
		super(max, count, "AP");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.req.ReqMinMax#getActCount(de.ninenations.game.onmap.
	 * NOnMapObject, int, int)
	 */
	@Override
	public int getActCount(Player player, NOnMapObject onMap, int x, int y) {
		return onMap.getAp();
	}

}

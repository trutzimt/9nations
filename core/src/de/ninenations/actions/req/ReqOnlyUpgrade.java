/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqOnlyUpgrade extends BaseReq {

	private static final long serialVersionUID = 8876404558713332415L;
	protected boolean unit;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqOnlyUpgrade() {}

	/**
	 * 
	 * @param unit
	 *            is unit?
	 */
	public ReqOnlyUpgrade(boolean unit) {
		super();
		this.unit = unit;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.game.quest.req.IQuestRequirement#checkReq(de.qossire.yaams.
	 * screens.map.MapScreen)
	 */
	@Override
	public boolean checkReq(Player player, NOnMapObject onMap, int x, int y) {

		if (unit) {
			return MapScreen.get().getData().isUnit(x, y);
		}
		return MapScreen.get().getData().isBuilding(x, y);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#isFinal()
	 */
	@Override
	public boolean isFinal() {
		return true;
	}

	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		return getDesc();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc() {
		return "Can only be upgraded and not build directly";
	}

}

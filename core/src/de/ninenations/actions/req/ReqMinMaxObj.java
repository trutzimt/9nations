/**
 * 
 */
package de.ninenations.actions.req;

import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class ReqMinMaxObj extends ReqMinMax {

	private static final long serialVersionUID = -5658077415567878644L;

	private EMapData mapType;
	private String type;
	private boolean kingdom;

	/**
	 * 
	 */
	@SuppressWarnings("unused")
	private ReqMinMaxObj() {}

	/**
	 * 
	 * @param max
	 * @param count
	 * @param mapType
	 * @param type
	 * @param kingdom,
	 *            true > whole kingdom, false > only town
	 */
	public ReqMinMaxObj(boolean max, int count, EMapData mapType, String type, boolean kingdom) {
		super(max, count, null);

		this.mapType = mapType;
		this.type = type;
		this.kingdom = kingdom;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.req.ReqMinMax#getActCount(de.ninenations.game.onmap.
	 * NOnMapObject, int, int)
	 */
	@Override
	public int getActCount(Player player, NOnMapObject onMap, int x, int y) {
		if (kingdom) {
			return S.onMap().countOnMapObjByPlayerType(player, mapType, type);
		}

		if (onMap == null || onMap.getTown() == null) {
			return 0;
		}

		return S.onMap().countOnMapObjByTownType(onMap.getTown(), mapType, type);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc() {
		if (title == null) {
			title = S.nData().get(mapType, type).getName();
		}

		return super.getDesc();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.actions.base.BaseReq#getDesc()
	 */
	@Override
	public String getDesc(Player player, NOnMapObject onMap, int x, int y) {
		if (title == null) {
			title = S.nData().get(mapType, type).getName();
		}

		return super.getDesc(player, onMap, x, y);
	}

}

/**
 * 
 */
package de.ninenations.actions.base;

/**
 * @author sven
 *
 */
public interface IReq<T> {

	/**
	 * Add a new req
	 * 
	 * @param req
	 * @return
	 */
	public T addReq(BaseReq req);
}

package de.ninenations.actions.base;

import java.io.Serializable;

import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;

/**
 * Voraussetzungen für irgendwas
 */
public abstract class BaseReq implements Serializable {

	private static final long serialVersionUID = -4575431665018748318L;

	/**
	 * Default constructor
	 */
	public BaseReq() {}

	/**
	 * 
	 * @param player
	 * @param onMap
	 * @param x
	 * @param y
	 * @return true > can use, false otherwise
	 */
	public abstract boolean checkReq(Player player, NOnMapObject onMap, int x, int y);

	/**
	 * Angabe, ob es eine temporäre einschränkung ist, oder finale. Wichtig zum
	 * Ausblenden.
	 * 
	 * @return
	 */
	public abstract boolean isFinal();

	/**
	 * @return
	 */
	public abstract String getDesc(Player player, NOnMapObject onMap, int x, int y);

	/**
	 * @return
	 */
	public abstract String getDesc();

	/**
	 * Add it to the table, if not final
	 * 
	 * @param table
	 * @param player
	 * @param onMap
	 * @param x
	 * @param y
	 */
	public void addToTable(YTable table, Player player, NOnMapObject onMap, int x, int y) {
		// skip?
		if (isFinal()) {
			return;
		}

		table.addI(YIcons.getIconI(checkReq(player, onMap, x, y) ? YIcons.RIGHT : YIcons.WRONG), getDesc(player, onMap, x, y));
	}

}
package de.ninenations.actions.base;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.widget.VisImageTextButton;

import de.ninenations.core.NArray;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.NDialog;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.YSounds;

/**
 * 
 */
public abstract class GAction extends BaseDisplay implements Serializable, IReq<GAction> {

	private static final long serialVersionUID = -4234192211422748697L;

	/**
	 * 
	 */
	protected NArray<BaseReq> reqs;

	/**
	 * Save the ap cost
	 */
	protected int ap;

	protected boolean preUsing, hiddenLexicon;

	protected GAction() {
		this(null, null);
	}

	public GAction(String type, String name) {
		super(type, name);

		reqs = new NArray<>();
	}

	/**
	 * Add a new req
	 * 
	 * @param req
	 * @return
	 */
	@Override
	public GAction addReq(BaseReq req) {
		reqs.add(req);
		return this;
	}

	/**
	 * @param obj
	 */
	public void performAtBuild(NOnMapObject obj) {}

	/**
	 * @param obj
	 */
	public void performAtRoundBegin(NOnMapObject obj) {
		canPerform(obj);

	}

	/**
	 * @param obj
	 */
	protected boolean canPerform(NOnMapObject obj) {
		// check pref
		for (BaseReq req : reqs) {
			if (!req.checkReq(obj.getPlayer(), obj, obj.getX(), obj.getY())) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @param player
	 * @param onMap
	 * @param x
	 * @param y
	 * @return true > update menu
	 */
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		if (ap > 0 && onMap != null) {
			if (ap > onMap.getAp() + onMap.getActionWaitAP()) {
				YSounds.pBuzzer();
				Dialogs.showOKDialog(MapScreen.get().getStage(), getName() + " has not enough AP",
						name + " need " + (ap - onMap.getAp() - onMap.getActionWaitAP()) + " AP, please wait.");
				return false;
			} else {
				// remove ap
				onMap.addAp(-ap + onMap.getActionWaitAP());
				onMap.setActionWait(null);
				return true;
			}

		}

		return true;
	}

	/**
	 * @param obj
	 */
	public void performAtDestroy(NOnMapObject obj) {}

	/**
	 * @param obj
	 */
	public void destroyActive() {}

	/**
	 * @param obj
	 */
	public void performAtPoint(int x, int y) {
		// TODO implement here
	}

	/**
	 * @param obj
	 */
	public void moveMouseAtPoint(int x, int y) {
		// TODO implement here
	}

	/**
	 * @return
	 */
	@Override
	public YTable getInfoPanel() {
		return getInfoPanel(null);
	}

	/**
	 * @return
	 */
	public YTable getInfoPanel(NOnMapObject obj) {
		YTable t = new YTable();
		t.addH("General");
		t.addI(getIcon(), name);
		t.addL("Cost", ap + " AP");

		if (reqs.size > 0) {
			t.addH("Requirements");
			for (BaseReq r : reqs) {
				if (obj != null) {
					r.addToTable(t, obj.getPlayer(), obj, obj.getX(), obj.getY());
				} else {
					t.addL(null, r.getDesc());
				}
			}
		}

		return t;
	}

	/**
	 * Generate the button, if possible
	 * 
	 * @param actPlayer
	 * @param unit
	 * @param x
	 * @param y
	 * @return
	 */
	public VisImageTextButton getActionButton(final Player player, final NOnMapObject onMap, final int x, final int y) {
		// not finish?
		if (!onMap.isFinish() && !preUsing) {
			return null;
		}

		// check if can show button?
		if (reqs.size > 0) {
			boolean fin = false;
			String err = null;

			for (BaseReq r : reqs) {

				if (!r.checkReq(player, onMap, x, y)) {
					if (r.isFinal()) {
						fin = true;
						continue;
					}

					err = "Error:" + r.getDesc(player, onMap, x, y);

				}
			}

			if (err != null) {
				if (fin) {
					return null;
				}
				final String e = err;

				// build disable button
				VisImageTextButton btn = new VisImageTextButton(getName() + ":" + e, getIcon().getDrawable());
				btn.getColor().a = 0.5f;
				btn.addCaptureListener(new YChangeListener() {

					@Override
					public void changedY(Actor actor) {
						Dialogs.showOKDialog(MapScreen.get().getStage(), getName(), e);

					}
				});

				return btn;
			}
		}

		// build action button
		VisImageTextButton btn = new VisImageTextButton(getName(), getIcon().getDrawable());
		// not enough ap?
		if (ap > onMap.getAp()) {
			btn.addCaptureListener(new YChangeListener() {

				@Override
				public void changedY(Actor actor) {
					NDialog d = new NDialog("Not enough AP", onMap.getName() + " has not enough AP at the moment.\n" + onMap.getName() + " can work for "
							+ (ap / onMap.getDataObject().getAp() + 1) + " rounds on it.");
					d.add("Work on " + getName(), new YChangeListener(true) {

						@Override
						public void changedY(Actor actor) {
							onMap.setActionWait(getType());
							MapScreen.get().getGui().showBottomMenuFor(x, y);

						}
					});
					d.addCancel();
					d.show(MapScreen.get().getStage());

				}
			});
		}

		btn.addCaptureListener(new YChangeListener() {

			@Override
			public void changedY(Actor actor) {

				if (ap > onMap.getAp()) {

				} else

				if (perform(player, onMap, x, y)) {
					MapScreen.get().getGui().showBottomMenuFor(x, y);
				}

			}
		});

		return btn;
	}

	/**
	 * @return the reqs
	 */
	public Array<BaseReq> getReqs() {
		return reqs;
	}

	/**
	 * @return the ap
	 */
	public int getAp() {
		return ap;
	}

	/**
	 * @param ap
	 *            the ap to set
	 */
	public void setAp(int ap) {
		this.ap = ap;
	}

	/**
	 * @return the preUsing
	 */
	public boolean isPreUsing() {
		return preUsing;
	}

	/**
	 * @return the hiddenLexicon
	 */
	public boolean isHiddenLexicon() {
		return hiddenLexicon;
	}

	public void addToTable(YTable t) {
		t.addI(getIcon(), getName());

	}

}
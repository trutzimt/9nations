/**
 * 
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.ObjectMap;

import de.ninenations.actions.base.GAction;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.player.Player.RessType;
import de.ninenations.towns.Town;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;
import de.ninenations.util.NGenerator;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionProduceRandom extends GAction {

	private static final long serialVersionUID = -7750380200493574735L;

	private ObjectMap<String, Integer> cost;
	private float value;
	private boolean worked;

	@SuppressWarnings("unused")
	private ActionProduceRandom() {}

	/**
	 * @param type
	 * @param name
	 */
	public ActionProduceRandom(Object[] objects) {
		super("alchemy", "Try with alchemy");

		cost = new ObjectMap<>();
		value = -1;
		setAp(5);

		// convert
		if (objects != null) {
			for (int i = 0; i + 1 < objects.length; i += 2) {
				cost.put((String) objects[i], Integer.parseInt(objects[i + 1].toString()));
			}
		}

	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		if (!super.perform(player, onMap, x, y)) {
			return true;
		}

		YSounds.pClick();
		worked = false;
		performAtRoundBegin(onMap);
		if (!worked) {
			onMap.getPlayer().addInfo(new YNotificationSaver(onMap.getName() + " tried with alchemy, but nothing happend.", getIcon()));

		}

		onMap.setAp(0);
		return true;
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtRoundBegin(NOnMapObject obj) {

		// produce it
		if (!canPerform(obj)) {
			return;
		}

		Town t = obj.getTown();
		if (t == null) {
			return;
		}

		// convert
		if (value == -1) {
			for (String ress : cost.keys()) {
				value += S.nData().getR(ress).getMarketCost() * cost.get(ress);
			}
		}

		// remove it
		for (String ress : cost.keys()) {
			if (t.getRess(ress) < cost.get(ress)) {
				// inform
				obj.setError(S.nData().getR(ress).getName() + " is missing " + (cost.get(ress) - t.getRess(ress)) + "times.", true);

				return;
			}
		}

		// cost
		for (String ress : cost.keys()) {
			t.addRess(ress, -cost.get(ress), RessType.CONSUME);
		}

		// produce?
		if (S.r().nextBoolean()) {
			// select ress
			String ress = getRess();
			BaseRess r = S.nData().getR(ress);

			// calc value
			int val = NGenerator.getIntBetween(0, (int) value * 2);
			val = (int) (val / r.getMarketCost());

			if (val == 0) {
				return;
			}

			// add it
			t.addRess(ress, val, RessType.PRODUCE);
			obj.getPlayer().addInfo(new YNotificationSaver(obj.getName() + "'s alchemy works: " + val + "x " + r.getName(), obj.getIcon()));
			worked = true;
		}

	}

	private String getRess() {
		// TODO
		for (int i = 0; i < 1000; i++) {
			String ress = S.nData().getRess().random();
			BaseRess r = S.nData().getR(ress);
			if (r.getMarketCost() > 0) {
				return ress;
			}
		}

		return S.nData().getRess().get(0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(71);
	}

	/**
	 * @param value
	 *            the value to set
	 */
	public ActionProduceRandom setValue(float value) {
		this.value = value;
		return this;
	}

}

/**
 * 
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public class ActionPlayerOtherMulti extends GAction {

	private static final long serialVersionUID = -7750380200493574735L;
	private int count;
	private String type;

	@SuppressWarnings("unused")
	private ActionPlayerOtherMulti() {
		super(null, null);
	}

	/**
	 * 
	 * @param type
	 * @param ress
	 * @param count
	 */
	public ActionPlayerOtherMulti(String type, int count) {
		super("playerOtherMulti", count + "% " + type + " for player");

		this.type = type;
		this.count = count;
	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		player.setOtherMultiplicator(type, count);
		return false;
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtDestroy(NOnMapObject obj) {
		obj.getPlayer().setOtherMultiplicator(type, -count);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.RESSOURCE);
	}

}

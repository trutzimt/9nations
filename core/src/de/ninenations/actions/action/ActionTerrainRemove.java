package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.actions.req.ReqEmptyField;
import de.ninenations.actions.req.ReqTerrain;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;

public class ActionTerrainRemove extends GAction {

	private static final long serialVersionUID = -6246976658839158335L;

	private String terrain;

	@SuppressWarnings("unused")
	private ActionTerrainRemove() {}

	public ActionTerrainRemove(String terrain) {
		super("terrain", "Remove the terrain");
		this.terrain = terrain;

		setAp(10);
		addReq(new ReqTerrain(false, terrain));
		addReq(new ReqEmptyField(false, true, false));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.map.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		if (!super.perform(player, onMap, x, y)) {
			return false;
		}
		// play sound
		S.nData().getT(terrain).playSound();

		// set it
		S.map().getAutotile().removeTile(x, y, terrain);

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return S.nData().getT(terrain).getIcon();
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return name + " " + S.nData().getT(terrain).getName();
	}

}

/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.NDialog;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionDegrade extends GAction {

	private static final long serialVersionUID = 8436672748638279728L;

	private EMapData mapType;
	private String uType;

	@SuppressWarnings("unused")
	private ActionDegrade() {}

	/**
	 * Create it
	 */
	public ActionDegrade(EMapData mapType, String uType) {
		super("degrade", "Degrade");

		this.mapType = mapType;
		this.uType = uType;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, final NOnMapObject onMap, final int x, final int y) {
		YSounds.pClick();

		// ask
		NDialog d = new NDialog("Degrade " + onMap.getName() + "?",
				"Do you really want to degrade " + onMap.getName() + " to " + S.nData().get(mapType, uType).getName() + "?");
		d.addCancel();
		d.add("Degrade " + onMap.getName(), new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				YSounds.pBuzzer();
				onMap.degradeTo(uType);
				MapScreen.get().getGui().showBottomMenuFor(x, y);

			}
		});
		d.show(MapScreen.get().getStage());

		return true;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.DEATH);
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return name + " to " + S.nData().get(mapType, uType).getName();
	}

}

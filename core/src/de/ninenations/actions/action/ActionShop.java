/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.data.ress.MarketWindow;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionShop extends GAction {

	private static final long serialVersionUID = 8436672748638279728L;

	/**
	 * Create it
	 */
	public ActionShop() {
		super("shop", "Buy and sell ressources");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		YSounds.play(YSounds.BUY);
		// show build window
		MapScreen.get().getStage().addActor(new MarketWindow(onMap.getTown()));

		return true;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.SHOP);
	}
}

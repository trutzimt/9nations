/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public class ActionResearchFinish extends GAction {

	private static final long serialVersionUID = 8436672748638279728L;

	/**
	 * Create it
	 */
	public ActionResearchFinish() {
		super("researchFinish", "Finish the actual research");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		player.getResearch().setCurrentCost(0);
		player.getResearch().nextRound(); // call it, to finish it, just in case
		return true;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.RESEARCH);
	}
}

/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.actions.req.ReqCanBuildHere;
import de.ninenations.actions.req.ReqResearch;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.elements.YTextButton;
import de.ninenations.ui.window.YAWindow;
import de.ninenations.ui.window.YWindow;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionUpgrade extends GAction {

	private static final long serialVersionUID = 8436672748638279728L;

	private EMapData mapType;
	private String uType;

	@SuppressWarnings("unused")
	private ActionUpgrade() {}

	/**
	 * Create it
	 */
	public ActionUpgrade(EMapData mapType, String uType) {
		super("upgrade", "Upgrade");

		this.mapType = mapType;
		this.uType = uType;

		// has a research?
		// Skip it, game is not finish loaded
		if (MapScreen.get() == null) {
			return;
		}

		// exist? skip it
		if (S.nData().existRS(uType)) {
			addReq(new ReqResearch(uType));
		}

		addReq(new ReqCanBuildHere(mapType, uType));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(final Player player, final NOnMapObject onMap, final int x, final int y) {
		YSounds.pClick();

		MapScreen.get().getStage().addActor(new YAWindow(getName()) {

			@Override
			public void create() {
				addTitleIcon(getIcon());

				// add content
				add(S.nData().get(mapType, uType).getInfoPanel(player, onMap, x, y).createScrollPane()).grow().row();

				add(new YTextButton(ActionUpgrade.this.getName(), true) {

					@Override
					public void perform() {
						// upgrade it
						onMap.upgradeTo(uType);
						MapScreen.get().getGui().showBottomMenuFor(x, y);
						close();

					}
				});

				setWidth(Gdx.graphics.getWidth() / 2);
				setHeight(Gdx.graphics.getHeight() / 2);

			}

		});

		return false;
	}

	@Override
	public Image getIcon() {
		return S.nData().get(mapType, uType).getIcon();
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return name + " to " + S.nData().get(mapType, uType).getName();
	}

	class UpgradeWindow extends YWindow {

		public UpgradeWindow(String title) {
			super(title);

		}

	}
}

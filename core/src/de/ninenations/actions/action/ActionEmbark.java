/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.req.ReqAp;
import de.ninenations.actions.req.ReqEmbark;
import de.ninenations.actions.req.ReqNearMapObject;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public class ActionEmbark extends ActionActive {

	private static final long serialVersionUID = 8436672748638279728L;

	/**
	 * Create it
	 */
	@SuppressWarnings("unused")
	private ActionEmbark() {}

	/**
	 * Create it
	 */
	public ActionEmbark(int max) {
		super("embark", "Embark");

		addReq(new ReqAp(false, 5));
		addReq(new ReqNearMapObject(true));
		addReq(new ReqEmbark(true, max));
	}

	@Override
	protected void generateField(NOnMapObject onMap, int x, int y) {
		showHere(onMap, x, y);
		showHere(onMap, x - 1, y);
		showHere(onMap, x, y - 1);
		showHere(onMap, x + 1, y);
		showHere(onMap, x, y + 1);

	}

	/**
	 * Helpermethod to colourfull the area
	 *
	 * @param x
	 * @param y
	 * @param map
	 * @param ActionPointLeft
	 * @return
	 */
	private void showHere(NOnMapObject onMap, int x, int y) {

		// has something to destroy?

		NOnMapObject o = S.unit(x, y);
		if (o != null && o != onMap) {
			generateImg(onMap, x, y);

		}

	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtDestroy(NOnMapObject obj) {
		// destroy all units
		int id = 0;
		while (obj.getData().containsKey(key(id))) {
			MapScreen.get().getData().getOnMap().get(Integer.parseInt(obj.getData().get(ActionEmbark.key(id)))).destroy();
			id++;

		}
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.EMBARK);
	}

	@Override
	protected void performOnMap(NOnMapObject onMap, int x, int y) {

		// remove
		super.performOnMap(onMap, x, y);

		// hide
		NMapUnit o = S.unit(x, y);
		o.hide();
		o.setDir(onMap.getX(), onMap.getY());
		o.getActor().addAction(Actions.moveTo(onMap.getX() * 32, onMap.getY() * 32, 1));

		onMap.getData().put(key(getNextEmbarkID(onMap)), Integer.toString(o.getId()));
		onMap.addAp(-5);

	}

	public static String key(int id) {
		return "embark." + id;
	}

	public static int getNextEmbarkID(NOnMapObject onMap) {
		int id = 0;
		while (onMap.getData().containsKey(key(id))) {
			id++;
		}

		return id;
	}
}

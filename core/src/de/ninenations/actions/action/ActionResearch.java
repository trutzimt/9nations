/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionResearch extends GAction {

	private static final long serialVersionUID = 8436672748638279728L;
	private String research;
	private boolean finish;

	/**
	 * Create it
	 */
	@SuppressWarnings("unused")
	private ActionResearch() {
		super();
	}

	/**
	 * Create it
	 */
	public ActionResearch(String research, boolean finish) {
		super("research", "Research it");

		this.research = research;
		this.finish = finish;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		YSounds.pClick();
		// show build window
		player.getResearch().setResearch(research, finish);
		return true;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.RESEARCH);
	}
}

/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.actions.req.ReqSpecial;
import de.ninenations.core.NN;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.NDialog;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionDestroyTown extends GAction {

	private static final long serialVersionUID = 8655142436817556579L;

	/**
	 * @param type
	 * @param x
	 * @param y
	 */
	public ActionDestroyTown() {
		super("destroyTown", "Destroy town");
	}

	@Override
	public boolean perform(Player player, final NOnMapObject onMap, final int x, final int y) {
		YSounds.pClick();
		// ask
		String t = onMap.getTown().getName();
		NDialog d = new NDialog("Destroy the town " + t + "?", "Do you really want to destroy the town " + t + " and all buildings?");
		d.addCancel();
		d.add("Destroy the town " + t, new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				// enable ach?
				if (onMap.getTown().getLevel() >= 3 && !NN.ach().getAchievement("destroytown").isFinish()) {
					((ReqSpecial) NN.ach().getAchievement("destroytown").getReq().get(0)).setFinish(true);
				}

				YSounds.pBuzzer();
				onMap.getTown().destroy();
				MapScreen.get().getGui().showBottomMenuFor(x, y);

			}
		});
		d.show(MapScreen.get().getStage());

		return false;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.DEATH);
	}
}

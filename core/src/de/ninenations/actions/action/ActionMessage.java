/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YTextDialogSaver;

/**
 * @author sven
 *
 */
public class ActionMessage extends GAction {

	private static final long serialVersionUID = 8436672748638279728L;

	private YTextDialogSaver[] message;

	/**
	 * Create it
	 */
	@SuppressWarnings("unused")
	private ActionMessage() {
		super();
	}

	/**
	 * 
	 * @param include
	 *            true=only the selected unit will produce, false=all except the
	 *            selected units
	 * @param units
	 */
	public ActionMessage(YTextDialogSaver... message) {
		super("message", "Show Message");

		this.message = message;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		// show message
		player.addMessage(message);
		return false;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.MESSAGE);
	}
}

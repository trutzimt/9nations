/**
 * 
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public class ActionUpgradeStorage extends GAction {

	private static final long serialVersionUID = -7750380200493574735L;
	private int capacity;

	/**
	 * @param type
	 * @param name
	 */
	@SuppressWarnings("unused")
	private ActionUpgradeStorage() {}

	/**
	 * @param type
	 * @param name
	 */
	public ActionUpgradeStorage(int capacity) {
		super("upgradeStorageCapacity", "Upgrade storage capacity with " + capacity);

		this.capacity = capacity;
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtBuild(NOnMapObject obj) {
		obj.getTown().setMaxStorage(obj.getTown().getMaxStorage() + capacity);
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtDestroy(NOnMapObject obj) {
		obj.getTown().setMaxStorage(obj.getTown().getMaxStorage() - capacity);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.RESSOURCE);
	}

}

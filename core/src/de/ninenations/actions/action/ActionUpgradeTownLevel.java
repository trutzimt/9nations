/**
 * 
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.towns.Town;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

/**
 * @author sven
 *
 */
public class ActionUpgradeTownLevel extends GAction {

	private static final long serialVersionUID = -7750380200493574735L;
	private int count;

	/**
	 * @param type
	 * @param name
	 */
	@SuppressWarnings("unused")
	private ActionUpgradeTownLevel() {
		super(null, null);
	}

	/**
	 * @param type
	 * @param name
	 */
	public ActionUpgradeTownLevel(int count) {
		super("upgradetown", "Upgrade town level " + count + "x");

		this.count = count;
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtBuild(NOnMapObject obj) {
		Town t = obj.getTown();

		t.setLevel(t.getLevel() + count);
		obj.getPlayer().addInfo(new YNotificationSaver(t.getName() + " upgraded to " + t.getLevelName(), t.getIcon()));
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtDestroy(NOnMapObject obj) {
		obj.getTown().setLevel(obj.getTown().getLevel() - count);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.TOWN);
	}

}

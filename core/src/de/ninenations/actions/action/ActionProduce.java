/**
 * 
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.player.Player.RessType;
import de.ninenations.towns.Town;
import de.ninenations.ui.TextHelper;
import de.ninenations.ui.UiHelper;
import de.ninenations.util.NGenerator;

/**
 * @author sven
 *
 */
public class ActionProduce extends GAction {

	private static final long serialVersionUID = -7750380200493574735L;

	private boolean rand, once;
	private ObjectMap<String, Integer> prod;

	@SuppressWarnings("unused")
	private ActionProduce() {}

	/**
	 * @param type
	 * @param name
	 */
	public ActionProduce(boolean rand, boolean once, Object... objects) {
		super("produce", "Produce Ressource");

		this.rand = rand;
		this.once = once;

		prod = new ObjectMap<>();

		// convert
		for (int i = 0; i + 1 < objects.length; i += 2) {
			prod.put((String) objects[i], (int) objects[i + 1]);
		}
	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		produce(onMap);

		// play sound
		S.nData().getR(prod.keys().toArray().get(0)).playSound();

		// has unit?
		if (onMap != null) {
			onMap.addAp(-onMap.getAp());
		}
		return true;
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtBuild(NOnMapObject obj) {
		if (once) {
			produce(obj);
		}
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtRoundBegin(NOnMapObject obj) {
		if (!once) {
			produce(obj);
		}
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtDestroy(NOnMapObject obj) {
		if (!once) {
			return;
		}

		// remove ress
		Town t = obj.getTown();
		if (t == null) {
			return;
		}

		// remove
		for (String ress : prod.keys()) {
			t.addRess(ress, rand ? -NGenerator.getIntBetween(0, prod.get(ress)) : -prod.get(ress), RessType.BUILD);
		}
	}

	/**
	 * @param obj
	 */
	protected void produce(NOnMapObject obj) {
		// produce it
		if (!canPerform(obj)) {
			return;
		}

		Town t = obj == null ? null : obj.getTown();
		if (t == null) {
			// TODO inform?
			t = S.town().getTownsByPlayer(S.actPlayer()).first();
		}

		// missing something?
		for (String ress : prod.keys()) {
			if (prod.get(ress) < 0 && t.getRess(ress) < -prod.get(ress)) {
				// TODO inform
				obj.setError(S.nData().getR(ress).getName() + " is missing " + prod.get(ress) + "times.", true);

				return;
			}
		}

		// produce
		for (String ress : prod.keys()) {
			int count = rand ? NGenerator.getIntBetween(0, prod.get(ress)) : prod.get(ress);
			t.addRess(ress, count, count > 0 ? RessType.PRODUCE : RessType.CONSUME);

			// show?
			if (rand) {
				UiHelper.textAnimation("Produce " + count + "x " + S.nData().getR(ress).getName(), obj.getX(), obj.getY(), true, Color.WHITE);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return S.nData().getR(prod.keys().next()).getIcon();
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName() {

		// build it
		Array<String> erg = new Array<>();
		for (String ress : prod.keys()) {
			erg.add(prod.get(ress, 0) + "x " + S.nData().getR(ress).getName());
		}

		return (once ? "Give once " : "Produce ") + (rand ? "max " : "") + TextHelper.getCommaSepA((String[]) erg.toArray(String.class));
	}

	/**
	 * @return the prod
	 */
	public ObjectMap<String, Integer> getRawProd() {
		return prod;
	}

}

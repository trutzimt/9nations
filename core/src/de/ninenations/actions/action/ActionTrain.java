/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

import de.ninenations.actions.base.GAction;
import de.ninenations.actions.req.ReqEmptyField;
import de.ninenations.data.buildingunitbase.BuildWindow;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionTrain extends GAction {

	private static final long serialVersionUID = 8436672748638279728L;

	private boolean include;
	private Array<String> units;

	/**
	 * Create it
	 */
	@SuppressWarnings("unused")
	private ActionTrain() {
		this(false);
	}

	/**
	 * 
	 * @param include
	 *            true=only the selected unit will produce, false=all except the
	 *            selected units
	 * @param units
	 */
	public ActionTrain(boolean include, String... units) {
		super("train", "Train a unit");

		setAp(5);
		addReq(new ReqEmptyField(true, false, false));

		this.include = include;
		this.units = new Array<>(units);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		if (!super.perform(player, onMap, x, y)) {
			return false;
		}
		YSounds.pClick();

		// show build window
		MapScreen.get().getStage().addActor(new BuildWindow(include, units, onMap, x, y));

		return true;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.TRAIN);
	}
}

/**
 * 
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public class ActionUpgradeRangeLimit extends GAction {

	private static final long serialVersionUID = -7750380200493574735L;
	private int count;

	/**
	 * @param type
	 * @param name
	 */
	@SuppressWarnings("unused")
	private ActionUpgradeRangeLimit() {
		super(null, null);
	}

	/**
	 * @param type
	 * @param name
	 */
	public ActionUpgradeRangeLimit(int count) {
		super("upgradeRangeLimit", "Extend the area of the town with " + count + " field" + (count == 1 ? "" : "s"));

		this.count = count;
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtBuild(NOnMapObject obj) {
		obj.getTown().setMaxRange(obj.getTown().getMaxRange() + count);
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtDestroy(NOnMapObject obj) {
		obj.getTown().setMaxRange(obj.getTown().getMaxRange() - count);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.BUILD);
	}

}

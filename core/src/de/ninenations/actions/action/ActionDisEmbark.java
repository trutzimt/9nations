/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.ninenations.actions.req.ReqAp;
import de.ninenations.actions.req.ReqEmbark;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YSplitTab;
import de.ninenations.ui.window.YTabWindow;
import de.ninenations.util.YLog;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionDisEmbark extends ActionActive {

	private static final long serialVersionUID = 8436672748638279728L;

	private transient NMapUnit selectedUnit;

	/**
	 * Create it
	 */
	public ActionDisEmbark() {
		super("disembark", "disembark");

		addReq(new ReqAp(false, 5));
		addReq(new ReqEmbark(false, 1));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		YSounds.pClick();
		selectedUnit = null;

		// show DisEmbarkWindow
		MapScreen.get().getStage().addActor(new DisEmbarkWindow(player, onMap, x, y));

		// show the options
		YLog.log(onMap.getData().get(ActionEmbark.key(0), "qq"), onMap.getData().get(ActionEmbark.key(1), "qq"));

		return false;
	}

	@Override
	protected void generateField(NOnMapObject onMap, int x, int y) {
		showHere(onMap, x, y);
		showHere(onMap, x - 1, y);
		showHere(onMap, x, y - 1);
		showHere(onMap, x + 1, y);
		showHere(onMap, x, y + 1);

	}

	/**
	 * Helpermethod to colourfull the area
	 *
	 * @param x
	 * @param y
	 * @param map
	 * @param ActionPointLeft
	 * @return
	 */
	private void showHere(NOnMapObject onMap, int x, int y) {

		// is full?
		if (MapScreen.get().getData().isUnit(x, y)) {
			return;
		}

		// can pass?
		if (!MapScreen.get().getMap().canPass(selectedUnit, x, y)) {
			return;
		}
		generateImg(onMap, x, y);
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.DISEMBARK);
	}

	@Override
	protected void performOnMap(NOnMapObject onMap, int x, int y) {

		// place unit
		selectedUnit.show(x, y);
		selectedUnit.getActor().addAction(Actions.fadeIn(1));

		// remove from stack
		int id = 0;
		while (onMap.getData().containsKey(ActionEmbark.key(id))) {
			int nId = Integer.parseInt(onMap.getData().get(ActionEmbark.key(id)));
			if (nId == selectedUnit.getId()) {
				onMap.getData().remove(ActionEmbark.key(id));
				break;
			}

			id++;
		}

		super.performOnMap(onMap, x, y);

	}

	class DisEmbarkWindow extends YTabWindow {

		public DisEmbarkWindow(Player player, NOnMapObject onMap, int x, int y) {
			super("Disembark units");
			tabbedPane.add(new DisEmbarkTab(player, onMap, x, y));
			super.buildIt();
		}

		class DisEmbarkTab extends YSplitTab<NMapUnit> {

			private VisTextButton disembark;

			Player player;
			NOnMapObject onMap;
			int x, y;

			public DisEmbarkTab(Player player, NOnMapObject onMap, int x, int y) {
				super("Disembark", "No unit for disembarking.");

				this.player = player;
				this.onMap = onMap;
				this.x = x;
				this.y = y;

				int id = 0;
				while (onMap.getData().containsKey(ActionEmbark.key(id))) {
					addElement((NMapUnit) MapScreen.get().getData().getOnMap().get(Integer.parseInt(onMap.getData().get(ActionEmbark.key(id)))));
					id++;
				}

				// add button
				disembark = new VisTextButton("Select a unit first");
				disembark.addCaptureListener(new YChangeListener(false) {

					@Override
					public void changedY(Actor actor) {
						doubleClickElement(null);

					}
				});
				disembark.setDisabled(true);
				buttonBar.addActor(disembark);
			}

			@Override
			protected void clickElement(Button btn) {// check can build?

				disembark.setDisabled(false);
				disembark.setText("Disembark " + active.getName());
			}

			@Override
			protected void doubleClickElement(Button btn) {
				selectedUnit = active;
				ActionDisEmbark.super.perform(player, onMap, x, y);
				DisEmbarkWindow.this.close();

			}

		}
	}

}

/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.actions.base.GAction;
import de.ninenations.actions.req.ReqEmptyArea;
import de.ninenations.actions.req.ReqEmptyField;
import de.ninenations.actions.req.ReqTownFreeForFound;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.towns.Town;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.NTextButton;
import de.ninenations.ui.elements.YRandomField;
import de.ninenations.ui.newWindow.NWindow;
import de.ninenations.util.NGenerator;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionFoundTown extends GAction {

	private static final long serialVersionUID = 8655142436817556579L;

	private boolean noKill;

	@SuppressWarnings("unused")
	private ActionFoundTown() {}

	/**
	 * @param type
	 * @param x
	 * @param y
	 */
	public ActionFoundTown(boolean noKill) {
		super("found" + (noKill ? "king" : ""), "Found a new town" + (noKill ? "" : " and kill the unit"));

		this.noKill = noKill;

		addReq(new ReqEmptyField(false, true, false));
		addReq(new ReqTownFreeForFound());
		addReq(new ReqEmptyArea(4, false, true, false));
	}

	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		YSounds.pClick();
		// ask for a name
		MapScreen.get().getGui().getStage().addActor(new TownFoundWindow(player, onMap, x, y));
		return true;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.TOWN);
	}

	class TownFoundWindow extends NWindow {

		private String name;
		private NTextButton btn;

		public TownFoundWindow(final Player player, final NOnMapObject onMap, final int x, final int y) {
			super("townfound", "Found a new Town", YIcons.TOWN);

			name = NGenerator.getTown();

			add(new VisLabel("Name"));

			add(new YRandomField(name) {

				@Override
				public void saveText(String text) {
					name = text;
					btn.setText("Found " + name);
				}

				@Override
				protected String getRndText() {
					return NGenerator.getTown();
				}
			}).row();

			btn = new NTextButton("Found " + name) {

				@Override
				public void perform() {
					YSounds.play(YSounds.STARTGAME);

					// found
					Town t = S.town().add(name, player, x, y);
					S.onMap().addB(S.actPlayer().getNation().getTownhall(), player, t, x, y);

					// close
					close();
					MapScreen.get().getGui().showBottomMenuFor(x, y);
					onMap.setAp(0);

					// kill?
					if (!noKill) {
						onMap.destroy();
					}
				}
			};

			add();
			add(btn);

			pack();

			loadPos();
		}

	}
}

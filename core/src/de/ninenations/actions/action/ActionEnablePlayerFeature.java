/**
 * 
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Feature;
import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public class ActionEnablePlayerFeature extends GAction {

	private static final long serialVersionUID = -7750380200493574735L;
	private Feature feature;

	/**
	 * @param type
	 * @param name
	 */
	@SuppressWarnings("unused")
	private ActionEnablePlayerFeature() {
		super(null, null);
	}

	/**
	 * @param type
	 * @param name
	 */
	public ActionEnablePlayerFeature(Feature feature) {
		super("enablePlayerFeature", "Enable " + feature.get());

		this.feature = feature;
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtBuild(NOnMapObject obj) {
		obj.getPlayer().setFeature(feature, 1);
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtDestroy(NOnMapObject obj) {
		obj.getPlayer().setFeature(feature, 0);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(feature.getLogo());
	}

}

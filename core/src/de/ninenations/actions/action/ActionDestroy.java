/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.NDialog;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionDestroy extends GAction {

	private static final long serialVersionUID = 8655142436817556579L;

	/**
	 * @param type
	 * @param x
	 * @param y
	 */
	public ActionDestroy() {
		super("destroy", "Destroy it");

		hiddenLexicon = true;
	}

	@Override
	public boolean perform(Player player, final NOnMapObject onMap, final int x, final int y) {
		YSounds.pClick();
		// ask
		NDialog d = new NDialog("Destroy " + onMap.getName() + "?", "Do you really want to destroy and remove " + onMap.getName() + "?");
		d.addCancel();
		d.add("Destroy " + onMap.getName(), new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				YSounds.play(YSounds.DESTROY);
				S.map().playAnimation("explosion", x, y);
				onMap.destroy();
				MapScreen.get().getGui().showBottomMenuFor(x, y);

			}
		});
		d.show(MapScreen.get().getStage());

		return false;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.DEATH);
	}
}

/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.req.ReqDayTime;
import de.ninenations.actions.req.ReqNearMapObject;
import de.ninenations.actions.req.ReqSeason;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.game.NRound.NSeason;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.player.Player.RessType;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.MoveTo;
import de.ninenations.ui.window.YNotificationSaver;

/**
 * @author sven
 *
 */
public class ActionGift extends ActionActive {

	private static final long serialVersionUID = 8436672748638279728L;

	/**
	 * Create it
	 */
	public ActionGift() {
		super("gift", "Send a gift");

		setAp(1);
		addReq(new ReqNearMapObject(true));
		addReq(new ReqSeason(NSeason.WINTER, false));
		addReq(new ReqDayTime(true, false, true));
	}

	@Override
	protected void generateField(NOnMapObject onMap, int x, int y) {
		showHere(onMap, x - 1, y);
		showHere(onMap, x, y - 1);
		showHere(onMap, x + 1, y);
		showHere(onMap, x, y + 1);

	}

	/**
	 * Helpermethod to colourfull the area
	 *
	 * @param x
	 * @param y
	 * @param map
	 * @param ActionPointLeft
	 * @return
	 */
	private void showHere(NOnMapObject onMap, int x, int y) {

		// has something to send?
		if (S.unit(x, y) != null && S.unit(x, y).getTown() != null) {
			generateImg(onMap, x, y);

		}

	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(70);
	}

	@Override
	protected void performOnMap(NOnMapObject onMap, int x, int y) {

		// walk
		onMap.addAp(-ap);

		// calc it
		BaseRess r = getRess();
		int c = S.r().nextInt(ap) + 1;
		NMapUnit u = S.unit(x, y);

		// gift it
		u.getTown().addRess(r.getType(), c, RessType.GIFT);
		S.unit(x, y).getPlayer().addInfo(new YNotificationSaver("You got " + c + "x " + r.getName() + " as a gift.", r.getiID(), new MoveTo(u)));

		// TODO add sound

		// remove
		super.performOnMap(onMap, x, y);

	}

	private BaseRess getRess() {
		// TODO
		for (int i = 0; i < 1000; i++) {
			String ress = S.nData().getRess().random();
			BaseRess r = S.nData().getR(ress);
			if (r.getMarketCost() > 0) {
				return r;
			}
		}

		return S.nData().getR(S.nData().getRess().get(0));
	}
}

/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

import de.ninenations.actions.base.GAction;
import de.ninenations.actions.req.ReqAp;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionMove extends GAction {

	private static final long serialVersionUID = 8436672748638279728L;
	private transient Array<Image> moveImg;

	/**
	 * Create it
	 */
	public ActionMove() {
		super("move", "Move Action");

		// TODO find better way
		addReq(new ReqAp(false, 1));

		moveImg = new Array<>();

		hiddenLexicon = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		MapScreen.get().setActiveAction(this);
		YSounds.pClick();

		// show it
		if (moveImg != null) {
			moveImg = new Array<>();
		}

		int[][] data = calcActionPoints((NMapUnit) onMap, x, y);
		showWalkHere(x, y, data, (NMapUnit) onMap);

		return false;
	}

	/**
	 * Helpermethod to colourfull the area
	 *
	 * @param x
	 * @param y
	 * @param map
	 * @param ActionPointLeft
	 * @return
	 */
	private void showWalkHere(int x, int y, int[][] data, final NMapUnit onMap) {

		Image base = YIcons.getBuildO(YIcons.MOVETILE);

		int diff = onMap.getAp() / 3 + 1;
		int eX = Math.min(x + diff, MapScreen.get().getMap().getWidth());
		int eY = Math.min(y + diff, MapScreen.get().getMap().getHeight());

		// run it
		for (int oX = Math.max(x - diff, 0); oX < eX; oX++) {
			for (int oY = Math.max(y - diff, 0); oY < eY; oY++) {
				// colour the area?
				if (data[oX][oY] < 0) {
					continue;
				}
				final int cost = data[oX][oY];

				final Image img = new Image(base.getDrawable());
				img.setX(oX * 32);
				img.setY(oY * 32);
				img.getColor().a = 0f;
				img.addAction(Actions.alpha(0.5f, 1, null));
				img.addListener(new InputListener() {
					/*
					 * (non-Javadoc)
					 * 
					 * @see
					 * com.badlogic.gdx.scenes.scene2d.InputListener#enter(com.badlogic.gdx.scenes.
					 * scene2d.InputEvent, float, float, int, com.badlogic.gdx.scenes.scene2d.Actor)
					 */
					@Override
					public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
						img.addAction(Actions.color(Color.CYAN, 1f));
						MapScreen.get().getGui().setInfo(onMap.getUnit().getMoveTyp().toString().toLowerCase() + " here cost " + cost + " AP",
								getIcon().getDrawable());
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see
					 * com.badlogic.gdx.scenes.scene2d.InputListener#exit(com.badlogic.gdx.scenes.
					 * scene2d.InputEvent, float, float, int, com.badlogic.gdx.scenes.scene2d.Actor)
					 */
					@Override
					public void exit(InputEvent event, float x, float y, int pointer, Actor fromActor) {
						img.addAction(Actions.color(new Color(1, 1, 1, 0.5f), 1f));
						MapScreen.get().getGui().setInfo("To far away", getIcon().getDrawable(), Color.SALMON);
					}

					/*
					 * (non-Javadoc)
					 * 
					 * @see
					 * com.badlogic.gdx.scenes.scene2d.InputListener#touchDown(com.badlogic.gdx.
					 * scenes.scene2d.InputEvent, float, float, int, int)
					 */
					@Override
					public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
						performClick(onMap, cost, (int) img.getX() / 32, (int) img.getY() / 32);
						return false;
					}
				});

				moveImg.add(img);

				MapScreen.get().getMap().getStage().addActor(img);

			}
		}

	}

	/**
	 * Get the clicks
	 */
	public void performClick(NMapUnit activeSelected, int cost, int x, int y) {

		// clear area
		destroyActive();

		// walk
		activeSelected.addAp(-cost);
		activeSelected.setXY(x, y);

		// show it
		MapScreen.get().setActiveAction(null);
		MapScreen.get().getGui().showBottomMenuFor(activeSelected.getX(), activeSelected.getY());

	}

	/**
	 * Build the array
	 *
	 * @param map
	 * @param activeSelected
	 * @param x
	 * @param y
	 * @return
	 */
	public static int[][] calcActionPoints(NMapUnit activeSelected, int x, int y) {
		int[][] data = new int[MapScreen.get().getMap().getWidth()][MapScreen.get().getMap().getHeight()];

		// empty array
		for (int x2 = 0; x2 < data.length; x2++) {
			for (int y2 = 0; y2 < data[0].length; y2++) {
				data[x2][y2] = -1;
			}
		}

		calcActionPointsHelper(activeSelected, x, y, 0, data);
		return data;
	}

	public static void calcActionPointsHelper(NMapUnit activeSelected, int x, int y, int actPoints, int[][] data) {

		// wrong position?
		if (!MapScreen.get().getMap().canPass(activeSelected, x, y)) {
			return;
		}

		// TODO check cost
		// has a value?
		if (data[x][y] != -1) {
			return;
		}

		// set cost
		int cost = MapScreen.get().getMap().getMoveCost(activeSelected, x, y) + actPoints;
		if (cost > activeSelected.getAp()) {
			return;
		}
		data[x][y] = cost;

		// call subs
		if (cost == activeSelected.getAp()) {
			return;
		}
		calcActionPointsHelper(activeSelected, x - 1, y, cost, data);
		calcActionPointsHelper(activeSelected, x, y - 1, cost, data);
		calcActionPointsHelper(activeSelected, x + 1, y, cost, data);
		calcActionPointsHelper(activeSelected, x, y + 1, cost, data);

	}

	/**
	 * Remove old img
	 */
	@Override
	public void destroyActive() {
		for (Image i : moveImg) {
			i.addAction(Actions.sequence(Actions.fadeOut(0.5f), Actions.removeActor()));
		}
		moveImg.clear();
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.WALK);
	}
}

/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.actions.req.ReqAp;
import de.ninenations.actions.req.ReqTown;
import de.ninenations.data.ress.SendRessWindow;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionSendRessource extends GAction {

	private static final long serialVersionUID = 8436672748638279728L;

	/**
	 * Create it
	 */
	public ActionSendRessource() {
		super("sendRess", "Send ress to an another town");

		reqs.add(new ReqTown(false, 2));
		reqs.add(new ReqAp(false, 5));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		YSounds.play(YSounds.BUY);
		// show build window
		MapScreen.get().getStage().addActor(new SendRessWindow(onMap.getTown()));

		onMap.addAp(-5);
		return true;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.RESSOURCE);
	}
}

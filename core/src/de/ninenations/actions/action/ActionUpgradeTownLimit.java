/**
 * 
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public class ActionUpgradeTownLimit extends GAction {

	private static final long serialVersionUID = -7750380200493574735L;
	private int count;

	/**
	 * @param type
	 * @param name
	 */
	@SuppressWarnings("unused")
	private ActionUpgradeTownLimit() {
		super(null, null);
	}

	/**
	 * @param type
	 * @param name
	 */
	public ActionUpgradeTownLimit(int count) {
		super("upgradeTownLimit", "Allow to found " + (count == 1 ? "one more town" : count + " more towns"));

		this.count = count;
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtBuild(NOnMapObject obj) {
		obj.getPlayer().addMaxTowns(count);
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtDestroy(NOnMapObject obj) {
		obj.getPlayer().addMaxTowns(-count);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.TOWN);
	}

}

/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.req.ReqNearMapObject;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.YIcons;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ActionAttack extends ActionActive {

	private static final long serialVersionUID = 8436672748638279728L;

	/**
	 * Create it
	 */
	public ActionAttack() {
		super("attack", "Attack");

		setAp(5);
		addReq(new ReqNearMapObject(true));
	}

	@Override
	protected void generateField(NOnMapObject onMap, int x, int y) {
		showHere(onMap, x, y);
		showHere(onMap, x - 1, y);
		showHere(onMap, x, y - 1);
		showHere(onMap, x + 1, y);
		showHere(onMap, x, y + 1);

	}

	/**
	 * Helpermethod to colourfull the area
	 *
	 * @param x
	 * @param y
	 * @param map
	 * @param ActionPointLeft
	 * @return
	 */
	private void showHere(NOnMapObject onMap, int x, int y) {

		// has something to destroy?

		for (NOnMapObject o : new NOnMapObject[] { S.unit(x, y), S.build(x, y) }) {
			if (o != null && o.getPlayer() != S.actPlayer()) {
				generateImg(onMap, x, y);

			}
		}

	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.ATTACK);
	}

	@Override
	protected void performOnMap(NOnMapObject onMap, int x, int y) {

		// walk
		onMap.addAp(-5);
		if (onMap.attack(MapScreen.get().getData().isUnit(x, y) ? S.unit(x, y) : S.build(x, y))) {
			YSounds.play(YSounds.ATTACK);
		} else {
			YSounds.play(YSounds.DEFEND);
		}

		// remove
		super.performOnMap(onMap, x, y);

	}
}

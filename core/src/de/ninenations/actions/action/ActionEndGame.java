/**
 * 
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public class ActionEndGame extends GAction {

	private static final long serialVersionUID = -7750380200493574735L;

	private boolean win;

	@SuppressWarnings("unused")
	private ActionEndGame() {}

	/**
	 * @param type
	 * @param name
	 */
	public ActionEndGame(boolean win) {
		super("endGame", win ? "Win game" : "Lose game");

		this.win = win;
	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {

		// TODO
		if (win) {
			player.setWillWin(true);
			return true;
		}

		player.setWillLose(true);

		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(win ? YIcons.WIN : YIcons.LOSE);
	}

}

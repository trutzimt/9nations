/**
 * 
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.player.Player.RessType;
import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public class ActionPlayerRessMulti extends GAction {

	private static final long serialVersionUID = -7750380200493574735L;
	private int count;
	private RessType type;
	private String ress;

	@SuppressWarnings("unused")
	private ActionPlayerRessMulti() {
		super(null, null);
	}

	/**
	 * 
	 * @param type
	 * @param ress
	 * @param count
	 */
	public ActionPlayerRessMulti(RessType type, String ress, int count) {
		super("playerRessMulti", count + "% " + type.toString().toLowerCase() + " for player's " + ress + " ress");

		this.type = type;
		this.ress = ress;
		this.count = count;
	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		player.setRessMultiplicator(type, ress, count);
		return false;
	}

	/**
	 * @param obj
	 */
	@Override
	public void performAtDestroy(NOnMapObject obj) {
		obj.getPlayer().setRessMultiplicator(type, ress, -count);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.RESSOURCE);
	}

}

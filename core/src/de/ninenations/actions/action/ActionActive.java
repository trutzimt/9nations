/**
 *
 */
package de.ninenations.actions.action;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.ninenations.actions.base.GAction;
import de.ninenations.actions.req.ReqAp;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.util.YError;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public abstract class ActionActive extends GAction {

	private static final long serialVersionUID = 8436672748638279728L;
	private transient Array<Image> moveImg;

	/**
	 * Create it
	 */
	protected ActionActive() {}

	/**
	 * Create it
	 */
	public ActionActive(String type, String name) {
		super(type, name);

		addReq(new ReqAp(false, 5));

		moveImg = new Array<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {

		// show it
		if (moveImg != null) {
			moveImg = new Array<>();
		}

		generateField(onMap, x, y);

		// found nothing?
		if (moveImg.size == 0) {
			YSounds.pBuzzer();
			Dialogs.showOKDialog(MapScreen.get().getStage(), "Nothing to " + name, "The " + onMap.getName() + " finds nothing to " + name + ".");
			return false;
		}

		MapScreen.get().setActiveAction(this);

		return false;
	}

	protected abstract void generateField(NOnMapObject onMap, int x, int y);

	/**
	 * Helpermethod to colourfull the area
	 *
	 * @param x
	 * @param y
	 * @param map
	 * @param ActionPointLeft
	 * @return
	 */
	protected void generateImg(final NOnMapObject onMap, int x, int y) {

		final Image img = YIcons.getBuildO(YIcons.MOVETILE);
		img.setX(x * 32);
		img.setY(y * 32);
		img.getColor().a = 0f;
		img.addAction(Actions.alpha(0.5f, 1, null));
		img.addListener(new InputListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.badlogic.gdx.scenes.scene2d.InputListener#enter(com.badlogic.gdx.scenes.
			 * scene2d.InputEvent, float, float, int, com.badlogic.gdx.scenes.scene2d.Actor)
			 */
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				try {
					img.addAction(Actions.color(Color.CYAN, 1f));

					MapScreen.get().getGui().setInfo(getName() + " " + onMap.getName(), getIcon().getDrawable());
				} catch (Throwable t) {
					YError.error(t, false);
				}
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.badlogic.gdx.scenes.scene2d.InputListener#exit(com.badlogic.gdx.scenes.
			 * scene2d.InputEvent, float, float, int, com.badlogic.gdx.scenes.scene2d.Actor)
			 */
			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				try {
					img.addAction(Actions.color(new Color(1, 1, 1, 0.5f), 1f));
					MapScreen.get().getGui().setInfo("Nothing to " + getName(), getIcon().getDrawable(), Color.SALMON);
				} catch (Throwable t) {
					YError.error(t, false);
				}
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.badlogic.gdx.scenes.scene2d.InputListener#touchDown(com.badlogic.gdx.
			 * scenes.scene2d.InputEvent, float, float, int, int)
			 */
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				try {
					int mX = (int) img.getX() / 32;
					int mY = (int) img.getY() / 32;

					performOnMap(onMap, mX, mY);
					return true;
				} catch (Throwable t) {
					YError.error(t, false);
					return false;
				}
			}
		});

		moveImg.add(img);

		MapScreen.get().getMap().getStage().addActor(img);

	}

	/**
	 * Remove old img
	 */
	@Override
	public void destroyActive() {
		for (Image i : moveImg) {
			i.addAction(Actions.sequence(Actions.fadeOut(0.5f), Actions.removeActor()));
		}
		moveImg.clear();
	}

	protected void performOnMap(NOnMapObject onMap, int x, int y) {

		// show it
		destroyActive();
		MapScreen.get().setActiveAction(null);
		MapScreen.get().getGui().resetObjectMenu();

	}
}

/**
 * 
 */
package de.ninenations.feedback;

import java.io.StringWriter;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.utils.ObjectMap;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTextArea;
import com.kotcrab.vis.ui.widget.VisTextField;

import de.ninenations.core.NN;
import de.ninenations.screen.ErrorScreen;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.NTable;
import de.ninenations.ui.elements.NTextButton;
import de.ninenations.ui.newWindow.NWindow;
import de.ninenations.util.NetHelper;
import de.ninenations.util.NetListener;
import de.ninenations.util.YError;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class FeedbackWindow extends NWindow {

	public static final String ID = "feedback";

	private VisTextField contact, title;
	private VisTextArea text;
	private VisCheckBox system, throwa;
	private Throwable throwable;
	private NTextButton send;

	/**
	 * @param id
	 * @param title
	 * @param icon
	 */
	public FeedbackWindow() {
		super(ID, "Feedback", YIcons.DEBUG);

		contact = new VisTextField();
		title = new VisTextField();
		text = new VisTextArea();
		system = new VisCheckBox("Include system infos", true);
		throwa = new VisCheckBox("Send informations about this error", true);
		throwa.setVisible(false);

		// build scrollpane
		VisScrollPane pane = new VisScrollPane(text);
		pane.setScrollingDisabled(true, false);
		pane.setFadeScrollBars(false);
		pane.setScrollbarsOnTop(true);
		// pane.setFillParent(true);

		send = new NTextButton("Send", true) {

			@Override
			public void perform() {
				send();

			}
		};

		// build gui
		NTable t = new NTable();
		t.addL("Contact", contact);
		t.addL("Title", title);
		t.addL("Message", pane);
		t.getCells().get(t.getCells().size - 1).grow();
		t.addL(null, system);
		t.addL(null, throwa);
		t.addSep().colspan(2);
		t.addL(null, send);
		add(t).grow();
		pack();
	}

	private void send() {
		send.setDisabled(true);
		send.setText("Please wait");
		// build map
		ObjectMap<String, String> data = new ObjectMap<>();
		data.put("contact", contact.getText());
		data.put("title", title.getText());
		data.put("message", text.getText());
		if (system.isChecked()) {
			data.put("system", ErrorScreen.writeSystemInfo((Screen) NN.screen()).toString());
		}
		// add exception?
		if (throwable != null && throwa.isChecked()) {
			StringWriter sw = new StringWriter();
			ErrorScreen.addError(sw, throwable);
			data.put("exception", sw.toString());
		}

		NetHelper.sendPOSTContent("https://9nations.de/feedbackFiles/send.php", data, new NetListener() {

			@Override
			public void changedY(String result) {
				if (result == null) {
					YSounds.pBuzzer();
					Dialogs.showOKDialog(NN.screen().getStage(), "Feedback error", "Can not send feedback. Check you network.");
					return;
				}

				Dialogs.showOKDialog(NN.screen().getStage(), "Feedback", result);
				close();

			}

			@Override
			public void error(Throwable t) {
				if (t == null) {
					YSounds.pBuzzer();
					Dialogs.showOKDialog(NN.screen().getStage(), "Feedback error", "Can not send feedback. Check you network.");
					return;
				}
				YError.error(t, false);
				send.setDisabled(false);
				send.setText("Send");
			}
		});

	}

	/**
	 * @param throwable
	 *            the throwable to set
	 */
	public void setThrowable(Throwable throwable) {
		throwa.setVisible(true);
		this.throwable = throwable;
		send.setText("Send Error");
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {
		this.title.setText(title);
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(String text) {
		this.text.setText(text);
	}

}

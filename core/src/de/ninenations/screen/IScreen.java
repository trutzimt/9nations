package de.ninenations.screen;

import com.badlogic.gdx.scenes.scene2d.Stage;

public interface IScreen {

	/**
	 * @return the stage
	 */
	public Stage getStage();
}

/**
 *
 */
package de.ninenations.screen;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.core.NN;
import de.ninenations.menu.MainMenuScreen;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.newWindow.WindowManagerLoader;
import de.ninenations.util.NSettings;
import de.ninenations.util.YError;
import de.ninenations.util.YMusic;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class LoaderScreen extends BaseMenuScreen {

	protected Image loadingHour;
	protected int status;
	protected VisLabel text;
	protected String title;

	/**
	 * 
	 */
	public LoaderScreen(String title) {
		super("menuLoading");

		int s = NSettings.isBiggerGui() ? 256 : 128;
		this.title = title;

		loadingHour = new Image(NN.asset().getT("system/logo/logo_" + s * 2 + ".png"));
		loadingHour.setOrigin(s, s);
		loadingHour.setX(Gdx.graphics.getWidth() / 2 - s);
		loadingHour.setY(Gdx.graphics.getHeight() / 2 - s);
		loadingHour.getColor().a = 0;
		loadingHour.addAction(Actions.fadeIn(1));
		stage.addActor(loadingHour);

		status = 0;
	}

	/**
	 * Load the nextScreen
	 */
	public void nextScreen() {
		if (NSettings.isDebug()) {
			// NN.get().switchScreen(new
			// GameStartScreen(CampaignMgmt.getCampaign("sandbox").getScenario("debug")));
			// NN.get().switchScreen(new
			// GameStartScreen(CampaignMgmt.getCampaign("beowulf").getScenario("1")));
			// NN.get().switchScreen(new
			// GameStartScreen(CampaignMgmt.getCampaign("tut").getScenario("2")));
			// NN.get().switchScreen(new
			// GameStartScreen(CampaignMgmt.getCampaign("single").getScenario("trading")));
			// return;

		}
		NN.get().switchScreen(new MainMenuScreen(true));
	}

	/**
	 * Load the loading code
	 */
	public void onceCode() {

		if (NSettings.isDebug()) {
			Gdx.app.setLogLevel(Application.LOG_DEBUG);
		}

		// Gdx.app.log("Qoss",Gdx.graphics.getWidth()+" "+Gdx.graphics.getHeight());

		// load skin. Big or small?
		if (VisUI.isLoaded()) {
			VisUI.dispose();
		}

		if (!NSettings.getPref().contains("biggerGui")) {
			// YLog.log(Gdx.graphics.getWidth() , Gdx.graphics.getHeight());

			if (Gdx.graphics.getWidth() > 1000 && Gdx.graphics.getHeight() > 700) {
				NSettings.getPref().putBoolean("biggerGui", true);
				if (Gdx.files.internal("64").exists()) {
					NSettings.getPref().putInteger("guiScale", 64);
				}
			} else {
				NSettings.getPref().putBoolean("biggerGui", false);
			}
		}
        NSettings.getPref().flush();

		VisUI.load(NSettings.isBiggerGui() ? "system/x2/tinted.json" : "system/x1/tinted.json");
		// VisUI.load(YSettings.isBiggerGui() ? SkinScale.X2 : SkinScale.X1);
		createLabel();

		if (Gdx.files.internal("music").exists()) {
			NN.get().setMusic(new YMusic());
		}
		YMusic.mainmenu();

		NN.start();
		WindowManagerLoader.load();

		// ScenarioManagement.init();

		NN.asset().setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));

		NN.plattform().startUpCode();

		// add loading ressources
		// YStatic.systemAssets.load("system/logo/logo256.png", Texture.class);
		// NN.asset().load("system/icons/IconSet.png", Texture.class);
		// NN.asset().loadT("system/logo/logo_" + (NSettings.isBiggerGui() ? "48.png" :
		// "32.png"));

		YSounds.loadSound();
		YIcons.loadIcons();
		NN.skin().loadAsset();

		// YLog.log(NSettings.isBiggerGui(), Gdx.files.internal("64").exists(),
		// NSettings.getGuiScale(), NN.asset().containsAsset("64/icons/iconSet.png"));
	}

	/**
	 * 
	 */
	protected void createLabel() {
		int s = NSettings.isBiggerGui() ? 256 : 128;

		text = new VisLabel(title);
		text.setX(Gdx.graphics.getWidth() / 2 - text.getWidth() / 2);
		text.setY(Gdx.graphics.getHeight() / 2 - (s - 10) * 2);
		// text.setColor(0, 0, 0, 0);
		text.addAction(Actions.fadeIn(1));
		stage.addActor(text);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#show()
	 */
	@Override
	public void show() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	@Override
	public void render(float delta) {
		super.render(delta);

		loadingHour.setRotation(loadingHour.getRotation() + delta * 10);
		loadingHour.act(delta);

		// display loading information
		if (text != null) {
			text.setText(title + (int) (NN.asset().getProgress() * 100) + "%");
		}

		if (status == 0) {
			status++;
			return;
		}

		// init code
		if (status == 1) {
			onceCode();
			status++;
			return;
		}

		if (!isFinishLoading()) {
			return;
		}

		// init code
		if (status == 2) {
			secondCode();
			status++;
			return;
		}

		// if (loadingHour.isVisible()) {
		// loadingHour.setVisible(false);
		// }

		// TODO show errors stage.getActors().size <= 3 &&
		// load it
		if (status == 3) {
			try {
				nextScreen();
			} catch (Throwable t) {
				YError.error(t, true);
			}

			status++;
		}

	}

	/**
	 * Run after loading the assets
	 */
	protected void secondCode() {
		NN.mods().startProgram();
	}

	/**
	 * @return
	 */
	public boolean isFinishLoading() {
		return NN.asset().update();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	// @Override
	// public void resize(int width, int height) {
	// super.resize(width, height);
	// int s = YSettings.isBiggerGui() ? 256 : 128;
	// // loadingHour.setX(width / 2);
	// // loadingHour.setY(height / 2);
	//
	// if (text == null) {
	// return;
	// }
	//
	// // text.setX(width / 2 - text.getWidth() / 2);
	// // text.setY(height / 2 - (s - 10) * 2);
	// }

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#pause()
	 */
	@Override
	public void pause() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resume()
	 */
	@Override
	public void resume() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#hide()
	 */
	@Override
	public void hide() {}

	/**
	 * @return the loadingHour
	 */
	public Image getLoadingHour() {
		return loadingHour;
	}

	/**
	 * @return the text
	 */
	public VisLabel getText() {
		return text;
	}

}

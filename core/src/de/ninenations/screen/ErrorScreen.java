package de.ninenations.screen;

import java.io.StringWriter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.ButtonBar;
import com.kotcrab.vis.ui.widget.ButtonBar.ButtonType;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTextArea;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.ninenations.core.NN;
import de.ninenations.util.NSettings;
import de.ninenations.util.YError;

public class ErrorScreen implements Screen {

	private Throwable e;
	private Stage stage;
	private Screen oldScreen;

	/**
	 * @param e
	 */
	public ErrorScreen(Throwable e, Screen oldScreen) {
		super();
		this.e = e;
		this.oldScreen = oldScreen;
		// Gdx.app.error("ErrorScreen", e.getMessage(), e);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#show()
	 */
	@Override
	public void show() {

		stage = new Stage();

		// generate string
		StringWriter sw = writeSystemInfo(oldScreen);

		addError(sw, e);

		// has more errors?
		try {
			if (YError.errors != null && YError.errors.size > 1) {
				sw.write("\nMore small errors:\n");
				// remove last one
				YError.errors.removeIndex(YError.errors.size - 1);
				// print it
				for (Throwable t : YError.errors) {
					sw.write("\n");
					addError(sw, t);
				}
			}
		} catch (Exception e1) {
			Gdx.app.error("Errorscreen", "Can not get more errors", e1);
		}

		// e.printStackTrace(new PrintWriter(sw));

		// Reset Gui
		if (VisUI.isLoaded()) {
			VisUI.dispose();
		}
		VisUI.load();

		// Generate Gui
		VisTable t = new VisTable();
		t.setFillParent(true);
		t.add(new VisLabel("9N crashed. To correct the error, please send a message with")).row();
		t.add(new VisLabel("this screen to https://yaams.de/feedback/ Thank you.")).row();

		// build scrollpane
		VisScrollPane v = new VisScrollPane(new VisTextArea(sw.toString()));
		v.setScrollingDisabled(true, false);
		v.setFadeScrollBars(false);
		v.setFlickScroll(false);
		t.add(v).grow().row();

		ButtonBar buttonBar = new ButtonBar();

		VisTextButton b = new VisTextButton("Exit Game");
		b.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.app.exit();
			}
		});
		buttonBar.setButton(ButtonType.LEFT, b);

		b = new VisTextButton("Send Issue");
		b.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				Gdx.net.openURI("https://9nations.de/feedback/");
			}
		});
		buttonBar.setButton(ButtonType.RIGHT, b);

		t.add(buttonBar.createTable());

		stage.addActor(t);
		Gdx.input.setInputProcessor(stage);

	}

	/**
	 * @param sw
	 */
	public static StringWriter writeSystemInfo(Screen oldScreen) {
		StringWriter sw = new StringWriter();
		try {
			sw.write("Version:" + NSettings.VERSION);
			sw.write(", Screen:" + (oldScreen == null ? "??" : oldScreen.getClass().getSimpleName()));
			sw.write(", " + NN.plattform().getSystemErrorInfo());
			sw.write("\n");
		} catch (Exception e1) {
			Gdx.app.error("Errorscreen", "Can not get Scene name", e1);
		}
		return sw;
	}

	/**
	 * @param sw
	 */
	public static void addError(StringWriter sw, Throwable e) {
		try {
			sw.write(e.getClass() + ": " + e.getMessage() + "\n");
			for (StackTraceElement st : e.getStackTrace()) {
				sw.write("   " + st.toString() + "\n");
			}
			if (e.getCause() != null) {
				sw.write("Caused by: ");
				addError(sw, e.getCause());
			}
		} catch (Exception f) {
			Gdx.app.error("Errorscreen", "Can not print error", f);
		}
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.9f, 0f, 0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().setScreenSize(width, height);
	}

	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void hide() {}

	@Override
	public void dispose() {}

}

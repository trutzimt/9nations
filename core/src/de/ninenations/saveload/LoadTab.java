/**
 * 
 */
package de.ninenations.saveload;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.Json;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YSplitTab;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class LoadTab extends YSplitTab<YSaveInfo> {

	private VisTextButton delete, load;

	public LoadTab() {
		super("Load Game", "At the moment you have no save games.");

		resetElements();

		// add button
		load = new VisTextButton("Please select a save game");
		load.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				doubleClickElement(null);

			}
		});
		buttonBar.addActor(load);

		delete = new VisTextButton("Delete it");
		delete.addCaptureListener(new YChangeListener(true) {

			@Override
			public void changedY(Actor actor) {
				for (int i = 0; i < elements.size; i++) {
					if (elements.get(i).getUserObject() == active) {
						SaveLoadManagement.delete(active.getFile().nameWithoutExtension());
						elements.removeIndex(i);
						break;
					}
				}
				reset();
				YSounds.pClick();
				rebuild();

			}
		});
		buttonBar.addActor(delete);

		reset();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#rebuild()
	 */
	@Override
	protected void rebuild() {
		resetElements();
		super.rebuild();
	}

	public void resetElements() {
		elements.clear();
		// add all elements
		Json j = new Json();
		// add saves
		for (FileHandle f : SaveLoadManagement.getSaves()) {
			YSaveInfo q = j.fromJson(YSaveInfo.class, f.read());
			if (q.isHidden()) {
				continue;
			}

			q.setFile(f);

			addElement(q);

		}
	}

	/**
	 * Reset the status
	 */
	@Override
	protected void reset() {
		super.reset();
		delete.setDisabled(true);
		load.setText("Please select a save game");
	}

	/**
	 * Place the selected artwork
	 */
	@Override
	protected void doubleClickElement(Button btn) {

		if (SaveLoadManagement.load(active.getFile())) {
			YSounds.pClick();
		} else {
			YSounds.pBuzzer();
		}

	}

	@Override
	protected void clickElement(Button btn) {
		load.setText("Load " + ((YSaveInfo) btn.getUserObject()).getName());
		delete.setDisabled(false);
	}
}
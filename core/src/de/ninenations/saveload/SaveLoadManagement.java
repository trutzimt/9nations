/**
 *
 */
package de.ninenations.saveload;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Json;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.ninenations.core.NN;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.CampaignMgmt;
import de.ninenations.util.NSettings;
import de.ninenations.util.YError;
import de.ninenations.util.YLog;

/**
 * @author svenå
 *
 */
public class SaveLoadManagement {

	private static final String FOLDER = "saves", END = ".9n";

	private SaveLoadManagement() {}

	/**
	 * Get all Saves
	 *
	 * @return
	 */
	public static LinkedList<FileHandle> getSaves() {
		LinkedList<FileHandle> files = new LinkedList<>();

		// add local files
		if (Gdx.files.isLocalStorageAvailable()) {
			FileHandle file = Gdx.files.local(FOLDER);
			if (file.isDirectory()) {
				for (FileHandle f : file.list(END)) {
					files.add(f);
				}

			}
		}

		// add external files
		if (Gdx.files.isExternalStorageAvailable()) {
			FileHandle file = Gdx.files.external(FOLDER);
			if (file.isDirectory()) {
				for (FileHandle f : file.list(END)) {
					files.add(f);
				}

			}
		}

		return files;
	}

	/**
	 * Save the game under a specific file name
	 *
	 * @param filename
	 * @param map
	 */
	public static boolean exist(String filename) {
		FileHandle active = createFile(filename + END);

		return active != null && active.exists();
	}

	/**
	 * delete the game under a spezific file name
	 *
	 * @param filename
	 * @param map
	 */
	public static boolean delete(String filename) {
		FileHandle active = createFile(filename + END);
		FileHandle data = createFile(filename + END + "data");

		return active != null && active.delete() && data != null && data.delete();
	}

	private static FileHandle createFile(String filename) {
		// save local?
		if (Gdx.files.isLocalStorageAvailable()) {
			return Gdx.files.local(FOLDER + "/" + filename);
		}
		if (Gdx.files.isExternalStorageAvailable()) {
			return Gdx.files.external(FOLDER + "/" + filename);
		}
		return null;
	}

	/**
	 * Save the game under a spezific file name
	 *
	 * @param filename
	 * @param map
	 */
	public static boolean save(String filename, boolean hidden) {
		try {
			FileHandle active = createFile(filename + END);

			if (active == null) {
				return false;
			}

			save(active, hidden);
			return true;
		} catch (Throwable e) {
			YError.error(new IllegalArgumentException("Can not save " + filename, e), false);
		}
		return false;
	}

	/**
	 * Save the game
	 *
	 * @param active
	 * @param map
	 */
	public static void save(FileHandle active, boolean hidden) {

		// new game?
		if (active == null) {
			// find next free spot
			int c = 1;
			String name = FOLDER + "/";

			if (Gdx.files.isLocalStorageAvailable()) {

				// find free file?
				do {
					active = Gdx.files.local(name + c + END);
					c++;
				} while (active.exists());
			} else if (Gdx.files.isExternalStorageAvailable()) {

				// find free file
				do {
					active = Gdx.files.external(name + c + END);
					c++;
				} while (active.exists());
			} else {
				Dialogs.showErrorDialog(NN.screen().getStage(), "No place or space for saving available.");
				return;
			}
		}

		// inform
		// MapScreen.get().getEvents().perform(MapEventHandlerTyp.FILE_SAVE);

		// build save game object
		YSaveInfo qi = new YSaveInfo();
		qi.setVersion(NSettings.VERSION);
		MapScreen.get().getData().addSaveSettings(qi.getData());
		qi.setHidden(hidden);

		// save the game data
		Json json = new Json();
		active.sibling(active.name() + "data").writeString(json.toJson(MapScreen.get().getData()), false);

		// save the game info
		active.writeString(json.toJson(qi), false);

		// active.writeString(json.toJson(map.getActivePlayer()), true);
		// active.writeString(json.toJson(map.getPlayers()), true);

		YLog.log2("Save file", active.name());

	}

	/**
	 * Load a game
	 *
	 * @param active
	 * @param qossire
	 */
	public static boolean load(String filename) {
		FileHandle active = createFile(filename);

		if (active == null) {
			return false;
		}

		return load(active);
	}

	/**
	 * Load a game
	 *
	 * @param active
	 * @param qossire
	 */
	public static boolean load(FileHandle saveGame) {

		Stage stage = NN.screen().getStage();

		// check can load?
		if (!saveGame.exists()) {
			Dialogs.showOKDialog(stage, "Load game error", "Can not load game: No save file exist.");
			return false;
		}

		// data exist?
		if (!saveGame.sibling(saveGame.nameWithoutExtension() + "data").exists()) {
			Dialogs.showOKDialog(stage, "Load game error", "Can not load game: No save game exist.");
			return false;
		}

		try {
			YSaveInfo q = new Json().fromJson(YSaveInfo.class, saveGame.read());

			// right version?
			if (q.getVersion() != NSettings.VERSION) {
				Dialogs.showOKDialog(stage, "Load game error",
						"Can not load game: It was saved with version " + q.getVersion() + ". You are using version " + NSettings.VERSION + ".");
				return false;
			}

			String c = q.getData().get("campaign");
			String s = q.getData().get("scenario");

			// camp exist??
			if (!CampaignMgmt.existCampaign(c)) {
				Dialogs.showOKDialog(stage, "Load game error", "Can not load game: The campaign " + c + " not exist.");
				return false;
			}

			// scenario exist??
			BaseScenario bS = null;
			// TODO find better way
			if (!"endlessgame".equals(s)) {

				if (!CampaignMgmt.getCampaign(c).existScenario(s)) {
					Dialogs.showOKDialog(stage, "Load game error", "Can not load game: The scenario " + s + " not exist.");
					return false;
				}
				bS = CampaignMgmt.getCampaign(c).getScenario(s);
			}

			NN.get().switchScreen(new GameLoadLoaderScreen(saveGame, bS));
		} catch (Exception e) {
			YError.error(e, false);
		}

		return true;
	}

	/**
	 * Load the config file
	 *
	 * @param filename
	 * @return
	 */
	public static YSaveInfo loadConfig(String filename) {
		FileHandle active = createFile(filename);

		if (active == null) {
			return null;
		}

		return new Json().fromJson(YSaveInfo.class, active.read());
	}

}

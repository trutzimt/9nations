package de.ninenations.saveload;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Json;

import de.ninenations.game.GameStartScreen;
import de.ninenations.game.screen.MapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.util.YLog;

public class GameLoadLoaderScreen extends GameStartScreen {

	private FileHandle saveGame;

	/**
	 * Load everything for the game
	 *
	 * @param qossire
	 * @param scenario
	 */
	public GameLoadLoaderScreen(FileHandle saveGame, BaseScenario bs) {
		super(bs);

		this.saveGame = saveGame;
	}

	@Override
	public void secondCode() {

		// YSaveInfo qi = new Json().fromJson(YSaveInfo.class, saveGame.read());

		// String c = (String) qi.getSettings().getPref().get("campaign");
		// String s = (String) qi.getSettings().getPref().get("scenario");
		//
		// // TODO find better way
		// BaseScenario scen = null;
		// if ("endlessgame".equals(s)) {
		// // scen = new EndlessGameScenario(qi.getSettings().get("map"), null);
		// } else {
		// scen = CampaignMgmt.getCampaign(c).getScenario(s);
		// }
		// qi.getSettings().setScenario(scen);

		MapData data = new Json().fromJson(MapData.class, saveGame.sibling(saveGame.name() + "data").read());
		YLog.log("Loading ", saveGame.name());
		MapScreen.load(data);

		// map.getScenario().preLoad(map);
		// map.getEvents().perform(MapEventHandlerTyp.FILE_LOAD);

		// delete it?
		// TODO
		if ("temp".equals(saveGame.nameWithoutExtension())) {
			SaveLoadManagement.delete("temp");
		}
		// TODO
		if ("crash".equals(saveGame.nameWithoutExtension())) {
			SaveLoadManagement.delete("crash");
		}

	}

}

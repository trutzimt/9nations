/**
 * 
 */
package de.ninenations.saveload;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.Json;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YSplitTab;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class SaveTab extends YSplitTab<YSaveInfo> {

	private VisTextButton delete, save;
	private boolean exitAfterSave;
	private static final String NEWSAVETEXT = "Create new save";

	public SaveTab(boolean exitAfterSave) {
		super("Save Game", "At the moment you have no save games.");

		this.exitAfterSave = exitAfterSave;

		resetElements();

		// add button
		save = new VisTextButton(NEWSAVETEXT);
		save.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				doubleClickElement(null);

			}
		});
		buttonBar.addActor(save);

		delete = new VisTextButton("Delete it");
		delete.addCaptureListener(new YChangeListener(true) {

			@Override
			public void changedY(Actor actor) {
				for (int i = 0; i < elements.size; i++) {
					if (elements.get(i).getUserObject() == active) {
						SaveLoadManagement.delete(active.getFile().nameWithoutExtension());
						elements.removeIndex(i);
						break;
					}
				}
				reset();
				YSounds.pClick();
				rebuild();

			}
		});
		buttonBar.addActor(delete);

		reset();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.ui.YSplitTab#rebuild()
	 */
	@Override
	protected void rebuild() {
		resetElements();
		super.rebuild();
	}

	public void resetElements() {
		elements.clear();
		// add all elements
		Json j = new Json();
		// add saves
		for (FileHandle f : SaveLoadManagement.getSaves()) {
			YSaveInfo q = j.fromJson(YSaveInfo.class, f.read());
			if (q.isHidden()) {
				continue;
			}

			q.setFile(f);
			addElement(q);

		}
		addElement("New Savegame", YIcons.getIconI(YIcons.FILE), null);
	}

	/**
	 * Reset the status
	 */
	@Override
	protected void reset() {
		super.reset();
		delete.setDisabled(true);
		save.setText(NEWSAVETEXT);
	}

	/**
	 * Place the selected artwork
	 */
	@Override
	protected void doubleClickElement(Button btn) {
		YSounds.pClick();
		// create new save?
		if (btn != null && btn.getUserObject() == null || active == null) {
			SaveLoadManagement.save((FileHandle) null, false);
		} else {
			SaveLoadManagement.save(active.getFile(), false);
		}

		if (exitAfterSave) {
			Gdx.app.exit();
		} else {
			window.close();
		}

	}

	@Override
	protected void clickElement(Button btn) {
		save.setText(btn.getUserObject() == null ? NEWSAVETEXT : "Overwrite " + ((YSaveInfo) btn.getUserObject()).getName());
		delete.setDisabled(btn.getUserObject() == null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.yaams.ui.YSplitTab#getInfoPanel(com.badlogic.gdx.scenes.scene2d.ui
	 * .Button)
	 */
	@Override
	protected Actor getInfoPanel(YSaveInfo o) {
		if (o == null) {
			return new VisLabel("");
		}
		return o.getInfoPanel();
	}
}
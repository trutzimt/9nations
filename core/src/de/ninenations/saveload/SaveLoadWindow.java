/**
 * 
 */
package de.ninenations.saveload;

import de.ninenations.core.NN;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YTabWindow;

/**
 * @author sven
 *
 */
public class SaveLoadWindow extends YTabWindow {

	/**
	 * @param name
	 */
	public SaveLoadWindow(boolean load, boolean exitAfterSave) {
		super("Save & Load");

		if (NN.screen() instanceof MapScreen) {
			tabbedPane.add(new SaveTab(exitAfterSave));
		}
		if (!exitAfterSave) {
			tabbedPane.add(new LoadTab());
		}

		buildIt();

		if (load && tabbedPane.getTabs().size >= 2) {
			tabbedPane.switchTab(tabbedPane.getTabs().size - 1);
		}

		addTitleIcon(YIcons.getIconI(YIcons.FILE));
	}

}

/**
 *
 */
package de.ninenations.saveload;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.ObjectMap;

import de.ninenations.core.NN;
import de.ninenations.endlessgame.EndlessGameScenario;
import de.ninenations.scenarios.CampaignMgmt;
import de.ninenations.ui.IDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class YSaveInfo implements IDisplay, java.io.Serializable {

	private static final long serialVersionUID = -3168521794619759256L;

	private ObjectMap<String, String> data;
	private double version;
	private boolean hidden;
	private transient FileHandle file;

	/**
	 * @param data
	 */
	public YSaveInfo() {
		super();
		data = new ObjectMap<>();
	}

	/**
	 * @return the version
	 */
	public double getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public void setVersion(double version) {
		this.version = version;
	}

	/**
	 * @return the hidden
	 */
	public boolean isHidden() {
		return hidden;
	}

	/**
	 * Get a description
	 *
	 * @return
	 */
	public String getDesc() {
		String c = data.get("campaign");
		String s = data.get("scenario");

		return CampaignMgmt.existCampaign(c) && CampaignMgmt.getCampaign(c).existScenario(s) ? CampaignMgmt.getCampaign(c).getScenario(s).getName()
				: "Endless game";

	}

	/**
	 * @param hidden
	 *            the hidden to set
	 */
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	@Override
	public YTable getInfoPanel() {
		YTable table = new YTable();
		if (file.nameWithoutExtension().length() > 4) {
			table.addL("File", getName());
		}

		if (data.containsKey("player")) {
			table.addL("Player", data.get("player"));
		}

		if (data.containsKey("round")) {
			table.addL("Round", data.get("round"));
		}

		// TODO show it
		// table.addL("Museum", settings.get("museum"));
		// table.addL("Time", settings.get("time") + ("1".equals(settings.get("time")) ?
		// " Day" : " Days"));

		String c = data.get("campaign");
		String s = data.get("scenario");

		if (EndlessGameScenario.ID.equals(s)) {
			table.addL("Scenario", "Endless game");
		} else if (CampaignMgmt.existCampaign(c)) {
			table.addL("Campaign", CampaignMgmt.getCampaign(c).getName());
			if (CampaignMgmt.getCampaign(c).existScenario(s)) {
				table.addL("Scenario", CampaignMgmt.getCampaign(c).getScenario(s).getName());
			}
		} else {
			table.addL("Game", s);
		}

		if (file != null) {
			table.addL("Saved", NN.plattform().formatDate(file.lastModified()));
			if (NSettings.isDebug()) {
				table.addL("Debug Path", file.path());
				NN.plattform().showSystemSavePath(table, file);
			}

		}

		return table;
	}

	/**
	 * @return the file
	 */
	public FileHandle getFile() {
		return file;
	}

	/**
	 * @return the file
	 */
	@Override
	public String getName() {
		return file.nameWithoutExtension().length() < 4 ? NN.plattform().formatDate(file.lastModified()) : file.nameWithoutExtension();
	}

	/**
	 * @param file
	 *            the file to set
	 */
	public void setFile(FileHandle file) {
		this.file = file;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.FILE);
	}

	/**
	 * @return the data
	 */
	public ObjectMap<String, String> getData() {
		return data;
	}

}

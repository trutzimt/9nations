/**
 * 
 */
package de.ninenations.quests;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.actions.base.GAction;
import de.ninenations.core.NArray;
import de.ninenations.game.S;
import de.ninenations.player.Player;
import de.ninenations.ui.IDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.window.YNotificationSaver;
import de.ninenations.util.NSettings;
import de.ninenations.util.YLog;

/**
 * @author sven
 *
 */
public class BaseQuestItem implements IDisplay, Serializable {

	private static final long serialVersionUID = -310899873495116782L;

	public enum QuestStatus {
		PRESTART, RUNNING, PAUSE, FINISH;
	}

	protected NArray<BaseReq> req;
	protected NArray<GAction> actions, preActions;
	protected String title, desc;
	protected QuestStatus status;
	protected boolean showDetail;

	/** 
	 * 
	 */
	public BaseQuestItem() {
		req = new NArray<>();
		actions = new NArray<>();
		preActions = new NArray<>();
		status = QuestStatus.PRESTART;
		showDetail = true;
	}

	/** 
	 * 
	 */
	public BaseQuestItem(String title) {
		this();
		this.title = title;
	}

	/**
	 * Update the quest
	 * 
	 * @return true > finish, false otherwise
	 */
	public boolean performAtRoundBegin(Player player) {
		if (status != QuestStatus.FINISH) {
			YLog.log("Quest", player.getName(), "Update " + getName() + " " + status);
		}

		if (status == QuestStatus.FINISH) {
			return true;
		}

		// take a break?
		if (status == QuestStatus.PAUSE) {
			return false;
		}

		// need to start perform?
		if (status == QuestStatus.PRESTART) {
			for (GAction bqa : preActions) {
				bqa.perform(player, null, -1, -1);
			}

			status = QuestStatus.RUNNING;
			player.addInfo(new YNotificationSaver("New quest " + getName(), getIcon(), YIcons.QUEST));
		}

		if (!checkReq(player)) {
			return false;
		}

		// quest is fullfilled
		for (GAction bqa : actions) {
			bqa.perform(player, null, -1, -1);
		}

		// inform player
		player.addInfo(new YNotificationSaver("Finish quest " + getName(), getIcon(), YIcons.QUEST));

		status = QuestStatus.FINISH;
		return true;
	}

	/**
	 * Check the req
	 * 
	 * @return true, will pass
	 */
	public boolean checkReq(Player player) {
		// ask the items
		for (BaseReq r : req) {
			if (!r.checkReq(player, null, -1, -1)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public BaseQuestItem setTitle(String title) {
		this.title = title;
		return this;
	}

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	@Override
	public YTable getInfoPanel() {

		YTable table = new YTable();

		if (title != null) {
			table.addL("Title", title);
		}

		if (showDetail && desc != null) {
			table.addH("Description");

		}
		if (desc != null) {
			VisLabel l = new VisLabel(desc);
			l.setWrap(true);
			table.add(l).fill().colspan(2).row();
		}

		if (showDetail) {
			if (req.size > 0) {
				table.addH("Tasks");
				for (BaseReq r : req) {
					r.addToTable(table, S.actPlayer(), null, -1, -1);
				}
			}
			if (NSettings.isDebug()) {
				table.addL("Status", status.toString());
			}
			if (actions.size > 0) {
				table.addH("Actions");
				for (GAction a : actions) {
					table.addI(a.getIcon(), a.getName());
				}
			}
		}

		return table;
	}

	/**
	 * @param desc
	 *            the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @return the req
	 */
	public BaseQuestItem addReq(BaseReq e) {
		req.add(e);
		return this;
	}

	/**
	 * @return the actions
	 */
	public BaseQuestItem addAction(GAction bqa) {
		actions.add(bqa);
		return this;
	}

	/**
	 * @return the preActions
	 */
	public BaseQuestItem addPreAction(GAction bqa) {
		preActions.add(bqa);
		return this;
	}

	/**
	 * @return the status
	 */
	public QuestStatus getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(QuestStatus status) {
		this.status = status;
	}

	/**
	 * @return the icon
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.QUEST);
	}

	/**
	 * @param showDetail
	 *            the showDetail to set
	 */
	public void setShowDetail(boolean showDetail) {
		this.showDetail = showDetail;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getName()
	 */
	@Override
	public String getName() {
		return (status == QuestStatus.FINISH ? "(v) " : "") + (title == null ? req.get(0).getDesc() : title);
	}

}

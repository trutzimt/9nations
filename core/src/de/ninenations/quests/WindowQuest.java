package de.ninenations.quests;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.ninenations.game.S;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YSplitTab;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.window.YTabWindow;

public class WindowQuest extends YTabWindow {

	public WindowQuest() {
		super("Quests");

		if (S.actPlayer().getQuests().size != 1) {
			tabbedPane.add(new OverviewTab());
		}
		// add quests
		for (final BaseQuest quest : S.actPlayer().getQuests()) {
			tabbedPane.add(new QuestTab(quest));
		}

		buildIt();

		addTitleIcon(YIcons.getIconI(YIcons.QUEST));
	}

	public class OverviewTab extends Tab {

		public OverviewTab() {
			super(false, false);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getTabTitle()
		 */
		@Override
		public String getTabTitle() {
			return "Overview";
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getContentTable()
		 */
		@Override
		public Table getContentTable() {

			YTable content = new YTable();

			// has quests?
			if (S.actPlayer().getQuests().size == 0) {
				content.add(new VisLabel("At the moment there are no quests."));
				return content;
			}

			// list every quest
			for (final BaseQuest quest : S.actPlayer().getQuests()) {
				content.add(new VisLabel(quest.getName())).align(Align.left).growX().row();

			}

			return content;
		}
	}

	public class QuestTab extends YSplitTab<BaseQuestItem> {

		protected BaseQuest quest;

		public QuestTab(BaseQuest quest) {
			super(quest.getName(), "This Task has nothing to do at the moment.");

			// list every quest
			for (final BaseQuestItem item : quest.getVisibleItems()) {
				addElement(item);
			}

		}

		@Override
		protected void doubleClickElement(Button btn) {}
	}
}

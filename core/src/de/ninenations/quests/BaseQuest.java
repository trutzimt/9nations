/**
 * 
 */
package de.ninenations.quests;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

import de.ninenations.player.Player;
import de.ninenations.quests.BaseQuestItem.QuestStatus;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class BaseQuest extends BaseDisplay implements Serializable {

	private static final long serialVersionUID = -8232159926961219625L;

	private Array<BaseQuestItem> items;
	protected QuestStatus status;

	@SuppressWarnings("unused")
	private BaseQuest() {
		super();
	}

	/**
	 * 
	 */
	public BaseQuest(String type, String title) {
		super(type, title);
		items = new Array<>();

	}

	/**
	 * Update the quest
	 * 
	 * @return
	 */
	public boolean performAtRoundBegin(Player player) {
		if (status == QuestStatus.FINISH) {
			return true;
		}

		// take a break?
		if (status == QuestStatus.PAUSE) {
			return false;
		}

		// ask the items
		for (BaseQuestItem r : items) {
			if (!r.performAtRoundBegin(player)) {
				return false;
			}
		}

		status = QuestStatus.FINISH;
		return true;
	}

	/**
	 * @return the items
	 */
	public Array<BaseQuestItem> getItems() {
		return items;
	}

	/**
	 * @return the items
	 */
	public Array<BaseQuestItem> getVisibleItems() {
		Array<BaseQuestItem> l = new Array<>();
		boolean nextB = false;
		for (BaseQuestItem r : items) {
			if (r.getStatus() == QuestStatus.FINISH) {
				l.add(r);
			} else if (!nextB) {
				l.add(r);
				nextB = true;
			} else {
				break;
			}
		}

		return l;
	}

	/**
	 * @return the items
	 */
	public BaseQuest addItem(BaseQuestItem bqi) {
		items.add(bqi);
		return this;
	}

	/**
	 * @return the title
	 */
	@Override
	public String getName() {
		return (status == QuestStatus.FINISH ? "(v) " : "") + name;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.QUEST);
	}

	@Override
	public YTable getInfoPanel() {
		YTable table = new YTable();
		table.addL("Name", getName());
		if (NSettings.isDebug()) {
			table.addL("Status", status.toString());
		}

		table.addH("Steps");
		for (BaseQuestItem i : getVisibleItems()) {
			table.addI(i.getIcon(), i.getName());
		}

		return table;
	}

}

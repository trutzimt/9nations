/**
 *
 */
package de.ninenations.quests;

import de.ninenations.actions.action.ActionEndGame;
import de.ninenations.actions.req.ReqMinMaxObj;
import de.ninenations.actions.req.ReqPlayerRess;
import de.ninenations.actions.req.ReqRound;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class QuestGenerator {

	private QuestGenerator() {};

	/**
	 * Generate a empty lose quest
	 *
	 * @return
	 */
	public static BaseQuestItem lose() {
		return new BaseQuestItem().setTitle("Lose the game").addAction(new ActionEndGame(false));
	}

	/**
	 * Generate a lose quest for a special unit
	 *
	 * @param type
	 * @return
	 */
	public static BaseQuestItem loseUnit(String type) {
		return lose().addReq(new ReqMinMaxObj(true, 0, EMapData.UNIT, type, true));
	}

	/**
	 * Generate a lose quest for a special unit
	 *
	 * @param type
	 * @return
	 */
	public static void loseLeader(Player p) {
		p.addQuest(loseUnit(p.getNation().getLeader()));
	}

	/**
	 * Generate a lose quest for the time
	 *
	 * @param rounds
	 * @return
	 */
	public static BaseQuestItem loseTime(int rounds) {
		return lose().addReq(new ReqRound(false, rounds));
	}

	/**
	 * Generate a empty win quest
	 *
	 * @return
	 */
	public static BaseQuestItem win() {
		return new BaseQuestItem().setTitle("Win the game").addAction(new ActionEndGame(true));
	}

	/**
	 * Generate a win quest for ressources
	 *
	 * @return
	 */
	public static BaseQuestItem winRess(String type, int count) {
		return win().addReq(new ReqPlayerRess(false, type, count));
	}

}

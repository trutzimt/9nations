/**
 *
 */
package de.ninenations.scenarios;

import java.util.ArrayList;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Tree.Node;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.core.IMapObject;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.IDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.YError;

/**
 * @author sven
 *
 */
public class BaseCampaign extends BaseDisplay implements IMapObject, IDisplay {

	protected String desc;
	protected ArrayList<BaseScenario> scenarios;
	protected boolean isCampaign;

	/**
	 * @param id
	 * @param name
	 * @param desc
	 */
	public BaseCampaign(String id, String name) {
		this(id, name, "No Desc");
	}

	/**
	 * @param id
	 * @param title
	 * @param desc
	 */
	public BaseCampaign(String id, String title, String desc) {
		super(id, title);
		this.desc = desc;
		scenarios = new ArrayList<>();

		isCampaign = true;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Add a new Scenario
	 *
	 * @param scen
	 */
	public void addScenario(BaseScenario scen) {
		scenarios.add(scen);
		scen.setCamp(this);
	}

	/**
	 * @return the scenarios
	 */
	public ArrayList<BaseScenario> getScenarios() {
		return scenarios;
	}

	/**
	 * Exist the scencario?
	 *
	 * @param id
	 * @return
	 */
	public boolean existScenario(String id) {
		for (BaseScenario b : scenarios) {
			if (b.getType().equals(id)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @return the scenario
	 */
	public BaseScenario getScenario(String id) {
		for (BaseScenario b : scenarios) {
			if (b.getType().equals(id)) {
				return b;
			}
		}
		YError.error(new IllegalArgumentException("Scenario " + id + " not exist"), false);
		return null;
	}

	/**
	 * Get a node for the level tree
	 *
	 * @return
	 */
	public Node getNode() {
		Node n = new Node(new VisLabel(getName()));
		n.setObject(this);

		boolean breakNext = false;

		// build child
		for (BaseScenario scen : getScenarios()) {
			if (breakNext) {
				break;
			}

			if (isCampaign && !scen.isWon()) {
				breakNext = true;
			}

			n.add(scen.getNode());
		}

		return n;
	}

	/**
	 * @return the isCampaign
	 */
	public boolean isCampaign() {
		return isCampaign;
	}

	/**
	 * @param isCampaign
	 *            the isCampaign to set
	 */
	public void setCampaign(boolean isCampaign) {
		this.isCampaign = isCampaign;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.WARNING);
	}

	@Override
	public YTable getInfoPanel() {
		YTable t = new YTable();
		t.addH("Description");
		t.addT(desc);

		return t;
	}
}

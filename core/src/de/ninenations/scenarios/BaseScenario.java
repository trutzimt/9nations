/**
 *
 */
package de.ninenations.scenarios;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Tree.Node;
import com.badlogic.gdx.utils.ObjectMap;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.core.NN;
import de.ninenations.game.ScenConf;
import de.ninenations.game.screen.MapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class BaseScenario extends BaseDisplay {

	protected String mapLink;
	protected String desc;
	protected BaseCampaign camp;
	protected ObjectMap<String, Object> baseData;
	protected ObjectMap<ScenConf, Boolean> conf;
	protected String humanNation;
	protected boolean allowSelectNation;
	protected long randomSeed;

	/**
	 *
	 * @param mapLink
	 */
	public BaseScenario(String id, String title, String mapLink) {
		super(id, title);
		this.mapLink = mapLink;
		baseData = new ObjectMap<>();
		conf = new ObjectMap<>();
		desc = "No Desc";
		allowSelectNation = true;
		randomSeed = NN.random().nextLong();

	}

	/**
	 * Set the settings for this game, need to load correctly the other datas
	 * 
	 * @param scenarioSettings
	 */
	public void setScenarioSettings(MapData data) {
		data.setP("campaign", camp.getType());
		data.setP("scenario", type);
		data.setP("map", mapLink);
		data.setConf(conf);

	}

	/**
	 * Overwrite to implement own scenario logic, Will call on start
	 */
	public void firstStart() {}

	/**
	 * Overwrite to implement own scenario logic, Will call, after every load
	 */
	public void afterLoad() {}

	/**
	 * Overwrite to implement own scenario logic
	 */
	public void nextRound() {}

	/**
	 * @return the mapLink
	 */
	public String getMapLink() {
		return mapLink;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * Get a node for the level tree
	 *
	 * @return
	 */
	public Node getNode() {
		Node n = new Node(new VisLabel(isWon() ? "(x) " + getName() : getName()));
		n.setObject(this);
		return n;
	}

	/**
	 * @return the id
	 */
	public String getFullId() {
		return camp.getType() + "." + getType();
	}

	/**
	 * @return the camp
	 */
	public BaseCampaign getCamp() {
		return camp;
	}

	/**
	 * @param camp
	 *            the camp to set
	 */
	public void setCamp(BaseCampaign camp) {
		this.camp = camp;
	}

	/**
	 * Load the settings, will call after loading
	 *
	 * @param map
	 */
	public void load(MapScreen map) {}

	/**
	 * Set if the scenario is won
	 * 
	 * @return
	 */
	public boolean isWon() {
		return NSettings.getPref().getBoolean("scen." + getFullId(), false);
	}

	/**
	 * Set the scenario is won
	 */
	public void setWon() {
		NSettings.getPref().putBoolean("scen." + getFullId(), true);
		NSettings.save();
	}

	/**
	 * Load the basics for the game, before the game data are loaded.
	 */
	public void preLoad(MapScreen map) {}

	/**
	 * @return the baseData
	 */
	public ObjectMap<String, Object> getBaseData() {
		return baseData;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.WARNING);
	}

	@Override
	public YTable getInfoPanel() {
		YTable t = new YTable();
		t.addH("Description");
		t.addT(desc);
		if (isWon()) {
			t.addL("Won?", "yes");
		}
		return t;
	}

	protected void enableAllConf() {
		for (ScenConf s : ScenConf.values()) {
			conf.put(s, true);
		}
	}

	/**
	 * Will call to add assets for loading
	 */
	public void assetLoad() {}

	/**
	 * @return the conf
	 */
	public ObjectMap<ScenConf, Boolean> getConf() {
		return conf;
	}

	/**
	 * @return the humanNation
	 */
	public String getHumanNation() {
		return humanNation;
	}

	/**
	 * @param humanNation
	 *            the humanNation to set
	 */
	public void setHumanNation(String humanNation) {
		this.humanNation = humanNation;
	}

	/**
	 * @return the allowSelectNation
	 */
	public boolean isAllowSelectNation() {
		return allowSelectNation;
	}

	/**
	 * @return the randomSeed
	 */
	public long getRandomSeed() {
		return randomSeed;
	}

	/**
	 * @param randomSeed
	 *            the randomSeed to set
	 */
	public void setRandomSeed(long randomSeed) {
		this.randomSeed = randomSeed;
	}
}

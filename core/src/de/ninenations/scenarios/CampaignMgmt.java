/**
 *
 */
package de.ninenations.scenarios;

import com.badlogic.gdx.utils.ObjectMap.Keys;

import de.ninenations.core.NN;
import de.ninenations.core.NObjectMap;
import de.ninenations.scenarios.beowolf.BeowulfCampaign;
import de.ninenations.scenarios.sandbox.SandboxCampaign;
import de.ninenations.scenarios.single.SingleScenarioCampaign;
import de.ninenations.scenarios.tut.TutCampaign;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class CampaignMgmt {

	private NObjectMap<BaseCampaign> camps;

	/**
	 * Load all campaigns
	 */
	public CampaignMgmt() {
		camps = new NObjectMap<>("campaign");

		addCamp(new SingleScenarioCampaign());
		addCamp(new TutCampaign());
		addCamp(new BeowulfCampaign());
		//
		if (NSettings.isDebug()) {
			addCamp(new SandboxCampaign());
		}
	}

	/**
	 * Add the campaign
	 *
	 * @param cam
	 */
	private void addCamp(BaseCampaign cam) {
		camps.put(cam.getType(), cam);
	}

	/**
	 * Add the campaign
	 *
	 * @param cam
	 */
	public static void addCampaign(BaseCampaign cam) {
		NN.camps().addCamp(cam);
	}

	/**
	 * Exist Campaign
	 *
	 * @param id
	 * @return
	 */
	public static boolean existCampaign(String id) {
		// exist campaign?
		return NN.camps().camps.containsKey(id);
	}

	/**
	 * Get a specific Campaign
	 *
	 * @param id
	 * @return
	 */
	public static BaseCampaign getCampaign(String id) {
		return NN.camps().camps.getS(id);
	}

	/**
	 * Get the Campaign Ids
	 *
	 * @return
	 */
	public static Keys<String> getCampaignIds() {
		return NN.camps().camps.keys();
	}
}

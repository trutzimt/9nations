/**
 *
 */
package de.ninenations.scenarios.sandbox;

import de.ninenations.scenarios.BaseCampaign;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class SandboxCampaign extends BaseCampaign {

	/**
	 */
	public SandboxCampaign() {
		super("sandbox", "Sandbox");

		if (NSettings.isDebug()) {
			addScenario(new DebugScenario());
		}

	}

}

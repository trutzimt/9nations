/**
 * 
 */
package de.ninenations.scenarios.sandbox;

import de.ninenations.actions.req.ReqPlayer;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.player.Player.RessType;
import de.ninenations.quests.QuestGenerator;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.ScenarioHelper;
import de.ninenations.towns.Town;

/**
 * @author sven
 *
 */
public class DebugScenario extends BaseScenario {

	/**
	 * @param id
	 * @param title
	 * @param mapLink
	 */
	public DebugScenario() {
		super("debug", "Debug Scenario", "map/debug/debug.tmx");
		enableAllConf();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.scenarios.BaseScenario#start()
	 */
	@Override
	public void firstStart() {

		Player p = ScenarioHelper.addHuman("north");

		Town t = MapScreen.get().getData().getTowns().add("test", p, 7, 5);
		t.setMaxStorage(2000);
		// t.setLevel(3);
		t.addRess(BaseRess.GOLD, 50, RessType.GIFT);
		t.addRess(BaseRess.WOOD, 100, RessType.GIFT);
		t.addRess(BaseRess.STONE, 100, RessType.GIFT);
		MapScreen.get().getData().getOnMap().addB("ntownhall", p, t, 7, 5);
		MapScreen.get().getData().getOnMap().addB("nresearch", p, t, 8, 5);

		ScenarioHelper.unit(p.getNation().getLeader(), p, t, 7, 5);

		ScenarioHelper.unit("nworker", p, t, 6, 6);
		ScenarioHelper.unit("nsettler", p, t, 6, 7);
		ScenarioHelper.unit("nship", p, t, 5, 6);

		p.addQuest(QuestGenerator.loseUnit(p.getNation().getLeader()));
		p.addQuest(QuestGenerator.win().addReq(new ReqPlayer(true, 1)));

		Player p2 = ScenarioHelper.addHuman("ranger");
		p2.setName("p2");

		p2.addQuest(QuestGenerator.loseUnit(p2.getNation().getLeader()));
		p2.addQuest(QuestGenerator.win().addReq(new ReqPlayer(true, 1)));
		ScenarioHelper.unit(p2.getNation().getLeader(), p2, t, 8, 5);

	}

}

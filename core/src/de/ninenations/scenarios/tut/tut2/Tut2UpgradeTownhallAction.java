package de.ninenations.scenarios.tut.tut2;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

public class Tut2UpgradeTownhallAction extends GAction {
	private static final long serialVersionUID = 794213978874849611L;

	public Tut2UpgradeTownhallAction() {
		super("upgrade", "Upgrade Townhall");
	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		player.addInfo(new YNotificationSaver(Tut2Scenario.getLang()[7], S.nData().getB("ntownhall2").getIcon(), YIcons.BUILD).setTime(-1));
		return false;
	}

	@Override
	public Image getIcon() {
		return S.nData().getB("ntownhall2").getIcon();
	}
}
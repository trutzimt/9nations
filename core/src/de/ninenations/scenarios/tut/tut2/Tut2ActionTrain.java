package de.ninenations.scenarios.tut.tut2;

import de.ninenations.actions.action.ActionTrain;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

public class Tut2ActionTrain extends ActionTrain {

	private static final long serialVersionUID = -2909393103297193350L;

	public Tut2ActionTrain() {
		super(true, "nworker");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		if (S.onMap().countOnMapObjByPlayerType(player, EMapData.UNIT, "nworker") == 0) {
			player.addInfo(new YNotificationSaver(Tut2Scenario.getLang()[2], S.nData().getU("nworker").getIcon(), YIcons.TRAIN).setTime(-1));
			player.addInfo(new YNotificationSaver(Tut2Scenario.getLang()[5], YIcons.NEXTPLAYER).setTime(-1));
		}
		return super.perform(player, onMap, x, y);
	}

}
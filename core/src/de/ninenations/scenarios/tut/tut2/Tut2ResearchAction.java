package de.ninenations.scenarios.tut.tut2;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

public class Tut2ResearchAction extends GAction {
	private static final long serialVersionUID = 794213978874849611L;

	public Tut2ResearchAction() {
		super("research", "Research the upgrade");
	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		player.addInfo(new YNotificationSaver(Tut2Scenario.getLang()[6], YIcons.RESEARCH).setTime(-1));
		return false;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.RESEARCH);
	}
}
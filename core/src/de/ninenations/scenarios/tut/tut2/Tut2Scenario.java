package de.ninenations.scenarios.tut.tut2;

import com.badlogic.gdx.utils.Array;

import de.ninenations.actions.action.ActionEndGame;
import de.ninenations.actions.base.GAction;
import de.ninenations.actions.req.ReqMinMaxObj;
import de.ninenations.actions.req.ReqResearch;
import de.ninenations.data.nations.BaseNation;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.player.Player;
import de.ninenations.quests.BaseQuest;
import de.ninenations.quests.BaseQuestItem;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.ScenarioHelper;
import de.ninenations.towns.Town;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;
import de.ninenations.util.NSettings;

public class Tut2Scenario extends BaseScenario {

	public Tut2Scenario() {
		super("2", "Advanced Tutorial", "map/tut1.tmx");

		desc = "Learn about research, units and upgrades.";
		desc += " The tutorial was writen for version 0.09. You have " + NSettings.VERSION + ". Maybe something changed.";

		allowSelectNation = false;

		enableAllConf();
		conf.put(ScenConf.WEALTH, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.scenarios.BaseScenario#start()
	 */
	@Override
	public void firstStart() {
		final String queen = S.nData().getN(BaseNation.NORTH).getLeader();

		// edit townhall
		deleteAction(S.nData().getB("ntownhall").getActiveActions(), "train");
		S.nData().getB("ntownhall").getActiveActions().insert(0, new Tut2ActionTrain());

		// add player
		Player p = ScenarioHelper.addHuman(BaseNation.NORTH);
		ScenarioHelper.unitI(queen, p, null, 13, 9);

		// add town + building
		Town t = ScenarioHelper.townI(p, 14, 8);
		ScenarioHelper.buildI("nlogger", p, t, 12, 9);
		ScenarioHelper.buildI("nstorage", p, t, 13, 9);
		ScenarioHelper.buildI("nfarm", p, t, 14, 9);
		ScenarioHelper.buildI("nhouse", p, t, 13, 8);

		// add first info
		final String[] l = getLang();
		p.addInfo(new YNotificationSaver(l[0], YIcons.LOGO).setTime(-1));
		p.addInfo(new YNotificationSaver(l[1], null, YIcons.TRAIN).setTime(-1));
		BaseQuest b = new BaseQuest("1", getName());

		// train worker
		BaseQuestItem i = new BaseQuestItem("Train a worker");
		i.setDesc(l[1] + l[2] + l[3] + l[5]);
		i.addReq(new ReqMinMaxObj(false, 1, EMapData.UNIT, "nworker", true));
		i.addAction(new Tut2QuarryAction());
		b.addItem(i);

		// build quarry
		i = new BaseQuestItem("Build a quarry");
		i.setDesc(l[3] + l[5]);
		i.addReq(new ReqMinMaxObj(false, 1, EMapData.BUILDING, "nquarry", true));
		i.addAction(new Tut2LibraryAction());
		b.addItem(i);

		// build libary
		i = new BaseQuestItem("Build a library");
		i.setDesc(l[4] + l[5]);
		i.addReq(new ReqMinMaxObj(false, 1, EMapData.BUILDING, "nlibrary", true));
		i.addAction(new Tut2ResearchAction());
		b.addItem(i);

		// research upgrade
		i = new BaseQuestItem("Research new Upgrade");
		i.setDesc(l[6]);
		i.addReq(new ReqResearch("ntownhall2"));
		i.addAction(new Tut2UpgradeTownhallAction());
		b.addItem(i);

		// build upgrade
		i = new BaseQuestItem("Upgrade the townhall");
		i.setDesc(l[7]);
		i.addReq(new ReqMinMaxObj(false, 1, EMapData.BUILDING, "ntownhall2", true));
		i.addAction(new ActionEndGame(true));
		b.addItem(i);

		p.addQuest(b);

	}

	public static final String[] getLang() {
		return new String[] {
				"Welcome to " + NSettings.NAME + " " + NSettings.VERSION
						+ ". Your task is reach a new town level. Therefore you need to build a quarry, research the upgrade and build a new town hall.",
				"Normally your queen can not climb a mountain. Click on your townhall. Select from the right menu the train unit button. ",
				"Train a worker and finish your round. ", "Move with your worker on the mountain and build a quarry. ",
				"Now you have the ressources to build a library. Move to a free field and build it.",
				/* 5 */"If you can not finish a task, finish your round. ",
				"Your libary produce research points. Select the new research menu point from the right top menu. The new window will show the areas where you can gain new experience. Pick the area 'Life' and wait a few rounds. ",
				"If your researchers have finished experimenting, you should have research the bigger village hall. Select your town hall and click at the bottom left to upgrade it. Then win the game." };
	}

	/**
	 * Remove the action with this id
	 * 
	 * @param actions
	 * @param key
	 */
	public static void deleteAction(Array<GAction> actions, String key) {
		for (int i = 0; i < actions.size; i++) {
			if (actions.get(i).getType().equals(key)) {
				actions.removeIndex(i);
				return;
			}
		}
	}
}

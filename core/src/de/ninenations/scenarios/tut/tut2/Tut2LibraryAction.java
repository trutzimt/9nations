package de.ninenations.scenarios.tut.tut2;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.action.ActionProduce;
import de.ninenations.actions.base.GAction;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

public class Tut2LibraryAction extends GAction {
	private static final long serialVersionUID = 794213978874849611L;

	public Tut2LibraryAction() {
		super("build", "Build a library");
	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		S.nData().getB("nlibrary").getPassiveActions().add(new ActionProduce(false, false, BaseRess.RESEARCH, 20));
		player.addInfo(new YNotificationSaver(Tut2Scenario.getLang()[4], S.nData().getB("nlibrary").getIcon(), YIcons.BUILD).setTime(-1));
		return false;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.BUILD);
	}
}
/**
 *
 */
package de.ninenations.scenarios.tut;

import de.ninenations.scenarios.BaseCampaign;
import de.ninenations.scenarios.tut.tut1.Tut1Scenario;
import de.ninenations.scenarios.tut.tut2.Tut2Scenario;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class TutCampaign extends BaseCampaign {

	/**
	 */
	public TutCampaign() {
		super("tut", "Tutorial", "Learn the basics of " + NSettings.NAME);

		addScenario(new Tut1Scenario());
		addScenario(new Tut2Scenario());
	}

}

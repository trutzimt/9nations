package de.ninenations.scenarios.tut.tut1;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

public class Tut1MessageAction extends GAction {
	private static final long serialVersionUID = 794213978874849611L;

	public Tut1MessageAction() {
		super("mess", "Last message");
	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		player.addInfo(new YNotificationSaver(Tut1Scenario.getLang()[14], YIcons.LOGO).setTime(-1));
		return false;

	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.BUILD);
	}
}
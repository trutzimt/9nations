package de.ninenations.scenarios.tut.tut1;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

public class Tut1BuildLoggerAction extends GAction {
	private static final long serialVersionUID = 794213978874849611L;

	public Tut1BuildLoggerAction() {
		super("found", "Build a logger and a quarry");
	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		player.addInfo(new YNotificationSaver(Tut1Scenario.getLang()[12], S.nData().getB("nlogger").getIcon(), YIcons.BUILD).setTime(-1));
		player.addInfo(new YNotificationSaver(Tut1Scenario.getLang()[13], S.nData().getB("nquarry").getIcon(), YIcons.BUILD).setTime(-1));
		return false;

	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.BUILD);
	}
}
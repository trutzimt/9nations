package de.ninenations.scenarios.tut.tut1;

import de.ninenations.actions.action.ActionBuild;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

public class TutActionBuild extends ActionBuild {

	private static final long serialVersionUID = -2909393103297193350L;

	public TutActionBuild() {
		super(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		if (S.onMap().countOnMapObjByPlayerType(player, EMapData.BUILDING, "nhunter") == 0) {
			player.addInfo(new YNotificationSaver(Tut1Scenario.getLang()[9], S.nData().getB("nhunter").getIcon(), YIcons.FOOD).setTime(-1));
			player.addInfo(new YNotificationSaver(Tut1Scenario.getLang()[10], null, YIcons.NEXTPLAYER).setTime(-1));
		}
		return super.perform(player, onMap, x, y);
	}

}
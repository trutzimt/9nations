package de.ninenations.scenarios.tut.tut1;

import de.ninenations.actions.action.ActionMove;
import de.ninenations.game.S;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

public class TutActionMove extends ActionMove {

	private static final long serialVersionUID = -2909393103297193350L;

	public TutActionMove() {}

	/**
	 * Get the clicks
	 */
	@Override
	public void performClick(NMapUnit activeSelected, int cost, int x, int y) {
		// first perform?
		if (S.town().getTownsByPlayerCount(S.actPlayer()) == 0) {
			S.actPlayer().addInfo(new YNotificationSaver(Tut1Scenario.getLang()[15], YIcons.LOGO).setTime(-1));
			S.actPlayer().addInfo(new YNotificationSaver(Tut1Scenario.getLang()[3], null, YIcons.NEXTPLAYER).setTime(-1));
		}

		super.performClick(activeSelected, cost, x, y);
	}

}
package de.ninenations.scenarios.tut.tut1;

import de.ninenations.actions.action.ActionFoundTown;
import de.ninenations.actions.req.ReqTown;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

public class TutActionFoundTown extends ActionFoundTown {

	private static final long serialVersionUID = -2909393103297193350L;

	public TutActionFoundTown() {
		super(true);
		addReq(new ReqTown(true, 0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.GAction#perform(de.ninenations.player.Player,
	 * de.ninenations.game.onmap.NOnMapObject, int, int)
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		if (S.town().getTownsByPlayerCount(player) == 0) {
			player.addInfo(new YNotificationSaver(Tut1Scenario.getLang()[6], null, YIcons.NEXTPLAYER).setTime(-1));
		}

		return super.perform(player, onMap, x, y);
	}

}
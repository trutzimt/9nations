package de.ninenations.scenarios.tut.tut1;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

public class Tut1BuildTentAction extends GAction {
	private static final long serialVersionUID = 794213978874849611L;

	public Tut1BuildTentAction() {
		super("buildtent", "Build a tent");
	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		S.nData().getU(player.getNation().getLeader()).setAp(15);
		player.addInfo(new YNotificationSaver(Tut1Scenario.getLang()[11], S.nData().getB("nhouse").getIcon(), YIcons.BUILD).setTime(-1));
		return false;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.BUILD);
	}
}
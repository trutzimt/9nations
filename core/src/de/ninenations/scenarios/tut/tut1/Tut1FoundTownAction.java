package de.ninenations.scenarios.tut.tut1;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

public class Tut1FoundTownAction extends GAction {
	private static final long serialVersionUID = 794213978874849611L;

	public Tut1FoundTownAction() {
		super("found", "Found a new town");
	}

	/**
	 * @param obj
	 */
	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		player.addInfo(new YNotificationSaver(Tut1Scenario.getLang()[5], null, YIcons.TOWN).setTime(-1));
		S.nData().getU(player.getNation().getLeader()).getActiveActions().add(new TutActionFoundTown());
		return false;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.TOWN);
	}
}
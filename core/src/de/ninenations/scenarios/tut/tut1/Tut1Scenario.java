package de.ninenations.scenarios.tut.tut1;

import de.ninenations.actions.action.ActionEndGame;
import de.ninenations.actions.req.ReqExplored;
import de.ninenations.actions.req.ReqMinMaxObj;
import de.ninenations.actions.req.ReqTown;
import de.ninenations.data.nations.BaseNation;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.player.Player;
import de.ninenations.quests.BaseQuest;
import de.ninenations.quests.BaseQuestItem;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.ScenarioHelper;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;
import de.ninenations.util.NSettings;
import de.ninenations.util.YLog;

public class Tut1Scenario extends BaseScenario {

	public Tut1Scenario() {
		super("1", "Beginner Tutorial", "map/tut1.tmx");

		desc = "Learn the basics of " + NSettings.NAME + ", like moving your units, build your buildings and win your first fight.";
		desc += " The tutorial was writen for version 0.09. You have " + NSettings.VERSION + ". Maybe something changed.";

		allowSelectNation = false;

		// set map data
		conf.put(ScenConf.FOG, true);
		conf.put(ScenConf.RESEARCH, true);
		conf.put(ScenConf.LOADSAVE, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.scenarios.BaseScenario#start()
	 */
	@Override
	public void firstStart() {
		final String queen = S.nData().getN(BaseNation.NORTH).getLeader();

		// edit queen
		S.nData().getU(queen).getActiveActions().clear();
		S.nData().getU(queen).getActiveActions().add(new TutActionMove());

		// remove buildings
		S.nData().removeB("nresearch");
		S.nData().getN(BaseNation.NORTH).getBuildings().removeValue("nresearch", false);
		YLog.log(S.nData().getN(BaseNation.NORTH).getBuildings().contains("nresearch", false));

		final String[] t = getLang();
		Player p = ScenarioHelper.addHuman(BaseNation.NORTH);
		ScenarioHelper.unitI(queen, p, null, 6, 10);

		BaseQuest b = new BaseQuest("1", getName());

		//
		p.addInfo(new YNotificationSaver(t[1], S.nData().getU(queen).getIcon(), -1).setTime(-1));
		p.addInfo(new YNotificationSaver(t[2], null, YIcons.WALK).setTime(-1));
		p.addInfo(new YNotificationSaver(t[4], null, YIcons.QUEST).setTime(-1));

		BaseQuestItem i = new BaseQuestItem("Movement");
		i.setDesc(t[1] + t[2] + t[3]);
		i.addReq(new ReqExplored(false, 8));
		i.addAction(new Tut1FoundTownAction());
		b.addItem(i);

		i = new BaseQuestItem().setTitle("Found a town");
		i.setDesc(t[5] + t[6]);
		i.addReq(new ReqTown(false, 1));
		i.addAction(new Tut1ClickBuildAction());
		b.addItem(i);

		i = new BaseQuestItem().setTitle("Build a hunter");
		i.setDesc(t[7] + t[8] + t[9] + t[10]);
		i.addReq(new ReqMinMaxObj(false, 1, EMapData.BUILDING, "nhunter", true));
		i.addAction(new Tut1BuildTentAction());
		b.addItem(i);

		i = new BaseQuestItem().setTitle("Build a tent");
		i.setDesc(t[7] + t[11]);
		i.addReq(new ReqMinMaxObj(false, 1, EMapData.BUILDING, "nhouse", true));
		i.addAction(new Tut1BuildLoggerAction());
		b.addItem(i);

		i = new BaseQuestItem().setTitle("Build a logger and a quarry");
		i.setDesc(t[7] + t[12] + t[13]);
		i.addReq(new ReqMinMaxObj(false, 1, EMapData.BUILDING, "nlogger", true));
		i.addReq(new ReqMinMaxObj(false, 1, EMapData.BUILDING, "nquarry", true));
		i.addAction(new ActionEndGame(true));
		i.addAction(new Tut1MessageAction());
		b.addItem(i);

		p.addQuest(b);

	}

	public static final String[] getLang() {
		return new String[] { "Welcome to " + NSettings.NAME + " " + NSettings.VERSION + ". Your first task is to found your city. ",
				"You see your queen. Your most important unit. Click on her. ",
				"Then you see on the bottom her actions, click on the walk icon and walk one field to the left. ",
				"You consume all your AP. Finish your round. Press the hourglass icon in the top right or space. ",
				"If you do not know, what to do, check your quests from upper right. ",
				/* 5 */"This is a perfect spot for a new town. Click on the icon and found it.",
				"You found your first town. Your Kingdom is based on different towns. Every town has his own building and ressources. Finish your round.",
				"You can only grow with enough ressources. The basics are food, wood and stones.",
				"Move your queen left to the grass, finish the round and then click the build action.",
				"Here is a perfect place for a hunter. Select food and then the hunter and build it.",
				/* 10 */"After the build wait, until it is finish (3x next Round).",
				"We have now food. For the city to grow, we need workers. Move to a free field and build a tent.",
				"We have enough worker. Go to a tree and build a logger for wood", "And to a hill and build a quarry for stone",
				"You learn to move a unit, found a town and build some buildings. Now its time to move forward and expand your kingdom.",
				/* 15 */NSettings.NAME + " is a turn based game. Every Unit has a his amount of AP (Action Point) per turn." };
	}
}

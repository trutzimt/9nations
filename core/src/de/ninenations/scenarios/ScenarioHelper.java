/**
 * 
 */
package de.ninenations.scenarios;

import de.ninenations.data.nations.BaseNation;
import de.ninenations.game.S;
import de.ninenations.game.map.NMapBuilding;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.player.Player;
import de.ninenations.towns.Town;
import de.ninenations.util.NGenerator;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class ScenarioHelper {

	/**
	 * 
	 */
	private ScenarioHelper() {}

	public static Player addHuman(String nation) {
		return MapScreen.get().getData().getPlayers().add("human", NSettings.getUserName(), nation == null ? BaseNation.NORTH : nation);
	}

	public static NMapUnit unit(String type, Player p, Town t, int x, int y) {

		// something on the field?
		if (MapScreen.get().getData().isUnit(x, y)) {
			throw new IllegalArgumentException("Their is a unit on " + x + "," + y + " already.");
		}

		return (NMapUnit) MapScreen.get().getData().getOnMap().addU(type, p, t, x, y).setFinish();
	}

	public static NMapUnit unitI(String type, Player p, Town t, int x, int y) {
		return unit(type, p, t, x, nY(y));
	}

	public static NMapBuilding build(String type, Player p, Town t, int x, int y) {
		// valid field?
		if (!S.valide(x, y)) {
			throw new IllegalArgumentException("The field " + x + "," + y + " is invalid.");
		}

		// something on the field?
		if (MapScreen.get().getData().isBuilding(x, y)) {
			throw new IllegalArgumentException("Their is a building on " + x + "," + y + " already.");
		}

		return (NMapBuilding) MapScreen.get().getData().getOnMap().addB(type, p, t, x, y).setFinish();
	}

	public static NMapBuilding buildI(String type, Player p, Town t, int x, int y) {
		return build(type, p, t, x, nY(y));
	}

	public static Town townI(Player p, int x, int y) {
		y = nY(y);
		Town t = MapScreen.get().getData().getTowns().add(NGenerator.getTown(), p, x, y);
		build(p.getNation().getTownhall(), p, t, x, y);
		return t;
	}

	public static int nY(int y) {
		return S.map().getHeight() - 1 - y;
	}
}

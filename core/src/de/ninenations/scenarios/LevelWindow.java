/**
 *
 */
package de.ninenations.scenarios;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Tree.Node;
import com.badlogic.gdx.scenes.scene2d.ui.Value;
import com.badlogic.gdx.scenes.scene2d.utils.Selection;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisSelectBox;
import com.kotcrab.vis.ui.widget.VisTable;
import com.kotcrab.vis.ui.widget.VisTree;

import de.ninenations.core.NN;
import de.ninenations.game.GameStartScreen;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YTextButton;
import de.ninenations.ui.window.YWindow;
import de.ninenations.util.YMusic;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class LevelWindow extends YWindow {

	private Label label;
	private VisTable labelHolder;
	private VisTree tree;
	private YTextButton button;
	private VisSelectBox<String> nations;

	private BaseScenario selected;

	/**
	 *
	 * @param qossire
	 */
	public LevelWindow() {
		super("Scenarios");

		tree = new VisTree();
		button = new YTextButton("Start it") {

			@Override
			public void perform() {
				startScenario(selected);
			}
		};
		button.setDisabled(true);

		// add all campaigns
		for (String id : CampaignMgmt.getCampaignIds()) {
			BaseCampaign cam = CampaignMgmt.getCampaign(id);

			tree.add(cam.getNode());
		}

		// show desc
		tree.addListener(new YChangeListener() {
			@Override
			public void changedY(Actor actor) {
				Selection<Node> y = ((VisTree) actor).getSelection();

				if (y.size() == 0) {
					// nothing new happend
					if (selected != null) {
						startScenario(selected);
					}
					return;
				}

				button.setDisabled(true);
				button.setText("Select a Scenario first");
				nations.setVisible(false);

				Object o = y.getLastSelected().getObject();

				if (o instanceof BaseScenario) {
					selected = (BaseScenario) o;
					button.setDisabled(false);
					button.setText("Start " + selected.getName());
					nations.setVisible(selected.isAllowSelectNation());

					labelHolder.clearChildren();
					labelHolder.add(selected.getInfoPanel()).grow();
				} else if (o instanceof BaseCampaign) {
					selected = null;
					BaseCampaign b = (BaseCampaign) o;
					((VisTree) actor).getSelection().getLastSelected().setExpanded(true);

					labelHolder.clearChildren();
					labelHolder.add(b.getInfoPanel()).grow();

				} else {
					selected = null;
					labelHolder.clearChildren();
					labelHolder.add(new VisLabel("Please select a scenario or campaign.")).grow();
				}

			}
		});

		setWidth(Gdx.graphics.getWidth() / 3 * 2);
		setHeight(Gdx.graphics.getHeight() / 3 * 2);

		VisTable content = new VisTable();

		// Add tree
		VisScrollPane scrollPane = new VisScrollPane(tree);
		scrollPane.setFlickScroll(false);
		scrollPane.setFadeScrollBars(false);
		content.add(scrollPane).align(Align.topLeft).width(Value.percentWidth(.3F, content)).growY();

		// Add description
		label = new VisLabel("Please select a scenario or campaign.");

		labelHolder = new VisTable();
		labelHolder.add(label).grow();

		// build nations
		Array<String> nation = new Array<>();
		nation.add("Select your nation");
		for (String n : NN.nData().getNations()) {
			nation.add(NN.nData().getN(n).getName());
		}

		// add nations
		nations = new VisSelectBox<>();
		nations.setItems(nation);
		nations.setVisible(false);

		content.add(labelHolder).align(Align.top).expand().growY().width(Value.percentWidth(.7F, content)).row();

		content.addSeparator().colspan(2);
		content.add(nations).align(Align.left);
		content.add(button).align(Align.right);

		add(content).grow();

	}

	/**
	 * Start it
	 *
	 * @param scen
	 */
	protected void startScenario(BaseScenario scen) {
		YSounds.play(YSounds.STARTGAME);
		YMusic.stop();
		if (nations.getSelectedIndex() > 0) {
			scen.setHumanNation(NN.nData().getNations().get(nations.getSelectedIndex() - 1));
		}
		NN.get().switchScreen(new GameStartScreen(scen));
	}

}

/**
 *
 */
package de.ninenations.scenarios.single;

import de.ninenations.actions.req.ReqMinMaxObj;
import de.ninenations.data.nations.BaseNation;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.player.Player;
import de.ninenations.quests.QuestGenerator;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.ScenarioHelper;

/**
 * @author sven
 *
 */
public class GoldenScenario extends BaseScenario {

	/**
	 */
	public GoldenScenario() {
		super("golden", "The golden palace", "map/field6.tmx");
		desc = "Research the golden palace and build it.";

		enableAllConf();

		allowSelectNation = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.scenarios.BaseScenario#start()
	 */
	@Override
	public void firstStart() {

		Player p = ScenarioHelper.addHuman(BaseNation.NORTH);
		ScenarioHelper.unitI(p.getNation().getLeader(), p, null, 12, 7);

		// win quest
		p.addQuest(QuestGenerator.win().addReq(new ReqMinMaxObj(false, 1, EMapData.BUILDING, "ngoldpalace", true)));

		// lose quest
		QuestGenerator.loseLeader(p);

	}

}

/**
 *
 */
package de.ninenations.scenarios.single;

import de.ninenations.actions.req.ReqPlayerRess;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.player.Player;
import de.ninenations.quests.QuestGenerator;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.ScenarioHelper;

/**
 * @author sven
 *
 */
public class HungerScenario extends BaseScenario {

	/**
	 */
	public HungerScenario() {
		super("hunger", "Hunger crisis", "map/tut1.tmx");
		desc = "Save the Hunger crisis and produce enough food";

		enableAllConf();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.scenarios.BaseScenario#start()
	 */
	@Override
	public void firstStart() {

		Player p = ScenarioHelper.addHuman(humanNation);
		ScenarioHelper.unitI(p.getNation().getLeader(), p, null, 10, 8);

		// win quest
		p.addQuest(QuestGenerator.win().addReq(new ReqPlayerRess(false, BaseRess.FOOD, 100)));

		// lose quest
		p.addQuest(QuestGenerator.loseTime(24));

	}

}

/**
 *
 */
package de.ninenations.scenarios.single;

import de.ninenations.actions.req.ReqExplored;
import de.ninenations.player.Player;
import de.ninenations.quests.QuestGenerator;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.ScenarioHelper;

/**
 * @author sven
 *
 */
public class ExploreTheWorldScenario extends BaseScenario {

	/** 
	 */
	public ExploreTheWorldScenario() {
		super("explore", "Explore the world", "map/field2.tmx");
		desc = "Explore the whole world. Who know, what you may be find?";

		enableAllConf();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.scenarios.BaseScenario#start()
	 */
	@Override
	public void firstStart() {

		Player p = ScenarioHelper.addHuman(humanNation);
		ScenarioHelper.unitI(p.getNation().getLeader(), p, null, 47, 61);

		// win quest
		p.addQuest(QuestGenerator.win().addReq(new ReqExplored(false, 9900)));

		// lose quest
		QuestGenerator.loseLeader(p);
	}

}

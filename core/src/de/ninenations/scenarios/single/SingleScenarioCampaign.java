/**
 *
 */
package de.ninenations.scenarios.single;

import de.ninenations.scenarios.BaseCampaign;

/**
 * @author sven
 *
 */
public class SingleScenarioCampaign extends BaseCampaign {

	/**
	 */
	public SingleScenarioCampaign() {
		super("single", "Single Senarios");

		isCampaign = false;

		addScenario(new HungerScenario());
		addScenario(new GoldenScenario());
		addScenario(new ExploreTheWorldScenario());
		addScenario(new NineCitiesScenario());
		addScenario(new TradingRouteScenario());

	}

}

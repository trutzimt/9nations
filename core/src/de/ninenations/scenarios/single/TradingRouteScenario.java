/**
 *
 */
package de.ninenations.scenarios.single;

import de.ninenations.actions.req.ReqMinMaxObj;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.player.Player;
import de.ninenations.quests.QuestGenerator;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.ScenarioHelper;
import de.ninenations.towns.Town;

/**
 * @author sven
 *
 */
public class TradingRouteScenario extends BaseScenario {

	/**
	 */
	public TradingRouteScenario() {
		super("trading", "Trading Route", "map/field2.tmx");
		desc = "To shorten times, a new trading route is to be built through an unknown land. The king has commissioned you with the implementation.";

		enableAllConf();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.scenarios.BaseScenario#start()
	 */
	@Override
	public void firstStart() {

		humanNation = "north";

		String street = humanNation.substring(0, 1) + "street";

		Player p = ScenarioHelper.addHuman(humanNation);
		p.getResearch().setResearch(street, true);

		// found north town
		Town n = ScenarioHelper.townI(p, 37, 4);
		ScenarioHelper.unitI(p.getNation().getLeader(), p, n, 37, 4);
		ScenarioHelper.unitI(humanNation.substring(0, 1) + "worker", p, n, 37, 5);

		// build street
		ScenarioHelper.buildI(street, p, n, 36, 0);
		ScenarioHelper.buildI(street, p, n, 36, 1);
		ScenarioHelper.buildI(street, p, n, 36, 2);
		ScenarioHelper.buildI(street, p, n, 37, 2);
		ScenarioHelper.buildI(street, p, n, 37, 3);
		ScenarioHelper.buildI(street, p, n, 38, 3);
		ScenarioHelper.buildI(street, p, n, 38, 4);
		ScenarioHelper.buildI(street, p, n, 38, 5);

		// S.map().getAutotile().removeTile(36, 99, "mountain");
		// S.map().getAutotile().removeTile(36, 98, "mountain");

		// found south town
		Town s = ScenarioHelper.townI(p, 77, 95);
		ScenarioHelper.unitI(humanNation.substring(0, 1) + "worker", p, s, 77, 95);

		for (int y = 0; y < 6; y++) {
			ScenarioHelper.build(street, p, n, 76, y);
		}
		// S.map().getAutotile().removeTile(77, 0, "mountain");
		// S.map().getAutotile().removeTile(77, 1, "mountain");
		// S.map().getAutotile().removeTile(77, 2, "forest");
		// S.map().getAutotile().removeTile(77, 3, "forest");

		// win quest
		p.addQuest(QuestGenerator.win().addReq(new ReqMinMaxObj(false, 140, EMapData.BUILDING, street, true)));

		// lose quest
		QuestGenerator.loseLeader(p);

	}

}

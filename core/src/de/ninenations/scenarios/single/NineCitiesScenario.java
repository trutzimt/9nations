/**
 *
 */
package de.ninenations.scenarios.single;

import de.ninenations.actions.req.ReqTown;
import de.ninenations.player.Player;
import de.ninenations.quests.QuestGenerator;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.ScenarioHelper;

/**
 * @author sven
 *
 */
public class NineCitiesScenario extends BaseScenario {

	/**
	 */
	public NineCitiesScenario() {
		super("ninecities", "9 Cities", "map/field5.tmx");
		desc = "A long time ago there was a big kingdom, which consisted of 9 major cities. It is now time to rebuild the 9 cities.";

		enableAllConf();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.scenarios.BaseScenario#start()
	 */
	@Override
	public void firstStart() {

		Player p = ScenarioHelper.addHuman(humanNation);
		ScenarioHelper.unitI(p.getNation().getLeader(), p, null, 45, 37);

		// win quest
		p.addQuest(QuestGenerator.win().addReq(new ReqTown(false, 9)));

		// lose quest
		QuestGenerator.loseLeader(p);

	}

}

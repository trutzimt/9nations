/**
 *
 */
package de.ninenations.scenarios.beowolf;

import de.ninenations.actions.action.ActionFoundTown;
import de.ninenations.actions.action.ActionProduce;
import de.ninenations.actions.req.ReqAp;
import de.ninenations.actions.req.ReqMinMaxObj;
import de.ninenations.actions.req.ReqTerrain;
import de.ninenations.actions.req.ReqTown;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.data.terrain.BaseTerrain;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.unit.DataUnit;

/**
 * @author sven
 *
 */
public class BeowulfUnit extends DataUnit {

	private static final long serialVersionUID = -6851848085474394678L;

	/**
	 * @param type
	 * @param title
	 * @param id
	 */
	public BeowulfUnit() {
		super("nbeowulf", "Beowulf", "gallery_25428_10_1188.png", 10, 5, NCat.DECO);

		visibleRange = 3;
		ap = 10;
		hp = 10;

		activeActions.removeIndex(0);
		activeActions.add(new ActionFoundTown(true).addReq(new ReqTown(true, 0)));
		activeActions.add(new ActionProduce(true, false, BaseRess.FOOD, 3).addReq(new ReqTerrain(false, BaseTerrain.GRASS)).addReq(new ReqAp(false, 3)));
		activeActions.add(new ActionProduce(true, false, BaseRess.WOOD, 3).addReq(new ReqTerrain(false, BaseTerrain.FOREST)).addReq(new ReqAp(false, 3)));

		reqs.add(new ReqMinMaxObj(true, 0, EMapData.UNIT, "nbeowulf", true));
	}

}

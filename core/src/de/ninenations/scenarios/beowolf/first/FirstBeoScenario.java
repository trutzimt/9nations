/**
 *
 */
package de.ninenations.scenarios.beowolf.first;

import com.badlogic.gdx.utils.Align;

import de.ninenations.actions.action.ActionMessage;
import de.ninenations.actions.action.ActionProduce;
import de.ninenations.actions.req.ReqExploredField;
import de.ninenations.actions.req.ReqMinMaxObj;
import de.ninenations.actions.req.ReqPlayerRess;
import de.ninenations.core.NN;
import de.ninenations.data.nations.BaseNation;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.quests.BaseQuest;
import de.ninenations.quests.BaseQuestItem;
import de.ninenations.quests.QuestGenerator;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.ScenarioHelper;
import de.ninenations.scenarios.beowolf.BeowulfUnit;
import de.ninenations.towns.Town;
import de.ninenations.ui.window.YTextDialogSaver;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class FirstBeoScenario extends BaseScenario {

	/**
	 */
	public FirstBeoScenario() {
		super("1", "Leaving", "map/field5.tmx");

		desc = "It's time to leave your home island to fulfill the goddess task.";

		enableAllConf();
		conf.put(ScenConf.WEALTH, false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.scenarios.BaseScenario#assetLoad()
	 */
	@Override
	public void assetLoad() {
		// add beowulf
		NN.asset().loadT("system/unit/gallery_25428_10_1188.png");

		if (NSettings.isBiggerGui()) {
			NN.asset().loadT("64/unit/gallery_25428_10_1188.png");
		}

		// add story
		NN.asset().loadT("system/face/Clockwork.png");
		NN.asset().loadT("system/face/ElfMaleMagicUser.png");
		NN.asset().loadT("system/face/HumanMaleFighter.png");
		NN.asset().loadT("system/face/raven.png");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * at.trutz.qossire.game.scenario.BaseScenario#nextRound(at.trutz.qossire.player
	 * .Player)
	 */
	@Override
	public void firstStart() {
		enableAllConf();
		MapScreen.get().getData().setConf(ScenConf.WEALTH, false);

		S.nData().add(new BeowulfUnit());

		Player n = MapScreen.get().getData().getPlayers().add("nature", "Village", BaseNation.NORTH);
		// add a town
		Town t = ScenarioHelper.townI(n, 85, 86);
		// ScenarioHelper.buildI("townhall", t, 84, 89, true);
		ScenarioHelper.buildI("nfarm", n, t, 83, 90);
		ScenarioHelper.buildI("nfarm", n, t, 85, 88);
		ScenarioHelper.buildI("nfarm2", n, t, 84, 91);
		ScenarioHelper.buildI("nfarm2", n, t, 83, 88);
		ScenarioHelper.buildI("nhouse", n, t, 86, 90);
		ScenarioHelper.buildI("nhouse2", n, t, 82, 88);
		ScenarioHelper.buildI("ntemple", n, t, 86, 87);
		ScenarioHelper.buildI("nstreet", n, t, 86, 88);
		ScenarioHelper.buildI("nstreet", n, t, 86, 89);
		ScenarioHelper.buildI("nstreet", n, t, 85, 89);
		ScenarioHelper.buildI("nstreet", n, t, 85, 90);
		ScenarioHelper.buildI("nstreet", n, t, 85, 91);
		ScenarioHelper.unitI("nmage", n, t, 92, 67);

		Player p = ScenarioHelper.addHuman(null);

		// add the player
		ScenarioHelper.unitI("nbeowulf", p, null, 83, 88); //

		// add intro dialog
		p.addMessage(new YTextDialogSaver("Background", "Ragnarok, destiny, is inevitable.\n" + "When the time comes to an end, the Fimbulwinter breaks.\n"
				+ "For three years, the cold will drag through every day. These are the years of the wolf. A wolf so big that its jaw touches the sky and the earth, a snake at its side,\n"
				+ "whose body wraps around Midgard, and Hel, the goddess herself.", "Clockwork").setAlign(Align.center));
		p.addMessage(new YTextDialogSaver("Background",
				"Ragnarok, destiny, is inevitable.\n" + "Yaggdrasil, the world tree, will wither, and the light will go out.\n"
						+ "The enemies of the world will come and put an end to the gods in the wigrid field, because they are of the same blood.\n"
						+ "Aloud, Garm howls to Gnipahellir, it breaks the fetters, it runs the wolf.\n" + "Then a new time begins.",
				"Clockwork").setAlign(Align.center));

		// add story
		p.addMessage(
				new YTextDialogSaver("Rave", "Hello Beowulf,\n" + "Greetings. I am Hugin, the raven of thought. I am here with an important message.", "raven")
						.setAlign(Align.right));
		p.addMessage(new YTextDialogSaver("Beowulf", "Which honour. I did not think that I would ever see you.", "HumanMaleFighter"));
		p.addMessage(new YTextDialogSaver("Hudin",
				"Well it's not up to me that you do not notice me. But this is not important. Soon, the Yaggdrasil, the world tree will wither and for that we must be prepared. You must sail to the court of the Danish king Hruodgers.",
				"raven").setAlign(Align.right));
		p.addMessage(new YTextDialogSaver("Beowulf", "Why me?", "HumanMaleFighter"));
		p.addMessage(new YTextDialogSaver("Hudin",
				"It is not in my power to answer or know this. I'm just delivering the messages, but Odin, the godfather, sent me to you. When the time is right, he will reveal himself and certainly explain everything to you.",
				"raven").setAlign(Align.right));
		p.addMessage(new YTextDialogSaver("Beowulf",
				"Do you seriously want to anger the godfather? You will already recognize the help if you need it. It is thus made. You build yourself a ship. Then you will sail to the court of the Danish King Hruodgers and assist him.\n"
						+ "I say goodbye to you now.",
				"HumanMaleFighter"));
		p.addMessage(new YTextDialogSaver("Priest",
				"I followed your conversation. Beowulf it is an honor that you serve the great Odin. Your task will certainly not be easy. But I am confident that you will master it.",
				"ElfMaleMagicUser").setAlign(Align.right));
		p.addMessage(new YTextDialogSaver("Priest",
				"I packed you some food. Move north to the mountains and build a settlement so you can build the boat. Unfortunately, I do not know how boats can be built. Surely you will come up with an idea.",
				"ElfMaleMagicUser").setAlign(Align.right));

		// add the main quest
		BaseQuestItem b = new BaseQuestItem("Build a Ship to start your journey.");
		b.addReq(new ReqMinMaxObj(false, 1, EMapData.UNIT, "nship", true));
		b.addAction(new ActionMessage(
				new YTextDialogSaver("Be patient", "The story ends here at the moment. Feel free to leave your comment and opinion.", "clockwork")));
		p.addQuest(b);

		// add helper quest line
		BaseQuest q = new BaseQuest("harr", "Hárr");
		b = new BaseQuestItem("Finding");
		b.setShowDetail(false);
		b.addReq(new ReqExploredField(92, 33));
		b.addAction(new ActionMessage(
				new YTextDialogSaver("Old grey man", "Ah Beowulf, good that you finally come. I thought so, you do not come anymore.", "ElfMaleMagicUser")
						.setAlign(Align.right),
				new YTextDialogSaver("Beowulf", "Who are you? How do you know my name? Did Hugin talk to you?", "HumanMaleFighter"),
				new YTextDialogSaver("Hárr",
						"My name is Hárr, which means gray. No, Hudin did not talk to me. I read it in the runes. It is a fascinating art. But that's why you're certainly not here.",
						"ElfMaleMagicUser").setAlign(Align.right),
				new YTextDialogSaver("Beowulf", "If you already know everything, then you surely know why I am here?", "HumanMaleFighter"),
				new YTextDialogSaver("Hárr",
						"Naturally. I can introduce you to the art of boat building, but I want to have a very special rune for that. If you mine me 50 stones to produce the rune, I will introduce you to the art of boat building.",
						"ElfMaleMagicUser").setAlign(Align.right)));
		q.addItem(b);

		b = new BaseQuestItem("50 Stones");
		b.addReq(new ReqPlayerRess(false, BaseRess.STONE, 50));
		b.addAction(new ActionMessage(new YTextDialogSaver("Hárr",
				"Gorgeous, you have mined the 50 stones. Here is the knowledge of boat art. use it wisely. Remember, your settlement must be at least level 2 so you can build a dockyard for the ship.",
				"ElfMaleMagicUser").setAlign(Align.right)));
		b.addAction(new ActionProduce(false, true, BaseRess.STONE, -50));
		b.addAction(new BeowulfFirstDockyardAction());
		q.addItem(b);
		p.addQuest(q);

		// lose quest
		p.addQuest(QuestGenerator.loseUnit("nbeowulf"));

	}

}

package de.ninenations.scenarios.beowolf.first;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.GAction;
import de.ninenations.actions.req.ReqTownLevel;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;

public class BeowulfFirstDockyardAction extends GAction {
	private static final long serialVersionUID = 1L;

	public BeowulfFirstDockyardAction() {
		super("dockyard", "Set Dockyardlevel to 2");
	}

	@Override
	public boolean perform(Player player, NOnMapObject onMap, int x, int y) {
		S.nData().getB("ndockyard").getReqs().removeIndex(1);
		S.nData().getB("ndockyard").getReqs().add(new ReqTownLevel(false, 2));
		return false;
	}

	@Override
	public Image getIcon() {
		return S.nData().getB("ndockyard").getIcon();
	}
}
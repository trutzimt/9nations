/**
 *
 */
package de.ninenations.scenarios.beowolf;

import de.ninenations.scenarios.BaseCampaign;
import de.ninenations.scenarios.beowolf.first.FirstBeoScenario;

/**
 * @author sven
 *
 */
public class BeowulfCampaign extends BaseCampaign {

	/**
	 */
	public BeowulfCampaign() {
		super("beowulf", "Journey of Beowulf", "This campaign tells the story of beowulf: A hero, who lifed at 600.");

		addScenario(new FirstBeoScenario());
	}

}

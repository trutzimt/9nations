/**
 * 
 */
package de.ninenations.core;

import java.io.Serializable;

import com.badlogic.gdx.utils.SnapshotArray;

/**
 * @author sven
 *
 */
public class BaseMgmtMgmt<T extends BaseMgmtObject> implements Serializable {

	private static final long serialVersionUID = -8298654840084759314L;

	protected int counter;
	protected SnapshotArray<T> elements;

	/**
	 * 
	 */
	public BaseMgmtMgmt() {
		elements = new SnapshotArray<>();

		counter = 1;
	}

	/**
	 * Get the object or null
	 * 
	 * @param id
	 * @return
	 */
	public T get(int id) {
		for (T b : elements) {
			if (b.getId() == id) {
				return b;
			}
		}

		return null;
	}

	/**
	 * Remove the object
	 * 
	 * @param id
	 * @return
	 */
	public void remove(int id) {
		for (int i = 0; i < elements.size; i++) {
			if (elements.get(i).getId() == id) {
				elements.removeIndex(i);
				return;
			}
		}
	}

	/**
	 * Inform all objects
	 */
	@SuppressWarnings("unchecked")
	public void nextRound() {
		Object[] items = elements.begin();
		for (int i = 0, n = elements.size; i < n; i++) {
			((T) items[i]).nextRound();
		}
		elements.end();
	}

	/**
	 * Inform all objects
	 */
	public void afterLoad() {
		for (int i = 0, n = elements.size; i < n; i++) {
			elements.get(i).afterLoad();
		}
	}

	public int getSize() {
		return elements.size;
	}

	/**
	 * @return the elements
	 */
	public SnapshotArray<T> getRawAccess() {
		return elements;
	}
}

/**
 * 
 */
package de.ninenations.core;

import java.io.Serializable;

import com.badlogic.gdx.utils.ObjectMap;

/**
 * @author sven
 *
 */
public class NSimpleObjectMap<V, X> extends ObjectMap<V, X> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7356840011405858307L;

	/**
	 * 
	 */
	public NSimpleObjectMap() {}

}

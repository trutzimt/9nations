package de.ninenations.core;

import java.util.Random;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.data.NData;
import de.ninenations.game.GameStartScreen;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.menu.MainMenuScreen;
import de.ninenations.mod.ModMmgt;
import de.ninenations.scenarios.CampaignMgmt;
import de.ninenations.screen.IScreen;
import de.ninenations.screen.LoaderScreen;
import de.ninenations.stats.AchievementMgmt;
import de.ninenations.ui.UiHelper;
import de.ninenations.ui.newWindow.NSkin;
import de.ninenations.ui.newWindow.WindowManager;
import de.ninenations.util.NSettings;
import de.ninenations.util.YError;
import de.ninenations.util.YLog;
import de.ninenations.util.YMusic;

public class NN extends Game {

	private static NN nineNations;

	private BasePlattform plattform;
	private NAssetManager asset;
	private NSkin skin;
	private YMusic music;
	private Preferences pref;
	private CampaignMgmt camps;
	private AchievementMgmt achievements;
	private NData nData;
	private Random random;
	private ModMmgt mods;
	private WindowManager windows;

	private NN(BasePlattform plattform) {
		this.plattform = plattform;

	}

	/**
	 * Create it
	 * 
	 * @param plattform
	 */
	public static NN init(BasePlattform plattform) {
		nineNations = new NN(plattform);

		return nineNations;
	}

	/**
	 * Get it
	 * 
	 * @return
	 */
	public static NN get() {
		return nineNations;
	}

	/**
	 * Load the base assests
	 */
	@Override
	public void create() {
		try {

			pref = Gdx.app.getPreferences("de.ninenations");

			// load for the loadingscreen:
			asset = new NAssetManager();
			asset.load("system/menu/black.png", Texture.class);
			asset.finishLoadingAsset("system/menu/black.png");
			asset.loadT("system/menu/menuBackground.jpg");
			asset.loadT("system/menu/menuLoading.jpg");
			asset.loadT("system/logo/logo_512.png");
			asset.loadT("system/logo/logo_256.png");
			asset.finishLoading();

			super.setScreen(new LoaderScreen("Loading 9 Nations: "));
		} catch (Exception e) {
			YError.error(e, true);

			// Gdx.app.error("Main", "Can not start game", e);
			// plattform.showErrorDialog(
			// "Can not start game " + e.getClass().getSimpleName() + " " +
			// e.getLocalizedMessage());
			// Gdx.app.exit();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Game#render()
	 */
	@Override
	public void render() {
		try {
			super.render();
			if (music != null) {
				music.act();
			}
		} catch (Exception e) {
			YError.error(e, true);

		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Game#setScreen(com.badlogic.gdx.Screen)
	 */
	@Override
	@Deprecated
	public void setScreen(Screen screen) {
		super.setScreen(screen);
	}

	/**
	 * Switch to a new screen with transition
	 * 
	 * @param game
	 * @param newScreen
	 */
	public void switchScreen(final Screen newScreen) {
		Screen oldScreen = getScreen();

		YLog.log(oldScreen.getClass().getSimpleName(), "switch", newScreen.getClass().getSimpleName());

		// find the matches
		if (oldScreen instanceof LoaderScreen && newScreen instanceof MainMenuScreen) {

			// just remove the loading
			LoaderScreen l = (LoaderScreen) oldScreen;
			l.getLoadingHour().addAction(Actions.sequence(Actions.fadeOut(1), Actions.run(new Runnable() {

				@Override
				public void run() {
					NN.super.setScreen(newScreen);
				}
			})));
			l.getText().addAction(Actions.fadeOut(1));

			// add the remove image
			Image img = new Image(asset.getT("system/menu/menuLoading.jpg"));
			img.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
			img.setFillParent(true);
			((MainMenuScreen) newScreen).getStage().addActor(img);

			return;

		}

		// find the matches
		if (oldScreen instanceof LoaderScreen && newScreen instanceof GameStartScreen) {
			NN.super.setScreen(newScreen);
			return;

		}

		// find the matches
		if (oldScreen instanceof MainMenuScreen && newScreen instanceof GameStartScreen) {

			// add the remove image
			Image img = new Image(asset.getT("system/menu/menuLoading.jpg"));
			img.getColor().a = 0;
			img.addAction(Actions.sequence(Actions.fadeIn(1), Actions.run(new Runnable() {

				@Override
				public void run() {
					NN.super.setScreen(newScreen);

				}
			})));
			img.setFillParent(true);
			((MainMenuScreen) oldScreen).getStage().addActor(img);
			return;

		}

		if (oldScreen instanceof MapScreen && newScreen instanceof MainMenuScreen) {
			Image black = UiHelper.createBlackImg(true);
			black.addAction(Actions.sequence(Actions.delay(1), Actions.run(new Runnable() {

				@Override
				public void run() {
					NN.super.setScreen(newScreen);
					MapScreen.exit();
				}
			})));
			((IScreen) oldScreen).getStage().addActor(black);
			newBlackScreen(newScreen);
			return;
		}

		// default
		oldBlackScreen(newScreen, oldScreen);
		newBlackScreen(newScreen);

		// found nothing?
		// Yaams.super.setScreen(newScreen);
	}

	/**
	 * @param newScreen
	 * @param g
	 */
	private void oldBlackScreen(final Screen newScreen, Screen oldScreen) {
		Image black = UiHelper.createBlackImg(true);
		black.addAction(Actions.sequence(Actions.delay(1), Actions.run(new Runnable() {

			@Override
			public void run() {
				NN.super.setScreen(newScreen);

			}
		})));
		((IScreen) oldScreen).getStage().addActor(black);
	}

	/**
	 * @param newScreen
	 * @param g
	 */
	private void newBlackScreen(Screen newScreen) {
		Image black = new Image(asset.getT("system/menu/black.png"));
		black.setFillParent(true);
		black.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
		((IScreen) newScreen).getStage().addActor(black);
	}

	/**
	 * Save it
	 */
	@Override
	public void dispose() {
		NSettings.save();
	}

	/**
	 * @return the plattform
	 */
	public static BasePlattform plattform() {
		return nineNations.plattform;
	}

	/**
	 * @param music
	 *            the music to set
	 */
	public void setMusic(YMusic music) {
		this.music = music;
	}

	/**
	 * @return the pref
	 */
	public static Preferences pref() {
		return nineNations.pref;
	}

	/**
	 * @return the assets
	 */
	public static NAssetManager asset() {
		return nineNations.asset;
	}

	/**
	 * @return the assets
	 */
	public static NData nData() {
		return nineNations.nData;
	}

	/**
	 * @return the assets
	 */
	public static YMusic music() {
		return nineNations.music;
	}

	/**
	 * @return the assets
	 */
	public static AchievementMgmt ach() {
		return nineNations.achievements;
	}

	/**
	 * @return the camps
	 */
	public static CampaignMgmt camps() {
		return nineNations.camps;
	}

	/**
	 * @return the camps
	 */
	public static IScreen screen() {
		return (IScreen) nineNations.getScreen();
	}

	/**
	 * @return the achievements
	 */
	public AchievementMgmt getAchievements() {
		return achievements;
	}

	/**
	 * @return the random
	 */
	public static Random random() {
		return nineNations.random;
	}

	/**
	 * @return the random
	 */
	public static ModMmgt mods() {
		return nineNations.mods;
	}

	/**
	 * @return the random
	 */
	public static WindowManager windows() {
		return nineNations.windows;
	}

	/**
	 * @return the random
	 */
	public static NSkin skin() {
		return nineNations.skin;
	}

	/**
	 * Start/Init the base system
	 */
	public static void start() {
		nineNations.random = new Random();
		nineNations.windows = new WindowManager();
		nineNations.camps = new CampaignMgmt();
		nineNations.achievements = new AchievementMgmt();
		nineNations.nData = new NData();
		nineNations.mods = new ModMmgt();
		nineNations.skin = new NSkin();
		nineNations.nData.generate();

	}
}

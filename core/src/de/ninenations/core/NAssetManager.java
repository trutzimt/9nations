/**
 * 
 */
package de.ninenations.core;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

import de.ninenations.util.YError;

/**
 * @author sven
 *
 */
public class NAssetManager extends AssetManager {

	/**
	 * 
	 */
	public NAssetManager() {}

	/**
	 * Addon loading method
	 * 
	 * @param filename
	 */
	public void loadT(String filename) {
		load(filename, Texture.class);
	}

	/**
	 * Addon loading method
	 * 
	 * @param filename
	 */
	public Texture getT(String filename) {
		if (this.isLoaded(filename, Texture.class)) {
			return get(filename, Texture.class);
		}
		// has an error?
		YError.error(new IllegalArgumentException("Texture Asset " + filename + " is missing."), false);
		return get("system/menu/black.png", Texture.class);
	}

}

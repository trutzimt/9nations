/**
 *
 */
package de.ninenations.core;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.widget.VisCheckBox;

import de.ninenations.game.another.MapKeyboardInputAdapter;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.menu.MainMenuScreen;
import de.ninenations.menu.MenuWindow.MWI;
import de.ninenations.mod.IMod;
import de.ninenations.mod.ModInfo;
import de.ninenations.ui.IDisplay;
import de.ninenations.ui.elements.ITextfield;
import de.ninenations.ui.elements.YRandomField;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.NSettings;
import de.ninenations.util.YSounds;

/**
 * Management Plattform code
 *
 * @author sven
 *
 */
public abstract class BasePlattform implements IDisplay {

	/**
	 * Format the long to a readable date
	 *
	 * @param date
	 * @return
	 */
	public abstract String formatDate(long date);

	/**
	 * Show a error message dialog for the user
	 *
	 * @param message
	 */
	public abstract void showErrorDialog(String message);

	/**
	 * Create a text input field
	 * 
	 * @return
	 */
	public abstract ITextfield createTextField(YRandomField base);

	/**
	 * This method will called at the beginn of a game to add plattform spezific
	 * code
	 * 
	 * @param mapScreen
	 * @param multiplexer
	 */
	public void addInputs(final MapScreen mapScreen, InputMultiplexer multiplexer) {
		multiplexer.addProcessor(new InputAdapter() {

			@Override
			public boolean keyDown(int keycode) {

				if (keycode == Input.Keys.W || keycode == Input.Keys.UP) {
					mapScreen.getMap().moveMapView(0, 32);
					return true;
				}

				if (keycode == Input.Keys.S || keycode == Input.Keys.DOWN) {
					mapScreen.getMap().moveMapView(0, -32);
					return true;
				}

				if (keycode == Input.Keys.A || keycode == Input.Keys.LEFT) {
					mapScreen.getMap().moveMapView(-32, 0);
					return true;
				}

				if (keycode == Input.Keys.D || keycode == Input.Keys.RIGHT) {
					mapScreen.getMap().moveMapView(32, 0);
					return true;
				}

				return false;

			}

			@Override
			public boolean touchDown(int screenX, int screenY, int pointer, int button) {

				if (button == Input.Buttons.RIGHT && mapScreen.getInputs().hasActiveAction()) {
					YSounds.pCancel();
					mapScreen.getInputs().setActiveAction(null);
					return true;
				}

				return false;
			}

			@Override
			public boolean scrolled(int amount) {
				// can scroll?
				if (mapScreen.getGui().anyWindowIsOpen()) {
					return false;
				}

				if (amount < 0) {
					mapScreen.getMap().zoomIn();
					return true;
				}
				if (amount > 0) {
					mapScreen.getMap().zoomOut();
					return true;
				}
				return false;
			}
		});

		multiplexer.addProcessor(new MapKeyboardInputAdapter());
	}

	/**
	 * This method will called at the beginning
	 */
	public abstract void startUpCode();

	/**
	 * Create a new Label
	 *
	 * @param message
	 * @return
	 */
	@Deprecated
	public abstract Label getAnimatedLabel(String message);

	/**
	 * Get the default name for the player for the plattform
	 * 
	 * @return
	 */
	public abstract String getPlayerDefaultName();

	/**
	 * Get the system info for the error screen
	 * 
	 * @return
	 */
	public abstract String getSystemErrorInfo();

	/**
	 * Only used on desktop
	 * 
	 * @param table
	 * @param file
	 */
	public abstract void showSystemSavePath(YTable table, FileHandle file);

	/**
	 * Remove unnessary items or add important items to the main menu and game menu
	 * 
	 * @param items
	 */
	public void checkMenu(List<MWI> items) {}

	/**
	 * Check the startup of the main menu
	 */
	public void startupCheck(MainMenuScreen screen) {}

	/**
	 * Add special audio options
	 * 
	 * @param table
	 */
	public void addOptionsAudio(YTable table) {}

	/**
	 * Add special game options
	 * 
	 * @param table
	 */
	public void addOptionsGame(YTable t) {
		// add autosave
		VisCheckBox box = new VisCheckBox("Autosave", NSettings.isAutosave());
		box.addCaptureListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				YSounds.pClick();
				NSettings.getPref().putBoolean("autosave", ((VisCheckBox) actor).isChecked());

			}
		});
		t.addL(null, box);
	}

	/**
	 * Add special graphic options
	 * 
	 * @param table
	 */
	public void addOptionsGraphic(YTable t) {}

	/**
	 * Add special update options
	 * 
	 * @param table
	 */
	public void addOptionsUpdate(YTable t) {}

	public abstract String getID();

	@Override
	public YTable getInfoPanel() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Create a instance of this class
	 * 
	 * @param info
	 * @return
	 */
	public abstract IMod loadMod(ModInfo info);

	/**
	 * Ask the system for the day of the year
	 * 
	 * @return
	 */
	public abstract int getDayOfYear();

	/**
	 * Return a list of files, on html a file in the folder list.9nhtml will used
	 * 
	 * @param folder
	 * @param extension
	 * @return
	 */
	public FileHandle[] getFolderContent(String folder, String extension) {
		FileHandle f = Gdx.files.internal(folder);
		return f.isDirectory() ? f.list(extension) : null;
	}

}

/**
 * 
 */
package de.ninenations.core;

import java.io.Serializable;

import com.badlogic.gdx.utils.Array;

/**
 * @author sven
 *
 */
public class NArray<T> extends Array<T> implements Serializable {

	private static final long serialVersionUID = 6130743419849273971L;

	/**
	 * 
	 */
	public NArray() {}

	/**
	 * @param capacity
	 */
	public NArray(int capacity) {
		super(capacity);
	}

	/**
	 * @param array
	 */
	public NArray(Array<? extends T> array) {
		super(array);
	}

	/**
	 * @param array
	 */
	public NArray(T[] array) {
		super(array);
	}

	/**
	 * @param ordered
	 * @param capacity
	 */
	public NArray(boolean ordered, int capacity) {
		super(ordered, capacity);
	}

	/**
	 * @param ordered
	 * @param array
	 * @param start
	 * @param count
	 */
	public NArray(boolean ordered, T[] array, int start, int count) {
		super(ordered, array, start, count);
	}

}

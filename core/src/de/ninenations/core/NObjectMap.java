/**
 * 
 */
package de.ninenations.core;

import java.io.Serializable;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

import de.ninenations.util.NGenerator;
import de.ninenations.util.YError;

/**
 * @author sven
 *
 */
public class NObjectMap<V extends IMapObject> extends ObjectMap<String, V> implements Serializable {

	private static final long serialVersionUID = -4066262090750506234L;

	private String type;
	private transient Array<String> pos;

	/**
	 * @param type
	 */
	public NObjectMap() {
		super();
	}

	/**
	 * @param type
	 */
	public NObjectMap(String type) {
		super();
		this.type = type;
	}

	/**
	 * Add a Single entry, if it exist a error will occure
	 *
	 * @param cam
	 */
	public void addS(V obj) {
		// exist campaign?
		if (containsKey(obj.getType())) {
			YError.error(new IllegalArgumentException(type + " " + get(obj.getType()).getClass().getSimpleName() + "(" + obj.getType()
					+ ") exist. Overwrite with " + obj.getClass().getSimpleName()), false);
		}

		put(obj.getType(), obj);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.utils.ObjectMap#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public V put(String key, V value) {
		pos = null;
		return super.put(key, value);
	}

	/**
	 * Get a specific object, or the first, if exist
	 *
	 * @param id
	 * @return
	 */
	public V getS(String id) {
		// exist campaign?
		if (!containsKey(id)) {
			try {
				// get object
				if (size == 0) {
					YError.error(new IllegalArgumentException(type + " (" + id + ") not exist. Null will returned."), false);
					return null;
				} else {
					V nobj = values().iterator().next();
					YError.error(new IllegalArgumentException(
							type + " (" + id + ") not exist. " + nobj.getClass().getSimpleName() + " (" + nobj.getType() + ") will returned"), false);
					return nobj;
				}
			} catch (Exception e) {
				YError.error(new IllegalArgumentException("ID " + id + " not exist and code is corrupt."), true);
				return null;
			}
		}

		return get(id);
	}

	/**
	 * Check if the research exist, otherwise notice the player
	 *
	 * @param type
	 */
	public void check(String id) {
		if (!containsKey(id)) {
			YError.error(new IllegalArgumentException("Check:" + type + " (" + id + ") not exist."), true);
		}
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @return the type
	 */
	public V getRnd() {
		Array<String> s = keys().toArray();
		return getS(s.get(NGenerator.getIntBetween(0, s.size)));
	}

	/**
	 * Return the intern position for this element
	 * 
	 * @param id
	 * @return
	 */
	public int getPos(String id) {
		for (int i = 0; i < getPos().size; i++) {
			if (getPos().get(i).equals(id)) {
				return i;
			}
		}

		return -1;
	}

	private Array<String> getPos() {
		if (pos == null) {
			pos = keys().toArray();
		}
		return pos;
	}

	/**
	 * Get the element on this position
	 * 
	 * @param p
	 * @return
	 */
	public V get(int p) {
		return get(getPos().get(p));
	}
}

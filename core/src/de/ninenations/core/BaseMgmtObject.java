package de.ninenations.core;

import de.ninenations.ui.BaseDisplay;

public abstract class BaseMgmtObject extends BaseDisplay {

	protected int id;

	public BaseMgmtObject() {
		super(null, null);
	}

	public BaseMgmtObject(String type, int id, String name) {
		super(type, name);

		this.id = id;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	public abstract void nextRound();

	public abstract void afterLoad();

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return getClass().getSimpleName() + " " + getType() + ": " + getName();
	}
}

/**
 * 
 */
package de.ninenations.core;

/**
 * @author sven
 *
 */
public interface IMapObject {

	/**
	 * Return the identifizer
	 * 
	 * @return
	 */
	public String getType();
}

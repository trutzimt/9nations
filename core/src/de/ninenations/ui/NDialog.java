package de.ninenations.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.util.dialog.ConfirmDialogListener;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.ninenations.core.NN;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.util.YSounds;

@Deprecated
public class NDialog {

	private Array<Integer> values;
	private Array<String> titles;
	private Array<YChangeListener> changeListeners;
	private String title, desc;

	public NDialog(String title, String desc) {
		this.title = title;
		this.desc = desc;
		values = new Array<>();
		titles = new Array<>();
		changeListeners = new Array<>();
	}

	public void addCancel() {
		add("Cancel", new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				YSounds.play(YSounds.CANCEL);

			}
		});
	}

	/**
	 * Add a new option
	 * 
	 * @param title
	 * @param changeListener
	 */
	public void add(String title, YChangeListener changeListener) {
		titles.add(title);
		values.add(values.size);
		changeListeners.add(changeListener);
	}

	/**
	 * Show it
	 */
	public void show(Stage stage) {
		final Image black = new Image(NN.asset().getT("system/menu/black.png"));
		black.getColor().a = 0f;
		black.setFillParent(true);
		black.addAction(Actions.alpha(0.75f, 10));
		stage.addActor(black);

		String[] t = new String[titles.size];
		Integer[] v = new Integer[titles.size];

		for (int i = 0; i < t.length; i++) {
			t[i] = titles.get(i);
			v[i] = values.get(i);
		}

		Dialogs.<Integer>showConfirmDialog(stage, title, desc, t, v, new ConfirmDialogListener<Integer>() {

			@Override
			public void result(Integer result) {
				// hide image
				black.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));

				// perform
				changeListeners.get(result).changed(null, null);

			}
		});
	}

}

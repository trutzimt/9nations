/**
 * 
 */
package de.ninenations.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.core.NN;
import de.ninenations.game.screen.MapScreen;

/**
 * @author sven
 *
 */
public class UiHelper {

	/**
	 * 
	 * @param value
	 * @param x
	 * @param y
	 * @param map,
	 *            Show at which stage? true = map
	 */
	public static void textAnimation(String txt, int x, int y, boolean map, Color color) {

		VisLabel top = new VisLabel(txt);
		top.setColor(color);
		top.getColor().a = 0;
		// add animation
		top.addAction(Actions.parallel(Actions.sequence(Actions.moveBy(0, 80, 3.5f)),
				Actions.sequence(Actions.fadeIn(0.5f), Actions.delay(1), Actions.fadeOut(2f), Actions.removeActor())));

		VisLabel bottom = new VisLabel(txt);
		bottom.setColor(Color.BLACK);
		bottom.getColor().a = 0;
		// // add animation
		bottom.addAction(Actions.parallel(Actions.sequence(Actions.moveBy(0, 80, 3.5f)),
				Actions.sequence(Actions.fadeIn(0.5f), Actions.delay(1), Actions.fadeOut(2f), Actions.removeActor())));

		// set position and add it to stage
		if (map) {
			bottom.setPosition(x * 32 + 1, y * 32 + 23);
			MapScreen.get().getMap().getStage().addActor(bottom);

			top.setPosition(x * 32, y * 32 + 24);
			MapScreen.get().getMap().getStage().addActor(top);
		} else {
			bottom.setPosition(x + 1, y - 1);
			MapScreen.get().getStage().addActor(bottom);

			top.setPosition(x, y);
			MapScreen.get().getStage().addActor(top);
		}

	}

	/**
	 * 
	 * @param fadeIn
	 * @return
	 */
	public static Image createBlackImg(boolean fadeIn) {
		Image black = new Image(NN.asset().getT("system/menu/black.png"));
		black.setFillParent(true);

		if (fadeIn) {

			black.getColor().a = 0;
			black.addAction(Actions.fadeIn(1));
		}

		return black;
	}

}

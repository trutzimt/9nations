/**
 * 
 */
package de.ninenations.ui.elements;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.VisUI;
import com.kotcrab.vis.ui.widget.Tooltip;

import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.util.YError;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public abstract class YGameImageButton extends Image {

	/**
	 * 
	 */
	public YGameImageButton(String title, int type, int key) {
		super(YIcons.getIconI(type).getDrawable());
		new Tooltip.Builder(title + " (" + Input.Keys.toString(key) + ")").target(this).build();

		// add key?
		MapScreen.get().addKeyBinding(key, new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				if (isVisible()) {
					pressActionIntern();
				} else {
					YSounds.pBuzzer();
				}
			}
		});

		addListener(new InputListener() {
			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.badlogic.gdx.scenes.scene2d.InputListener#enter(com.badlogic.gdx.scenes.
			 * scene2d.InputEvent, float, float, int, com.badlogic.gdx.scenes.scene2d.Actor)
			 */
			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				addAction(Actions.color(VisUI.getSkin().getColor("t-highlight-dark"), 1f));
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.badlogic.gdx.scenes.scene2d.InputListener#exit(com.badlogic.gdx.scenes.
			 * scene2d.InputEvent, float, float, int, com.badlogic.gdx.scenes.scene2d.Actor)
			 */
			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				addAction(Actions.color(Color.WHITE, 1f));
			}

			/*
			 * (non-Javadoc)
			 * 
			 * @see
			 * com.badlogic.gdx.scenes.scene2d.InputListener#touchDown(com.badlogic.gdx.
			 * scenes.scene2d.InputEvent, float, float, int, int)
			 */
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				pressActionIntern();
				return false;
			}
		});
	}

	/**
	 * This Action will called, if something pressing, with try an catch
	 */
	protected void pressActionIntern() {
		// disable active actions
		MapScreen.get().getInputs().setActiveAction(null);
		// MapScreen.get().getGui().setInfo(null, null);

		try {
			pressAction();
		} catch (Exception e) {
			YError.error(e, false);
		}
	}

	/**
	 * This Action will called, if something pressing
	 */
	public abstract void pressAction();

}

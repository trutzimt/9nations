/**
 * 
 */
package de.ninenations.ui.elements;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.Tooltip;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisTable;

import de.ninenations.core.NN;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public abstract class YRandomField extends VisTable {

	private ITextfield name;
	private VisImageButton btn;

	/**
	 * @param text
	 */
	public YRandomField(String text) {
		super();

		name = NN.plattform().createTextField(this);
		name.setText(text);
		add(name.getActor()).fill().expand().grow();

		btn = new VisImageButton(YIcons.getIconI(YIcons.RANDOM).getDrawable());
		btn.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				YSounds.play(YSounds.RANDOM);
				name.setText(getRndText());
				saveText(name.getText());

			}
		});
		new Tooltip.Builder("Create a new random entry.").target(btn).build();
		add(btn).align(Align.right);
	}

	/**
	 * Generate the random text
	 * 
	 * @return
	 */
	protected abstract String getRndText();

	/**
	 * Generate the random text
	 * 
	 * @return
	 */
	public String getText() {
		return name.getText();
	}

	/**
	 * Save the text
	 * 
	 * @return
	 */
	public abstract void saveText(String text);

}

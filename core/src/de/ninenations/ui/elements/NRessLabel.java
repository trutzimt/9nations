/**
 * 
 */
package de.ninenations.ui.elements;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.kotcrab.vis.ui.layout.HorizontalFlowGroup;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.game.S;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class NRessLabel extends HorizontalFlowGroup {

	/**
	 * 
	 */
	public NRessLabel(String txt) {
		super(1);

		// build it
		String[] eles = txt.split(" ");

		for (String e : eles) {
			if (e.length() == 0) {
				continue;
			}

			if (e.startsWith("@R")) {
				addActor(S.nData().getR(e.substring(2)).getIcon());
				continue;
			}

			addActor(new VisLabel(e));
		}
	}

	public static String generateRessLabel(ObjectMap<String, Integer> ress) {
		Array<String> s = ress.keys().toArray();
		String erg = "";
		for (String key : s) {

			// skip?
			if (!NSettings.isDebug() && S.nData().getR(key).getStorageWeight() == 0) {
				continue;
			}

			if (erg.length() > 0) {
				erg += ",";
			}

			erg += getRessLabel(key, ress.get(key));

		}
		return erg;
	}

	public static String getRessLabel(String r, int cost) {
		return cost + "x @R" + r + " ";
	}
}

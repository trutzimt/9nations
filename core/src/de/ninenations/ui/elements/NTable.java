/**
 * 
 */
package de.ninenations.ui.elements;

import com.badlogic.gdx.scenes.scene2d.ui.Cell;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.kotcrab.vis.ui.widget.VisCheckBox;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;

import de.ninenations.core.NN;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class NTable extends VisTable {

	/**
	 * Create with a litte space
	 */
	public NTable() {
		super(true);
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public Widget addL(String label, Widget content) {
		if (label == null) {
			add();
		} else {
			add(label + ":").align(Align.right);
		}
		add(content).align(Align.left).growX().row();

		return content;
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public Widget addA(Widget topic, Widget content) {
		add(topic).align(Align.right);
		add(content).align(Align.left).growX().row();

		return content;
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public VisLabel addI(Image header, String content) {
		VisLabel l = new VisLabel(content);
		l.setWrap(true);

		add(header).align(Align.right);
		add(l).align(Align.left).growX().row();

		return l;
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public void addI(Image header, WidgetGroup content) {
		add(header).align(Align.right);
		add(content).align(Align.left).growX().row();
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public VisLabel addL(String label, String content) {
		VisLabel l = new VisLabel(content);
		l.setWrap(true);
		return (VisLabel) addL(label, l);
	}

	/**
	 * Helpermethod to add a Header
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public VisLabel addH(String label) {
		VisLabel l = new VisLabel(label);
		l.setWrap(true);
		add(l).colspan(2).growX().row();
		addSeparator().colspan(2);
		return l;
	}

	/**
	 * Helpermethod to add a text, for description
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public VisLabel addT(String label) {
		VisLabel l = new VisLabel(label);
		l.setWrap(true);
		add(l).colspan(2).growX().row();
		return l;
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	public WidgetGroup addL(String label, WidgetGroup content) {
		if (label == null || label.length() == 0) {
			add();
		} else {
			add(label + ":").align(Align.right);
		}

		if (content instanceof VisCheckBox) {
			((VisCheckBox) content).getLabelCell().growX();
		}
		add(content).align(Align.left).growX().row();

		return content;
	}

	/**
	 * Create a scrollY pane and return it
	 * 
	 * @return
	 */
	public VisScrollPane createScrollPane() {
		VisScrollPane pane = new VisScrollPane(this);
		pane.setScrollingDisabled(true, false);
		pane.setFadeScrollBars(false);
		pane.setScrollbarsOnTop(true);
		// pane.setFillParent(true);
		return pane;
	}

	/**
	 * Create a scrollY pane and return it in a table
	 * 
	 * @return
	 */
	public NTable createScrollPaneInTable() {
		NTable t = new NTable();
		t.add(createScrollPane()).grow().row();
		return t;
	}

	/**
	 * Add a sepeator image
	 * 
	 * @return
	 */
	public Cell<Image> addSep() {
		Image img = new Image(NN.asset().getT(NSettings.getGuiScaleFolder() + "/skin/separator.png"));
		img.setScaling(Scaling.stretchX);

		Cell<Image> cell = add(img);
		cell.fillX().expandX().row();

		return cell;
	}

}

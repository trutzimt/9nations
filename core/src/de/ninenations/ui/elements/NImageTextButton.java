package de.ninenations.ui.elements;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.VisImageTextButton;

import de.ninenations.core.NN;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.util.NSettings;

public abstract class NImageTextButton extends VisImageTextButton {

	/**
	 * 
	 * @param title
	 */
	public NImageTextButton(String title, Image img) {
		this(title, img, false);

	}

	/**
	 * 
	 * @param title
	 */
	public NImageTextButton(String title, Image img, boolean sound) {
		super(title, img.getDrawable());

		// left image
		getLabelCell().growX();

		addListener(new YChangeListener(sound) {

			@Override
			public void changedY(Actor actor) {
				perform();

			}
		});

		// add gui elements?
		if (NSettings.isCleanerGuiDesign()) {
			return;
		}

		// add hove effect
		addCaptureListener(new InputListener() {

			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				setStyle(NN.skin().getImageTextButtonHover());
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				setStyle(NN.skin().getImageTextButton());

			}
		});

		// setStyle(NN.skin().getImageButton());

	}

	public abstract void perform();

}

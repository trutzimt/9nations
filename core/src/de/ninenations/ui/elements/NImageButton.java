package de.ninenations.ui.elements;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.kotcrab.vis.ui.widget.VisImageButton;

import de.ninenations.core.NN;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.util.NSettings;

public abstract class NImageButton extends VisImageButton {

	/**
	 * 
	 * @param title
	 */
	public NImageButton(Drawable image) {
		this(image, false);

	}

	/**
	 * 
	 * @param title
	 */
	public NImageButton(Drawable image, boolean sound) {
		super(image);

		addListener(new YChangeListener(sound) {

			@Override
			public void changedY(Actor actor) {
				perform();

			}
		});

		// add gui elements?
		if (NSettings.isCleanerGuiDesign()) {
			return;
		}

		// add hove effect
		addCaptureListener(new InputListener() {

			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				setStyle(NN.skin().getImageButtonHover());
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				setStyle(NN.skin().getImageButton());

			}
		});

		setStyle(NN.skin().getImageButton());

	}

	public abstract void perform();

}

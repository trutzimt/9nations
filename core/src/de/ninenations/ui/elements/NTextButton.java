package de.ninenations.ui.elements;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.ninenations.core.NN;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.util.NSettings;

public abstract class NTextButton extends VisTextButton {

	/**
	 * 
	 * @param title
	 */
	public NTextButton(String title) {
		this(title, false);

	}

	/**
	 * 
	 * @param title
	 */
	public NTextButton(String title, boolean sound) {
		super(title);

		addListener(new YChangeListener(sound) {

			@Override
			public void changedY(Actor actor) {
				perform();

			}
		});

		// add gui elements?
		if (NSettings.isCleanerGuiDesign()) {
			return;
		}

		// add hove effect
		addCaptureListener(new InputListener() {

			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				if (!isDisabled()) {
					setStyle(NN.skin().getButtonHover());
				}
			}

			@Override
			public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
				if (!isDisabled()) {
					setStyle(NN.skin().getButton());
				}

			}
		});

		setStyle(NN.skin().getButton());

	}

	/**
	 * When true, the button will not toggle {@link #isChecked()} when clicked and
	 * will not fire a {@link ChangeEvent}.
	 */
	@Override
	public void setDisabled(boolean isDisabled) {
		super.setDisabled(isDisabled);

		// add gui elements?
		if (NSettings.isCleanerGuiDesign()) {
			return;
		}

		if (isDisabled) {
			getColor().a = 0.5f;
			setStyle(NN.skin().getButtonDisabled());
		} else {
			getColor().a = 1;
		}

	}

	public abstract void perform();

}

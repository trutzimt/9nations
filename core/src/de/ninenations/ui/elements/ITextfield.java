/**
 * 
 */
package de.ninenations.ui.elements;

import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * @author sven
 *
 */
public interface ITextfield {

	public void setText(String text);

	public String getText();

	public Actor getActor();
}

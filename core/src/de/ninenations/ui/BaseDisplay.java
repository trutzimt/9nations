
package de.ninenations.ui;

import de.ninenations.core.IMapObject;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.NSettings;
import de.ninenations.util.YLog;

/**
 * 
 */
public abstract class BaseDisplay implements IDisplay, IMapObject {

	protected String name;
	protected String type;

	public BaseDisplay() {
		this(null, null);
	}

	/**
	 * @param name
	 * @param type
	 */
	public BaseDisplay(String type, String name) {
		super();
		this.name = name;
		this.type = type;
	}

	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * @return the type
	 */
	@Override
	public String getType() {
		return type;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Check if this object has some errors
	 */
	public void validate() {
		if (type.equals(name)) {
			YLog.log("Validate", type, "type and name the same");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getInfoPanel()
	 */
	@Override
	public YTable getInfoPanel() {
		YTable t = new YTable();
		t.addH("General");
		t.addI(getIcon(), NSettings.isDebug() ? getName() + " (" + type + ")" : name);
		return t;
	}

}
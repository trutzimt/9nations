/**
 *
 */
package de.ninenations.ui;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.core.NN;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class YIcons {

	private YIcons() {}

	public static final int MOD = 242, MAP = 190, MOVETILE = 1, DEBUG = 643, WALK = 82, MAINMENU = 83, NEXTPLAYER = 220, FILE = 628, NEXTUNIT = 323,
			WARNING = 7, RESEARCH = 382, WIN = 70, LOSE = 298, ATTACK = 76, DEFEND = 329, DISEMBARK = 73, RANDOM = 75, EMBARK = 74, SHOP = 464, RESSOURCE = 210,
			QUEST = 193, BUILD = 223, TRAIN = 132, FOOD = 265, STONE = 305, RIGHT = 632, WRONG = 633, MESSAGE = 4, CULTURE = 80, GOLD = 464, FAITH = 528,
			WEALTH = 147, WORKER = 716, TOWN = 404, COMMUNITY = 4, ACHIEVEMENT = 88, SLEEP = 8, LOGO = 405, SPRING = 557, SUMMER = 558, AUTUMN = 559,
			WINTER = 556, LINUX = 245, MAC = 407, ANDROID = 244, WINDOWS = 408, HTML = 409;
	// to fix
	// public static final int ;

	// Feuer, Wasser, Luft, Erde, Metall, Holz, Leben, Tod, Energie
	public static final int FIRE = 64, WATER = 67, AIR = 69, EARTH = 543, METAL = 88, WOOD = 295, LIFE = 70, DEATH = 1, ENERGY = 66;

	/**
	 *
	 */
	public static void loadIcons() {
		NN.asset().loadT("system/icons/iconSet.png");
		NN.asset().loadT(NSettings.getGuiScaleFolder() + "/icons/iconSet.png");
	}

	/**
	 * Get the icon as image
	 * 
	 * @param type
	 * @return
	 */
	public static Image getIconI(int id) {
		int s = NSettings.getGuiScale();
		return new Image(new TextureRegion(NN.asset().getT(NSettings.getGuiScaleFolder() + "/icons/iconSet.png"), id % 16 * s, id / 16 * s, s, s));
	}

	/**
	 * Get the icon as image
	 * 
	 * @param type
	 * @return
	 */
	public static Image getIconO(int id) {
		int s = 32;
		return new Image(new TextureRegion(NN.asset().getT("system/icons/iconSet.png"), id % 16 * s, id / 16 * s, s, s));
	}

	public static Image getBuildO(int id) {
		return new Image(new TextureRegion(NN.asset().getT("system/tileset/tilemap.png"), id % 32 * 32, id / 32 * 32, 32, 32));
	}

	/**
	 * Scaled version
	 * 
	 * @param id
	 * @return
	 */
	public static Image getBuild(int id) {
		int s = NSettings.getGuiScale();

		return new Image(new TextureRegion(NN.asset().getT(NSettings.getGuiScaleFolder() + "/tileset/tilemap.png"), id % 32 * s, id / 32 * s, s, s));
	}

	/**
	 * Scaled version
	 * 
	 * @param id
	 * @return
	 */
	public static Image getAutotile(int id) {
		int s = NSettings.getGuiScale();

		return new Image(new TextureRegion(NN.asset().getT(NSettings.getGuiScaleFolder() + "/tileset/autotiles.png"), id % 32 * s, id / 32 * s, s, s));
	}

}

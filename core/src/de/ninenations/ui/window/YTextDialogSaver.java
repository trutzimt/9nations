/**
 * 
 */
package de.ninenations.ui.window;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;

import de.ninenations.core.NArray;
import de.ninenations.core.NN;
import de.ninenations.ui.elements.YTextButton;

/**
 * @author sven
 *
 */
public class YTextDialogSaver implements Serializable {

	private static final long serialVersionUID = 2541811118392546032L;

	private String header, message, img;
	private NArray<YTextButton> buttons;
	private int align;

	public YTextDialogSaver() {}

	/**
	 * @param header
	 * @param message
	 * @param img
	 */
	public YTextDialogSaver(String header, String message, String img) {
		super();
		this.header = header;
		this.message = message;
		this.img = "system/face/" + img + ".png";
		buttons = new NArray<>();
		align = Align.left;
	}

	/**
	 * @return the header
	 */
	public String getHeader() {
		return header;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @return the img
	 */
	public Image getImage() {
		// TODO better asset loading
		if (!NN.asset().isLoaded(img)) {
			NN.asset().loadT(img);
			NN.asset().finishLoadingAsset(img);
		}

		return new Image(NN.asset().getT(img));
	}

	/**
	 * @return the img
	 */
	public String getImagePath() {
		return img;
	}

	/**
	 * @return the buttons
	 */
	public NArray<YTextButton> getButtons() {
		return buttons;
	}

	/**
	 * @param buttons
	 *            the buttons to set
	 */
	public void addButton(YTextButton button) {
		buttons.add(button);
	}

	/**
	 * @param right
	 *            the right to set
	 */
	public YTextDialogSaver setAlign(int align) {
		this.align = align;
		return this;
	}

	/**
	 * @return the align
	 */
	public int getAlign() {
		return align;
	}

}

/**
 * 
 */
package de.ninenations.ui.window;

/**
 * @author sven
 *
 */
public abstract class YAWindow extends YWindow {

	/**
	 * @param title
	 */
	public YAWindow(String title) {
		super(title);
		create();
	}

	/**
	 * @param title
	 * @param settings
	 */
	public YAWindow(String title, boolean settings) {
		super(title, settings);
		create();
	}

	public abstract void create();
}

/**
 * 
 */
package de.ninenations.ui.window;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class YNotificationSaver implements Serializable {

	private static final long serialVersionUID = 2541811118392546032L;

	private String text;
	private transient Image icon;
	private int id;
	private int time;
	private transient YChangeListener action;
	private boolean shown;

	/**
	 * @param title
	 * @param text
	 * @param icon
	 */
	@SuppressWarnings("unused")
	private YNotificationSaver() {}

	/**
	 * @param title
	 * @param text
	 * @param icon
	 */
	@Deprecated
	public YNotificationSaver(String text, int id) {
		this(text, null, id);
	}

	/**
	 * @param title
	 * @param text
	 * @param icon
	 */
	public YNotificationSaver(String text, int id, YChangeListener action) {
		this(text, null, id, action);
	}

	/**
	 * @param title
	 * @param text
	 * @param icon
	 */
	@Deprecated
	public YNotificationSaver(String text, Image icon) {
		this(text, icon, -1);
	}

	/**
	 * @param title
	 * @param text
	 * @param icon
	 */
	@Deprecated
	public YNotificationSaver(String text, Image icon, int id) {
		this(text, icon, id, null);
	}

	/**
	 * @param title
	 * @param text
	 * @param icon
	 */
	public YNotificationSaver(String text, Image icon, int id, YChangeListener action) {
		super();
		this.text = text;
		this.icon = icon;
		this.id = id;
		time = NSettings.getNoticationDuration();

		this.action = action;
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @return the icon
	 */
	public Image getIcon() {
		if (icon == null) {
			return YIcons.getIconI(id >= 0 ? id : YIcons.WARNING);
		}
		return icon;
	}

	/**
	 * @return the time
	 */
	public int getTime() {
		return time;
	}

	/**
	 * @param time
	 *            the time to set
	 */
	public YNotificationSaver setTime(int time) {
		this.time = time;
		return this;
	}

	/**
	 * @return the action
	 */
	public YChangeListener getAction() {
		return action;
	}

	/**
	 * @return the shown
	 */
	public boolean isShown() {
		return shown;
	}

	/**
	 * @param shown
	 *            the shown to set
	 */
	public void setShown(boolean shown) {
		this.shown = shown;
	}

}

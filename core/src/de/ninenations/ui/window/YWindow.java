package de.ninenations.ui.window;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisWindow;

import de.ninenations.util.YSounds;

public class YWindow extends VisWindow {

	private boolean addedCloseButton;

	public YWindow(String title) {
		this(title, true);
	}

	/**
	 * Add default options
	 * 
	 * @param title
	 * @param settings
	 */
	public YWindow(String title, boolean settings) {
		super(title);

		addedCloseButton = settings;

		if (settings) {
			addCloseButton();
			closeOnEscape();
			setResizable(true);
			setCenterOnAdd(true);
		}

	}

	/**
	 * Add add the left a icon
	 * 
	 * @param i
	 */
	public void addTitleIcon(Image i) {
		// remove old
		getTitleTable().clearChildren();

		// add icon
		getTitleTable().add(i);
		getTitleTable().add(getTitleLabel()).expandX().fillX().minWidth(0);

		if (addedCloseButton) {
			addCloseButton();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.VisWindow#close()
	 */
	@Override
	public void close() {
		super.close();
		YSounds.play(YSounds.WINDOWCLOSE);
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	protected Widget addL(String label, String content) {
		return addL(label, new VisLabel(content));
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	protected Widget addL(String label, Widget content) {
		add(label + ":").align(Align.right);
		add(content).align(Align.left).growX().row();

		return content;
	}

	/**
	 * Helpermethod to add a Widget with label
	 * 
	 * @param label
	 * @param content
	 * @return
	 */
	protected WidgetGroup addL(String label, WidgetGroup content) {
		add(label + ":").align(Align.right);
		add(content).align(Align.left).growX().row();

		return content;
	}

	public void setCenterX() {
		setX(Gdx.graphics.getWidth() / 2 - getWidth() / 2);
	}

	public void setCenterY() {
		setY(Gdx.graphics.getHeight() / 2 - getHeight() / 2);
	}

	public void setRight(int columns) {
		setX(Gdx.graphics.getWidth() - Gdx.graphics.getWidth() / columns + Gdx.graphics.getWidth() / columns / 2 - getWidth() / 2);
	}

	public void setBottom(int rows) {
		setY(Gdx.graphics.getHeight() / rows - Gdx.graphics.getHeight() / rows / 2 - getHeight() / 2);

		//
	}

	public void setLeft(int columns) {
		setX(Gdx.graphics.getWidth() / columns / 2 - getWidth() / 2);
	}
}

package de.ninenations.ui.window;

import com.badlogic.gdx.Gdx;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;
import com.kotcrab.vis.ui.widget.tabbedpane.TabbedPane;
import com.kotcrab.vis.ui.widget.tabbedpane.TabbedPaneAdapter;

import de.ninenations.ui.elements.YSplitTab;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.YError;
import de.ninenations.util.YSounds;

public class YTabWindow extends YWindow {

	protected YTable content;
	protected TabbedPane tabbedPane;

	public YTabWindow(String name) {
		super(name);

		tabbedPane = new TabbedPane();

	}

	/**
	 * Add all listener
	 */
	@SuppressWarnings("rawtypes")
	protected void buildIt() {
		// add connection
		for (Tab t : tabbedPane.getTabs()) {
			if (t instanceof YSplitTab) {
				((YSplitTab) t).setWindow(this);
			}
		}

		// has a tab?
		if (tabbedPane.getTabs().size == 0) {
			add(new VisLabel("No content")).grow();
		} else

		// has tabs?
		if (tabbedPane.getTabs().size > 1) {
			tabbedPane.addListener(new TabbedPaneAdapter() {
				@Override
				public void switchedTab(Tab tab) {
					try {
						YSounds.play(YSounds.SWITCHTAB);
						content.clearChildren();
						content.add(tab.getContentTable()).grow();
					} catch (Throwable t) {
						YError.error(t, false);
					}
				}
			});

			add(tabbedPane.getTable()).growX().row();
			content = new YTable();
			add(content).grow().row();

			tabbedPane.switchTab(0);

		} else {
			add(tabbedPane.getTabs().get(0).getContentTable()).grow();
			getTitleLabel().setText(tabbedPane.getTabs().get(0).getTabTitle());
		}

		setResizable(true);
		setWidth(Gdx.graphics.getWidth() / 2);
		setHeight(Gdx.graphics.getHeight() / 2);
	}

	/**
	 * Close it
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public void close() {
		super.close();

		// inform the tabs
		for (Tab t : tabbedPane.getTabs()) {
			if (t instanceof YSplitTab) {
				((YSplitTab) t).close();
			}
		}
	}

	/**
	 * @return the tabbedPane
	 */
	public TabbedPane getTabbedPane() {
		return tabbedPane;
	}
}

/*
*
 */
package de.ninenations.ui;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.ui.elements.YTable;

/**
 * 
 */
public interface IDisplay {

	/**
	 * @return
	 */
	public abstract Image getIcon();

	/**
	 * @return
	 */
	public abstract String getName();

	/**
	 * @return
	 */
	public abstract YTable getInfoPanel();

}
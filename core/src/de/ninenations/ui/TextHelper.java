/**
 * 
 */
package de.ninenations.ui;

/**
 * @author sven
 *
 */
public class TextHelper {

	/**
	 * Get the time stamp
	 * 
	 * @param l
	 * @return
	 */
	public static String getCommaSep(String... l) {
		return getCommaSepA(l);
	}

	/**
	 * Get the time stamp
	 * 
	 * @param l
	 * @return
	 */
	public static String getCommaSepA(String[] l) {
		String erg = "";
		for (String s : l) {
			if (s == null || s.length() == 0) {
				continue;
			}

			if (erg.length() > 0) {
				erg += ", ";
			}
			erg += s;
		}

		return erg;
	}

	public static String plus(int num) {
		return num > 0 ? "+" + num : Integer.toString(num);
	}
}

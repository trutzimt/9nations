/**
 * 
 */
package de.ninenations.ui.actions;

import de.ninenations.ui.newWindow.NWindow;
import de.ninenations.util.YError;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public abstract class NWindowBuilder {

	protected boolean playSound;

	/**
	 * 
	 */
	public NWindowBuilder() {
		this(true);
	}

	/**
	 * 
	 */
	public NWindowBuilder(boolean playSound) {
		this.playSound = playSound;
	}

	public NWindow openE(String[] id) {
		try {
			if (playSound) {
				YSounds.pClick();
			}
			return open(id);
		} catch (Exception e) {
			YError.error(e, false);
		}
		return null;
	}

	public abstract NWindow open(String[] id);

}

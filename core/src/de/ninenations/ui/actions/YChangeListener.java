/**
 * 
 */
package de.ninenations.ui.actions;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import de.ninenations.util.YError;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public abstract class YChangeListener extends ChangeListener {

	protected boolean playSound;

	/**
	 * 
	 */
	public YChangeListener() {
		this(true);
	}

	/**
	 * 
	 */
	public YChangeListener(boolean playSound) {
		this.playSound = playSound;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.badlogic.gdx.scenes.scene2d.utils.ChangeListener#changed(com.badlogic.gdx
	 * .scenes.scene2d.utils.ChangeListener.ChangeEvent,
	 * com.badlogic.gdx.scenes.scene2d.Actor)
	 */
	@Override
	public void changed(ChangeEvent event, Actor actor) {
		try {
			if (playSound)
				YSounds.pClick();
			changedY(actor);
		} catch (Exception e) {
			YError.error(e, false);
		}

	}

	public abstract void changedY(Actor actor);

}

package de.ninenations.ui.actions;

import com.badlogic.gdx.scenes.scene2d.Actor;

import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;

public class MoveTo extends YChangeListener {

	private NOnMapObject obj;

	/**
	 * 
	 * @param obj
	 */
	public MoveTo(NOnMapObject obj) {
		super(true);

		this.obj = obj;
	}

	@Override
	public void changedY(Actor actor) {
		S.map().setCenterMapView(obj.getX(), obj.getY());
		MapScreen.get().getGui().showBottomMenuFor(obj.getX(), obj.getY());

	}

}

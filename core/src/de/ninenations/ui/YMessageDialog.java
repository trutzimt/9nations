package de.ninenations.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;

import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YTextButton;
import de.ninenations.ui.window.YWindow;
import de.ninenations.util.YSounds;

public class YMessageDialog extends YWindow {

	private YChangeListener action;
	protected VisTable buttons;

	public YMessageDialog(String title, String message, YChangeListener action) {
		super(title);

		this.action = action;

		// add content
		VisLabel t = new VisLabel(message);
		t.setWrap(message.length() > 50);
		VisScrollPane scrollPane = new VisScrollPane(t);
		scrollPane.setFadeScrollBars(false);
		scrollPane.setFlickScroll(false);
		scrollPane.setOverscroll(false, false);
		scrollPane.setScrollingDisabled(true, false);
		add(scrollPane).align(Align.left).grow().row();

		buttons = new VisTable(true);

		add(buttons).align(Align.right);

		if (message.length() < 50) {
			pack();
		} else {
			setWidth(Gdx.graphics.getWidth() / 2);
			setHeight(Gdx.graphics.getHeight() / 2);
		}

	}

	/**
	 * Build it to the end
	 * 
	 * @param stage
	 */
	public void build(Stage stage) {
		YTextButton t = new YTextButton("Close") {

			@Override
			public void perform() {
				YSounds.pClick();
				close();

			}
		};
		buttons.add(t);

		stage.addActor(this);
	}

	/**
	 *
	 * @param title
	 * @param action
	 */
	public void addButton(YTextButton button) {
		buttons.add(button);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.VisWindow#close()
	 */
	@Override
	public void close() {
		super.close();
		if (action != null) {
			action.changed(null, this);
		}
	}
}
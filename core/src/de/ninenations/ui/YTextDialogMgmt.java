/**
 * 
 */
package de.ninenations.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.core.NArray;
import de.ninenations.core.NN;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.elements.YTextButton;
import de.ninenations.ui.window.YTextDialogSaver;
import de.ninenations.ui.window.YWindow;
import de.ninenations.util.YError;

/**
 * @author sven
 *
 */
public class YTextDialogMgmt {

	private NArray<YTextDialogSaver> dialogs;
	private int act;
	private Image image, bottom, top;

	/**
	 * 
	 */
	public YTextDialogMgmt() {
		dialogs = new NArray<>();
		act = -1;

	}

	/**
	 * Check if their is some dialogs and show them
	 */
	public void checkToShow() {
		// need to show?
		if (act == -1 && dialogs.size == 0) {
			return;
		}

		// is showing?
		if (image != null) {
			return;
		}

		// show the dialogs
		show();
	}

	/**
	 * Add a new dialog
	 * 
	 * @param character
	 * @param message
	 * @param more,
	 *            follow more messages?
	 * @return
	 */
	public void add(YTextDialogSaver mess) {

		try {
			dialogs.add(mess);
		} catch (Exception e) {
			YError.error(e, false);
		}
	}

	/**
	 * Show the dialogs
	 */
	public void show() {
		// get act dialog
		act++;
		YTextDialogSaver d = dialogs.get(act);

		// first show?
		if (top == null) {
			// top
			top = new Image(NN.asset().getT("system/menu/black.png"));
			top.setScale(Gdx.graphics.getWidth() / 10f, Gdx.graphics.getHeight() / 6f / 10f);
			top.setY(Gdx.graphics.getHeight() - top.getHeight() * top.getScaleY());
			top.getColor().a = 0;
			top.addAction(Actions.fadeIn(1));
			MapScreen.get().getStage().addActor(top);

			// bottom
			bottom = new Image(NN.asset().getT("system/menu/black.png"));
			bottom.setScale(Gdx.graphics.getWidth() / 10f, Gdx.graphics.getHeight() / 6f / 10f);
			bottom.getColor().a = 0;
			bottom.addAction(Actions.fadeIn(1));
			MapScreen.get().getStage().addActor(bottom);
		}

		// hide old image?
		if (image != null && !d.getImagePath().equals(dialogs.get(act - 1).getImagePath())) {
			hideImage();
		}

		// show new image
		if (image == null) {

			// int sw = Gdx.graphics.getWidth() / 2;
			// int ww = Gdx.graphics.getWidth() / 3;

			// person
			image = d.getImage();

			// set scale
			int hw = Gdx.graphics.getWidth() / 2, hh = Gdx.graphics.getHeight() / 2, qh = Gdx.graphics.getHeight() / 3;
			image.setScaling(Scaling.fit);
			if (d.getAlign() == Align.center) {
				image.setBounds(hw / 2, qh, hw, hh);
			} else {
				image.setBounds(d.getAlign() == Align.left ? 0 : hw, qh, hw, hh);
			}
			// image.setScale(Gdx.graphics.getHeight() / image.getHeight());
			image.getColor().a = 0;
			// image.setX(d.isRight() ? Gdx.graphics.getWidth() - image.getWidth() *
			// image.getScaleX() : 0);

			// set pos
			// image.setOriginX(image.getWidth() * image.getScaleX() / 2);
			// image.setX(d.isRight() ? sw + ww : sw - ww);
			// image.setY(Gdx.graphics.getHeight() / 3);
			image.addAction(Actions.fadeIn(1));
			MapScreen.get().getStage().addActor(image);
		}

		// show it
		MapScreen.get().getStage().addActor(new YTextDialogWindow(d, act + 1 == dialogs.size));

		//
		// // last screen?
		// if (act + 1 == dialogs.size) {
		// Dialogs.showOKDialog(MapScreen.get().getStage(), d.getHeader(),
		// d.getMessage()).addListener(new ChangeListener() {
		// @Override
		// public void changed(ChangeEvent event, Actor actor) {
		// clear();
		// }
		// });
		// return;
		// }
		//
		// // TODO show normal dialog
		// Dialogs.showOKDialog(MapScreen.get().getStage(), d.getHeader(),
		// d.getMessage()).addListener(new ChangeListener() {
		// @Override
		// public void changed(ChangeEvent event, Actor actor) {
		// show();
		// }
		// });

	}

	private void hideImage() {
		image.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
		image = null;
	}

	private void hideTop() {
		top.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
		top = null;
		bottom.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
		bottom = null;
	}

	/**
	 * Clear everything
	 */
	private void clear() {
		act = -1;
		dialogs.clear();
		hideTop();
		hideImage();
		image = null;
	}

	class YTextDialogWindow extends YWindow {

		private boolean lastScreen;

		public YTextDialogWindow(YTextDialogSaver mess, boolean lastScreen) {
			super(mess.getHeader());

			this.lastScreen = lastScreen;

			// set header
			getTitleLabel().setAlignment(mess.getAlign());

			// add text
			VisLabel l = new VisLabel(mess.getMessage());
			l.setWrap(true);
			super.add(l).grow().row();

			// add button
			YTextButton t = new YTextButton(lastScreen ? "Close" : "Next") {

				@Override
				public void perform() {
					close();

				}
			};
			super.add(t).row();

			// set position
			setCenterOnAdd(false);
			setWidth(Gdx.graphics.getWidth() / 3 * 2);
			setHeight(Gdx.graphics.getHeight() / 3);
			setCenterX();

		}

		@Override
		public void close() {
			super.close();

			try {
				if (lastScreen) {
					YTextDialogMgmt.this.clear();
				} else {
					show();
				}
			} catch (Throwable t) {
				YError.error(t, false);
			}

		}
	}
}

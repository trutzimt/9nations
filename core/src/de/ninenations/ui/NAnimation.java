/**
 * 
 */
package de.ninenations.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * @author sven
 *
 */
public class NAnimation extends Actor {

	protected Animation<TextureRegion> animation;
	protected TextureRegion currentRegion;
	protected float time = 0f;

	/**
	 * 
	 */
	public NAnimation(Animation<TextureRegion> animation) {
		this.animation = animation;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.scenes.scene2d.Actor#act(float)
	 */
	@Override
	public void act(float delta) {
		super.act(delta);
		time += delta;

		currentRegion = animation.getKeyFrame(time, false);

		if (time > animation.getAnimationDuration()) {
			remove();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.badlogic.gdx.scenes.scene2d.Actor#draw(com.badlogic.gdx.graphics.g2d.
	 * Batch, float)
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);

		// reset color
		Color color = getColor();
		batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

		batch.draw(currentRegion, getX(), getY());
	}

}

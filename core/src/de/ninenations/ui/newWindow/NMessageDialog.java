package de.ninenations.ui.newWindow;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;

import de.ninenations.core.NN;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.NTextButton;

public class NMessageDialog extends NWindow {

	private YChangeListener action;
	protected VisTable buttons;
	protected Image black;

	public NMessageDialog(String id, String title, int icon, String message, YChangeListener action) {
		super(id, title, icon);

		this.action = action;

		// add content
		VisLabel t = new VisLabel(message);
		t.setWrap(message.length() > 50);
		VisScrollPane scrollPane = new VisScrollPane(t);
		scrollPane.setFadeScrollBars(false);
		scrollPane.setFlickScroll(false);
		scrollPane.setOverscroll(false, false);
		scrollPane.setScrollingDisabled(true, false);
		add(scrollPane).align(Align.left).grow().row();

		buttons = new VisTable(true);

		add(buttons).align(Align.right);

		if (message.length() < 50) {
			pack();
		} else {
			setWidth(Gdx.graphics.getWidth() / 3 * 2);
			setHeight(Gdx.graphics.getHeight() / 2);
		}

	}

	public void build(Stage stage) {

		if (black != null) {
			stage.addActor(black);
		}

		pack();

		loadPos();

		stage.addActor(this);
	}

	public void addClose() {
		buttons.add(new NTextButton("Close") {

			@Override
			public void perform() {
				close();

			}
		});
	}

	/**
	 *
	 * @param title
	 * @param action
	 */
	public void addButton(NTextButton button) {
		buttons.add(button);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.VisWindow#close()
	 */
	@Override
	public void close() {
		super.close();
		if (action != null) {
			action.changed(null, this);
		}

		// remove image?
		if (black != null) {
			black.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
		}
	}

	/**
	 * Set this dialog
	 */
	@Override
	public void setModal(boolean isModal) {
		super.setModal(true);

		black = new Image(NN.asset().getT("system/menu/black.png"));
		black.getColor().a = 0f;
		black.setFillParent(true);
		black.addAction(Actions.alpha(0.75f, 10));
	}
}

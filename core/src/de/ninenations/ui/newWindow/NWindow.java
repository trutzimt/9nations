package de.ninenations.ui.newWindow;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisWindow;

import de.ninenations.core.NN;
import de.ninenations.ui.YIcons;
import de.ninenations.util.NSettings;
import de.ninenations.util.YSounds;

public class NWindow extends VisWindow {

	protected boolean addedCloseButton;
	protected String baseid, baseTitle;
	protected int icon;

	/**
	 * Create it
	 * 
	 * @param title
	 */
	public NWindow(String id, String title, int icon) {
		this(id, title, icon, true);
	}

	/**
	 * Add default options
	 * 
	 * @param title
	 * @param settings
	 */
	public NWindow(String id, String title, int icon, boolean settings) {
		super(title);
		baseTitle = title;
		baseid = id;
		this.icon = icon;

		addedCloseButton = settings;
		addSetting();

		addTitleIcon(YIcons.getIconI(icon));
		if (!NSettings.isCleanerGuiDesign()) {
			setBackground(NN.skin().getWindowSimple());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.VisWindow#addCloseButton()
	 */
	@Override
	public void addCloseButton() {
		Label titleLabel = getTitleLabel();
		Table titleTable = getTitleTable();

		VisImageButton closeButton = new VisImageButton("close-window");
		titleTable.add(closeButton).padRight(-getPadRight() + 0.7f);
		closeButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				close();
			}
		});
		closeButton.addListener(new ClickListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				event.cancel();
				return true;
			}
		});

		if (titleLabel.getLabelAlign() == Align.center && titleTable.getChildren().size == 2) {
			titleTable.getCell(titleLabel).padLeft(closeButton.getWidth() * 2);
		}
	}

	/**
	 * @param settings
	 */
	protected void addSetting() {
		if (addedCloseButton) {
			addCloseButton();
			closeOnEscape();
			setResizable(true);
			setCenterOnAdd(true);
		}
	}

	/**
	 * Add add the left a icon
	 * 
	 * @param i
	 */
	public void addTitleIcon(Image i) {
		// remove old
		getTitleTable().clearChildren();

		// add icon
		getTitleTable().add(i);
		getTitleTable().add(getTitleLabel()).expandX().fillX().minWidth(0);

		if (addedCloseButton) {
			addCloseButton();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.VisWindow#close()
	 */
	@Override
	public void close() {
		savePos();

		super.close();
		YSounds.play(YSounds.WINDOWCLOSE);
		NN.windows().remove(baseid);
	}

	/**
	 * save the window position
	 */
	protected void savePos() {
		// save pos
		NSettings.getPref().putString(baseid + ".pos", getX() + "," + getY() + "," + getWidth() + "," + getHeight());
	}

	/**
	 * load the window position
	 */
	protected void loadPos() {
		// load pos?
		if (!NSettings.getPref().contains(baseid + ".pos")) {
			return;
		}

		String[] pos = NSettings.getPref().getString(baseid + ".pos").split(",");

		setX(Float.parseFloat(pos[0]));
		setY(Float.parseFloat(pos[1]));
		setWidth(Float.parseFloat(pos[2]));
		setHeight(Float.parseFloat(pos[3]));
		setCenterOnAdd(false);
	}

	public void setCenterX() {
		setX(Gdx.graphics.getWidth() / 2 - getWidth() / 2);
	}

	public void setCenterY() {
		setY(Gdx.graphics.getHeight() / 2 - getHeight() / 2);
	}

	public void setRight(int columns) {
		setX(Gdx.graphics.getWidth() - Gdx.graphics.getWidth() / columns + Gdx.graphics.getWidth() / columns / 2 - getWidth() / 2);
	}

	public void setBottom(int rows) {
		setY(Gdx.graphics.getHeight() / rows - Gdx.graphics.getHeight() / rows / 2 - getHeight() / 2);

		//
	}

	public void setLeft(int columns) {
		setX(Gdx.graphics.getWidth() / columns / 2 - getWidth() / 2);
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return baseid;
	}

	public NWindow show(Object[] detailID) {
		return this;
	}
}

/**
 * 
 */
package de.ninenations.ui.newWindow;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public abstract class NTab extends Tab {

	private Image icon;
	private String text, id;

	/**
	 * 
	 * @param id
	 * @param text
	 * @param icon
	 */
	public NTab(String id, String text, int icon) {
		this(id, text, YIcons.getIconI(icon));
	}

	/**
	 * 
	 * @param id
	 * @param text
	 * @param icon
	 */
	public NTab(String id, String text, Image icon) {
		super(false, false);

		this.id = id;
		this.text = text;
		this.icon = icon;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getTabTitle()
	 */
	@Override
	public String getTabTitle() {
		return text;
	}

	/**
	 * @return the icon
	 */
	public Image getIcon() {
		return icon;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

}

/**
 * 
 */
package de.ninenations.ui.newWindow;

import java.util.HashMap;

import com.badlogic.gdx.utils.Array;

import de.ninenations.core.NN;
import de.ninenations.ui.actions.NWindowBuilder;
import de.ninenations.util.YLog;

/**
 * @author sven
 *
 */
public class WindowManager {

	private HashMap<String, NWindowBuilder> builder;
	private Array<NWindow> windows;

	/**
	 * 
	 */
	public WindowManager() {
		builder = new HashMap<>();
		windows = new Array<>();
	}

	public void register(String id, NWindowBuilder b) {
		if (builder.containsKey(id)) {
			throw new IllegalArgumentException("window builder " + id + " already exist");
		}

		builder.put(id, b);
	}

	private String getBaseID(String id) {
		return id.split("-")[0];
	}

	private String[] getDetailID(String id) {
		return id.split("-");
	}

	/**
	 * Check if this window is open
	 * 
	 * @param id
	 * @return
	 */
	public boolean isOpen(String id) {
		return get(id) != null;
	}

	/**
	 * Check if this window is open
	 * 
	 * @param id
	 * @return
	 */
	public NWindow get(String id) {
		for (NWindow window : windows) {
			if (window.getId().equals(id)) {
				return window;
			}
		}

		return null;
	}

	public void remove(String id) {
		String bid = getBaseID(id);
		NWindow w = get(bid);
		if (w != null) {
			windows.removeValue(w, true);
		}
	}

	/**
	 * Open this window
	 * 
	 * @param id
	 */
	public NWindow open(String id) {
		String bid = getBaseID(id);

		// is open?
		if (!isOpen(bid)) {
			NWindow w = builder.get(bid).openE(getDetailID(id));
			if (w == null) {
				return null;
			}
			windows.add(w);
			NN.screen().getStage().addActor(w);
			return w;
		} else {
			// TODO set focus
			NWindow w = get(bid);
			w.toFront();
			// show
			w.show(getDetailID(id));
			return w;
		}

	}

	/**
	 * Close this window, if open
	 * 
	 * @param id
	 */
	public void close(String id) {
		String bid = getBaseID(id);

		NWindow w = get(bid);
		if (w != null) {
			w.close();
		}
	}

	/**
	 * Close this window, if open
	 * 
	 * @param baseid
	 */
	public void closeAll() {
		for (NWindow window : windows) {
			window.close();
		}
		windows.clear();
	}

	/**
	 * Open or close this window
	 * 
	 * @param id
	 */
	public void swap(String id) {
		String bid = getBaseID(id);
		YLog.log("swap", id, bid, isOpen(bid));
		// is open?
		if (isOpen(bid)) {
			close(bid);
		} else {
			open(bid);
		}
	}

}

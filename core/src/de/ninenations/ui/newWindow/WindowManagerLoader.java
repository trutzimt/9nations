/**
 * 
 */
package de.ninenations.ui.newWindow;

import de.ninenations.core.NN;
import de.ninenations.feedback.FeedbackWindow;
import de.ninenations.menu.MenuWindow;
import de.ninenations.menu.OptionsWindow;
import de.ninenations.player.PlayerMapPreview;
import de.ninenations.ui.actions.NWindowBuilder;

/**
 * @author sven
 *
 */
public class WindowManagerLoader {

	private WindowManagerLoader() {}

	/**
	 * 
	 */
	public static void load() {
		NN.windows().register(OptionsWindow.ID, new NWindowBuilder(true) {

			@Override
			public NWindow open(String[] id) {
				return new OptionsWindow().show(id);
			}
		});
		NN.windows().register(MenuWindow.ID, new NWindowBuilder(true) {

			@Override
			public NWindow open(String[] id) {
				return new MenuWindow(NN.screen());
			}
		});
		NN.windows().register(PlayerMapPreview.ID, new NWindowBuilder(true) {

			@Override
			public NWindow open(String[] id) {
				return new PlayerMapPreview();
			}
		});
		NN.windows().register(FeedbackWindow.ID, new NWindowBuilder(true) {

			@Override
			public NWindow open(String[] id) {
				return new FeedbackWindow();
			}
		});
	}

}

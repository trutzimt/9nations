/**
 * 
 */
package de.ninenations.ui.newWindow;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.SnapshotArray;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.NTable;
import de.ninenations.util.NSettings;
import de.ninenations.util.YError;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public abstract class NTabWindow extends NWindow {

	protected Array<NTab> tabs;
	protected NTable tabContent, tabBar;
	protected int actTab;
	protected String error;
	protected boolean pack;

	/**
	 * @param title
	 * @param settings
	 */
	public NTabWindow(String id, String title, int icon) {
		super(id, title, icon, true);
		tabs = new Array<>();
		pack = true;
		error = "Nothing to show";
	}

	protected void addTab(NTab tab) {
		tabs.add(tab);
	}

	/**
	 * 
	 */
	protected void buildIt() {
		// no tabs?
		if (tabs.size == 0) {
			add(new VisLabel(error));
			return;
		}

		// TODO only one tab

		tabContent = new NTable();
		tabBar = new NTable();

		// add tabs
		for (final NTab tab : tabs) {
			VisImageButton i = new VisImageButton(tab.getIcon().getDrawable(), tab.getTabTitle());
			i.addCaptureListener(new YChangeListener() {

				@Override
				public void changedY(Actor actor) {
					YSounds.play(YSounds.SWITCHTAB);
					showTab(tabs.indexOf(tab, true));
				}
			});
			i.setGenerateDisabledImage(true);
			tabBar.add(i).align(Align.left);
		}

		// build table
		// VisLabel b = new VisLabel("");
		// b.setFillParent(true);
		// tabBar.add(b);// .growX();
		tabBar.row();
		tabBar.addSep().expandX().colspan(tabs.size);

		add(tabBar).fillX().row();
		showTab(0);

		// add content
		add(tabContent).grow();

		if (pack) {
			pack();
		} else {
			setWidth(Gdx.graphics.getWidth() / 2);
			setHeight(Gdx.graphics.getHeight() / 2);
		}
		loadPos();
	}

	protected NTab showTab(int id) {
		try {
			tabContent.clearChildren();
			tabContent.add(tabs.get(id).getContentTable()).grow();
		} catch (Throwable t) {
			YError.error(t, false);
		}

		// show it
		SnapshotArray<Actor> ary = tabBar.getChildren();
		for (int i = 0; i < ary.size; i++) {
			if (!(ary.get(i) instanceof VisImageButton)) {
				continue;
			}
			VisImageButton b = (VisImageButton) ary.get(i);
			b.setDisabled(i == id);
			// b.getImage().getColor().a = i == id ? 0.1f : 1;
			// b.setTouchable(i == id ? Touchable.disabled : Touchable.enabled);
		}

		// set title
		getTitleLabel().setText(baseTitle + " | " + tabs.get(id).getTabTitle());

		actTab = id;

		return tabs.get(id);
	}

	protected NTab showTab(String id) {
		// show it
		for (int i = 0; i < tabs.size; i++) {
			if (tabs.get(i).getId().equals(id)) {
				return showTab(i);
			}
		}

		return null;
	}

	/**
	 * save the window position
	 */
	@Override
	protected void savePos() {
		super.savePos();
		// save tab
		NSettings.getPref().putString(baseid + ".tab", tabs.get(actTab).getId());
	}

	/**
	 * load the window position
	 */
	@Override
	protected void loadPos() {
		super.loadPos();
		// load pos?
		if (!NSettings.getPref().contains(baseid + ".tab")) {
			return;
		}

		// has tab? show it
		showTab(NSettings.getPref().getString(baseid + ".tab"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.newWindow.NWindow#show(java.lang.String[])
	 */
	@Override
	public NWindow show(Object[] detailID) {
		// show some tab?
		if (detailID.length < 2) {
			return this;
		}
		// show it
		showTab((String) detailID[1]);

		return this;
	}
}

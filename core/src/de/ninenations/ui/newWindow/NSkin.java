/**
 * 
 */
package de.ninenations.ui.newWindow;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.ObjectMap;
import com.kotcrab.vis.ui.widget.VisImageButton.VisImageButtonStyle;
import com.kotcrab.vis.ui.widget.VisImageTextButton.VisImageTextButtonStyle;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.core.NN;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class NSkin {

	private TextButtonStyle button, buttonHover, buttonDisabled;
	private VisImageButtonStyle imageButton, imageButtonHover;
	private VisImageTextButtonStyle imageTextButton, imageTextButtonHover;
	private NinePatchDrawable patch, patchHover, patchDown, simpleWindow;

	private ObjectMap<String, Animation<TextureRegion>> animations;

	/**
	 * 
	 */
	public NSkin() {
		animations = new ObjectMap<>();
	}

	public void loadAsset() {
		String folder = NSettings.getGuiScaleFolder() + "/skin/";
		NN.asset().loadT(folder + "boton.png");
		NN.asset().loadT(folder + "botonHover.png");
		NN.asset().loadT(folder + "botonDown.png");
		NN.asset().loadT(folder + "separator.png");
		NN.asset().loadT(folder + "simple.png");

		// load all animations
		for (FileHandle f : Gdx.files.internal("system/animations").list("png")) {
			NN.asset().loadT(f.path());
		}

	}

	public Animation<TextureRegion> getAnimation(String name) {
		// contains?
		if (animations.containsKey(name)) {
			return animations.get(name);
		}

		// build it
		Texture ani = NN.asset().getT("system/animations/" + name + ".png");
		String[] data = Gdx.files.internal("system/animations/" + name + ".conf").readString().split("-");

		// Use the split utility method to create a 2D array of TextureRegions. This is
		// possible because this sprite sheet contains frames of equal size and they are
		// all aligned.
		TextureRegion[][] tmp = TextureRegion.split(ani, ani.getWidth() / Integer.valueOf(data[1]), ani.getHeight() / Integer.valueOf(data[2]));

		// Place the regions into a 1D array in the correct order, starting from the top
		// left, going across first. The Animation constructor requires a 1D array.
		TextureRegion[] walkFrames = new TextureRegion[tmp.length * tmp[0].length];
		int index = 0;
		for (TextureRegion[] element : tmp) {
			for (TextureRegion element2 : element) {
				walkFrames[index++] = element2;
			}
		}

		// Initialize the Animation with the frame interval and array of frames
		Animation<TextureRegion> animation = new Animation<>(Float.valueOf(data[0]), walkFrames);
		animations.put(name, animation);
		return animation;
	}

	public NinePatchDrawable getWindowSimple() {
		// load it
		if (simpleWindow == null) {
			int fak = NSettings.getGuiScale() / 32;
			fak *= 6;
			String folder = NSettings.getGuiScaleFolder() + "/skin/";
			NinePatch n = new NinePatch(NN.asset().getT(folder + "simple.png"), fak, fak, fak + fak * 6, fak);
			n.setMiddleHeight(20);
			n.setMiddleWidth(20);
			simpleWindow = new NinePatchDrawable(n);
		}

		return simpleWindow;
	}

	/**
	 * @return the button
	 */
	public TextButtonStyle getButton() {
		// load it
		if (button == null) {
			int fak = NSettings.getGuiScale() / 32;
			String folder = NSettings.getGuiScaleFolder() + "/skin/";

			patch = new NinePatchDrawable(new NinePatch(NN.asset().getT(folder + "boton.png"), 8 * fak, 8 * fak, 5 * fak, 5 * fak));
			patch.getPatch().setMiddleWidth(20);
			patchHover = new NinePatchDrawable(new NinePatch(NN.asset().getT(folder + "botonHover.png"), 8 * fak, 8 * fak, 5 * fak, 5 * fak));
			patchHover.getPatch().setMiddleWidth(20);
			patchDown = new NinePatchDrawable(new NinePatch(NN.asset().getT(folder + "botonDown.png"), 8 * fak, 8 * fak, 5 * fak, 5 * fak));
			patchDown.getPatch().setMiddleWidth(20);

			button = new TextButtonStyle(patch, patchDown, patch, new VisLabel().getStyle().font);

		}

		return button;
	}

	/**
	 * @return the buttonHover
	 */
	public TextButtonStyle getButtonHover() {
		// load it
		if (buttonHover == null) {
			buttonHover = new TextButtonStyle(patchHover, patchDown, patchHover, new VisLabel().getStyle().font);
		}

		return buttonHover;
	}

	/**
	 * @return the buttonHover
	 */
	public TextButtonStyle getButtonDisabled() {
		// load it
		if (buttonDisabled == null) {
			buttonDisabled = new TextButtonStyle(patch, patch, patch, new VisLabel().getStyle().font);
		}

		return buttonDisabled;
	}

	/**
	 * @return the imageButton
	 */
	public VisImageTextButtonStyle getImageTextButton() {
		// load it
		if (imageTextButton == null) {
			// load it?
			if (patch == null) {
				getButton();
			}

			imageTextButton = new VisImageTextButtonStyle(patch, patchDown, patch, new VisLabel().getStyle().font);
		}
		return imageTextButton;
	}

	/**
	 * @return the imageButtonHover
	 */
	public VisImageTextButtonStyle getImageTextButtonHover() {
		// load it
		if (imageTextButtonHover == null) {
			imageTextButtonHover = new VisImageTextButtonStyle(patchHover, patchDown, patchHover, new VisLabel().getStyle().font);
		}
		return imageTextButtonHover;
	}

	/**
	 * @return the imageButton
	 */
	public VisImageButtonStyle getImageButton() {
		// load it
		if (imageButton == null) {
			// load it?
			if (patch == null) {
				getButton();
			}

			imageButton = new VisImageButtonStyle(patch, patchDown, patch, patch, patchDown, patch);
		}
		return imageButton;
	}

	/**
	 * @return the imageButtonHover
	 */
	public VisImageButtonStyle getImageButtonHover() {
		// load it
		if (imageButtonHover == null) {
			imageButtonHover = new VisImageButtonStyle(patchHover, patchDown, patchHover, patchHover, patchDown, patchHover);
		}
		return imageButtonHover;
	}

}

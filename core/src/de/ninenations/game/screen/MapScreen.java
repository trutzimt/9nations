/**
 * 
 */
package de.ninenations.game.screen;

import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;

import de.ninenations.actions.base.GAction;
import de.ninenations.core.NN;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.another.MapGeneralInputAdapter;
import de.ninenations.game.map.NMap;
import de.ninenations.saveload.SaveLoadManagement;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.screen.IScreen;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.util.NSettings;
import de.ninenations.util.YError;
import de.ninenations.util.YLog;
import de.ninenations.util.YMusic;

/**
 * @author sven
 *
 */
public class MapScreen implements IScreen, Screen {

	private static MapScreen mapScreen;

	private NGui gui;
	private MapData data;
	private NMap map;
	private MapGeneralInputAdapter inputs;

	private GAction activeAction;

	private HashMap<Integer, YChangeListener> keyBindings;

	/**
	 * 
	 */
	private MapScreen() {
		inputs = new MapGeneralInputAdapter();
		gui = new NGui();
		data = new MapData();
		map = new NMap();

		keyBindings = new HashMap<>();
	}

	public static void init(BaseScenario scen) {
		mapScreen = new MapScreen();

		// init the scenario
		scen.setScenarioSettings(mapScreen.data);
		mapScreen.map.init();
		mapScreen.data.init(scen);
		scen.afterLoad();
		scen.firstStart();
		mapScreen.map.initMap();
		NN.mods().startGame();

		mapScreen.gui.createGui();
		mapScreen.managementInput();
		YMusic.startInGameMusic();

		// start it
		mapScreen.data.getPlayers().start();
	}

	public static void load(MapData data) {
		mapScreen = new MapScreen();

		// init the scenario
		mapScreen.data = data;
		mapScreen.map.init();
		mapScreen.data.afterLoad();
		mapScreen.map.initMap();
		NN.mods().startGame();

		mapScreen.gui.createGui();
		mapScreen.managementInput();
		YMusic.startInGameMusic();

		for (ScenConf c : ScenConf.values()) {
			YLog.log(c, data.isConf(c));
		}

		S.actPlayer().performAtRoundBegin();
	}

	public static void exit() {
		mapScreen = null;
	}

	/**
	 * Add all listener
	 */
	private void managementInput() {
		InputMultiplexer multiplexer = new InputMultiplexer();
		multiplexer.addProcessor(gui.getStage());
		multiplexer.addProcessor(map.getStage());
		multiplexer.addProcessor(inputs);

		NN.plattform().addInputs(this, multiplexer);

		Gdx.input.setInputProcessor(multiplexer);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#show()
	 */
	@Override
	public void show() {
		// map.so

		// show the gui
		gui.nextPlayer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.3f, 0.3f, 0.3f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		map.render(delta);
		gui.render(delta);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
		map.resize(width, height);
		gui.resize(width, height);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#pause()
	 */
	@Override
	public void pause() {
		if (S.isActive(ScenConf.LOADSAVE)) {
			SaveLoadManagement.save("temp", true);
		}
		NSettings.getPref().flush();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resume()
	 */
	@Override
	public void resume() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#hide()
	 */
	@Override
	public void hide() {}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	@Override
	public void dispose() {
		map.dispose();
		gui.dispose();
		NN.windows().closeAll();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.screen.IScreen#getStage()
	 */
	@Override
	public Stage getStage() {
		return gui.getStage();
	}

	/**
	 * @return the mapScreen
	 */
	public static MapScreen get() {
		return mapScreen;
	}

	/**
	 * @return the gui
	 */
	public NGui getGui() {
		return gui;
	}

	/**
	 * @return the data
	 */
	public MapData getData() {
		return data;
	}

	/**
	 * @return the map
	 */
	public NMap getMap() {
		return map;
	}

	/**
	 * @return the inputs
	 */
	public MapGeneralInputAdapter getInputs() {
		return inputs;
	}

	/**
	 * @param activeAction
	 *            the activeAction to set
	 */
	public void setActiveAction(GAction activeAction) {
		if (this.activeAction != null) {
			this.activeAction.destroyActive();
		}

		this.activeAction = activeAction;
		inputs.setActiveAction(activeAction);

		gui.setActiveAction(activeAction);
	}

	/**
	 * @return the keyBindings
	 */
	public HashMap<Integer, YChangeListener> getKeyBindings() {
		return keyBindings;
	}

	/**
	 * @param keyBindings
	 *            the keyBindings to set
	 */
	public void addKeyBinding(int key, YChangeListener action) {

		if (keyBindings.containsKey(key)) {
			YError.error(new IllegalArgumentException("Key " + Keys.toString(key) + " already exist."), false);
		}
		keyBindings.put(key, action);

	}

}

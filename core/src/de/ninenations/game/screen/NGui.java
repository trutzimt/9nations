/**
 * 
 */
package de.ninenations.game.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.kotcrab.vis.ui.util.ToastManager;
import com.kotcrab.vis.ui.widget.Tooltip;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisWindow;
import com.kotcrab.vis.ui.widget.toast.ToastTable;

import de.ninenations.actions.base.GAction;
import de.ninenations.core.NN;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.game.S;
import de.ninenations.game.map.DebugWindow;
import de.ninenations.game.map.NMapBuilding;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.kingdom.KingdomOverviewWindow;
import de.ninenations.menu.MenuWindow;
import de.ninenations.player.PlayerMapPreview;
import de.ninenations.quests.WindowQuest;
import de.ninenations.research.ResearchWindow;
import de.ninenations.towns.Town;
import de.ninenations.towns.TownWindow;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.YTextDialogMgmt;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.NImageButton;
import de.ninenations.ui.elements.YGameImageButton;
import de.ninenations.ui.window.YNotificationSaver;
import de.ninenations.util.NSettings;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class NGui {

	private Stage stage;
	private Table mainMenu, unitMenu, buildMenu, overviewMenu;

	private YGameImageButton nextUnit;

	private VisLabel timeLabel;
	private Image timeImg;
	private ObjectMap<String, YGameImageButton> mainMenuButtons;

	/**
	 * Info for an active action
	 */
	private VisImageTextButton btnActionCancel;
	private VisLabel lblInfo;
	private Image imgInfo;

	private ToastManager toasts;

	private YTextDialogMgmt dialog;
	private NWeather weather;

	/**
	 * 
	 */
	public NGui() {
		stage = new Stage();
	}

	public void createGui() {

		// add weather
		weather = new NWeather();
		weather.init(stage);
		weather.checkRound();

		createBars();

		Table root = new Table(), rootTop = new Table(), rootBottom = new Table();

		overviewMenu = new Table();

		// build time label
		timeLabel = new VisLabel("?");
		rootTop.add(overviewMenu).align(Align.topLeft);

		createMenuButton();

		// add bottom
		unitMenu = new Table();
		buildMenu = new Table();

		nextUnit = new YGameImageButton("Next Unit", YIcons.NEXTUNIT, Keys.ALT_LEFT) {

			@Override
			public void pressAction() {
				// remove old action
				setActiveAction(null);

				if (S.actPlayer().showNextUnit()) {

					YSounds.pClick();
					nextUnit.getColor().a = 1;

				} else {
					YSounds.pBuzzer();
					addToast(new YNotificationSaver("No unit with AP left. Finish your round.", null, YIcons.NEXTUNIT));
					nextUnit.getColor().a = 0.5f;
				}
			}
		};

		YGameImageButton nextPlayer = new YGameImageButton("Next Player", YIcons.NEXTPLAYER, Keys.SPACE) {

			@Override
			public void pressAction() {
				// remove old action
				setActiveAction(null);

				YSounds.pClick();
				closeAllWindow();
				MapScreen.get().getData().getPlayers().nextPlayer();
			}
		};

		rootTop.add().growX();
		rootTop.add(nextUnit).align(Align.topRight);
		rootTop.add(nextPlayer).align(Align.topRight);
		rootTop.add(mainMenu).align(Align.topRight);

		resetObjectMenu();

		rootBottom.add(unitMenu).align(Align.bottomLeft);
		rootBottom.add().growX();
		rootBottom.add(buildMenu).align(Align.bottomRight);

		root.add(rootTop).align(Align.top).growX().row();
		root.add().grow().row();
		root.add(rootBottom).align(Align.bottom).growX().row();

		root.setFillParent(true);
		stage.addActor(root);

		imgInfo = new Image();
		imgInfo.setVisible(false);
		lblInfo = new VisLabel("?");
		lblInfo.setVisible(false);

		// cancel button
		btnActionCancel = new VisImageTextButton("?", YIcons.getIconI(YIcons.WARNING).getDrawable());
		btnActionCancel.addCaptureListener(new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				MapScreen.get().setActiveAction(null);

			}
		});
		btnActionCancel.setVisible(false);

		toasts = new ToastManager(stage);
		toasts.setAlignment(Align.topLeft);
		toasts.setScreenPadding((int) (1.25f * NSettings.getGuiScale()));

		// add dialogs
		dialog = new YTextDialogMgmt();
	}

	/**
	 * Show the info for some thing
	 * 
	 * @param baseAction
	 */
	public void setInfo(String text, Drawable ico) {
		setInfo(text, ico, null);
	}

	/**
	 * Show the info for some thing
	 * 
	 * @param baseAction
	 */
	public void setInfo(String text, Drawable ico, Color c) {
		if (text == null) {
			lblInfo.setVisible(false);
			imgInfo.setVisible(false);
			return;
		}

		lblInfo.setText(text);
		lblInfo.setVisible(true);
		lblInfo.setColor(c != null ? c : Color.WHITE);

		imgInfo.setDrawable(ico);
		imgInfo.setVisible(true);
		imgInfo.setColor(c != null ? c : Color.WHITE);

	}

	/**
	 * 
	 */
	private void createMenuButton() {
		// build mainmenu
		mainMenu = new Table();
		mainMenuButtons = new ObjectMap<>();

		// add the buttons
		YGameImageButton b = new YGameImageButton("Overview", YIcons.RESSOURCE, Keys.O) {

			@Override
			public void pressAction() {
				if (checkToOpenWindow(KingdomOverviewWindow.class)) {
					stage.addActor(new KingdomOverviewWindow());
				}
			}
		};
		mainMenuButtons.put("overview", b);

		// add the buttons
		b = new YGameImageButton("Research", YIcons.RESEARCH, Keys.R) {

			@Override
			public void pressAction() {
				if (checkToOpenWindow(ResearchWindow.class)) {
					stage.addActor(new ResearchWindow());
				}
			}
		};
		mainMenuButtons.put("research", b);

		// add the buttons
		b = new YGameImageButton("Quests", YIcons.QUEST, Keys.Q) {

			@Override
			public void pressAction() {
				if (checkToOpenWindow(WindowQuest.class)) {
					stage.addActor(new WindowQuest());
				}
			}
		};

		mainMenuButtons.put("quest", b);

		// add the buttons
		b = new YGameImageButton("Main Menu", YIcons.LOGO, Keys.ESCAPE) {

			@Override
			public void pressAction() {
				NN.windows().swap(MenuWindow.ID);
			}
		};
		mainMenuButtons.put("main", b);

		// add the buttons
		b = new YGameImageButton("Minimap", YIcons.MAP, Keys.M) {

			@Override
			public void pressAction() {
				NN.windows().swap(PlayerMapPreview.ID);
			}
		};
		mainMenuButtons.put("minimap", b);
	}

	public void nextPlayer() {

		// show timelabel
		timeLabel.setText(S.actPlayer().getName() + " | " + MapScreen.get().getData().getRound().getRoundString());
		timeImg = YIcons.getIconI(MapScreen.get().getData().getRound().getSeason().getLogo());
		// reset buttons
		mainMenu.clearChildren();

		// show buttons
		Array<String> a = S.actPlayer().getVisibleMenu();
		for (String s : a) {
			mainMenu.add(mainMenuButtons.get(s));
		}

		mainMenu.invalidate();

		// next unit
		nextUnit.getColor().a = 1;

		resetObjectMenu();

		// show weather?
		weather.checkRound();
	}

	public void resetObjectMenu() {
		unitMenu.clearChildren();
		unitMenu.invalidate();
		buildMenu.clearChildren();
		buildMenu.invalidate();

		overviewMenu.clearChildren();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	public void render(float delta) {

		stage.act(Math.min(delta, 1 / 30f));
		stage.draw();
	}

	/**
	 * Create the bars
	 */
	private void createBars() {
		float b = NSettings.getGuiScale();
		String e = NSettings.getGuiScaleFolder();

		// build base top
		Image top = new Image(NN.asset().getT(e + "/icons/verlauf.png"));
		top.setScale((Gdx.graphics.getWidth() + 1) / b, 1);
		top.setY(Gdx.graphics.getHeight() - b);
		stage.addActor(top);

		// build base bottom
		top = new Image(NN.asset().getT(e + "/icons/verlaufB.png"));
		top.setScale((Gdx.graphics.getWidth() + 1) / b, 1);
		stage.addActor(top);
	}

	/**
	 * Checks if the selected window is open
	 *
	 * @param clas
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private boolean windowIsOpen(Class clas) {
		for (Actor actor : stage.getActors()) {
			if (actor.getClass().equals(clas)) {
				return true;
			}
		}
		return false;

	}

	/**
	 * Checks if the selected window is open
	 *
	 * @param clas
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private boolean checkToOpenWindow(Class clas) {
		if (!windowIsOpen(clas)) {
			YSounds.pClick();
			return true;
		} else {
			YSounds.pCancel();
			closeWindow(clas);
			return false;
		}

	}

	/**
	 * Close the speficic window
	 *
	 * @param clas
	 */
	@SuppressWarnings("rawtypes")
	private void closeWindow(Class clas) {
		for (Actor actor : stage.getActors()) {
			if (actor.getClass().equals(clas)) {
				actor.remove();
			}
		}

	}

	/**
	 * Close the speficic window
	 *
	 * @param clas
	 */
	private void closeAllWindow() {
		for (Actor actor : stage.getActors()) {
			if (actor instanceof Window) {
				actor.remove();
			}
		}

		toasts.clear();

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	public void dispose() {
		stage.dispose();
	}

	/**
	 * Checks if any window is open
	 *
	 * @param clas
	 * @return
	 */
	public boolean anyWindowIsOpen() {
		for (Actor actor : stage.getActors()) {
			if (actor instanceof VisWindow) {
				return true;
			}
		}
		return false;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, false);
		toasts.resize();
	}

	/**
	 * Show a notification
	 * 
	 * @param note
	 */
	public void addToast(final YNotificationSaver note) {
		if (note.isShown()) {
			return;
		}
		// prepare text?
		String text = note.getText();
		// YLog.log("Toast", text.length(), text);
		if (text.length() > 50) {
			String textC = text;
			text = "";
			for (int i = 0; i < textC.length() / 50 + 1; i++) {
				if (i * 50 + 50 >= textC.length()) {
					text += textC.substring(i * 50);
				} else {
					text += textC.substring(i * 50, (i + 1) * 50) + "\n";
				}
			}
		}

		ToastTable tt = new ToastTable();
		tt.add(note.getIcon());
		tt.add(text);
		// add listener?
		if (note.getAction() != null) {
			tt.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					note.getAction().changed(null, null);
				}
			});
		}
		note.setShown(true);

		toasts.show(tt, note.getTime());

	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * Show it
	 * 
	 * @param screenXToMapX
	 * @param screenYToMapY
	 */
	public void showBottomMenuFor(int x, int y) {
		resetObjectMenu();
		// TODO has active action?

		// check rights
		if (S.actPlayer().getMapData(EMapData.ISVISIBLE, x, y) == 0) {
			overviewMenu.add(timeImg);
			overviewMenu.add(timeLabel);
			return;
		}

		// has unit?
		final NMapUnit unit = MapScreen.get().getData().getUnit(x, y);
		if (unit != null) {
			unit.addMenu(unitMenu);

		}

		// has unit?
		final NMapBuilding building = MapScreen.get().getData().getBuilding(x, y);
		if (building != null) {
			building.addMenu(buildMenu);
		}

		// show debug=
		if (NSettings.isDebug()) {
			final int oX = x, oY = y;
			// show it
			VisImageButton vis = new VisImageButton(YIcons.getIconI(YIcons.DEBUG).getDrawable(), "Debug Window");
			vis.addListener(new YChangeListener() {

				@Override
				public void changedY(Actor actor) {
					MapScreen.get().getStage().addActor(new DebugWindow(oX, oY));

				}
			});

			NImageButton t = new NImageButton(YIcons.getIconI(YIcons.DEBUG).getDrawable(), true) {

				@Override
				public void perform() {
					MapScreen.get().getStage().addActor(new DebugWindow(oX, oY));

				}
			};

			overviewMenu.add(t);
		}

		// show town?
		if (unit != null || building != null) {
			final Town t = (unit != null ? unit : building).getTown();

			// add town
			if (t != null) {
				VisImageTextButton vis = new VisImageTextButton(t.getName(), t.getIcon().getDrawable());
				vis.addListener(new YChangeListener() {

					@Override
					public void changedY(Actor actor) {
						MapScreen.get().getStage().addActor(new TownWindow(t));

					}
				});
				overviewMenu.add(vis);

				// own or another town?
				if (t.getPlayer() == S.actPlayer()) {
					// show worker
					BaseRess r = S.nData().getR(BaseRess.WORKER);
					// add icon
					Image i = r.getIcon();
					new Tooltip.Builder(r.getName()).target(i).build();
					overviewMenu.add(i);
					overviewMenu.add(new VisLabel(t.getRess(BaseRess.WORKER) + "/" + t.getRess(BaseRess.WORKERMAX) + "  "));

					// show ress
					int max = 5;
					for (String key : t.getRessTypes()) {

						r = S.nData().getR(key);
						// skip it?
						if (r.getMarketCost() == 0) {
							continue;
						}

						// add icon
						i = r.getIcon();
						new Tooltip.Builder(r.getName()).target(i).build();
						overviewMenu.add(i);
						overviewMenu.add(new VisLabel(t.getRess(key) + "  "));
						max--;
						if (max == 0) {
							break;
						}
					}
				} else {
					overviewMenu.add(new VisLabel(" part of " + t.getPlayer().getName()));
				}
			} else {
				// no town
				overviewMenu.add(YIcons.getIconI(YIcons.TOWN));
				overviewMenu.add(new VisLabel("No Town"));
			}

		} else {
			overviewMenu.add(timeImg);
			overviewMenu.add(timeLabel);
		}

		// YLog.log("Click on ", x, y, "unit", unit, "building", building);
		// NOnMapObject o = MapScreen.get().getData().getBuilding(x, y);
		// if (o != null) {
		// YLog.log(o.getIcon().getX(), o.getIcon().getY());
		// YLog.log(o.getIcon().getColor());
		// o.setXY(x, y);
		// }
		// YLog.log(S.actPlayer().getMapData(EMapData.ISVISIBLE, x, y),
		// MapScreen.get().getData().getMapData(EMapData.UNIT, x, y),
		// MapScreen.get().getData().getMapData(EMapData.BUILDING, x, y));
	}

	/**
	 * Show the button to cancel the action
	 * 
	 * @param baseAction
	 */
	public void setActiveAction(GAction baseAction) {
		if (baseAction == null) {
			btnActionCancel.setVisible(false);
			resetObjectMenu();
			return;
		}

		btnActionCancel.setText("Cancel '" + baseAction.getName() + "'");
		btnActionCancel.getStyle().imageUp = baseAction.getIcon().getDrawable();
		btnActionCancel.getStyle().imageDown = baseAction.getIcon().getDrawable();
		btnActionCancel.setVisible(true);

		// reset menu
		resetObjectMenu();

		unitMenu.add(imgInfo);
		unitMenu.add(lblInfo);

		buildMenu.add(btnActionCancel);
	}

	/**
	 * @return the dialog
	 */
	public YTextDialogMgmt getDialog() {
		return dialog;
	}

}

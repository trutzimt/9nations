package de.ninenations.game.screen;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.core.NN;
import de.ninenations.game.NRound.NDaytime;
import de.ninenations.game.S;

public class NWeather {

	private Image night;
	private int lastRound;

	public NWeather() {
		lastRound = -1;
	}

	public void init(Stage stage) {
		night = new Image(NN.asset().getT("system/menu/white.png"));
		night.setColor(Color.valueOf("283B5100"));
		night.setFillParent(true);
		night.setZIndex(0);
		stage.addActor(night);
	}

	public void checkRound() {
		// same round?
		if (S.round().getRoundRaw() == lastRound) {
			return;
		}
		lastRound = S.round().getRoundRaw();

		// new round, change?
		if (S.round().is(NDaytime.NIGHT)) {
			night.getColor().a = 0f;
			night.addAction(Actions.alpha(0.5f, 10));
			// show night
		} else if (night != null) {
			// remove night
			night.addAction(Actions.fadeOut(5));
		}
	}

}

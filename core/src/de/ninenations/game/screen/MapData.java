package de.ninenations.game.screen;

import java.io.Serializable;
import java.util.Random;

import com.badlogic.gdx.utils.ObjectMap;

import de.ninenations.core.NN;
import de.ninenations.data.NData;
import de.ninenations.game.NRound;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.map.NMapBuilding;
import de.ninenations.game.map.OnMapMgmt;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.player.PlayerMgmt;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.scenarios.CampaignMgmt;
import de.ninenations.towns.TownMgmt;
import de.ninenations.util.YError;
import de.ninenations.util.YLog;

/**
 * 
 */
public class MapData implements Serializable {

	private static final long serialVersionUID = 1539854633524996387L;

	public enum EMapData {
		BUILDING(0), UNIT(1), ISVISIBLE(2), MOVEWALKCOST(3);

		public final int typ;

		EMapData(int typ) {
			this.typ = typ;
		}
	}

	private ObjectMap<String, Object> pref;

	/**
	 * 
	 */
	private NData data;

	/**
	 * Speicherung von Mapeigenschaften, wie Terrain, Ressourcen left, Wegekosten
	 * 
	 * 
	 * - Wegkosten - Building - Terrain - Ressourcen übrig
	 */
	private int[][][] mapData;

	/**
	 * Save the map structure, terrain
	 */
	private int[][][] mapStructure;

	private TownMgmt towns;
	private PlayerMgmt players;
	private OnMapMgmt onMap;
	private NRound round;

	private transient BaseScenario scenario;

	/**
	 * Random generator
	 */
	private Random random;

	/**
	 * Default constructor
	 */
	public MapData() {
		round = new NRound();
		data = NN.nData();
		towns = new TownMgmt();
		players = new PlayerMgmt();
		onMap = new OnMapMgmt();
		pref = new ObjectMap<>();
	}

	/**
	 * Get the building or null
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public NMapBuilding getBuilding(int x, int y) {
		int b = getMapData(EMapData.BUILDING, x, y);

		// has a building?
		if (b == 0) {
			return null;
		}

		return (NMapBuilding) onMap.get(b);
	}

	/**
	 * Get the map data
	 * 
	 * @param type
	 * @param x
	 * @param y
	 * @return
	 */
	public int getMapData(EMapData type, int x, int y) {
		// outer border?
		if (!MapScreen.get().getMap().isValide(x, y)) {
			return 0;
		}

		return mapData[type.typ][x][y];
	}

	/**
	 * Set the map data
	 * 
	 * @param type
	 * @param x
	 * @param y
	 * @return
	 */
	public void setMapData(EMapData type, int x, int y, int value) {

		mapData[type.typ][x][y] = value;
	}

	/**
	 * Check if the config active
	 * 
	 * @return the config
	 */
	public boolean isConf(ScenConf c) {

		if (pref.containsKey(getKey(c))) {
			return (boolean) pref.get(getKey(c));
		} else {
			return false;
		}
	}

	/**
	 * Set the config active
	 * 
	 * @return the config
	 */
	public void setConf(ScenConf c, boolean val) {
		pref.put(getKey(c), val);
	}

	/**
	 * @param c
	 * @return
	 */
	private String getKey(ScenConf c) {
		return "s." + c.toString();
	}

	/**
	 * 
	 */
	public void performAtRoundBegin() {
		// TODO implement here
	}

	/**
	 * 
	 */
	public void nextRound() {
		if (getScenario() != null) {
			scenario.nextRound();
		}
		round.nextRound();
		players.nextRound();
		towns.nextRound();
		onMap.nextRound();
	}

	/**
	 * 
	 */
	public void afterLoad() {
		YLog.log("after load");
		if (getScenario() != null) {
			scenario.afterLoad();
		}
		players.afterLoad();
		towns.afterLoad();
		onMap.afterLoad();
	}

	/**
	 * @return the towns
	 */
	public TownMgmt getTowns() {
		return towns;
	}

	/**
	 * @return the players
	 */
	public PlayerMgmt getPlayers() {
		return players;
	}

	/**
	 * Set the settings
	 * 
	 * @param key
	 * @return
	 */
	public void setP(String key, Object o) {
		pref.put(key, o);
	}

	/**
	 * Get the settings
	 * 
	 * @param key
	 * @return
	 */
	public Object getP(String key) {
		return pref.get(key);
	}

	/**
	 * Get the settings
	 * 
	 * @param key
	 * @return
	 */
	public String getPS(String key) {
		if (pref.get(key) == null) {
			return null;
		}
		return pref.get(key).toString();
	}

	/**
	 * Get the settings
	 * 
	 * @param key
	 * @return
	 */
	public int getPI(String key) {
		try {
			return Integer.valueOf(pref.get(key).toString());
		} catch (Exception e) {
			YError.error(new IllegalArgumentException("Data for Key " + key + " is missing.", e), false);
			return 0;
		}
	}

	/**
	 * Get the settings or the standard value
	 * 
	 * @param key
	 * @return
	 */
	public int getPI(String key, int standard) {
		if (existP(key)) {
			return getPI(key);
		} else {
			return standard;
		}
	}

	/**
	 * Get the settings
	 * 
	 * @param key
	 * @return
	 */
	public boolean getPB(String key) {
		return Boolean.valueOf(pref.get(key).toString());
	}

	/**
	 * Get the settings or the standard value
	 * 
	 * @param key
	 * @return
	 */
	public boolean getPB(String key, boolean standard) {
		if (existP(key)) {
			return getPB(key);
		} else {
			return standard;
		}
	}

	/**
	 * Check the settings
	 * 
	 * @param key
	 * @return
	 */
	public boolean existP(String key) {
		return pref.containsKey(key);
	}

	/**
	 * @return the round
	 */
	public NRound getRound() {
		return round;
	}

	/**
	 * @return the onMap
	 */
	public OnMapMgmt getOnMap() {
		return onMap;
	}

	/**
	 * @return the data
	 */
	public NData getData() {
		return data;
	}

	public void init(BaseScenario scenario) {
		mapData = new int[EMapData.values().length][S.map().getWidth()][S.map().getHeight()];
		random = new Random(scenario.getRandomSeed());
		players.init();
		towns.init();
		onMap.init();
		data.generate();

		this.scenario = scenario;
		pref.put("campaign", scenario.getCamp().getType());
		pref.put("scenario", scenario.getType());

	}

	/**
	 * Return the unit on this point or null
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	public NMapUnit getUnit(int x, int y) {
		int u = getMapData(EMapData.UNIT, x, y);

		return (NMapUnit) (u == 0 ? null : getOnMap().get(u));
	}

	/**
	 * Check if a unit on the field
	 * 
	 * @param x
	 * @param y
	 * @return true > there is a unit or false > there is no unit or no valide
	 *         position
	 */
	public boolean isUnit(int x, int y) {
		return getMapData(EMapData.UNIT, x, y) > 0;
	}

	/**
	 * Check if a building on the field
	 * 
	 * @param x
	 * @param y
	 * @return true > there is a building or false > there is no building or no
	 *         valide position
	 */
	public boolean isBuilding(int x, int y) {
		return getMapData(EMapData.BUILDING, x, y) > 0;
	}

	public void addSaveSettings(ObjectMap<String, String> data) {
		data.put("player", S.actPlayer().getName());
		data.put("round", MapScreen.get().getData().getRound().getRoundString());
		data.put("campaign", (String) pref.get("campaign"));
		data.put("scenario", (String) pref.get("scenario"));

	}

	/**
	 * @return the scenario
	 */
	public BaseScenario getScenario() {
		if (scenario == null && pref.containsValue("scenario", false)) {
			String c = (String) pref.get("campaign");
			String s = (String) pref.get("scenario");

			if (CampaignMgmt.existCampaign(c) && CampaignMgmt.getCampaign(c).existScenario(s)) {
				scenario = CampaignMgmt.getCampaign(c).getScenario(s);
			}
		}

		return scenario;
	}

	/**
	 * @return the mapStructure
	 */
	public int[][][] getMapStructureRaw() {
		return mapStructure;
	}

	/**
	 * Return the terrain id on this point
	 * 
	 * @param z
	 * @param x
	 * @param y
	 * @return
	 */
	public int getMapStructure(int z, int x, int y) {
		return mapStructure[z][x][y];
	}

	/**
	 * @param mapStructure
	 *            the mapStructure to set
	 */
	public void setMapStructure(int[][][] mapStructure) {
		this.mapStructure = mapStructure;
	}

	/**
	 * @return the random
	 */
	public Random getRandom() {
		return random;
	}

	/**
	 * @param conf
	 *            the conf to set
	 */
	public void setConf(ObjectMap<ScenConf, Boolean> conf) {
		for (ScenConf c : conf.keys()) {
			setConf(c, conf.get(c));
		}
	}

}
package de.ninenations.game;

import java.util.Random;

import de.ninenations.data.NData;
import de.ninenations.game.map.NMap;
import de.ninenations.game.map.NMapBuilding;
import de.ninenations.game.map.OnMapMgmt;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.player.Player;
import de.ninenations.player.PlayerMgmt;
import de.ninenations.towns.TownMgmt;

public class S {

	private S() {}

	public static Player actPlayer() {
		return MapScreen.get().getData().getPlayers().getActPlayer();
	}

	public static NData nData() {
		return MapScreen.get().getData().getData();
	}

	public static boolean valide(int x, int y) {
		return MapScreen.get().getMap().isValide(x, y);
	}

	public static NMap map() {
		return MapScreen.get().getMap();
	}

	public static int width() {
		return MapScreen.get().getMap().getWidth();
	}

	public static int height() {
		return MapScreen.get().getMap().getHeight();
	}

	public static TownMgmt town() {
		return MapScreen.get().getData().getTowns();
	}

	public static boolean isActive(ScenConf conf) {
		return MapScreen.get().getData().isConf(conf);
	}

	public static NMapBuilding build(int x, int y) {
		return MapScreen.get().getData().getBuilding(x, y);
	}

	public static NMapUnit unit(int x, int y) {
		return MapScreen.get().getData().getUnit(x, y);
	}

	public static OnMapMgmt onMap() {
		return MapScreen.get().getData().getOnMap();
	}

	public static PlayerMgmt players() {
		return MapScreen.get().getData().getPlayers();
	}

	public static NRound round() {
		return MapScreen.get().getData().getRound();
	}

	public static Random r() {
		return MapScreen.get().getData().getRandom();
	}

	/**
	 * Check if a game is running
	 * 
	 * @return
	 */
	public static boolean activeGame() {
		return MapScreen.get() != null;
	}

}

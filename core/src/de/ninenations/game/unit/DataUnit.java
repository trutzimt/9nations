/**
 * 
 */
package de.ninenations.game.unit;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.action.ActionAttack;
import de.ninenations.actions.action.ActionMove;
import de.ninenations.core.NN;
import de.ninenations.data.buildingunitbase.DataObject;
import de.ninenations.data.terrain.BaseTerrain.NMove;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.NSettings;
import de.ninenations.util.YLog;

/**
 * @author sven
 *
 */
public class DataUnit extends DataObject {

	private static final long serialVersionUID = -9132720131863147390L;

	protected String filename;
	protected transient TextureRegion textureRegion, textureRegionO;
	protected NMove moveTyp;

	@SuppressWarnings("unused")
	private DataUnit() {
		super();
	}

	/**
	 * @param type
	 * @param name
	 */
	public DataUnit(String type, String name, String filename, int ap, int atk, NCat category) {
		super(type, name, category);

		this.filename = filename;
		moveTyp = NMove.WALK;
		this.ap = ap;
		this.atk = atk;

		// add move
		activeActions.add(new ActionMove());

		if (atk > 0) {
			activeActions.add(new ActionAttack());
		}
	}

	/**
	 * Return the file name scaled
	 * 
	 * @return
	 */
	public String getFilePath() {
		return NSettings.getGuiScaleFolder() + "/unit/" + (filename.contains("-") ? filename.split("-")[0] : filename);
	}

	/**
	 * Return the file name scaled
	 * 
	 * @return
	 */
	public String getFilePathO() {
		return "system/unit/" + (filename.contains("-") ? filename.split("-")[0] : filename);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		int s = NSettings.getGuiScale();
		return new Image(new TextureRegion(getTextureRegion(), s, 0, s, s));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIconO() {
		return new Image(new TextureRegion(getTextureRegionO(), 32, 0, 32, 32));
	}

	/**
	 * Get the unit scaled
	 * 
	 * @return
	 */
	public TextureRegion getTextureRegion() {
		if (textureRegion != null) {
			return textureRegion;
		}

		int w = 3 * NSettings.getGuiScale(), h = 4 * NSettings.getGuiScale();

		// need to load?
		if (filename.contains("-")) {
			String paths[] = filename.split("-");
			int x = Integer.parseInt(paths[1]), y = Integer.parseInt(paths[2]);
			textureRegion = new TextureRegion(NN.asset().getT(NSettings.getGuiScaleFolder() + "/unit/" + paths[0]), x * w, y * h, w, h);
		} else {
			textureRegion = new TextureRegion(NN.asset().getT(NSettings.getGuiScaleFolder() + "/unit/" + filename), 0, 0, w, h);
		}

		return textureRegion;
	}

	/**
	 * Get the unit original size
	 * 
	 * @return
	 */
	public TextureRegion getTextureRegionO() {
		if (textureRegionO != null) {
			return textureRegionO;
		}

		int w = 96, h = 128;

		// need to load?
		if (filename.contains("-")) {
			String paths[] = filename.split("-");
			int x = Integer.parseInt(paths[1]), y = Integer.parseInt(paths[2]);
			textureRegionO = new TextureRegion(NN.asset().getT("system/unit/" + paths[0]), x * w, y * h, w, h);
		} else {
			textureRegionO = new TextureRegion(NN.asset().getT("system/unit/" + filename), 0, 0, w, h);
		}

		return textureRegionO;
	}

	/**
	 * @return the moveTyp
	 */
	public NMove getMoveTyp() {
		return moveTyp;
	}

	/**
	 * @param filename
	 *            the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.data.buildingunitbase.DataObject#validate()
	 */
	@Override
	public void validate() {
		super.validate();

		if (!Gdx.files.internal(getFilePath()).exists()) {
			YLog.log("Validate", type, getFilePath(), "is missing");
		}
	}

	/**
	 * @param moveTyp
	 *            the moveTyp to set
	 */
	public void setMoveTyp(NMove moveTyp) {
		this.moveTyp = moveTyp;
	}

	/**
	 * Add hp/ap details
	 * 
	 * @param table
	 */
	@Override
	protected void addDetailPanel(YTable table) {
		table.addL("HP/AP", hp + "/" + ap + " (Move: " + moveTyp.toString().toLowerCase() + ")");
	}

}

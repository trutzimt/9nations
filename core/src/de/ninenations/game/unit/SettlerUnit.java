/**
 *
 */
package de.ninenations.game.unit;

import de.ninenations.actions.action.ActionFoundTown;

/**
 * @author sven
 *
 */
public class SettlerUnit extends DataUnit {

	private static final long serialVersionUID = 3810602735876475520L;

	/**
	 * @param type
	 * @param title
	 * @param id
	 */
	public SettlerUnit() {
		super("nsettler", "Settler", "vx_chara04_a_2a7.png", 10, 0, NCat.PROD);

		activeActions.add(new ActionFoundTown(false));

	}

}

/**
 * 
 */
package de.ninenations.game.unit;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * @author sven
 *
 */
public class YAnimationActor extends Actor {

	public enum PersonDir {
		SOUTH, WEST, EAST, NORTH;
	}

	protected Animation<TextureRegion> animation;
	protected TextureRegion currentRegion;
	protected TextureRegion previewRegion;
	protected float time = 0f;
	protected boolean stop;

	private PersonDir dir;
	private DataUnit unit;

	/**
	 * 
	 */
	public YAnimationActor(DataUnit unit) {
		this.unit = unit;
		setDir(PersonDir.SOUTH);
		setStop(true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.scenes.scene2d.Actor#act(float)
	 */
	@Override
	public void act(float delta) {
		super.act(delta);
		time += delta;

		if (currentRegion == null || !stop) {
			currentRegion = animation.getKeyFrame(time, true);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.badlogic.gdx.scenes.scene2d.Actor#draw(com.badlogic.gdx.graphics.g2d.
	 * Batch, float)
	 */
	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);

		// reset color
		Color color = getColor();
		batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

		batch.draw(currentRegion, getX(), getY());
	}

	/**
	 * @param stop
	 *            the stop to set
	 */
	public void setStop(boolean stop) {
		this.stop = stop;
		if (stop) {
			currentRegion = animation.getKeyFrames()[1];
		}
	}

	/**
	 * @return the stop
	 */
	public boolean isStop() {
		return stop;
	}

	/**
	 * @return the previewRegion
	 */
	public TextureRegion getPreviewRegion() {
		return previewRegion;
	}

	/**
	 * Return the direction to show for this person
	 * 
	 * @param file
	 * @param dir
	 * @return
	 */
	public void setAnimation() {
		// Load the sprite sheet as a Texture
		TextureRegion ani = unit.getTextureRegionO();

		// Use the split utility method to create a 2D array of TextureRegions. This is
		// possible because this sprite sheet contains frames of equal size and they are
		// all aligned.
		TextureRegion[][] tmp = ani.split(ani.getRegionWidth() / 3, ani.getRegionHeight() / 4);

		int c = dir == PersonDir.SOUTH ? 0 : dir == PersonDir.EAST ? 1 : dir == PersonDir.WEST ? 2 : 3;

		animation = new Animation<>(0.4f, tmp[c]);
		currentRegion = animation.getKeyFrames()[1];
	}

	/**
	 * @return the dir
	 */
	public PersonDir getDir() {
		return dir;
	}

	/**
	 * @param dir
	 *            the dir to set
	 */
	public void setDir(PersonDir dir) {
		this.dir = dir;
		setAnimation();
	}

}

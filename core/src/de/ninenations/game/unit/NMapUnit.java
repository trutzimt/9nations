/**
 * 
 */
package de.ninenations.game.unit;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.VisImageTextButton;

import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.map.OnMapObjWindow;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.unit.YAnimationActor.PersonDir;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.actions.YChangeListener;

/**
 * @author sven
 *
 */
public class NMapUnit extends NOnMapObject implements Serializable {

	private static final long serialVersionUID = -5092002363297987211L;

	private transient YAnimationActor actor;
	private transient DataUnit unit;

	/**
	 * 
	 */
	public NMapUnit() {}

	/**
	 * @param mapType
	 * @param type
	 * @param id
	 * @param name
	 * @param town
	 * @param x
	 * @param y
	 */
	public NMapUnit(String type, int id, int player, int town, int x, int y) {
		super(EMapData.UNIT, type, id, S.nData().getU(type).getName(), player, town, x, y);

	}

	/**
	 * Set the new position
	 * 
	 * @param x
	 * @param y
	 */
	@Override
	public void setXY(int x, int y) {
		// unregister
		if (this.x >= 0) {

			MapScreen.get().getData().setMapData(mapType, this.x, this.y, 0);
			// move image
			actor.setDir(this.x > x ? PersonDir.EAST : this.y < y ? PersonDir.NORTH : this.x < x ? PersonDir.WEST : PersonDir.SOUTH);
			actor.addAction(Actions.sequence(new Action() {

				@Override
				public boolean act(float delta) {
					NMapUnit.this.actor.setStop(false);
					return true;
				}
			}, Actions.moveTo(x * 32, y * 32, Math.abs(this.x - x) + Math.abs(this.y - y)), new Action() {

				@Override
				public boolean act(float delta) {
					NMapUnit.this.actor.setStop(true);
					return true;
				}
			}));
		}

		this.x = x;
		this.y = y;

		// register
		if (this.x >= 0) {
			MapScreen.get().getData().setMapData(mapType, this.x, this.y, id);
		}

		updateVisibleRange();
	}

	@Override
	public NOnMapObject setFinish() {
		super.setFinish();

		actor.getColor().a = 1;

		return this;
	}

	/**
	 * 
	 */
	@Override
	public void destroy() {
		super.destroy();
		// remove it
		actor.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BaseMgmtObject#afterLoad()
	 */
	@Override
	public void afterLoad() {

		unit = S.nData().getU(type);

		coreAfterLoad();

		actor = new YAnimationActor(unit);
		actor.setX(x * 32);
		actor.setY(y * 32);
		actor.getColor().a = isFinish() ? 1 : 0.5f;
		MapScreen.get().getMap().getStage().addActor(actor);
	}

	/**
	 * @return the unit
	 */
	public DataUnit getUnit() {
		return unit;
	}

	/**
	 * @return the actor
	 */
	@Override
	public Actor getActor() {
		return actor;
	}

	/**
	 * Attack to obj
	 * 
	 * @param obj
	 */
	@Override
	public boolean attack(NOnMapObject obj) {
		setDir(obj.getX(), obj.getY());
		return super.attack(obj);
	}

	public void setDir(int x, int y) {
		actor.setDir(this.x > x ? PersonDir.EAST : this.y < y ? PersonDir.NORTH : this.x < x ? PersonDir.WEST : PersonDir.SOUTH);
	}

	/**
	 * Add click menu for this object
	 * 
	 * @param menu
	 */
	@Override
	public void addMenu(Table menu) {
		// show it
		VisImageTextButton vis = new VisImageTextButton(getName(), getDataObject().getIcon().getDrawable());
		vis.addListener(new YChangeListener() {

			@Override
			public void changedY(Actor actor) {
				MapScreen.get().getStage().addActor(new OnMapObjWindow(NMapUnit.this));

			}
		});
		menu.add(vis);

		super.addMenu(menu);
	}

}

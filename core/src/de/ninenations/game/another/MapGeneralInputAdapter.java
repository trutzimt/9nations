/**
 * 
 */
package de.ninenations.game.another;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector3;

import de.ninenations.actions.base.GAction;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.util.YError;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class MapGeneralInputAdapter extends InputAdapter {

	// managent clicks
	private GAction activeAction;

	// important for action move
	private int oldX, oldY;

	/**
	 * @param mapScreen
	 * 
	 */
	public MapGeneralInputAdapter() {}

	/**
	 * Convert Screen X to Map X
	 *
	 * @param x
	 */
	private int screenXToMapX(int x) {
		Vector3 touch = new Vector3(x, 0, 0);
		MapScreen.get().getMap().getCamera().unproject(touch);
		return (int) touch.x / 32;

	}

	/**
	 * Convert Screen Y to Map Y
	 *
	 * @param y
	 */
	private int screenYToMapY(int y) {
		Vector3 touch = new Vector3(0, y, 0);
		MapScreen.get().getMap().getCamera().unproject(touch);
		return (int) touch.y / 32;
	}

	/**
	 * Set an action who is respond for every click
	 *
	 * @param baseAction
	 * @param selected
	 */
	public void setActiveAction(GAction baseAction) {

		activeAction = baseAction;

		if (baseAction == null) {
			oldX = -1;
			oldY = -1;
		}

		// inform the gui
		// mapScreen.getMapgui().setActiveAction(baseAction);
	}

	/**
	 * Check if an action running?
	 *
	 * @param baseAction
	 * @param selected
	 */
	public boolean hasActiveAction() {
		return activeAction != null;
	}

	/**
	 * @return the activeAction
	 */
	public GAction getActiveAction() {
		return activeAction;
	}

	/**
	 * 
	 */
	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {

		try {
			// running an action?
			if (activeAction != null && button == Buttons.LEFT) {
				x = screenXToMapX(x);
				y = screenYToMapY(y);

				if (!MapScreen.get().getMap().isValide(x, y)) {
					YSounds.pCancel();
					setActiveAction(null);
					return false;
				}
				// for android
				activeAction.performAtPoint(x, y);
				activeAction.moveMouseAtPoint(x, y);
				return true;
			}

			// save pos
			if (activeAction == null && button == Buttons.LEFT) {
				MapScreen.get().getGui().showBottomMenuFor(screenXToMapX(x), screenYToMapY(y));
			}

			// show menu
		} catch (Exception e) {
			YError.error(e, false);
		}
		return false;

	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;

	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// running an action?
		if (activeAction != null) {
			try {
				int x = screenXToMapX(screenX);
				int y = screenYToMapY(screenY);

				if (!MapScreen.get().getMap().isValide(x, y)) {
					return false;
				}

				if (oldX == x && oldY == x) {
					return false;
				}

				oldX = x;
				oldY = y;
				activeAction.moveMouseAtPoint(x, y);
				return true;
			} catch (Exception e) {
				YError.error(e, false);
				// reset action
				setActiveAction(null);
			}
		}
		return false;
	}
}

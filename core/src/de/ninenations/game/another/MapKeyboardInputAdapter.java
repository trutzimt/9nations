/**
 * 
 */
package de.ninenations.game.another;

import java.util.HashMap;

import com.badlogic.gdx.InputAdapter;

import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.actions.YChangeListener;

/**
 * @author sven
 *
 */
public class MapKeyboardInputAdapter extends InputAdapter {

	// managent clicks

	// important for map scrolling
	private int oldPosX, oldPosY;

	/**
	 * @param mapScreen
	 * 
	 */
	public MapKeyboardInputAdapter() {}

	/**
	 * 
	 */
	@Override
	public boolean touchDown(int x, int y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// can run?
		if (MapScreen.get().getInputs().hasActiveAction()) {
			return false;
		}

		// changed?
		int aX = screenX - oldPosX;
		int aY = screenY - oldPosY;
		if (aX == 0 && aY == 0) {
			return true;
		}

		// move map
		// MapScreen.get().getMap().moveMapView(aX, aY);

		// save
		oldPosX = screenX;
		oldPosY = screenY;

		return true;

	}

	@Override
	public boolean keyDown(int keycode) {
		HashMap<Integer, YChangeListener> keyBindings = MapScreen.get().getKeyBindings();
		if (keyBindings != null && keyBindings.containsKey(keycode)) {
			keyBindings.get(keycode).changedY(null);
			return true;
		}
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}
}

/**
 * 
 */
package de.ninenations.game;

import java.io.Serializable;

import de.ninenations.ui.YIcons;

/**
 * @author sven
 *
 */
public class NRound implements Serializable {

	private static final long serialVersionUID = 7217044050297399606L;

	public enum NSeason {
		SPRING("Spring", 0, YIcons.SPRING), SUMMER("Summer", 1, YIcons.SUMMER), AUTUMN("Autumn", 2, YIcons.AUTUMN), WINTER("Winter", 3, YIcons.WINTER);

		private final String text;
		private final int id;
		private final int logo;

		private NSeason(String text, int id, int logo) {
			this.text = text;
			this.id = id;
			this.logo = logo;
		}

		/**
		 * @return the value
		 */
		public String get() {
			return text;
		}

		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}

		/**
		 * @return the logo
		 */
		public int getLogo() {
			return logo;
		}
	}

	public enum NDaytime {
		MORNING("Morning", 0), EVENING("Evening", 1), NIGHT("Night", 2);

		private final String text;
		private final int id;

		private NDaytime(String text, int id) {
			this.text = text;
			this.id = id;
		}

		/**
		 * @return the value
		 */
		public String get() {
			return text;
		}

		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}
	}

	private int round;

	/**
	 * 
	 */
	public NRound() {
		round = 0;
	}

	public void nextRound() {
		round++;
	}

	public String getRoundString() {
		return getSeason().get() + " " + getDayTime().get() + " " + ", Year " + getYear();
	}

	/**
	 * Get the year
	 * 
	 * @return
	 */
	public int getYear() {
		return round / 12 + 1;
	}

	/**
	 * Get the act day time: morning, evening, night
	 * 
	 * @return
	 */
	public NDaytime getDayTime() {
		return NDaytime.values()[round % 3];
	}

	/**
	 * Get the act season: spring, ... winter
	 * 
	 * @return
	 */
	public NSeason getSeason() {
		return NSeason.values()[round % 12 / 3];
	}

	/**
	 * Check if it the right daytime
	 * 
	 * @param day
	 * @return
	 */
	public boolean is(NDaytime day) {
		return getDayTime() == day;
	}

	/**
	 * Check if it the right season
	 * 
	 * @param season
	 * @return
	 */
	public boolean is(NSeason season) {
		return getSeason() == season;
	}

	/**
	 * @return the round
	 */
	public int getRoundRaw() {
		return round;
	}

}

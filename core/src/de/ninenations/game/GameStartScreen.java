package de.ninenations.game;

import com.badlogic.gdx.maps.tiled.TiledMap;

import de.ninenations.core.NN;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.scenarios.BaseScenario;
import de.ninenations.screen.LoaderScreen;
import de.ninenations.util.NSettings;
import de.ninenations.util.YLog;

public class GameStartScreen extends LoaderScreen {

	protected BaseScenario scenario;

	/**
	 * Load everything for the game
	 *
	 * @param qossire
	 * @param scenario
	 */
	public GameStartScreen(BaseScenario scenario) {
		super("Loading " + (scenario == null ? "savegame" : scenario.getName()) + " ");

		this.scenario = scenario;

		createLabel();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.menu.BaseLoaderScreen#nextScreen()
	 */
	@Override
	public void nextScreen() {
		NN.get().switchScreen(MapScreen.get());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.menu.BaseLoaderScreen#onceCode()
	 */
	@Override
	public void onceCode() {

		String e = NSettings.getGuiScaleFolder();
		boolean loadAddon = !"system".equals(e);

		NN.asset().loadT(e + "/tileset/tilemap.png");
		NN.asset().loadT(e + "/tileset/autotiles.png");

		// load system tileset?
		if (loadAddon) {
			NN.asset().loadT("system/tileset/tilemap.png");
			NN.asset().loadT("system/tileset/autotiles.png");
		}

		// add icons
		for (String s : new String[] { "verlauf", "verlaufB" }) {
			NN.asset().loadT(e + "/icons/" + s + ".png");
		}

		NN.asset().loadT("system/menu/white.png");

		// load all assets
		for (String s : NN.nData().getUnits()) {
			NN.asset().loadT(NN.nData().getU(s).getFilePathO());
			if (loadAddon) {
				NN.asset().loadT(NN.nData().getU(s).getFilePath());
			}
		}

		// load sounds
		for (String r : NN.nData().getRess()) {
			NN.nData().getR(r).assetLoad();
		}

		// load sounds
		for (String t : NN.nData().getTerrain()) {
			NN.nData().getT(t).assetLoad();
		}

		// // add icons
		// for (int i = 0; i < 11; i++) {
		// YStatic.gameAssets.load("system/emotion/" + i + ".png", Texture.class);
		// }
		// YIcons.loadIcons();
		//
		// // add character
		// for (int i = 0; i < (YSettings.isDebug() ? 5 : 136); i++) {
		// YStatic.gameAssets.load("system/character/" + i + ".png", Texture.class);
		// }
		//
		// Gdx.app.debug("Game Loader", title);

		// TODO loading scenario?

		if (scenario != null) {
			scenario.assetLoad();
			YLog.log("Starting", scenario.getFullId());

			NN.asset().load(scenario.getMapLink(), TiledMap.class);
		}
	}

	/**
	 * Run after loading the assets
	 */
	@Override
	protected void secondCode() {
		MapScreen.init(scenario);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.screens.base.BaseLoaderScreen#isFinishLoading()
	 */
	@Override
	public boolean isFinishLoading() {
		return NN.asset().update();
	}

}

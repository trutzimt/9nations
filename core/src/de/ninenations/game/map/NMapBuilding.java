/**
 * 
 */
package de.ninenations.game.map;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.VisImageTextButton;

import de.ninenations.data.building.DataBuilding;
import de.ninenations.game.S;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.game.screen.MapScreen;

/**
 * @author sven
 *
 */
public class NMapBuilding extends NOnMapObject implements Serializable {

	private static final long serialVersionUID = -5092002363297987211L;

	private transient DataBuilding build;

	/**
	 * 
	 */
	public NMapBuilding() {}

	/**
	 * @param mapType
	 * @param type
	 * @param id
	 * @param name
	 * @param town
	 * @param x
	 * @param y
	 */
	public NMapBuilding(String type, int id, int player, int town, int x, int y) {
		super(EMapData.BUILDING, type, id, S.nData().getB(type).getName(), player, town, x, y);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BaseMgmtObject#afterLoad()
	 */
	@Override
	public void afterLoad() {
		build = S.nData().getB(type);
		super.afterLoad();

		img.setZIndex(0);
		// reset image?
		if (build.isConnected()) {
			updateMapLook();
			updateConnectedMapLook();
		}
	}

	@Override
	public NOnMapObject setFinish() {
		super.setFinish();
		// reset image?
		if (build.isConnected()) {
			updateMapLook();
			updateConnectedMapLook();
		}
		return this;
	}

	protected void updateConnectedMapLook() {
		NMapBuilding b = S.build(x, y - 1);
		if (b != null && b.getBuild() != null && b.getBuild().isConnected()) {
			b.updateMapLook();
		}
		b = S.build(x, y + 1);
		if (b != null && b.getBuild() != null && b.getBuild().isConnected()) {
			b.updateMapLook();
		}
		b = S.build(x - 1, y);
		if (b != null && b.getBuild() != null && b.getBuild().isConnected()) {
			b.updateMapLook();
		}
		b = S.build(x + 1, y);
		if (b != null && b.getBuild() != null && b.getBuild().isConnected()) {
			b.updateMapLook();
		}
	}

	protected void updateMapLook() {
		img.setDrawable(build.getIconConnected(this).getDrawable());
	}

	/**
	 * @return the build
	 */
	public DataBuilding getBuild() {
		return build;
	}

	/**
	 * 
	 */
	@Override
	public void destroy() {
		super.destroy();

		if (build.isConnected()) {
			updateConnectedMapLook();
		}
	}

	/**
	 * Add click menu for this object
	 * 
	 * @param menu
	 */
	@Override
	public void addMenu(Table menu) {
		super.addMenu(menu);

		// show it
		VisImageTextButton vis = new VisImageTextButton(getName(), getDataObject().getIcon().getDrawable());
		vis.addListener(new YChangeListener(true) {

			@Override
			public void changedY(Actor actor) {
				MapScreen.get().getStage().addActor(new OnMapObjWindow(NMapBuilding.this));

			}
		});

		menu.add(vis);
	}
}

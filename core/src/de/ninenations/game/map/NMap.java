package de.ninenations.game.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;

import de.ninenations.core.NN;
import de.ninenations.data.building.DataBuilding;
import de.ninenations.data.terrain.BaseTerrain;
import de.ninenations.data.terrain.BaseTerrain.NMove;
import de.ninenations.game.S;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.game.unit.DataUnit;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.ui.NAnimation;
import de.ninenations.util.NSettings;
import de.ninenations.util.YError;

/**
 * Stellt die Karte da
 */
public class NMap {

	private Stage stage;

	private int width, height, length;

	private OrthographicCamera camera;
	private Vector3 dstCamera;

	private NMapAutotile autotile;

	/**
	 * Default constructor
	 */
	public NMap() {
		camera = new OrthographicCamera();
		camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		dstCamera = camera.position.cpy();

		stage = new Stage();
		stage.setViewport(new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), camera));

	}

	/**
	 * Init
	 */
	public void init() {
		TiledMap tiledMap = NN.asset().get(MapScreen.get().getData().getPS("map"), TiledMap.class);

		// load layers
		TiledMapTileLayer t = (TiledMapTileLayer) tiledMap.getLayers().get(0);
		width = t.getWidth();
		height = t.getHeight();
		length = tiledMap.getLayers().size();

		// loading structure?
		readMapData(tiledMap);

		setCameraZoom(NSettings.isBiggerGui() ? 0.5f : 1);
	}

	/**
	 * Read the map data and save it
	 */
	public void readMapData(TiledMap tiledMap) {
		int[][][] mapStructure = new int[S.map().getLayerCount()][S.map().getWidth()][S.map().getHeight()];
		// open map
		for (int z = 0; z < length; z++) {
			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					String t;
					Cell c = ((TiledMapTileLayer) tiledMap.getLayers().get(z)).getCell(x, y);

					// has a cell?
					if (c == null) {
						t = null;
					} else if (c.getTile().getProperties().containsKey("region")) {
						t = (String) c.getTile().getProperties().get("region");
					} else {
						t = BaseTerrain.DEF;
					}

					// has terrain to read?
					mapStructure[z][x][y] = t == null ? -1 : S.nData().getTerrainRaw().getPos(t);
				}
			}

		}

		MapScreen.get().getData().setMapStructure(mapStructure);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	public void render(float delta) {
		camera.position.lerp(dstCamera, 0.075f);// vector of the camera desired position and smoothness of the movement

		// tell the camera to update its matrices.
		camera.update();

		// show it
		// tiledMapRenderer.setView(camera);
		// tiledMapRenderer.render();

		autotile.render(camera);

		stage.act(Math.min(delta, 1 / 30f));
		stage.draw();

	}

	public void playAnimation(String name, int x, int y) {
		NAnimation ani = new NAnimation(NN.skin().getAnimation(name));
		ani.setX(x * 32);
		ani.setY(y * 32);
		stage.addActor(ani);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, false);
		camera.setToOrtho(false, width, height);
		setCameraZoom(camera.zoom);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	public void dispose() {
		autotile.dispose();
		// unitMap.dispose();
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @return the width
	 */
	public int getLayerCount() {
		return length;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param zoom
	 *            the zoom to set
	 */
	public void zoomIn() {
		if (camera.zoom > 0.25f) {
			setCameraZoom(Math.round(camera.zoom * 4 - 1) / 4f);// / 2f);
		}
	}

	/**
	 * @param zoom
	 *            the zoom to set
	 */
	public void zoomOut() {
		if (camera.zoom < 10) {
			setCameraZoom(Math.round(camera.zoom * 4 + 1) / 4f);// / 2f);
		}

	}

	/**
	 * @param zoom
	 *            the zoom to set
	 */
	public void setCameraZoom(float zoom) {
		camera.zoom = zoom;
	}

	/**
	 * 
	 * @return
	 */
	public OrthographicCamera getCamera() {
		return camera;
	}

	/**
	 * Get the cost for this tile for this move typ
	 *
	 * @param obj
	 * @param x
	 * @param y
	 * @return
	 */
	public int getMoveCost(NMapUnit obj, int x, int y) {
		// is it self?
		int u = MapScreen.get().getData().getMapData(EMapData.UNIT, x, y);
		if (u > 0 && u == obj.getId()) {
			return 0;
		}

		// ask player for special move cost
		if (obj.getUnit().getMoveTyp() == NMove.WALK) {
			int c = obj.getPlayer().getMapData(EMapData.MOVEWALKCOST, x, y);
			if (c > 0) {
				return c;
			}
		}

		// get it from the terrain
		BaseTerrain t = getTerrain(x, y);
		int cost = t.getCost(obj.getUnit().getMoveTyp());

		// change by nation?
		int nCost = obj.getPlayer().getNation().getTerrainCost(t.getType());
		cost = nCost > 0 ? nCost : cost - nCost;

		// has a building?
		NMapBuilding b = MapScreen.get().getData().getBuilding(x, y);
		if (b != null && b.isFinish()) {
			// calc diff
			int dCost = 0;
			DataBuilding build = b.getBuild();
			DataUnit unit = obj.getUnit();
			// diff build?
			if (b.getPlayer() != obj.getPlayer() && build.getPublicCost().containsKey(unit.getMoveTyp())) {
				dCost = build.getPublicCost().get(unit.getMoveTyp());
			}

			// own build?
			if (b.getPlayer() == obj.getPlayer() && build.getOwnerCost().containsKey(unit.getMoveTyp())) {
				dCost = build.getOwnerCost().get(unit.getMoveTyp());
			}

			if (dCost < 0) {
				return Math.max(cost + dCost, 0);
			}

			if (dCost > 0) {
				return dCost;
			}
		}

		return cost;
	}

	/**
	 * Check if this obj can pass this tile
	 * 
	 * @param obj
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean canPass(NMapUnit obj, int x, int y) {

		if (!isValide(x, y)) {
			return false;
		}

		// is a unit on the field?
		int u = MapScreen.get().getData().getMapData(EMapData.UNIT, x, y);
		// and is not me?
		if (u > 0 && u != obj.getId()) {
			return false;
			// it is self?
		} else if (u == obj.getId()) {
			return true;
		}

		// check fog
		if (obj.getPlayer().getMapData(EMapData.ISVISIBLE, x, y) == 0) {
			return false;
		}

		// is their a building?
		NMapBuilding b = MapScreen.get().getData().getBuilding(x, y);

		// get terrain
		String r = getTerrainID(x, y);
		BaseTerrain t = S.nData().getT(r == null ? "default" : r);

		// disable building it? &&
		if (b != null && b.isFinish()) {
			DataBuilding build = b.getBuild();
			DataUnit unit = obj.getUnit();

			// diff build?
			if (b.getPlayer() != obj.getPlayer() && build.getPublicCost().containsKey(unit.getMoveTyp())) {
				int cost = build.getPublicCost().get(unit.getMoveTyp());
				// building disabled move?
				if (cost == 0) {
					return false;
				}
				// building enable move?
				if (cost > 0) {
					return true;
				}
			}

			// own build?
			if (b.getPlayer() == obj.getPlayer() && build.getOwnerCost().containsKey(unit.getMoveTyp())) {
				int cost = build.getOwnerCost().get(unit.getMoveTyp());
				// building disabled move?
				if (cost == 0) {
					return false;
				}
				// building enable move?
				if (cost > 0) {
					return true;
				}
			}

		}

		// check terrain
		// can the unit typ pass?

		// pass it
		return t.canPass(obj.getUnit().getMoveTyp());
	}

	/**
	 * 
	 * @param z
	 * @param x
	 * @param y
	 * @return
	 */
	public String getTerrainID(int z, int x, int y) {
		int t = MapScreen.get().getData().getMapStructure(z, x, y);

		if (t == -1) {
			return null;
		}

		return S.nData().getT(t).getType();
	}

	/**
	 * Get the region properties from the map tileset
	 *
	 * @param x
	 * @param y
	 * @return
	 */
	public String getTerrainID(int x, int y) {

		try {
			for (int i = length - 1; i >= 0; i--) {
				String e = getTerrainID(i, x, y);

				if (e != null) {
					return e;
				}
			}
		} catch (Exception e) {
			Exception f = new IllegalArgumentException("Can not get region for " + x + " " + y);
			f.addSuppressed(e);
			YError.error(f, false);
		}
		return BaseTerrain.DEF;
	}

	public BaseTerrain getTerrain(int x, int y) {
		return S.nData().getT(getTerrainID(x, y));
	}

	/**
	 * Move the map to this coordinates
	 *
	 * @param x
	 * @param y
	 */
	public void setCenterMapView(int x, int y) {
		// YLog.log2("setCenterMapView", x, y);

		// valide poisiton?
		if (!isValide(x, y)) {
			return;
		}

		setCamera(x * 32 + 32 / 2, y * 32 + 32 / 2);
	}

	private void setCamera(float x, float y) {
		dstCamera.x = Math.max(0, Math.min(x, width * 32));
		dstCamera.y = Math.max(0, Math.min(y, height * 32));
	}

	/**
	 * Move the map to this coordinates
	 *
	 * @param x
	 * @param y
	 */
	public void moveMapView(float x, float y) {
		// YLog.log2("moveMapView", x, y);

		setCamera(dstCamera.x + x, dstCamera.y + y);
	}

	/**
	 * Check if the coordinates outside the map
	 *
	 * @param x
	 * @param y
	 * @return
	 */
	public boolean isValide(int x, int y) {
		return x >= 0 && y >= 0 && x < width && y < height;
	}

	public void nextPlayer() {
		// TODO Auto-generated method stub

	}

	/**
	 * @return the stage
	 */
	public Stage getStage() {
		return stage;
	}

	/**
	 * Load a map, after starting a new game
	 */
	public void initMap() {
		autotile = new NMapAutotile(this);
		autotile.buildMap();
	}

	/**
	 * @return the autotile
	 */
	public NMapAutotile getAutotile() {
		return autotile;
	}

}
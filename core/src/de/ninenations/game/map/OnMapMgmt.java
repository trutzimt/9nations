package de.ninenations.game.map;

import com.badlogic.gdx.utils.Array;

import de.ninenations.core.BaseMgmtMgmt;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.unit.NMapUnit;
import de.ninenations.player.Player;
import de.ninenations.towns.Town;

public class OnMapMgmt extends BaseMgmtMgmt<NOnMapObject> {

	private static final long serialVersionUID = -6913295756296267878L;

	public OnMapMgmt() {
		super();
	}

	/**
	 * Create a new
	 * 
	 * @param type
	 * @param name
	 * @param town
	 * @param x
	 * @param y
	 */
	public NMapUnit addU(String type, Player p, Town t, int x, int y) {
		NMapUnit no = new NMapUnit(type, counter++, p.getId(), t == null ? -1 : t.getId(), x, y);
		elements.add(no);
		return no;
	}

	/**
	 * Create a new
	 * 
	 * @param type
	 * @param name
	 * @param town
	 * @param x
	 * @param y
	 */
	public NMapBuilding addB(String type, Player p, Town t, int x, int y) {
		NMapBuilding no = new NMapBuilding(type, counter++, p.getId(), t == null ? -1 : t.getId(), x, y);
		elements.add(no);
		return no;
	}

	public void init() {
		// TODO Auto-generated method stub

	}

	/**
	 * Get all OnMap form this player
	 * 
	 * @param player
	 * @return
	 */
	public Array<NOnMapObject> getOnMapObjByPlayer(Player player) {
		Array<NOnMapObject> a = new Array<>();
		for (NOnMapObject n : elements) {
			if (n.getPlayer() == player) {
				a.add(n);
			}
		}

		return a;

	}

	/**
	 * Get all OnMap form this player
	 * 
	 * @param player
	 * @return
	 */
	public Array<NOnMapObject> getOnMapObjByPlayer(Player player, EMapData type) {

		Array<NOnMapObject> a = new Array<>();
		for (NOnMapObject n : elements) {
			if (n.getPlayer() == player && (type == EMapData.BUILDING && n instanceof NMapBuilding || type == EMapData.UNIT && n instanceof NMapUnit)) {
				a.add(n);
			}
		}

		return a;

	}

	/**
	 * Count all OnMaoObj for this player from this type
	 * 
	 * @param player
	 * @param type
	 * @param oType
	 * @return
	 */
	public int countOnMapObjByPlayerType(Player player, EMapData type, String oType) {
		int c = 0;
		for (NOnMapObject n : elements) {
			if (n.getPlayer() == player && (type == EMapData.BUILDING && n instanceof NMapBuilding || type == EMapData.UNIT && n instanceof NMapUnit)
					&& n.getType().equals(oType)) {
				c++;
			}
		}

		return c;
	}

	/**
	 * Get all OnMap form this player
	 * 
	 * @param player
	 * 
	 * @return
	 */
	public Array<NOnMapObject> getOnMapObjByTown(Town town, EMapData type) {
		Array<NOnMapObject> a = new Array<>();
		for (NOnMapObject n : elements) {
			if (n.getTown() == town && (type == EMapData.BUILDING && n instanceof NMapBuilding || type == EMapData.UNIT && n instanceof NMapUnit)) {
				a.add(n);
			}
		}

		return a;

	}

	/**
	 * Count all OnMaoObj for this town from this type
	 * 
	 * @param player
	 * @param type
	 * @param oType
	 * @return
	 */
	public int countOnMapObjByTownType(Town town, EMapData type, String oType) {
		int c = 0;
		for (NOnMapObject n : elements) {
			if (n.getTown() == town && (type == EMapData.BUILDING && n instanceof NMapBuilding || type == EMapData.UNIT && n instanceof NMapUnit)
					&& (oType == null || n.getType().equals(oType))) {
				c++;
			}
		}

		return c;
	}

	public void remove(NOnMapObject obj) {
		elements.removeIndex(elements.indexOf(obj, true));

	}
}

package de.ninenations.game.map;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer.Cell;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

import de.ninenations.data.terrain.BaseTerrain;
import de.ninenations.game.NRound.NSeason;
import de.ninenations.game.S;
import de.ninenations.game.screen.MapScreen;

public class NMapAutotile {

	private static final int AUTOWIDTH = 64;
	private int setX, setY, autoTileID, winterAutoTileID;

	private NMap map;
	private TiledMap autoMap, winterMap;
	private TiledMapRenderer autoMapRenderer, winterMapRenderer;
	private TiledMap tiledMap;

	public NMapAutotile(NMap map) {
		this.map = map;

		autoMap = new TmxMapLoader().load("system/tileset/autotilesGenerate.tmx");
		autoMapRenderer = new OrthogonalTiledMapRenderer(autoMap);

		winterMap = new TmxMapLoader().load("system/tileset/autotilesGenerate.tmx");
		winterMapRenderer = new OrthogonalTiledMapRenderer(winterMap);

	}

	/**
	 * 
	 * @param z
	 * @param x
	 * @param y
	 * @return
	 */
	private String getTerrainID(int z, int x, int y) {
		Cell c = ((TiledMapTileLayer) tiledMap.getLayers().get(z)).getCell(x, y);

		// has a cell?
		if (c == null) {
			return null;
		}

		if (c.getTile().getProperties().containsKey("region")) {
			return (String) c.getTile().getProperties().get("region");
		}

		return BaseTerrain.DEF;
	}

	/**
	 * 
	 */
	public void buildMap() {

		for (int z = 0; z < map.getLayerCount(); z++) {
			autoMap.getLayers().add(new TiledMapTileLayer(map.getWidth() * 2, map.getHeight() * 2, 16, 16));
			winterMap.getLayers().add(new TiledMapTileLayer(map.getWidth() * 2, map.getHeight() * 2, 16, 16));
		}

		// build fill map
		for (int z = 0; z < map.getLayerCount(); z++) {
			for (int x = 0; x < map.getWidth(); x++) {
				for (int y = 0; y < map.getHeight(); y++) {
					buildAutotileTopLeft(z, x, y);
					buildAutotileTopRight(z, x, y);
					buildAutotileBottomLeft(z, x, y);
					buildAutotileBottomRight(z, x, y);
				}
			}
		}
	}

	private int getTerrain(int z, int x, int y, int def) {
		if (!S.valide(x, y)) {
			return def;
		}

		return MapScreen.get().getData().getMapStructureRaw()[z][x][y];
	}

	/**
	 * Build this autotile
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	private void buildAutotileTopLeft(int z, int x, int y) {
		int t = getTerrain(z, x, y, 0);
		setX = x * 2;
		setY = y * 2 + 1;

		// remove autotile?
		if (t == -1) {
			clearAutotile(z);
			return;
		}

		// check neighbors
		boolean tl = getTerrain(z, x - 1, y, t) == t;
		boolean tt = getTerrain(z, x, y + 1, t) == t;
		boolean ttl = getTerrain(z, x - 1, y + 1, t) == t;

		// YLog.log(z, x, y, map.getTerrainID(z, x, y), tt, tl, ttl);

		autoTileID = S.nData().getTerrainRaw().get(t).getAutoTile();
		winterAutoTileID = S.nData().getTerrainRaw().get(t).getAutoTileWinter();

		// all?
		if (tt && tl && ttl) {
			setAutotile(z, AUTOWIDTH * 4 + 2);
			return;
		}

		// only left & right
		if (tt && tl) {
			setAutotile(z, 2);
			return;
		}

		// only left
		if (tl) {
			setAutotile(z, AUTOWIDTH * 2 + 2);
			return;
		}

		// only left
		if (tt) {
			setAutotile(z, AUTOWIDTH * 4);
			return;
		}

		// no connection?
		setAutotile(z, AUTOWIDTH * 2);
	}

	/**
	 * Build this autotile
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	private void buildAutotileTopRight(int z, int x, int y) {
		int t = getTerrain(z, x, y, 0);
		setX = x * 2 + 1;
		setY = y * 2 + 1;

		// remove autotile?
		if (t == -1) {
			clearAutotile(z);
			return;
		}

		// check neighbors
		boolean tr = getTerrain(z, x + 1, y, t) == t;
		boolean tt = getTerrain(z, x, y + 1, t) == t;
		boolean ttl = getTerrain(z, x + 1, y + 1, t) == t;

		// YLog.log(z, x, y, map.getTerrainID(z, x, y), tt, tl, ttl);

		autoTileID = S.nData().getTerrainRaw().get(t).getAutoTile();
		winterAutoTileID = S.nData().getTerrainRaw().get(t).getAutoTileWinter();

		// all?
		if (tt && tr && ttl) {
			setAutotile(z, AUTOWIDTH * 4 + 1);
			return;
		}

		// only left & right
		if (tt && tr) {
			setAutotile(z, 3);
			return;
		}

		// only left
		if (tr) {
			setAutotile(z, AUTOWIDTH * 2 + 1);
			return;
		}

		// only left
		if (tt) {
			setAutotile(z, AUTOWIDTH * 4 + 3);
			return;
		}

		// no connection?
		setAutotile(z, AUTOWIDTH * 2 + 3);
	}

	/**
	 * Build this autotile
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	private void buildAutotileBottomLeft(int z, int x, int y) {
		int t = getTerrain(z, x, y, 0);
		setX = x * 2;
		setY = y * 2;

		// remove autotile?
		if (t == -1) {
			clearAutotile(z);
			return;
		}

		// check neighbors
		boolean tl = getTerrain(z, x - 1, y, t) == t;
		boolean tb = getTerrain(z, x, y - 1, t) == t;
		boolean tbl = getTerrain(z, x - 1, y - 1, t) == t;

		// YLog.log(z, x, y, map.getTerrainID(z, x, y), tt, tl, ttl);

		autoTileID = S.nData().getTerrainRaw().get(t).getAutoTile();
		winterAutoTileID = S.nData().getTerrainRaw().get(t).getAutoTileWinter();

		// all?
		if (tb && tl && tbl) {
			setAutotile(z, AUTOWIDTH * 3 + 2);
			return;
		}

		// only left & right
		if (tb && tl) {
			setAutotile(z, AUTOWIDTH + 2);
			return;
		}

		// only left
		if (tl) {
			setAutotile(z, AUTOWIDTH * 5 + 2);
			return;
		}

		// only left
		if (tb) {
			setAutotile(z, AUTOWIDTH * 3);
			return;
		}

		// no connection?
		setAutotile(z, AUTOWIDTH * 5);
	}

	/**
	 * Build this autotile
	 * 
	 * @param x
	 * @param y
	 * @param z
	 */
	private void buildAutotileBottomRight(int z, int x, int y) {
		int t = getTerrain(z, x, y, 0);
		setX = x * 2 + 1;
		setY = y * 2;

		// remove autotile?
		if (t == -1) {
			clearAutotile(z);
			return;
		}

		// check neighbors
		boolean tr = getTerrain(z, x + 1, y, t) == t;
		boolean tb = getTerrain(z, x, y - 1, t) == t;
		boolean tbr = getTerrain(z, x + 1, y - 1, t) == t;

		// YLog.log(z, x, y, map.getTerrainID(z, x, y), tt, tl, ttl);

		autoTileID = S.nData().getTerrainRaw().get(t).getAutoTile();
		winterAutoTileID = S.nData().getTerrainRaw().get(t).getAutoTileWinter();

		// all?
		if (tb && tr && tbr) {
			setAutotile(z, AUTOWIDTH * 3 + 1);
			return;
		}

		// only left & right
		if (tb && tr) {
			setAutotile(z, AUTOWIDTH + 3);
			return;
		}

		// only left
		if (tr) {
			setAutotile(z, AUTOWIDTH * 5 + 1);
			return;
		}

		// only left
		if (tb) {
			setAutotile(z, AUTOWIDTH * 3 + 3);
			return;
		}

		// no connection?
		setAutotile(z, AUTOWIDTH * 5 + 3);
	}

	/**
	 * 
	 * @param z
	 * @param id
	 */
	private void setAutotile(int z, int id) {
		TiledMapTileLayer a = (TiledMapTileLayer) autoMap.getLayers().get(z + 1);

		Cell c = new Cell();
		c.setTile(autoMap.getTileSets().getTileSet(0).getTile(autoTileID + id + 1));
		a.setCell(setX, setY, c);

		// add winter
		a = (TiledMapTileLayer) winterMap.getLayers().get(z + 1);

		c = new Cell();
		c.setTile(winterMap.getTileSets().getTileSet(0).getTile(winterAutoTileID + id + 1));
		a.setCell(setX, setY, c);

		// if (map.getTerrainID(z, setX / 2, setY / 2).equals(BaseTerrain.WATER)) {
		// YLog.log(z, setX / 2, setY / 2, map.getTerrainID(z, setX / 2, setY / 2), id -
		// 32);
		// }

	}

	private void clearAutotile(int z) {
		TiledMapTileLayer a = (TiledMapTileLayer) autoMap.getLayers().get(z + 1);
		a.setCell(setX, setY, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	public void dispose() {
		autoMap.dispose();
		winterMap.dispose();
	}

	/**
	 * Remove this terrain from this coordinate, if exist
	 * 
	 * @param x
	 * @param y
	 * @param tile
	 */
	public void removeTile(int x, int y, String tile) {
		// convert
		int t = S.nData().getTerrainRaw().getPos(tile);

		// find z, on which layer need to work?
		int z = map.getLayerCount() - 1;
		for (;; z--) {
			int erg = MapScreen.get().getData().getMapStructure(z, x, y);
			if (t == erg) {
				break;
			}
			if (z <= 0) {
				// nothing found
				return;
			}

		}
		MapScreen.get().getData().getMapStructureRaw()[z][x][y] = -1;

		// set
		buildAutotileBottomRight(z, x - 1, y + 1);
		buildAutotileBottomLeft(z, x, y + 1);
		buildAutotileBottomRight(z, x, y + 1);
		buildAutotileBottomLeft(z, x + 1, y + 1);

		buildAutotileTopRight(z, x - 1, y - 1);
		buildAutotileTopLeft(z, x, y - 1);
		buildAutotileTopRight(z, x, y - 1);
		buildAutotileTopLeft(z, x + 1, y - 1);

		buildAutotileTopRight(z, x - 1, y);
		buildAutotileBottomRight(z, x - 1, y);
		buildAutotileTopLeft(z, x + 1, y);
		buildAutotileBottomLeft(z, x + 1, y);

		buildAutotileTopLeft(z, x, y);
		buildAutotileTopRight(z, x, y);
		buildAutotileBottomLeft(z, x, y);
		buildAutotileBottomRight(z, x, y);

	}

	public void setTile(int x, int y, String tile) {
		// convert
		int t = S.nData().getTerrainRaw().getPos(tile);

		// find z, on which layer need to work?
		int z = map.getLayerCount() - 1;
		for (; z > 0; z--) {
			int erg = MapScreen.get().getData().getMapStructure(z, x, y);
			if (t == -1) {
				if (erg != -1) {
					break;
				}
			} else {
				break;
			}

		}
		MapScreen.get().getData().getMapStructureRaw()[z][x][y] = t;

		// delete?
		if (t == -1) {
			for (setX = x * 2 - 1; setX < x * 2 + 2; setX++) {
				for (setY = y * 2 - 1; setY < y * 2 + 2; setY++) {
					clearAutotile(z);
				}
			}
			return;

		}

		// set

		buildAutotileBottomRight(z, x - 1, y + 1);
		buildAutotileBottomLeft(z, x, y + 1);
		buildAutotileBottomRight(z, x, y + 1);
		buildAutotileBottomLeft(z, x + 1, y + 1);

		buildAutotileTopRight(z, x - 1, y - 1);
		buildAutotileTopLeft(z, x, y - 1);
		buildAutotileTopRight(z, x, y - 1);
		buildAutotileTopLeft(z, x + 1, y - 1);

		buildAutotileTopRight(z, x - 1, y);
		buildAutotileBottomRight(z, x - 1, y);
		buildAutotileTopLeft(z, x + 1, y);
		buildAutotileBottomLeft(z, x + 1, y);

		buildAutotileTopLeft(z, x, y);
		buildAutotileTopRight(z, x, y);
		buildAutotileBottomLeft(z, x, y);
		buildAutotileBottomRight(z, x, y);
	}

	/**
	 * 
	 * @param camera
	 */
	public void render(OrthographicCamera camera) {

		if (S.round().is(NSeason.WINTER)) {
			winterMapRenderer.setView(camera);
			winterMapRenderer.render();
			return;
		}
		autoMapRenderer.setView(camera);
		autoMapRenderer.render();

	}

}

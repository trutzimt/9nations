/**
 * 
 */
package de.ninenations.game.map;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.ninenations.game.S;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.elements.YTextButton;
import de.ninenations.ui.window.YTabWindow;

/**
 * @author sven
 *
 */
public class DebugWindow extends YTabWindow {

	protected int x, y;

	/**
	 * @param name
	 */
	public DebugWindow(int x, int y) {
		super("Field " + x + "/" + y);

		this.x = x;
		this.y = y;

		tabbedPane.add(new OverviewTab());
		if (MapScreen.get().getData().isBuilding(x, y)) {
			tabbedPane.add(new NOnMapObjectTab(S.build(x, y)));
		}
		if (MapScreen.get().getData().isUnit(x, y)) {
			tabbedPane.add(new NOnMapObjectTab(S.unit(x, y)));
		}
		buildIt();
	}

	class OverviewTab extends Tab {

		public OverviewTab() {
			super(false, false);
		}

		@Override
		public String getTabTitle() {
			return x + "/" + y;
		}

		@Override
		public Table getContentTable() {
			YTable t = new YTable();
			t.addL("Terrain", S.map().getTerrainID(x, y));

			for (EMapData e : EMapData.values()) {
				t.addL(e.toString(), MapScreen.get().getData().getMapData(e, x, y) + "");
			}

			t.addL("Player visible", S.actPlayer().getMapData(EMapData.ISVISIBLE, x, y) + "");

			return t;
		}
	}

	class NOnMapObjectTab extends Tab {

		NOnMapObject o;

		public NOnMapObjectTab(NOnMapObject o) {
			super(false, false);

			this.o = o;
		}

		@Override
		public String getTabTitle() {
			return o.getName();
		}

		@Override
		public Table getContentTable() {
			YTable t = new YTable();
			t.add(o.getInfoPanel()).row();
			t.add(new YTextButton("Set finish", true) {

				@Override
				public void perform() {
					o.setFinish();

				}
			});
			return t.createScrollPaneInTable();
		}
	}
}

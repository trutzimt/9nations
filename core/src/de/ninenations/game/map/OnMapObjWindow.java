/**
 * 
 */
package de.ninenations.game.map;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.ninenations.actions.base.GAction;
import de.ninenations.data.buildingunitbase.DataObject;
import de.ninenations.game.S;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YSplitTab;
import de.ninenations.ui.window.YTabWindow;

/**
 * @author sven
 *
 */
public class OnMapObjWindow extends YTabWindow {

	private NOnMapObject obj;

	/**
	 * @param title
	 */
	public OnMapObjWindow(NOnMapObject obj) {
		super(obj.getName());

		this.obj = obj;
		DataObject data = obj.getDataObject();

		// add generall
		tabbedPane.add(new Tab(false, false) {

			@Override
			public String getTabTitle() {
				return "General";
			}

			@Override
			public Table getContentTable() {
				return OnMapObjWindow.this.obj.getInfoPanel();
			}
		});

		// add actions?
		if (obj.getPlayer() == S.actPlayer() && data.getActiveActions().size > 0) {
			tabbedPane.add(new ActionActiveTab(data));
		}

		// add actions?
		if (obj.getPlayer() == S.actPlayer() && data.getPassiveActions().size > 0) {
			tabbedPane.add(new ActionPassiveTab(data));
		}

		// add generall
		tabbedPane.add(new Tab(false, false) {

			@Override
			public String getTabTitle() {
				return "Lexicon";
			}

			@Override
			public Table getContentTable() {
				return OnMapObjWindow.this.obj.getDataObject().getInfoPanel().createScrollPaneInTable();
			}
		});

		addTitleIcon(data.getIcon());
		buildIt();
	}

	class ActionActiveTab extends YSplitTab<GAction> {

		private Table placeholder;
		private VisImageTextButton button;

		public ActionActiveTab(DataObject data) {
			super("Actions", "There are no active actions");

			// add actions
			for (GAction a : data.getActiveActions()) {
				addElement(a);
			}

			placeholder = new Table();
			buttonBar.addActor(placeholder);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * de.ninenations.ui.elements.YSplitTab#clickElement(com.badlogic.gdx.scenes.
		 * scene2d.ui.Button)
		 */
		@Override
		protected void clickElement(Button btn) {
			placeholder.clearChildren();

			button = active.getActionButton(obj.getPlayer(), obj, obj.getX(), obj.getY());
			if (button != null) {
				button.addCaptureListener(new YChangeListener() {

					@Override
					public void changedY(Actor actor) {
						OnMapObjWindow.this.close();

					}
				});

				placeholder.add(button);
			}
		}

		@Override
		protected void doubleClickElement(Button btn) {
			// TODO run button
		}

		/*
		 * 
		 */
		@Override
		protected Actor getInfoPanel(GAction o) {
			return o.getInfoPanel(obj);
		}

	}

	class ActionPassiveTab extends YSplitTab<GAction> {

		public ActionPassiveTab(DataObject data) {
			super("Passive actions", "There are no passive actions");

			// add actions
			for (GAction a : data.getPassiveActions()) {
				addElement(a);
			}
		}

		@Override
		protected void doubleClickElement(Button btn) {}

		/*
		 * 
		 */
		@Override
		protected Actor getInfoPanel(GAction o) {
			return o.getInfoPanel(obj);
		}

	}

}

package de.ninenations.game.map;

import java.io.Serializable;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ObjectMap;
import com.kotcrab.vis.ui.widget.Tooltip;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.actions.base.GAction;
import de.ninenations.core.BaseMgmtObject;
import de.ninenations.core.NSimpleObjectMap;
import de.ninenations.data.buildingunitbase.DataObject;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.stats.Stats;
import de.ninenations.towns.Town;
import de.ninenations.towns.TownWindow;
import de.ninenations.ui.UiHelper;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.MoveTo;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YProgressBar;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.elements.YTextButton;
import de.ninenations.ui.window.YNotificationSaver;
import de.ninenations.util.NGenerator;
import de.ninenations.util.NSettings;
import de.ninenations.util.YSounds;

/**
 * 
 */
public class NOnMapObject extends BaseMgmtObject implements Serializable {

	private static final long serialVersionUID = -1972086591870954886L;

	protected transient Image img, infoImg;

	/**
	 * 
	 */
	protected int x;
	protected int y;

	/**
	 * 
	 */
	protected int town;
	protected int player;

	/**
	 * 
	 */
	protected int ap;
	protected int hp;

	protected EMapData mapType;

	protected transient DataObject dataObject;

	protected int buildtime;
	protected NSimpleObjectMap<String, Integer> cost;
	protected NSimpleObjectMap<String, String> data;

	protected String error;
	protected transient VisLabel errorLbl;

	protected boolean sleep;

	/*
	 * Collect ap to perform this action
	 */
	protected int actionWaitAP;
	protected String actionWait;

	/**
	 * Default constructor
	 */
	public NOnMapObject() {
		super(null, 0, null);
	}

	/**
	 * Default constructor
	 */
	public NOnMapObject(EMapData mapType, String type, int id, String name, int player, int town, int x, int y) {
		super(type, id, name);

		this.x = -1;
		this.y = -1;

		this.mapType = mapType;
		this.town = town;
		this.player = player;

		data = new NSimpleObjectMap<>();
		// set it
		setXY(x, y);

		buildtime = -1;
		// set ap
		afterLoad();

		updateVisibleRange();

		reset();
	}

	/**
	 * 
	 */
	protected void reset() {
		float mod = x == -1 ? 1 : 1 + S.map().getTerrain(x, y).getBuildTime() / 100f;
		buildtime = (int) (dataObject.getBuildTime() * mod);

		if (town != -1) {
			buildtime -= buildtime * getTown().getWealthMultiplicator();
		}

		// copy cost
		cost = new NSimpleObjectMap<>();
		ObjectMap<String, Integer> costn = dataObject.getCost();
		for (String key : costn.keys()) {
			cost.put(key, costn.get(key));
		}

		// finish?
		if (buildtime == 0 && cost.size == 0) {
			setFinish();
		}
	}

	/**
	 * Update the field of view
	 */
	public void updateVisibleRange() {
		if (!isFinish()) {
			return;
		}

		if (!S.isActive(ScenConf.FOG)) {
			return;
		}

		// loaded?
		if (dataObject == null) {
			return;
		}

		Player p = getPlayer();
		int v = getViewDistance();

		// negative?
		if (v <= 0) {
			return;
		}

		// update it?
		if (p.getMapData(EMapData.ISVISIBLE, Math.min(0, x - v), y) == 1 || p.getMapData(EMapData.ISVISIBLE, x, Math.min(0, y - v)) == 1
				|| p.getMapData(EMapData.ISVISIBLE, Math.max(S.map().getWidth(), x + v), y) == 1
				|| p.getMapData(EMapData.ISVISIBLE, x, Math.max(S.map().getWidth(), y + v)) == 1) {
			return;
		}

		updateVisibleRangeHelper(p, x, y, v);

	}

	/**
	 * @return
	 */
	public int getViewDistance() {
		int v = dataObject.getVisibleRange() + S.map().getTerrain(x, y).getVisibleRange();
		v += getPlayer().getOtherMultiplicator("view");

		return Math.max(0, v);
	}

	private void updateVisibleRangeHelper(Player p, int x, int y, int range) {

		// can use?
		if (!S.valide(x, y)) {
			return;
		}

		// set
		p.setMapData(EMapData.ISVISIBLE, x, y, 1);

		if (range == 0) {
			return;
		}

		// update neighbors
		range--;
		updateVisibleRangeHelper(p, x - 1, y, range);
		updateVisibleRangeHelper(p, x, y - 1, range);
		updateVisibleRangeHelper(p, x + 1, y, range);
		updateVisibleRangeHelper(p, x, y + 1, range);

	}

	public void upgradeTo(String type) {
		this.type = type;
		name = S.nData().get(mapType, type).getName();
		buildtime = -1;
		// remove old img
		img.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));

		// load it
		afterLoad();

		// reset it
		reset();
	}

	public void degradeTo(String type) {
		// remove old
		actionDestroy();

		this.type = type;
		name = S.nData().get(mapType, type).getName();

		// remove old img
		img.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));

		// load it
		afterLoad();

		// reset it
		reset();
	}

	/**
	 * Set the new position
	 * 
	 * @param x
	 * @param y
	 */
	public void setXY(int x, int y) {
		// unregister
		if (this.x >= 0) {

			MapScreen.get().getData().setMapData(mapType, this.x, this.y, 0);
			// move image
			getActor().addAction(Actions.moveTo(x * 32, y * 32, Math.abs(this.x - x) + Math.abs(this.y - y)));
		}

		this.x = x;
		this.y = y;

		// register
		if (this.x >= 0) {
			MapScreen.get().getData().setMapData(mapType, this.x, this.y, id);
		}

		updateVisibleRange();
	}

	/**
	 * 
	 */
	public void destroy() {
		// remove it
		hide();
		MapScreen.get().getData().getOnMap().remove(this);

		if (town == -1) {
			return;
		}
		Town t = getTown();
		// get ress back
		for (String key : dataObject.getCost().keys()) {
			t.addRess(key, (int) (dataObject.getCost().get(key, 0) * (S.nData().getR(key).getRechangeAtDestroy() / 100f)));
		}

		actionDestroy();
	}

	/**
	 * 
	 */
	private void actionDestroy() {
		// destroy actions
		for (GAction a : dataObject.getActiveActions()) {
			a.performAtDestroy(this);
		}

		// destroy actions
		for (GAction a : dataObject.getPassiveActions()) {
			a.performAtDestroy(this);
		}
	}

	@Override
	public Image getIcon() {
		return new Image(img.getDrawable());
	}

	@Override
	public YTable getInfoPanel() {
		YTable table = new YTable();
		table.addH("Basics");
		table.addL("Name", name);
		if (S.actPlayer() == getPlayer()) {
			if (error != null) {
				table.addL("Error", error);
			}
			table.addL("AP", new YProgressBar(ap, getDataObject().getAp()));
			table.addL("HP", new YProgressBar(hp, getDataObject().getHp()));
			if (town >= 0) {
				table.addL("Town", new YTextButton(getTown().getName()) {

					@Override
					public void perform() {
						YSounds.pClick();
						MapScreen.get().getStage().addActor(new TownWindow(getTown()));

					}
				});
			}
			if (!isFinish()) {
				table.addH("Under construction");
				table.addL("Buildtime", new YProgressBar(buildtime, getDataObject().getBuildTime()));
				// ressources?
				for (String key : cost.keys()) {
					table.addI(S.nData().getR(key).getIcon(), cost.get(key, 0) + "x " + S.nData().getR(key).getName());
				}
			}
		} else {
			table.addL("AP", "??");
			table.addL("HP", "??");
			table.addL("Player", getPlayer().getName());
		}

		return table;
	}

	/**
	 * Set the last error
	 * 
	 * @param mess
	 */
	public void setError(String mess, boolean inform) {
		error = mess;

		// update label
		if (mess == null) {
			if (errorLbl != null && errorLbl.isVisible()) {
				errorLbl.setVisible(false);
			}
			return;
		}

		if (errorLbl == null) {
			errorLbl = new VisLabel();
			errorLbl.setX(x * 32);
			errorLbl.setY(y * 32 + 16);
			errorLbl.setVisible(false);
			MapScreen.get().getMap().getStage().addActor(errorLbl);
		}

		errorLbl.setText("!" + mess.substring(0, 1));

		// inform the player
		if (inform) {
			getPlayer().addInfo(getName() + ": " + mess, dataObject.getIcon(), YIcons.BUILD);
		}

	}

	public NOnMapObject setFinish() {
		buildtime = 0;
		cost.clear();

		hp = dataObject.getHp();
		ap = dataObject.getAp();
		updateVisibleRange();

		// perform
		for (GAction g : dataObject.getPassiveActions()) {
			g.performAtBuild(this);
		}
		img.addAction(Actions.fadeIn(1));

		// add points
		int points = 0;
		for (String ress : dataObject.getCost().keys()) {
			points += S.nData().getR(ress).getMarketCost() * dataObject.getCost().get(ress);
		}
		getPlayer().getStats().add(Stats.POINTS, points);
		setInfoImg(null);

		return this;
	}

	/**
	 * 
	 */
	public void performAtPlayerRoundBegin() {
		// enable imgs
		if (getInfoImg() != null) {
			getInfoImg().setVisible(true);
		}

		if (errorLbl != null && error != null) {
			errorLbl.setVisible(true);
		}

		// perform an action?
		if (actionWait != null) {
			// get action
			GAction a = getDataObject().getAction(actionWait);

			// has enough?
			if (actionWaitAP + ap > a.getAp()) {
				// inform
				getPlayer().addInfo(new YNotificationSaver(name + " perform automatical " + a.getName(), getIcon()));

				// perform
				a.perform(getPlayer(), this, x, y);

				// reset
				setActionWait(null);
			} else {
				actionWaitAP += ap;
				ap = 0;
			}
		}
	}

	/**
	 * Set a new action to wait
	 * 
	 * @param action
	 */
	public void setActionWait(String action) {
		actionWaitAP = 0;
		actionWait = action;

		// add first ap
		if (action != null) {
			actionWaitAP += ap;
			ap = 0;
		}
	}

	/**
	 * 
	 */
	public void performAtPlayerRoundEnd() {
		// disable imgs
		if (getInfoImg() != null) {
			getInfoImg().setVisible(false);
		}

		if (errorLbl != null) {
			errorLbl.setVisible(false);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BaseMgmtObject#nextRound()
	 */
	@Override
	public void nextRound() {

		// takes a break?
		if (sleep) {
			return;
		}

		// reset it
		setError(null, false);

		// under construcation?
		if (!isFinish()) {
			// has a town?
			if (cost.size > 0 && town == -1) {
				setError("Town is missing, can not build", true);
				return;
			}

			build();

			// finish it
			if (!isFinish()) {
				return;
			}

			getPlayer().addInfo(new YNotificationSaver(getName() + " is finish build.", dataObject.getIcon(), YIcons.BUILD));
			setFinish();
		}

		ap = dataObject.getAp();

		// has a town?
		if (town == -1) {
			return;
		}
		// run actions
		for (GAction action : dataObject.getPassiveActions()) {
			action.performAtRoundBegin(this);
		}

		ap *= 1 + getTown().getWealthMultiplicator();
		int maxhp = (int) (dataObject.getHp() * (1 + getTown().getWealthMultiplicator()));

		// heal?
		if (hp < maxhp) {
			hp++;
		} else if (hp > maxhp) {
			hp = maxhp;
		}
	}

	/**
	 * 
	 */
	protected void build() {

		if (cost.size > 0) {
			Town t = getTown();
			for (String key : cost.keys()) {
				int c = cost.get(key);
				int r = t.getRess(key);
				// has enough ressorces?
				if (r >= c) {
					t.addRess(key, -c);
					cost.remove(key);
					continue;
				}
				// has part?
				if (r > 0) {
					cost.put(key, c - r);
					t.addRess(key, -r);
					continue;
				}

				// has nothing?
				setError(S.nData().getR(key).getName() + " is missing " + c + "x in " + t.getName(), true);
			}
		}

		if (buildtime > 1) {
			buildtime--;
			hp += dataObject.getHp() / dataObject.getBuildTime();
			return;
		}

		if (cost == null || cost.size == 0) {
			buildtime = 0;
		}
	}

	public boolean isFinish() {
		return buildtime == 0 && (cost == null || cost.size == 0);
	}

	/**
	 * Calc the damage
	 * 
	 * @param defensor
	 * @return
	 */
	public int calcDamage(NOnMapObject defensor) {
		float dam = NGenerator.getIntBetween(dataObject.getDamMin(), dataObject.getDamMax() + 1);
		int atk = dataObject.getAtk(), def = defensor.getDataObject().getDef();

		// check multi
		if (atk >= def) {
			dam *= 1 + 0.05 * (atk - def);
		} else {
			dam *= 1 - 0.25 * (def - atk);
		}

		return (int) dam;
	}

	/**
	 * Attack to obj
	 * 
	 * @param defensor
	 * @return true > successfull, false otherwise
	 */
	public boolean attack(NOnMapObject defensor) {
		// calc damage
		int damage = calcDamage(defensor);

		// check it
		if (damage == 0) {
			getPlayer().addInfo(
					new YNotificationSaver(getName() + " and " + defensor.getName() + " are equal, nothing to win.", YIcons.DEFEND, new MoveTo(defensor)));
			return false;
		}

		// counter fight
		if (damage < 0) {
			defensor.attack(this);
			return false;
		}

		// win
		int oX = defensor.getX();
		int oY = defensor.getY();
		defensor.addHp(-damage);
		UiHelper.textAnimation("-" + damage + " hp", oX, oY, true, Color.SALMON);

		// add animation
		defensor.getActor().addAction(Actions.sequence(Actions.color(Color.RED, 1), Actions.color(Color.WHITE, 1)));
		getActor().addAction(Actions.parallel(Actions.sequence(Actions.moveTo(oX * 32, oY * 32, 1), Actions.moveTo(x * 32, y * 32, 1)),
				Actions.sequence(Actions.color(Color.BLACK, 1), Actions.color(Color.WHITE, 1))));

		return true;
	}

	public void addHp(int hp) {
		this.hp += hp;

		if (this.hp <= 0) {
			getPlayer().addInfo(new YNotificationSaver(getName() + " was destroyed, based on the damage.", getIcon(), YIcons.WARNING));
			destroy();
		}

	}

	/**
	 * Return the actor from the map
	 * 
	 * @return
	 */
	public Actor getActor() {
		return img;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BaseMgmtObject#afterLoad()
	 */
	@Override
	public void afterLoad() {
		coreAfterLoad();

		img.setX(x * 32);
		img.setY(y * 32);
		MapScreen.get().getMap().getStage().addActor(img);
		img.getColor().a = isFinish() ? 1 : 0.5f;

		if (!isFinish()) {
			setInfoImg(YIcons.getIconO(YIcons.BUILD));
		}

		if (isSleep()) {
			setInfoImg(YIcons.getIconO(YIcons.SLEEP));
		}
	}

	/**
	 * 
	 */
	protected void coreAfterLoad() {
		dataObject = S.nData().get(mapType, type);
		img = dataObject.getIconO();
	}

	public DataObject getDataObject() {
		return dataObject;
	}

	/**
	 * @return the ap
	 */
	public int getAp() {
		return ap;
	}

	/**
	 * @param ap
	 *            the ap to set
	 */
	public void setAp(int ap) {
		this.ap = ap;
	}

	/**
	 * @param ap
	 *            the ap to set
	 */
	public void addAp(int ap) {
		this.ap += ap;
		if (this.ap < 0) {
			this.ap = 0;
		}
	}

	public Player getPlayer() {
		return MapScreen.get().getData().getPlayers().get(player);
	}

	/**
	 * Return the town or null
	 * 
	 * @return
	 */
	public Town getTown() {
		if (town == -1) {
			return null;
		}
		return MapScreen.get().getData().getTowns().get(town);
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return y;
	}

	/**
	 * @param town
	 *            the town to set
	 */
	public void setTown(int town) {
		this.town = town;
	}

	/**
	 * @return the buildtime
	 */
	public int getBuildtimeProcent() {
		return Math.round((dataObject.getBuildTime() * 1.0f - buildtime) / dataObject.getBuildTime() * 100);
	}

	/**
	 * Hide this map object, but not destroy it
	 */
	public void hide() {
		MapScreen.get().getData().setMapData(mapType, x, y, 0);
		getActor().addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
		x = -1;
		y = -1;

		if (errorLbl != null) {
			errorLbl.remove();
			errorLbl = null;
		}

	}

	/**
	 * Show a hided map object
	 */
	public void show(int x, int y) {
		MapScreen.get().getData().setMapData(mapType, x, y, id);
		this.x = x;
		this.y = y;
		afterLoad();

	}

	/**
	 * @return the data
	 */
	public ObjectMap<String, String> getData() {
		return data;
	}

	/**
	 * @param player
	 *            the player to set
	 */
	public void setPlayer(int player) {
		this.player = player;
	}

	/**
	 * @return the sleep
	 */
	public boolean isSleep() {
		return sleep;
	}

	/**
	 * @param sleep
	 *            the sleep to set
	 */
	public void setSleep(boolean sleep) {
		this.sleep = sleep;
		setInfoImg(sleep ? YIcons.getIconO(YIcons.SLEEP) : null);
	}

	/**
	 * Add click menu for this object
	 * 
	 * @param menu
	 */
	public void addMenu(Table menu) {
		// own unit?
		if (getPlayer() != S.actPlayer()) {
			return;

		}

		// sleeping?
		if (sleep) {
			VisImageTextButton t = new VisImageTextButton(getName() + " takes a break", YIcons.getIconI(YIcons.SLEEP).getDrawable());
			t.addCaptureListener(new YChangeListener() {

				@Override
				public void changedY(Actor actor) {
					YSounds.pCancel();
					setSleep(false);
					MapScreen.get().getGui().showBottomMenuFor(x, y);
				}
			});
			menu.add(t);
			return;
		}

		// waiting?
		if (actionWait != null) {
			GAction a = getDataObject().getAction(actionWait);

			VisImageTextButton t = new VisImageTextButton("Working on " + a.getName() + " (" + actionWaitAP + "/" + a.getAp() + "AP)",
					a.getIcon().getDrawable());
			t.addCaptureListener(new YChangeListener() {

				@Override
				public void changedY(Actor actor) {
					YSounds.pCancel();
					setActionWait(null);
					MapScreen.get().getGui().showBottomMenuFor(x, y);
				}
			});
			menu.add(t);
			return;
		}

		// under construction?
		if (!isFinish()) {
			VisImageTextButton t = new VisImageTextButton(getBuildtimeProcent() + "% under construction", YIcons.getIconI(YIcons.BUILD).getDrawable());
			t.setTouchable(Touchable.disabled);
			menu.add(t);
		}

		// add actions
		for (GAction g : getDataObject().getActiveActions()) {
			VisImageTextButton a = g.getActionButton(S.actPlayer(), this, x, y);
			if (a != null) {
				if (a.getColor().a == 0.5f && !NSettings.isShowDisabled()) {
					continue;
				}
				new Tooltip.Builder(a.getText().toString()).target(a).build();
				a.setText(null);
				menu.add(a);
			}
		}
	}

	/**
	 * @param infoImg
	 *            the infoImg to set , null to remove
	 */
	public void setInfoImg(Image infoImg) {
		// remove old?
		if (this.infoImg != null) {
			this.infoImg.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
		}

		this.infoImg = infoImg;

		if (this.infoImg != null) {
			// this.infoImg.setScale(0.5f);
			// this.infoImg.setX(x * 32 + 8);
			// this.infoImg.setY(y * 32 + 8);
			this.infoImg.setZIndex(img.getZIndex() + 1);
			this.infoImg.setX(x * 32);
			this.infoImg.setY(y * 32);
			this.infoImg.getColor().a = 0;
			this.infoImg.addAction(Actions.fadeIn(1));
			MapScreen.get().getMap().getStage().addActor(this.infoImg);
		}

	}

	/**
	 * @return the infoImg
	 */
	public Image getInfoImg() {
		return infoImg;
	}

	/**
	 * @return the actionWaitAP
	 */
	public int getActionWaitAP() {
		return actionWaitAP;
	}
}
package de.ninenations.mod;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.ninenations.core.NN;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.NSettings;
import de.ninenations.util.YError;
import de.ninenations.util.YLog;

/**
 * 
 * @author sven
 *
 */
public class ModInfo extends BaseDisplay {

	private int icon;
	private boolean enable;
	private FileHandle folder;
	private String version, author, compatible, desc, className;
	private IMod modClass;

	public ModInfo(FileHandle folder) {
		super(folder.nameWithoutExtension(), folder.nameWithoutExtension());

		icon = YIcons.MOD;
		this.folder = folder;

		enable = NSettings.getPref().getBoolean(getFullID("status"), false);

		readFile();
		loadModClass();
	}

	private void loadModClass() {
		if (!enable) {
			return;
		}

		if (className == null || !folder.child(type + ".jar").exists()) {
			YLog.log("mod", type, "hos no class to load");
			return;
		}

		try {
			modClass = NN.plattform().loadMod(this);
		} catch (Exception e) {
			YError.error(e, false);
		}
	}

	public String getFullID(String type) {
		return "mod." + this.type + "." + type;
	}

	private void readFile() {
		FileHandle info = folder.child("desc.txt");
		if (!info.exists()) {
			return;
		}

		// read it
		String[] lines = info.readString().split("\\r?\\n");
		for (String line : lines) {
			String[] ele = line.split("=");

			switch (ele[0]) {
			case "name":
				name = ele[1];
				continue;
			case "icon":
				icon = Integer.parseInt(ele[1]);
				continue;
			case "version":
				version = ele[1];
				continue;
			case "author":
				author = ele[1];
				continue;
			case "compatible":
				compatible = ele[1];
				continue;
			case "desc":
				desc = ele[1];
				continue;
			case "className":
				className = ele[1];
				continue;
			default:
				YLog.log("Mod setting " + ele[0] + " not supported");
			}
		}
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(icon);
	}

	@Override
	public YTable getInfoPanel() {
		YTable t = new YTable();
		t.addH("General");
		t.addL("Name", name);
		if (version != null) {
			t.addL("Version", version);
		}
		if (author != null) {
			t.addL("Author", author);
		}
		if (desc != null) {
			t.addL("Desc", desc);
		}

		t.addH("Technic");
		t.addL("Enable", "" + enable);
		if (compatible != null) {
			t.addL("Compatible", compatible);
		}

		return t;
	}

	/**
	 * Check if this mod can work here
	 * 
	 * @return
	 */
	public boolean checkCompatible() {
		if (compatible == null || !compatible.contains(Double.toString(NSettings.VERSION))) {
			Dialogs.showOKDialog(NN.screen().getStage(), "Not compatible", "Mod " + name + " is not compatible with your system. Please update it.");
			return false;
		}

		return true;
	}

	/**
	 * @return the enable
	 */
	public boolean isEnable() {
		return enable;
	}

	/**
	 * @param enable
	 *            the enable to set
	 */
	public void setEnable(boolean enable) {
		NSettings.getPref().putBoolean(getFullID("status"), enable);
		this.enable = enable;

		if (enable) {
			loadModClass();
		}
	}

	/**
	 * @return the folder
	 */
	public FileHandle getFolder() {
		return folder;
	}

	/**
	 * @return the modClass
	 */
	public IMod getModClass() {
		return modClass;
	}

	/**
	 * @return the className
	 */
	public String getClassName() {
		return className;
	}

}

/**
 * 
 */
package de.ninenations.mod;

import com.badlogic.gdx.scenes.scene2d.ui.Button;

import de.ninenations.core.NN;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.NSplitTab;
import de.ninenations.ui.elements.NTextButton;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ModTab extends NSplitTab<ModInfo> {

	private NTextButton enable;

	/**
	 * @param title
	 * @param error
	 */
	public ModTab() {
		super("mod", "Mods", YIcons.MOD, "No mods found");

		enable = new NTextButton("Select a mod first") {

			@Override
			public void perform() {
				doubleClickElement(null);

			}
		};
		enable.setDisabled(true);
		buttonBar.add(enable);

		for (String key : NN.mods().getModIds()) {
			addElement(NN.mods().get(key));
		}
	}

	/**
	 * First click on the list element
	 * 
	 * @param btn
	 */
	@Override
	protected void clickElement(Button btn) {
		enable.setText((active.isEnable() ? "Disable" : "Enable") + " mod " + active.getName());
		enable.setDisabled(false);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.ui.elements.YSplitTab#doubleClickElement(com.badlogic.gdx.
	 * scenes.scene2d.ui.Button)
	 */
	@Override
	protected void doubleClickElement(Button btn) {
		// check condition
		if (!active.isEnable() && !active.checkCompatible()) {
			YSounds.pBuzzer();
			return;
		}

		YSounds.pClick();
		active.setEnable(!active.isEnable());

		// update it
		clickElement(btn);
		updateRightArea();

	}

}

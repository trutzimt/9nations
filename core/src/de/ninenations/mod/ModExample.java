/**
 * 
 */
package de.ninenations.mod;

import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.ninenations.core.NN;

/**
 * @author sven
 *
 */
public class ModExample implements IMod {

	@Override
	public void startProgram() {
		Dialogs.showOKDialog(NN.screen().getStage(), "title", "text");

	}

	@Override
	public void startGame() {
		Dialogs.showOKDialog(NN.screen().getStage(), "title", "text");

	}

}

/**
 * 
 */
package de.ninenations.mod;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;

import de.ninenations.data.NData;
import de.ninenations.util.YLog;

/**
 * @author sven
 *
 */
public class ModMmgt {

	private ObjectMap<String, ModInfo> mods;

	/**
	 * 
	 */
	public ModMmgt() {
		mods = new ObjectMap<>();
		init();
	}

	/**
	 * Get all ids
	 * 
	 * @return
	 */
	public Array<String> getModIds() {
		return mods.keys().toArray();
	}

	/**
	 * 
	 */
	public void init() {
		// add local files
		if (Gdx.files.isLocalStorageAvailable()) {
			FileHandle file = Gdx.files.local("mods");
			if (file.isDirectory()) {
				readFolder(file);
			}
		}

		// add external files
		if (Gdx.files.isExternalStorageAvailable()) {
			FileHandle file = Gdx.files.external("mods");
			if (file.isDirectory()) {
				readFolder(file);

			}
		}
	}

	/**
	 * Start every enabled mod
	 */
	public void startProgram() {
		YLog.log("mod", "startProgram");
		for (Entry<String, ModInfo> m : mods.entries()) {
			if (!m.value.isEnable()) {
				continue;
			}

			IMod iMod = m.value.getModClass();
			if (iMod != null) {
				iMod.startProgram();
			}
		}
	}

	/**
	 * Start every enabled mod for this game
	 */
	public void startGame() {
		YLog.log("mod", "startGame");
		for (Entry<String, ModInfo> m : mods.entries()) {
			if (!m.value.isEnable()) {
				continue;
			}

			IMod iMod = m.value.getModClass();
			if (iMod != null) {
				iMod.startGame();
			}
		}
	}

	/**
	 * Check every mod folder and read the csv
	 */
	public void readnData(NData nData) {

		for (Entry<String, ModInfo> m : mods.entries()) {
			if (!m.value.isEnable()) {
				continue;
			}

			nData.addFiles(m.value.getFolder().list(".csv"));
		}
	}

	private void readFolder(FileHandle folder) {
		for (FileHandle sub : folder.list()) {
			if (sub.isDirectory()) {
				addMod(sub);
			}
		}
	}

	/**
	 * Add a new mod
	 * 
	 * @param folder
	 */
	private void addMod(FileHandle folder) {
		ModInfo m = new ModInfo(folder);

		if (mods.containsKey(m.getType())) {
			YLog.log("Overwrite mod", m.getType());
		}
		mods.put(m.getType(), m);
	}

	/**
	 * Get this mod
	 * 
	 * @param key
	 * @return
	 */
	public ModInfo get(String key) {
		return mods.get(key);
	}
}

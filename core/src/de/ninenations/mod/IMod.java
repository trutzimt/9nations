/**
 * 
 */
package de.ninenations.mod;

/**
 * @author sven
 *
 */
public interface IMod {

	public void startProgram();

	public void startGame();
}

/**
 *
 */
package de.ninenations.research;

import java.io.Serializable;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

import de.ninenations.core.NArray;
import de.ninenations.data.research.BaseResearch;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.game.S;
import de.ninenations.player.Player;
import de.ninenations.towns.Town;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YNotificationSaver;

/**
 * @author sven
 *
 */
public class ResearchMgmt implements Serializable {

	private static final long serialVersionUID = -2740680157761763562L;

	protected int pid;
	protected int currentCost;
	protected NArray<String> currentElements;
	protected ObjectMap<String, Boolean> finishResearch;

	/**
	 *
	 */
	public ResearchMgmt() {
		this(-1);
	}

	/**
	 *
	 */
	public ResearchMgmt(int pid) {
		finishResearch = new ObjectMap<>();
		this.pid = pid;
	}

	/**
	 * Research new elements
	 *
	 * @param elements
	 */
	public void setNewResearch(NArray<String> elements) {
		currentCost = 0;
		if (elements.size == 0) {
			currentElements = null;
			return;
		}

		currentElements = elements;

		int s = Math.max(0, elements.contains(S.players().get(pid).getNation().getResearchElement(), false) ? elements.size - 1 : elements.size);

		// calc cost
		currentCost = Math.round(s + s * s * 0.25f);
	}

	/**
	 * Get all avaible research for this player
	 *
	 * @param map
	 * @param player
	 * @param elements,
	 *            optional
	 * @return
	 */
	public static Array<String> getAvaibleResearch(Player player, Array<String> elements) {
		Array<String> ergs = new Array<>();

		parent: for (String type : player.getNation().getResearch()) {
			// has it already?
			if (player.getResearch().hasFinishResearch(type)) {
				continue;
			}

			// can research it?
			BaseResearch r = S.nData().getRS(type);

			if (r.canPerform(player)) {
				if (elements == null) {
					ergs.add(type);
				} else {
					// part in elements?
					for (String ele : r.getElements()) {
						if (!elements.contains(ele, false)) {
							continue parent;
						}
					}

					// all contains? at it
					ergs.add(type);
				}
			}
		}

		return ergs;
	}

	/**
	 * Run the next round
	 */
	public void nextRound() {
		Player player = S.players().get(pid);
		// search it?
		if (currentCost == 0 && currentElements == null) {
			// reset research
			for (Town t : S.town().getTownsByPlayer(player)) {
				t.addRess(BaseRess.RESEARCH, -t.getRess(BaseRess.RESEARCH));
			}

			// has something to research?
			if (player.getRess(BaseRess.RESEARCH) == 0) {
				return;
			}

			if (ResearchMgmt.getAvaibleResearch(player, null).size != 0) {
				player.addInfo(new YNotificationSaver("Research: You do not research anything.", YIcons.RESEARCH));
			}
			return;
		}

		currentCost -= player.getRess(BaseRess.RESEARCH);
		// reset research
		for (Town t : S.town().getTownsByPlayer(player)) {
			t.addRess(BaseRess.RESEARCH, -t.getRess(BaseRess.RESEARCH));
		}

		// finish it?
		if (currentCost <= 0) {
			Array<String> av = ResearchMgmt.getAvaibleResearch(player, currentElements);

			// found it?
			if (av.size == 0) {
				player.addInfo(new YNotificationSaver("Research: You discovered nothing new in the selected areas.", YIcons.RESEARCH));
				currentCost = 0;
				currentElements = null;
				return;
			}

			// select own
			BaseResearch newR = S.nData().getRS(av.random());
			finishResearch.put(newR.getType(), true);
			player.addInfo(
					new YNotificationSaver("Research: Eureka. Your researchers have discovered " + newR.getName() + ".", newR.getIcon(), YIcons.RESEARCH));

			// restart same eras
			setNewResearch(currentElements);
		}

	}

	/**
	 * Has the player finished this research?
	 *
	 * @param type
	 * @return
	 */
	public boolean hasFinishResearch(String type) {
		return finishResearch.get(type, false);
	}

	/**
	 * Finish or unfinish a research
	 * 
	 * @param type
	 * @param value
	 */
	public void setResearch(String type, boolean value) {
		finishResearch.put(type, value);
	}

	/**
	 * @return the currentElements
	 */
	public Array<String> getCurrentElements() {
		return currentElements;
	}

	/**
	 * Return the number of finished researches
	 * 
	 * @return
	 */
	public int getFinishResearchCount() {
		return finishResearch.size;
	}

	/**
	 * @return the currentCost
	 */
	public int getCurrentCost() {
		return currentCost;
	}

	/**
	 * @param currentCost
	 *            the currentCost to set
	 */
	public void setCurrentCost(int currentCost) {
		this.currentCost = currentCost;
	}

}

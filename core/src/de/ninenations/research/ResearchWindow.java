/**
 * 
 */
package de.ninenations.research;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.ObjectMap;
import com.kotcrab.vis.ui.widget.VisTextButton;

import de.ninenations.core.NArray;
import de.ninenations.data.elements.BaseElement;
import de.ninenations.data.research.BaseResearch;
import de.ninenations.game.S;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YSplitTab;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.window.YTabWindow;
import de.ninenations.util.NSettings;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class ResearchWindow extends YTabWindow {

	/**
	 * @param name
	 */
	public ResearchWindow() {
		super("Research");

		tabbedPane.add(new ResearchTab());
		tabbedPane.add(new OldResearchTab());

		buildIt();

		addTitleIcon(YIcons.getIconI(YIcons.RESEARCH));
	}

	public class ResearchTab extends YSplitTab<BaseElement> {

		private ObjectMap<String, Boolean> selected;
		private VisTextButton add, startResearch;

		public ResearchTab() {
			super("Research", "At the moment there is no active research or new research possible");

			selected = new ObjectMap<>();

			// add button
			add = new VisTextButton("Select a area first");
			add.addCaptureListener(new YChangeListener(false) {

				@Override
				public void changedY(Actor actor) {
					add();

				}
			});
			add.setDisabled(true);
			buttonBar.addActor(add);

			// add button
			startResearch = new VisTextButton("Start a new research");
			startResearch.addCaptureListener(new YChangeListener() {

				@Override
				public void changedY(Actor actor) {
					// build list
					NArray<String> elements = new NArray<>();

					for (String key : selected.keys()) {
						if (selected.get(key)) {
							elements.add(key);
						}
					}

					S.actPlayer().getResearch().setNewResearch(elements);
					ResearchWindow.this.close();
				}
			});
			startResearch.setDisabled(true);
			buttonBar.addActor(startResearch);

			resetElements();

		}

		public void resetElements() {
			elements.clear();
			// add all elements
			ObjectMap<String, Boolean> vis = new ObjectMap<>();

			// find elements to add
			for (String type : S.actPlayer().getNation().getResearch()) {

				// has researched
				if (S.actPlayer().getResearch().hasFinishResearch(type)) {
					continue;
				}

				// can research?
				BaseResearch b = S.nData().getRS(type);
				if (!b.canPerform(S.actPlayer())) {
					continue;
				}

				// add all elements
				for (String key : b.getElements()) {
					vis.put(key, true);
				}
			}

			// add it
			for (String type : vis.keys()) {
				addElement(S.nData().getE(type));
			}
		}

		/**
		 * Reset the status
		 */
		@Override
		protected void reset() {
			super.reset();
			active = null;
			add.setDisabled(true);
			add.setText("Select a area first");
			startResearch.setDisabled(!selected.containsValue(true, true));

		}

		/**
		 * Place the selected artwork
		 */
		@Override
		protected void doubleClickElement(Button btn) {
			add();
		}

		/**
		 * Buy the selected artwork
		 */
		protected void add() {
			YSounds.pClick();
			// switch
			if (selected.containsKey(active.getType())) {
				selected.remove(active.getType());
			} else {
				selected.put(active.getType(), true);
			}

			startResearch.setDisabled(!selected.containsValue(true, true));
			clickElement(null);
			updateRightArea();
		}

		@Override
		protected void clickElement(Button btn) {
			add.setText(selected.get(active.getType(), false) ? "Drop area " + active.getName() + " from new research"
					: "Add area " + active.getName() + " to new research");
			add.setDisabled(false);
		}

		@Override
		protected Actor getInfoPanel(BaseElement ress) {
			YTable table = new YTable();

			if (!selected.containsValue(true, true)) {
				// nothing selected?
				table.addL(null, "Add the areas first.");
			} else {
				// show selected
				table.addH("Research areas");
				for (String key : selected.keys()) {
					BaseElement r = S.nData().getE(key);
					table.addI(r.getIcon(), r.getName());
				}
			}

			if (S.actPlayer().getResearch().getCurrentElements() != null) {
				// show old
				table.addH("Actuell research");
				for (String key : S.actPlayer().getResearch().getCurrentElements()) {
					BaseElement r = S.nData().getE(key);
					table.addI(r.getIcon(), r.getName());
				}

				if (NSettings.isDebug()) {
					table.addL("Cost", S.actPlayer().getResearch().getCurrentCost() + "x left");
				}
			}

			return table;
		}

		/**
		 * Get the default panel, if nothing clicked
		 */
		@Override
		protected Actor getDefaultPanel() {
			return getInfoPanel(null);
		}
	}

	public class OldResearchTab extends YSplitTab<BaseResearch> {

		public OldResearchTab() {
			super("Old researches", "At the moment you have nothing discovered");

			for (String type : S.actPlayer().getNation().getResearch()) {
				if (S.actPlayer().getResearch().hasFinishResearch(type)) {
					addElement(S.nData().getRS(type));
				}

			}

		}

		/**
		 * Place the selected artwork
		 */
		@Override
		protected void doubleClickElement(Button btn) {}
	}
}

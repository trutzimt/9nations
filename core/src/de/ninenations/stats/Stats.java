/**
 * 
 */
package de.ninenations.stats;

import java.io.Serializable;

import de.ninenations.core.NArray;
import de.ninenations.game.NRound.NDaytime;
import de.ninenations.game.NRound.NSeason;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.elements.YTable;

/**
 * @author sven
 *
 */
public class Stats implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8993266537488792030L;
	public static final int POINTS = 0, WEALTH = 1, GOLD = 2, RESEARCH = 3;
	private static final int MAX = 4;

	private NArray<Integer>[] stat;
	private int round;

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public Stats() {
		stat = new NArray[MAX];

		for (int i = 0; i < MAX; i++) {
			stat[i] = new NArray<>();
		}
	}

	/**
	 * Add the round
	 */
	public void nextRound() {
		round++;
	}

	/**
	 * Get the stats for this day
	 * 
	 * @param key
	 * @return
	 */
	public int get(int key) {
		return stat[key].size <= round ? 0 : stat[key].get(round);
	}

	/**
	 * Get the stats for this day
	 * 
	 * @param key
	 * @return
	 */
	public int getTotal(int key) {
		int t = 0;
		for (int i = 0; i <= round; i++) {
			t += get(key);
		}
		return t;
	}

	/**
	 * Get the stats for yesterday, if not exist return -1
	 * 
	 * @param key
	 * @return
	 */
	public int getYesterday(int key) {
		return get(key, round - 1);
	}

	/**
	 * Get the stats for this special day, if not exist return -1
	 * 
	 * @param key
	 * @return
	 */
	public int get(int key, int round) {
		return round < 0 || round > this.round ? -1 : round >= stat[key].size ? 0 : stat[key].get(round);
	}

	/**
	 * Add a special value for the day
	 * 
	 * @param key
	 * @param value
	 */
	public void add(int key, int value) {
		int old = stat[key].size <= round ? 0 : stat[key].get(round);
		if (old == 0) {
			stat[key].add(value);
		} else {
			stat[key].set(round, old + value);
		}
	}

	public YTable getStatsTable(int key) {
		YTable table = new YTable();

		// build header
		table.add("Year");
		for (NDaytime n : NDaytime.values()) {
			table.add(n.get());
		}
		table.row();
		table.add("Year 1").colspan(4).row();
		table.add(NSeason.values()[0].get());

		int s = 0;
		// add points
		for (int i = 0, l = MapScreen.get().getData().getRound().getRoundRaw(); i <= l; i++) {
			if (i == l) {
				table.add("?");
				continue;
			}

			table.add(Integer.toString(get(key, i)));
			if (i % 3 == 2) {
				table.row();

				if (s == 3) {
					table.add("Year " + (i / 12 + 2)).colspan(4).row();
					s = 0;
				}
				table.add(NSeason.values()[s].get());
				s++;
			}
		}
		return table.createScrollPaneInTable();
	}

}

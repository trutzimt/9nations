/**
 * 
 */
package de.ninenations.stats;

import com.badlogic.gdx.scenes.scene2d.ui.Button;

import de.ninenations.core.NN;
import de.ninenations.ui.elements.YSplitTab;
import de.ninenations.ui.elements.YTextButton;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class AchievementTab extends YSplitTab<Achievement> {

	/**
	 * @param title
	 * @param error
	 */
	public AchievementTab() {
		super("Achievements", "You have no achievments");

		for (Achievement a : NN.get().getAchievements().getAchievements()) {
			if (a.isFinish() || !a.isHide()) {
				addElement(a);
			}
		}

		if (NSettings.isDebug()) {
			YTextButton dis = new YTextButton("Change status", true) {

				@Override
				public void perform() {
					active.setFinish(!active.isFinish());

				}
			};
			buttonBar.addActor(dis);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.ui.elements.YSplitTab#doubleClickElement(com.badlogic.gdx.
	 * scenes.scene2d.ui.Button)
	 */
	@Override
	protected void doubleClickElement(Button btn) {}

}

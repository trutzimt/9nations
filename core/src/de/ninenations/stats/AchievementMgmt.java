/**
 * 
 */
package de.ninenations.stats;

import com.badlogic.gdx.utils.Array;

import de.ninenations.actions.req.ReqResearchCount;
import de.ninenations.actions.req.ReqRess;
import de.ninenations.actions.req.ReqScenarioFinish;
import de.ninenations.actions.req.ReqSpecial;
import de.ninenations.actions.req.ReqTown;
import de.ninenations.actions.req.ReqTownLevelExact;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.game.S;
import de.ninenations.player.Player;
import de.ninenations.stats.gameservice.BaseGameService;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.window.YNotificationSaver;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class AchievementMgmt {

	private Array<Achievement> achievements;
	private Array<BaseGameService> gameService;
	private Array<String[]> highscore;

	/**
	 * 
	 */
	public AchievementMgmt() {
		achievements = new Array<>();

		addAchievements();

		gameService = new Array<>();
		highscore = new Array<>();

		// load it?
		if (NSettings.getPref().contains("points")) {
			String[] data = NSettings.getPref().getString("points").split(";");
			for (int i = 0, l = data.length / 3; i < l; i++) {
				highscore.add(new String[] { data[i * 3], data[i * 3 + 1], data[i * 3 + 2] });
			}
		}

	}

	/**
	 * 
	 */
	private void addAchievements() {
		achievements.add(new Achievement("More than a kingdom", "9towns", YIcons.TOWN).addReq(new ReqTown(false, 9)));
		achievements.add(new Achievement("One Thousand and One", "1001gold", YIcons.GOLD).addReq(new ReqRess(false, BaseRess.GOLD, 1001, true)));
		achievements.add(new Achievement("Answer to life", "42research", YIcons.RESEARCH).addReq(new ReqResearchCount(false, 42)));
		achievements.add(
				new Achievement("I read the manual", "finishtut", 189).addReq(new ReqScenarioFinish("tut", "1")).addReq(new ReqScenarioFinish("tut", "2")));
		achievements.add(new Achievement("All my towns", "4towns", YIcons.TOWN).addReq(new ReqTown(false, 4)).addReq(new ReqTownLevelExact(false, 1, 1))
				.addReq(new ReqTownLevelExact(false, 2, 1)).addReq(new ReqTownLevelExact(false, 3, 1)).addReq(new ReqTownLevelExact(false, 4, 1)));

		achievements.add(new Achievement("Ups, i did it again", "destroytown", YIcons.TOWN).addReq(new ReqSpecial("Destroy a town")).setHide(true));
	}

	/**
	 * Update the quest
	 * 
	 * @return true > finish, false otherwise
	 */
	public void check(Player player) {
		for (Achievement a : achievements) {
			if (a.check(player)) {

				// inform game services
				for (BaseGameService bgs : gameService) {
					bgs.unlockAchievement(a.getID());
				}
			}
		}
	}

	/**
	 * Submit a new point score
	 * 
	 * @param scen
	 * @param highscore
	 * @param win
	 */
	public void addPoints(String scen, int point, boolean win) {
		String[] d = { scen, Integer.toString(point), win ? "Win" : "Lose" };
		// find place
		for (int i = 0, l = highscore.size; i < l; i++) {
			if (point > Integer.parseInt(highscore.get(i)[1])) {
				highscore.insert(i, d);
				d = null;

				// inform user
				S.actPlayer().addInfo(new YNotificationSaver("New Highscore: " + (i + 1) + ". place: " + point, YIcons.ACHIEVEMENT));

				break;
			}
		}

		// add an last position?
		if (d != null && highscore.size < 10) {
			S.actPlayer().addInfo(new YNotificationSaver("New Highscore: " + (highscore.size + 1) + ". place: " + point, YIcons.ACHIEVEMENT));
			highscore.add(d);
		}

		// remove last?
		if (highscore.size > 10) {
			highscore.removeIndex(10);
		}

		save();

		// inform game services
		for (BaseGameService bgs : gameService) {
			bgs.addHighscore(scen, point);
		}
	}

	public YTable getHighscoreTable() {
		YTable table = new YTable();

		// has nothing?
		if (highscore.size == 0) {
			table.add("You has no highscore");
			return table;
		}

		// add header
		table.add("Points");
		table.add("Scenario");
		table.add("End").row();

		// add data
		for (String[] d : highscore) {
			table.add(d[1]);
			table.add(d[0]);
			table.add(d[2]).row();
		}

		return table;
	}

	/**
	 * @return the achievemehts
	 */
	public Achievement getAchievement(String key) {
		for (Achievement a : achievements) {
			if (a.getID().equals(key)) {
				return a;
			}
		}
		return null;
	}

	/**
	 * @return the achievemehts
	 */
	public Array<Achievement> getAchievements() {
		return achievements;
	}

	/**
	 * Save the points
	 */
	public void save() {
		String erg = "";
		for (String[] d : highscore) {
			if (erg.length() > 0) {
				erg += ";";
			}

			erg += d[0] + ";" + d[1] + ";" + d[2];
		}

		NSettings.getPref().putString("points", erg);
	}

	/**
	 * @return the gameService
	 */
	public Array<BaseGameService> getGameService() {
		return gameService;
	}

	/**
	 * @param gameService
	 *            the gameService to set
	 */
	public void addGameService(BaseGameService gameService) {
		this.gameService.add(gameService);
	}
}

/**
 * 
 */
package de.ninenations.stats.gameservice;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.util.dialog.Dialogs.OptionDialogType;
import com.kotcrab.vis.ui.util.dialog.OptionDialogListener;
import com.kotcrab.vis.ui.widget.VisCheckBox;

import de.golfgl.gdxgamesvcs.IGameServiceClient;
import de.golfgl.gdxgamesvcs.IGameServiceListener;
import de.ninenations.core.NN;
import de.ninenations.ui.IDisplay;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public abstract class BaseGameService implements IDisplay, IGameServiceListener {

	protected IGameServiceClient igsc;
	protected boolean active, achievement, highscore, autoLogin;
	protected String id, leaderboardid;

	/**
	 * 
	 */
	public BaseGameService(String id) {
		this.id = id;

		active = NSettings.getPref().getBoolean(getBaseKey() + "active", false);
		achievement = NSettings.getPref().getBoolean(getBaseKey() + "achievement", true);
		highscore = NSettings.getPref().getBoolean(getBaseKey() + "highscore", true);
		autoLogin = NSettings.getPref().getBoolean(getBaseKey() + "autoLogin", true);

	}

	protected String getBaseKey() {
		return "gs." + id + ".";
	}

	protected abstract void init();

	protected abstract void logoff();

	/**
	 * Add points to leader board
	 * 
	 * @param scen
	 * @param points
	 * @return
	 */
	public boolean addHighscore(String scen, long points) {
		if (igsc == null || !highscore) {
			return false;
		}

		return igsc.submitToLeaderboard(leaderboardid, points, scen);
	}

	/**
	 * unlock Achievement
	 * 
	 * @param scen
	 * @param points
	 * @return
	 */
	public boolean unlockAchievement(String ach) {
		if (igsc == null || !achievement) {
			return false;
		}
		return igsc.unlockAchievement(ach);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getInfoPanel()
	 */
	@Override
	public YTable getInfoPanel() {
		YTable r = new YTable();
		final YTable t = new YTable();
		t.setVisible(active);

		VisCheckBox c = new VisCheckBox("Active " + getName(), active);
		c.addCaptureListener(new YChangeListener(true) {

			@Override
			public void changedY(final Actor actor) {
				NSettings.getPref().putBoolean(getBaseKey() + "active", ((VisCheckBox) actor).isChecked());

				if (((VisCheckBox) actor).isChecked()) {
					Dialogs.showOptionDialog(NN.screen().getStage(), "Do you want to enable game service " + getName() + "?",
							"Please note, that you need an account on " + getName() + " and " + NSettings.NAME
									+ " will send some data \n (mostly profil data, session length, highscore and achievements) to " + getName()
									+ ".\n All Data (like passwords) will be saved local unencrypted.",
							OptionDialogType.YES_NO, new OptionDialogListener() {

								@Override
								public void yes() {
									active = true;
									init();
									t.setVisible(true);
								}

								@Override
								public void no() {
									((VisCheckBox) actor).setChecked(false);
								}

								@Override
								public void cancel() {}
							});
				} else {
					t.setVisible(false);
					logoff();
				}
			}
		});
		r.addL(null, c);
		buildConfigTable(t);

		t.addH("Options");
		c = new VisCheckBox("Login at game start", autoLogin);
		c.addCaptureListener(new YChangeListener(true) {

			@Override
			public void changedY(Actor actor) {
				autoLogin = ((VisCheckBox) actor).isChecked();
				NSettings.getPref().putBoolean(getBaseKey() + "autoLogin", ((VisCheckBox) actor).isChecked());
			}
		});
		t.addL(null, c);
		c = new VisCheckBox("Send achievements", achievement);
		c.addCaptureListener(new YChangeListener(true) {

			@Override
			public void changedY(Actor actor) {
				achievement = ((VisCheckBox) actor).isChecked();
				NSettings.getPref().putBoolean(getBaseKey() + "achievement", ((VisCheckBox) actor).isChecked());
			}
		});
		t.addL(null, c);
		c = new VisCheckBox("Send highscore", highscore);
		c.addCaptureListener(new YChangeListener(true) {

			@Override
			public void changedY(Actor actor) {
				highscore = ((VisCheckBox) actor).isChecked();
				NSettings.getPref().putBoolean(getBaseKey() + "highscore", ((VisCheckBox) actor).isChecked());
			}
		});
		t.addL(null, c);

		r.add(t).colspan(2).grow();

		return r;
	}

	protected abstract void buildConfigTable(YTable table);

}

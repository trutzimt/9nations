/**
 * 
 */
package de.ninenations.stats.gameservice;

import com.badlogic.gdx.scenes.scene2d.ui.Button;

import de.ninenations.core.NN;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.NSplitTab;

/**
 * @author sven
 *
 */
public class GameServiceTab extends NSplitTab<BaseGameService> {

	public GameServiceTab() {
		super("achievement", "Game Services", YIcons.ACHIEVEMENT, "Their are no game services for your plattform at the moment.");

		for (BaseGameService bgs : NN.get().getAchievements().getGameService()) {
			addElement(bgs);
		}
	}

	@Override
	protected void doubleClickElement(Button btn) {
		// TODO Auto-generated method stub

	}

}

/**
 * 
 */
package de.ninenations.stats;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.ninenations.core.NN;
import de.ninenations.game.S;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.window.YTabWindow;

/**
 * @author sven
 *
 */
public class StatsWindow extends YTabWindow {

	/**
	 * @param name
	 */
	public StatsWindow() {
		super("Statistics");

		tabbedPane.add(new AchievementTab());
		tabbedPane.add(new HighscoreTab());

		if (NN.screen() instanceof MapScreen) {
			tabbedPane.add(new PlayerTab("Points", Stats.POINTS));
			tabbedPane.add(new PlayerTab("Researches", Stats.RESEARCH));
			tabbedPane.add(new StatTownTab("Wealth", Stats.WEALTH));
			tabbedPane.add(new StatTownTab("Gold", Stats.GOLD));
		}

		addTitleIcon(YIcons.getIconI(YIcons.ACHIEVEMENT));

		buildIt();

	}

	class PlayerTab extends Tab {

		private String name;
		private int id;

		public PlayerTab(String name, int id) {
			super(false, false);

			this.name = name;
			this.id = id;
		}

		@Override
		public String getTabTitle() {
			return name;
		}

		@Override
		public Table getContentTable() {
			return S.actPlayer().getStats().getStatsTable(id);
		}
	}
}

/**
 * 
 */
package de.ninenations.stats;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.core.NArray;
import de.ninenations.game.S;
import de.ninenations.player.Player;
import de.ninenations.ui.IDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.window.YNotificationSaver;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class Achievement implements IDisplay, Serializable {

	private static final long serialVersionUID = -310899873495116782L;

	private NArray<BaseReq> req;
	private String title, id;
	private int icon;
	private boolean finish, hide;

	/** 
	 * 
	 */
	public Achievement(String title, String id, int icon) {
		req = new NArray<>();

		this.title = title;
		this.id = id;
		this.icon = icon;

		finish = NSettings.getPref().getBoolean(getKey(), false);
	}

	/**
	 * Update the quest
	 * 
	 * @return true > finish, false otherwise
	 */
	public boolean check(Player player) {
		if (finish) {
			return false;
		}

		if (!checkReq(player)) {
			return false;
		}

		// inform player
		player.addInfo(new YNotificationSaver("Got achievement: " + getName(), getIcon(), YIcons.QUEST));
		setFinish(true);

		return true;
	}

	/**
	 * Check the req
	 * 
	 * @return true, will pass
	 */
	public boolean checkReq(Player player) {
		// ask the items
		for (BaseReq r : req) {
			if (!r.checkReq(player, null, -1, -1)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Generate a info panel
	 * 
	 * @return
	 */
	@Override
	public YTable getInfoPanel() {

		YTable table = new YTable();

		table.addL("Title", title);
		table.addL("Won", Boolean.toString(finish));

		table.addH("Tasks");
		if (S.activeGame()) {
			for (BaseReq r : req) {
				table.addI(YIcons.getIconI(r.checkReq(S.actPlayer(), null, -1, -1) ? YIcons.RIGHT : YIcons.WRONG), r.getDesc(S.actPlayer(), null, -1, -1));
			}
		} else {
			table.addL(null, "Please start a game to see the tasks.");
		}

		return table;
	}

	/**
	 * @return the req
	 */
	public Achievement addReq(BaseReq e) {
		req.add(e);
		return this;
	}

	/**
	 * @return the icon
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(icon);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getName()
	 */
	@Override
	public String getName() {
		return (finish ? "(v) " : "") + title;
	}

	public String getKey() {
		return "achievment." + id;
	}

	public String getID() {
		return id;
	}

	/**
	 * @return the finish
	 */
	public boolean isFinish() {
		return finish;
	}

	/**
	 * @param finish
	 *            the finish to set
	 */
	public void setFinish(boolean finish) {
		this.finish = finish;
		NSettings.getPref().putBoolean(getKey(), finish);
		NSettings.save();
	}

	/**
	 * @return the hide
	 */
	public boolean isHide() {
		return hide;
	}

	/**
	 * @param hide
	 *            the hide to set
	 */
	public Achievement setHide(boolean hide) {
		this.hide = hide;

		return this;
	}

	/**
	 * @return the req
	 */
	public NArray<BaseReq> getReq() {
		return req;
	}

}

package de.ninenations.stats;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.game.S;
import de.ninenations.stats.StatTownTab.StatInfo;
import de.ninenations.towns.Town;
import de.ninenations.ui.IDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YSplitTab;
import de.ninenations.ui.elements.YTable;

public class StatTownTab extends YSplitTab<StatInfo> {

	// private NObjectMap<BaseDisplay> display;

	private int id;

	public StatTownTab(String name, int id) {
		super(name, "You have no towns");

		this.id = id;

		// add all towns
		for (Town t : S.town().getTownsByPlayer(S.actPlayer())) {
			addElement(new StatInfo(t));
		}
	}

	@Override
	protected void doubleClickElement(Button btn) {}

	class StatInfo implements IDisplay {

		Town town;

		StatInfo(Town town) {
			this.town = town;
		}

		@Override
		public Image getIcon() {
			return YIcons.getIconI(YIcons.TOWN);
		}

		@Override
		public String getName() {
			return town.getName();
		}

		@Override
		public YTable getInfoPanel() {
			return town.getStats().getStatsTable(id);
		}

	}
}
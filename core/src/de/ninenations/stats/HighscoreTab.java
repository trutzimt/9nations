/**
 * 
 */
package de.ninenations.stats;

import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.tabbedpane.Tab;

import de.ninenations.core.NN;

/**
 * @author sven
 *
 */
public class HighscoreTab extends Tab {

	/**
	 * @param title
	 * @param error
	 */
	public HighscoreTab() {
		super(false, false);
	}

	@Override
	public String getTabTitle() {
		return "Highscore";
	}

	@Override
	public Table getContentTable() {
		return NN.get().getAchievements().getHighscoreTable();
	}

}

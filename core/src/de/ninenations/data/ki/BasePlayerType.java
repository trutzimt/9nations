/**
 *
 */
package de.ninenations.data.ki;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.player.Player;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;

/**
 * @author sven
 *
 */
public abstract class BasePlayerType extends BaseDisplay implements Serializable {

	private static final long serialVersionUID = 2537703743859302320L;

	/**
	 * Is it a ki or a player?
	 */
	protected boolean human;
	/**
	 * Has this ki a real economy?
	 */
	protected boolean economy;
	/**
	 * Can be choiced to play?
	 */
	protected boolean selectable;

	@SuppressWarnings("unused")
	private BasePlayerType() {
		super();
	}

	/**
	 * @param human
	 * @param economy
	 */
	public BasePlayerType(String type, String name, boolean human, boolean economy) {
		super(type, name);
		this.human = human;
		this.economy = economy;

		selectable = true;
	}

	/**
	 * Run the KI
	 */
	public abstract void run(Player player);

	/**
	 * @return the human
	 */
	public boolean isHuman() {
		return human;
	}

	/**
	 * @return the economy
	 */
	public boolean isEconomy() {
		return economy;
	}

	@Override
	public Image getIcon() {
		// TODO Auto-generated method stub
		return YIcons.getIconI(YIcons.WARNING);
	}

	@Override
	public YTable getInfoPanel() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @return the selectable
	 */
	public boolean isSelectable() {
		return selectable;
	}

}

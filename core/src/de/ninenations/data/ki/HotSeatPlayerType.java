/**
 *
 */
package de.ninenations.data.ki;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.UiHelper;

/**
 * @author sven
 *
 */
public class HotSeatPlayerType extends BasePlayerType {
	private static final long serialVersionUID = 1L;

	public HotSeatPlayerType() {
		super("hotseat", "Hotseat player", true, true);

		selectable = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see at.trutz.qossire.player.ki.BaseKI#run()
	 */
	@Override
	public void run(Player player) {
		// show it
		final Image black = UiHelper.createBlackImg(false);
		MapScreen.get().getStage().addActor(black);

		Dialogs.showOKDialog(MapScreen.get().getStage(), "Hotseat", "Now is " + player.getName() + " playing.").addListener(new ChangeListener() {

			@Override
			public void changed(ChangeEvent event, Actor actor) {
				black.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));

			}
		});
	}

}

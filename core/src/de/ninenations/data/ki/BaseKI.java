/**
 *
 */
package de.ninenations.data.ki;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.kotcrab.vis.ui.widget.VisLabel;

import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.UiHelper;

/**
 * KI does not nothing. It is for holding the neutral units
 *
 * @author sven
 *
 */
public abstract class BaseKI extends BasePlayerType {
	private static final long serialVersionUID = 1L;

	public BaseKI(String type, String name, boolean economy) {
		super(type, name, false, economy);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see at.trutz.qossire.player.ki.BaseKI#run()
	 */
	@Override
	public void run(Player player) {
		// show it
		Image black = UiHelper.createBlackImg(false);
		MapScreen.get().getStage().addActor(black);

		VisLabel label = new VisLabel("Player " + player.getName() + " is playing.");
		label.setFillParent(true);
		label.setAlignment(Align.center);
		MapScreen.get().getStage().addActor(label);

		// perform
		runKI(player);

		// remove it
		black.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
		label.addAction(Actions.sequence(Actions.fadeOut(1), Actions.removeActor()));
	}

	protected abstract void runKI(Player player);

}

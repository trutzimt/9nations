/**
 *
 */
package de.ninenations.data.ki;

import de.ninenations.player.Player;

/**
 * KI does not nothing. It is for holding the neutral units
 *
 * @author sven
 *
 */
public class NatureKI extends BaseKI {
	private static final long serialVersionUID = 1L;

	public NatureKI() {
		super("nature", "Nature KI", false);
		selectable = false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see at.trutz.qossire.player.ki.BaseKI#run()
	 */
	@Override
	protected void runKI(Player player) {}

}

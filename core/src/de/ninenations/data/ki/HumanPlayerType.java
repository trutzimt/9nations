/**
 *
 */
package de.ninenations.data.ki;

import de.ninenations.player.Player;

/**
 * @author sven
 *
 */
public class HumanPlayerType extends BasePlayerType {
	private static final long serialVersionUID = 1L;

	public HumanPlayerType() {
		super("human", "Human player", true, true);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see at.trutz.qossire.player.ki.BaseKI#run()
	 */
	@Override
	public void run(Player player) {}

}

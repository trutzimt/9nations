/**
 * 
 */
package de.ninenations.data.elements;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;

/**
 * @author sven
 *
 */
public class BaseElement extends BaseDisplay implements Serializable {

	private static final long serialVersionUID = -5506508892461477369L;

	// Feuer, Wasser, Luft, Erde, Metall, Holz, Leben, Tod, Energie
	public static final String FIRE = "fire", WATER = "water", AIR = "air", EARTH = "earth", METAL = "metal", WOOD = "wood", LIFE = "life", DEATH = "death",
			ENERGY = "energy";

	private int icon;

	@SuppressWarnings("unused")
	private BaseElement() {}

	/**
	 * Important for research
	 * 
	 * public BaseElement() { super(); }
	 * 
	 * /**
	 * 
	 * @param type
	 * @param name
	 */
	public BaseElement(String type, String name, int icon) {
		super(type, name);

		this.icon = icon;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getInfoPanel()
	 */
	@Override
	public YTable getInfoPanel() {
		YTable t = new YTable();
		t.addL("Name", name);
		return t;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(icon);
	}

}

package de.ninenations.data.buildingunitbase;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

import de.ninenations.actions.action.ActionDestroy;
import de.ninenations.actions.action.ActionSleep;
import de.ninenations.actions.base.BaseReq;
import de.ninenations.actions.base.GAction;
import de.ninenations.actions.base.IReq;
import de.ninenations.actions.req.ReqResearch;
import de.ninenations.core.IMapObject;
import de.ninenations.core.NArray;
import de.ninenations.data.building.DataBuilding;
import de.ninenations.data.research.DataObjectResearch;
import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.player.Player;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.NRessLabel;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.YLog;

/**
 * 
 */
public abstract class DataObject extends BaseDisplay implements IMapObject, Serializable, IReq<DataObject> {

	private static final long serialVersionUID = 1981267916501838872L;

	protected NCat category;

	/**
	 * 
	 */
	protected int atk;

	/**
	 * 
	 */
	protected int def;

	/**
	 * 
	 */
	protected int damMin, damMax;

	/**
	 * 
	 */
	protected int hp;

	/**
	 * 
	 */
	protected int visibleRange;

	/**
	 * 
	 */
	protected int ap;

	/**
	 * How long it need to build?
	 */
	protected int buildTime;
	protected ObjectMap<String, Integer> cost;

	/**
	 * Req to build it
	 */
	protected NArray<BaseReq> reqs;

	/**
	 * 
	 */
	protected NArray<GAction> activeActions;

	/**
	 * 
	 */
	protected NArray<GAction> passiveActions;

	public enum NCat {
		FOOD("Food", YIcons.FOOD), EXPLO("Exploration", YIcons.NEXTUNIT), NEEDS("Needs", YIcons.WEALTH), PROD("Production", YIcons.MAINMENU),
		MOVE("Movement", YIcons.WALK), WAR("War", YIcons.ATTACK), DECO("Decoration", 627), GENERAL("General", YIcons.LOGO);

		private final String text;
		private final int icon;

		private NCat(String text, int icon) {
			this.text = text;
			this.icon = icon;
		}

		/**
		 * @return the value
		 */
		public String get() {
			return text;
		}

		/**
		 * @return the icon
		 */
		public int getIcon() {
			return icon;
		}
	}

	public static final int TOP = 1;
	public static final int LEFT = 2;
	public static final int RIGHT = 4;
	public static final int DOWN = 8;

	@SuppressWarnings("unused")
	protected DataObject() {
		super();
	}

	/**
	 * Default constructor
	 */
	public DataObject(String type, String name, NCat category) {
		super(type, name);

		this.category = category;

		activeActions = new NArray<>();
		passiveActions = new NArray<>();
		reqs = new NArray<>();

		buildTime = 1;
		cost = new ObjectMap<>();

		visibleRange = 1;
		hp = 10;
		ap = 5;

		activeActions.add(new ActionDestroy());
		activeActions.add(new ActionSleep());
	}

	/**
	 * Add a new req
	 * 
	 * @param req
	 * @return
	 */
	@Override
	public DataObject addReq(BaseReq req) {
		reqs.add(req);
		return this;
	}

	/**
	 * Check if it can build here
	 * 
	 * @param player
	 * @param onMap
	 * @param x
	 * @param y
	 * @return null=can build, otherwise errormessage
	 */
	public String check(Player player, NOnMapObject onMap, int x, int y) {
		for (BaseReq r : reqs) {
			if (!r.checkReq(player, onMap, x, y)) {
				return r.getDesc(player, onMap, x, y);
			}
		}

		return null;
	}

	/**
	 * Check if it can not build it final here
	 * 
	 * @param player
	 * @param onMap
	 * @param x
	 * @param y
	 * @return null=can build, otherwise errormessage
	 */
	public String checkFinal(Player player, NOnMapObject onMap, int x, int y) {
		for (BaseReq r : reqs) {
			if (r.isFinal() && !r.checkReq(player, onMap, x, y)) {
				return r.getDesc(player, onMap, x, y);
			}
		}

		return null;
	}

	/**
	 * @return the atk
	 */
	public int getAtk() {
		return atk;
	}

	/**
	 * @return the def
	 */
	public int getDef() {
		return def;
	}

	/**
	 * @return the ap
	 */
	public int getAp() {
		return ap;
	}

	/**
	 * @return the activeActions
	 */
	public Array<GAction> getActiveActions() {
		return activeActions;
	}

	/**
	 * @return the passiveActions
	 */
	public Array<GAction> getPassiveActions() {
		return passiveActions;
	}

	/**
	 * @return the hp
	 */
	public int getHp() {
		return hp;
	}

	/**
	 * @return the visibleRange
	 */
	public int getVisibleRange() {
		return visibleRange;
	}

	/**
	 * @return the category
	 */
	public NCat getCategory() {
		return category;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	public abstract Image getIconO();

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getInfoPanel()
	 */
	@Override
	public YTable getInfoPanel() {
		YTable table = super.getInfoPanel();
		table.addL("Category", category.get());
		table.addL("Range", "View " + visibleRange + " fields");
		if (atk != 0 || def != 0) {
			table.addL("ATK/DEF", atk + "/" + def);
			table.addL("Damage", damMin + "-" + damMax);
		}
		addDetailPanel(table);

		addActionPanel(table, null, null, -1, -1);

		return table;
	}

	/**
	 * Add hp/ap details
	 * 
	 * @param table
	 */
	protected void addDetailPanel(YTable table) {
		table.addL("HP/AP", hp + "/" + ap);
	}

	/**
	 * Add action overview
	 * 
	 * @param table
	 * @param player
	 * @param onMap
	 * @param x
	 * @param y
	 */
	protected void addActionPanel(YTable table, Player player, NOnMapObject onMap, int x, int y) {
		if (buildTime > 0 || cost.size > 0) {
			table.addH("Cost");
			if (buildTime >= 1) {
				table.addL("Time", buildTime + " rounds");
			}
			table.addL("Cost", new NRessLabel(NRessLabel.generateRessLabel(cost)));
		}

		// add actions
		if (reqs.size > 0) {
			table.addH("Requirements");
			for (BaseReq r : reqs) {
				if (player == null) {
					table.addI(null, r.getDesc());
				} else {
					r.addToTable(table, player, onMap, x, y);
				}
			}
		}

		// add actions
		if (activeActions.size > 0) {
			table.addH("Active Actions");
			for (GAction g : activeActions) {
				if (!g.isHiddenLexicon()) {
					table.addI(g.getIcon(), g.getName());
				}
			}
		}

		// add actions
		if (passiveActions.size > 0) {
			table.addH("Passive Actions");
			for (GAction g : passiveActions) {
				if (!g.isHiddenLexicon()) {
					table.addI(g.getIcon(), g.getName());
				}
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getInfoPanel()
	 */
	public YTable getInfoPanel(Player player, NOnMapObject onMap, int x, int y) {
		YTable table = new YTable();
		table.addL("Name", name);
		table.addL("Range", "View " + (visibleRange + S.map().getTerrain(x, y).getVisibleRange()) + " fields");
		if (atk != 0 || def != 0) {
			table.addL("ATK/DEF", atk + "/" + def);
		}
		addDetailPanel(table);

		addActionPanel(table, player, onMap, x, y);

		return table;
	}

	/**
	 * @return the cost
	 */
	public ObjectMap<String, Integer> getCost() {
		return cost;
	}

	/**
	 * @return the buildTime
	 */
	public int getBuildTime() {
		return buildTime;
	}

	/**
	 * Add a research req
	 * 
	 * @param level
	 * @param ress
	 */
	public void addResearch(int level, String... ress) {
		// Skip it, game is not finish loaded
		if (MapScreen.get() == null) {
			return;
		}

		// enabled?
		if (!S.isActive(ScenConf.RESEARCH)) {
			return;
		}

		// exist? skip it
		if (S.nData().existRS(type)) {
			return;
		}

		DataObjectResearch r = new DataObjectResearch(this, this instanceof DataBuilding ? EMapData.BUILDING : EMapData.UNIT, level, ress);
		S.nData().add(r);

		reqs.insert(0, new ReqResearch(r.getType()));
	}

	/**
	 * @param hp
	 *            the hp to set
	 */
	public void setHp(int hp) {
		this.hp = hp;
	}

	/**
	 * @param ap
	 *            the ap to set
	 */
	public void setAp(int ap) {
		this.ap = ap;
	}

	/**
	 * @return the reqs
	 */
	public NArray<BaseReq> getReqs() {
		return reqs;
	}

	/**
	 * @param category
	 *            the category to set
	 */
	public void setCategory(NCat category) {
		this.category = category;
	}

	/**
	 * @param visibleRange
	 *            the visibleRange to set
	 */
	public void setVisibleRange(int visibleRange) {
		this.visibleRange = visibleRange;
	}

	/**
	 * @param buildTime
	 *            the buildTime to set
	 */
	public void setBuildTime(int buildTime) {
		this.buildTime = buildTime;
	}

	/**
	 * @param atk
	 *            the atk to set
	 */
	public void setAtk(int atk) {
		this.atk = atk;
	}

	/**
	 * @param def
	 *            the def to set
	 */
	public void setDef(int def) {
		this.def = def;
	}

	/**
	 * Check if this object has some errors
	 */
	@Override
	public void validate() {
		super.validate();

		for (BaseReq q : reqs) {
			if (q == null) {
				YLog.log("Validate", type, "null reqs");
			}
		}

		for (GAction a : activeActions) {
			if (a == null) {
				YLog.log("Validate", type, "null activeActions");
			}
		}

		for (GAction a : passiveActions) {
			if (a == null) {
				YLog.log("Validate", type, "null passiveActions");
			}
		}
	}

	/**
	 * Get the special action, based on the type
	 * 
	 * @param type
	 * @return
	 */
	public GAction getAction(String type) {
		for (GAction a : activeActions) {
			if (a.getType().equals(type)) {
				return a;
			}
		}

		for (GAction a : passiveActions) {
			if (a.getType().equals(type)) {
				return a;
			}
		}

		return null;
	}

	/**
	 * @return the damMin
	 */
	public int getDamMin() {
		return damMin;
	}

	/**
	 * @param damMin
	 *            the damMin to set
	 */
	public void setDamMin(int damMin) {
		this.damMin = damMin;
	}

	/**
	 * @return the damMax
	 */
	public int getDamMax() {
		return damMax;
	}

	/**
	 * @param damMax
	 *            the damMax to set
	 */
	public void setDamMax(int damMax) {
		this.damMax = damMax;
	}
}
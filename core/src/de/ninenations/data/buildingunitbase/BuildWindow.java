/**
 * 
 */
package de.ninenations.data.buildingunitbase;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.kotcrab.vis.ui.util.dialog.Dialogs;

import de.ninenations.core.NN;
import de.ninenations.data.buildingunitbase.DataObject.NCat;
import de.ninenations.game.S;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.NSplitTab;
import de.ninenations.ui.elements.NTextButton;
import de.ninenations.ui.newWindow.NTabWindow;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class BuildWindow extends NTabWindow {

	private NOnMapObject caller;
	private boolean unit;
	private int mX, mY;

	public static final String ID = "build";

	/**
	 * @param name
	 */
	public BuildWindow(boolean include, Array<String> elements, NOnMapObject caller, int x, int y) {
		super(ID, "Build ", YIcons.BUILD);

		mX = x;
		mY = y;

		this.caller = caller;

		// unit build?
		unit = MapScreen.get().getData().getUnit(x, y) == null;

		// its full?
		if (!unit && MapScreen.get().getData().getBuilding(x, y) != null) {
			Dialogs.showOKDialog(NN.screen().getStage(), "No Place", "Here is no space to build a unit or building.");
			return;
		}

		getTitleLabel().setText(unit ? "Train a unit" : "Build a new building");

		// Collect it
		ObjectMap<NCat, Array<DataObject>> objs = new ObjectMap<>();

		Array<String> keys = unit ? S.actPlayer().getNation().getUnits() : S.actPlayer().getNation().getBuildings();
		keys = include ? elements : keys;

		// check every element
		for (String key : keys) {
			// skip?
			if (!include && elements.contains(key, false)) {
				continue;
			}

			DataObject u = unit ? S.nData().getU(key) : S.nData().getB(key);
			// YLog.log(u.getCategory(), u, u.checkFinal(S.actPlayer(), null, x, y));
			if (u.checkFinal(S.actPlayer(), null, x, y) == null) {
				// add it
				if (!objs.containsKey(u.getCategory())) {
					objs.put(u.getCategory(), new Array<DataObject>());
				}
				objs.get(u.getCategory()).add(u);
			}
		}

		addTitleIcon(YIcons.getIconI(unit ? YIcons.TRAIN : YIcons.BUILD));

		// build it
		for (NCat key : objs.keys()) {
			addTab(new BuildTab(key, objs.get(key)));
		}

		// found nothing?
		error = "Nothing to build. Maybe you need to develop it first?";
		pack = false;

		buildIt();
	}

	public class BuildTab extends NSplitTab<DataObject> {

		private NTextButton build;

		public BuildTab(NCat cat, Array<DataObject> obj) {
			super(cat.toString(), cat.get(), cat.getIcon(), "At the moment there is no elements for " + cat.get());

			for (DataObject d : obj) {
				addElement(d);
				elements.get(elements.size - 1).getColor().a = d.check(S.actPlayer(), null, mX, mY) == null ? 1 : 0.5f;
			}

			// add button
			build = new NTextButton("") {

				@Override
				public void perform() {
					build();

				}
			};
			buttonBar.add(build);
			reset();
		}

		/**
		 * Reset the status
		 */
		@Override
		protected void reset() {
			super.reset();
			build.setDisabled(true);
			build.setText(unit ? "Select a unit to train" : "Select a building to build");

		}

		/**
		 * Place the selected artwork
		 */
		@Override
		protected void doubleClickElement(Button btn) {
			if (build.isDisabled()) {
				YSounds.pBuzzer();
				Dialogs.showOKDialog(getStage(), "Error", build.getText().toString());
				return;
			}
			build();
		}

		/**
		 * 
		 * @param btn
		 * @return
		 */
		@Override
		protected Actor getInfoPanel(DataObject o) {
			return o.getInfoPanel(S.actPlayer(), caller, mX, mY);
		}

		/**
		 * Buy the selected artwork
		 */
		protected void build() {
			YSounds.pClick();

			if (unit) {
				MapScreen.get().getData().getOnMap().addU(active.getType(), S.actPlayer(),
						MapScreen.get().getData().getTowns().getNearstTown(S.actPlayer(), mX, mY, false), mX, mY);
			} else {
				MapScreen.get().getData().getOnMap().addB(active.getType(), S.actPlayer(),
						MapScreen.get().getData().getTowns().getNearstTown(S.actPlayer(), mX, mY, false), mX, mY);

			}

			BuildWindow.this.close();
		}

		@Override
		protected void clickElement(Button btn) {// check can build?
			String erg = active.check(S.actPlayer(), null, mX, mY);

			build.setDisabled(erg != null);
			build.setText(erg == null ? (unit ? "Train " : "Build ") + active.getName() : (unit ? "Can not train: " : "Can not build: ") + erg);

			btn.getColor().a = erg == null ? 1 : 0.5f;
		}
	}

}

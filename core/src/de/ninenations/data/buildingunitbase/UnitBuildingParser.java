/**
 * 
 */
package de.ninenations.data.buildingunitbase;

import java.util.Arrays;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

import de.ninenations.actions.action.ActionBuild;
import de.ninenations.actions.action.ActionDegrade;
import de.ninenations.actions.action.ActionDestroyTown;
import de.ninenations.actions.action.ActionDisEmbark;
import de.ninenations.actions.action.ActionEmbark;
import de.ninenations.actions.action.ActionEnablePlayerFeature;
import de.ninenations.actions.action.ActionFoundTown;
import de.ninenations.actions.action.ActionGift;
import de.ninenations.actions.action.ActionProduce;
import de.ninenations.actions.action.ActionProduceRandom;
import de.ninenations.actions.action.ActionSendRessource;
import de.ninenations.actions.action.ActionShop;
import de.ninenations.actions.action.ActionTerrainRemove;
import de.ninenations.actions.action.ActionTerrainSet;
import de.ninenations.actions.action.ActionTrain;
import de.ninenations.actions.action.ActionUpgrade;
import de.ninenations.actions.action.ActionUpgradeBuildingLimit;
import de.ninenations.actions.action.ActionUpgradeRangeLimit;
import de.ninenations.actions.action.ActionUpgradeStorage;
import de.ninenations.actions.action.ActionUpgradeTownLevel;
import de.ninenations.actions.action.ActionUpgradeTownLimit;
import de.ninenations.actions.base.GAction;
import de.ninenations.actions.req.ReqAp;
import de.ninenations.actions.req.ReqNearTown;
import de.ninenations.actions.req.ReqOnlyUpgrade;
import de.ninenations.actions.req.ReqRess;
import de.ninenations.actions.req.ReqTerrain;
import de.ninenations.actions.req.ReqTown;
import de.ninenations.data.NData;
import de.ninenations.data.building.DataBuilding;
import de.ninenations.data.buildingunitbase.DataObject.NCat;
import de.ninenations.data.config.BaseParser;
import de.ninenations.data.nations.BaseNation;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.data.terrain.BaseTerrain;
import de.ninenations.data.terrain.BaseTerrain.NMove;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.unit.DataUnit;
import de.ninenations.player.Feature;
import de.ninenations.util.YLog;

/**
 * @author sven
 *
 */
public class UnitBuildingParser extends BaseParser<DataObject> {

	private ActionProduce produce, produceOnce;
	private Array<String> research;
	private int townLevel;
	private String nation;

	/**
	 * 
	 */
	public UnitBuildingParser(NData nData, FileHandle file) {
		super(nData, file, 2, 2);
	}

	@Override
	protected DataObject registerType(String[][] data, int l) {
		// register type
		if (data[l][0].equals("building")) {
			return building(data[l][1]);
		} else if (data[l][0].equals("unit")) {
			return unit(data[l][1]);
		}
		return null;

	}

	@Override
	protected void unRegisterType(String[][] data, int l) {
		// add research?
		if (research != null) {
			String[] res = new String[research.size];
			for (int i = 0; i < research.size; i++) {
				res[i] = research.get(i);
			}

			active.addResearch(townLevel, res);

			// add research?
			if (nation != null) {
				nData.getN(nation).getResearch().add(active.getType());
			}
		}

		// reset it
		active = null;
		produce = null;
		produceOnce = null;
		research = null;
		townLevel = 0;
		nation = null;

	}

	/**
	 * Run the method from type
	 * 
	 * @param type
	 * @param param
	 */
	@Override
	protected void run(String type, String mparam, String param) {

		switch (type) {
		case "imageid":
			imageid(param);
			return;
		case "nation":
			nation(param);
			return;
		case "terrain":
			terrain(param);
			return;
		case "dam":
			dam(mparam, param);
			return;
		case "cat":
			category(param);
			return;
		case "buildtime":
			buildtime(param);
			return;
		case "visible":
			visible(param);
			return;
		case "cost":
			cost(mparam, param);
			return;
		case "produce":
			produce(mparam, param);
			return;
		case "onceproduce":
			produceOnce(mparam, param);
			return;
		case "research":
			research(mparam);
			return;
		case "action":
			action(mparam, param);
			return;
		case "passiveAction":
			passiveAction(mparam, param);
			return;
		case "upgrade":
			upgrade(param);
			return;
		case "atk":
			atk(param);
			return;
		case "def":
			def(param);
			return;
		case "hp":
			active.setHp(con(param, 10));
			return;
		case "pass":
			pass(mparam, param);
			return;
		case "connected":
			((DataBuilding) active).setConnected(true);
			return;
		case "ap":
			active.setAp(con(param, 5));
			return;
		case "remove":
			remove(mparam, param);
			return;
		case "actionTerrainAdd":
			actionTerrainAdd(mparam, param);
			return;
		case "actionTerrainRemove":
			actionTerrainRemove(mparam, param);
			return;
		case "file":
			file(param);
			return;
		case "movetyp":
			((DataUnit) active).setMoveTyp(NMove.valueOf(param));
			return;
		case "req":
			req(active, mparam, param);
			return;
		default:
			YLog.log("type unknown", type);
		}
	}

	private void dam(String mparam, String param) {
		if (mparam.equals("max")) {
			active.setDamMax(con(param, active.getDamMin()));
		} else {
			active.setDamMin(con(param, 0));
		}

	}

	private void pass(String mparam, String param) {
		String[] d = param.split("#");

		DataBuilding b = (DataBuilding) active;

		if (d[0].equals("public")) {
			b.getPublicCost().put(NMove.valueOf(mparam.toUpperCase()), con(d[1], 5));
		} else {
			b.getOwnerCost().put(NMove.valueOf(mparam.toUpperCase()), con(d[1], 5));
		}

	}

	private void actionTerrainAdd(String mparam, String param) {
		ActionTerrainSet a = new ActionTerrainSet(mparam);

		if (!param.equals("1")) {
			a.addReq(new ReqTerrain(false, param.split(SPLIT)));
		}

		active.getActiveActions().add(a);

	}

	private void actionTerrainRemove(String mparam, String param) {
		active.getActiveActions().add(new ActionTerrainRemove(mparam));

	}

	private void remove(String type, String param) {
		switch (type) {
		case "activeAction":
			for (GAction a : active.getActiveActions()) {
				if (a.getType().equals(param)) {
					active.getActiveActions().removeValue(a, true);
				}
			}
			return;
		case "passiveAction":
			for (GAction a : active.getPassiveActions()) {
				if (a.getType().equals(param)) {
					active.getPassiveActions().removeValue(a, true);
				}
			}
			return;
		default:
			YLog.log("remove type unknown", type);
		}

	}

	private void nation(String type) {
		// create nation?
		if (!nData.existN(type)) {
			nData.add(new BaseNation(type));
		}

		// add it
		if (active instanceof DataBuilding) {
			nData.getN(type).getBuildings().add(active.getType());
		} else {
			nData.getN(type).getUnits().add(active.getType());
		}

		nation = type;

	}

	/**
	 * Set or create the building
	 * 
	 * @param param
	 */
	private DataBuilding building(String param) {
		// contain?
		if (nData.existB(param)) {
			return nData.getB(param);
		}

		// create
		DataBuilding active = new DataBuilding(param, param, -1, NCat.GENERAL);
		nData.add(active);
		return active;
	}

	/**
	 * Set or create the building
	 * 
	 * @param param
	 */
	private DataUnit unit(String param) {
		// contain?
		if (nData.existU(param)) {
			return nData.getU(param);
		}

		// create
		DataUnit active = new DataUnit(param, param, param, 5, 0, NCat.GENERAL);
		nData.add(active);
		return active;
	}

	private void file(String param) {
		DataUnit u = (DataUnit) active;
		u.setFilename(param);
	}

	private void atk(String param) {
		active.setAtk(con(param, 0));
	}

	private void def(String param) {
		active.setDef(con(param, 0));
	}

	private void imageid(String l) {
		DataBuilding d = (DataBuilding) active;
		d.setFid(Integer.parseInt(l));
	}

	private void upgrade(String param) {
		EMapData type = active instanceof DataBuilding ? EMapData.BUILDING : EMapData.UNIT;
		DataObject up = nData.get(type, param);

		active.getActiveActions().add(new ActionUpgrade(type, param));

		// disable build
		up.getReqs().add(new ReqOnlyUpgrade(type == EMapData.UNIT));

		// remove destroy
		for (GAction a : up.getActiveActions()) {
			if (a.getType().equals("destroy")) {
				up.getActiveActions().removeValue(a, true);
				break;
			}
		}

		// add remove
		up.getActiveActions().add(new ActionDegrade(type, active.getType()));
	}

	private void terrain(String param) {
		String[] terrains = param.split("-");

		// special?
		if (terrains[0].equals("near")) {
			// build terrain string
			String[] st = new String[terrains.length - 1];
			for (int i = 0; i < st.length; i++) {
				st[i] = terrains[i + 1];
			}
			active.getReqs().add(new ReqTerrain(true, st));
		} else if (terrains[0].equals("nearTown")) {
			active.getReqs().add(new ReqNearTown());
		} else {
			active.getReqs().add(new ReqTerrain(false, terrains));
		}
	}

	private void category(String param) {
		active.setCategory(NCat.valueOf(param.toUpperCase()));
	}

	private void visible(String param) {
		active.setVisibleRange(con(param, 1));
	}

	private void buildtime(String param) {
		active.setBuildTime(con(param, 0));
	}

	private void cost(String type, String cost) {
		active.getCost().put(type, con(cost, 0));
	}

	private void produce(String type, String cost) {
		int c = con(cost, 0);
		// new?
		if (produce == null) {
			produce = new ActionProduce(false, false, type, c);
			active.getPassiveActions().add(produce);
		} else {
			produce.getRawProd().put(type, c);
		}

		// neg?
		if (c < 0) {
			produce.addReq(new ReqRess(false, type, c * -1));
		}
	}

	private void produceOnce(String type, String cost) {
		int c = con(cost, 0);
		// new?
		if (produceOnce == null) {
			produceOnce = new ActionProduce(false, true, type, c);
			active.getPassiveActions().add(produceOnce);
		} else {
			produceOnce.getRawProd().put(type, c);
		}

		if (type.equals(BaseRess.WORKER)) {
			produceOnce.getRawProd().put(BaseRess.WORKERMAX, c);
		}
	}

	private void research(String type) {
		// new?
		if (research == null) {
			research = new Array<>();
		}

		research.add(type);
	}

	private void action(String type, String config) {
		switch (type) {
		case "shop":
			active.getActiveActions().add(new ActionShop());
			return;
		case "destroyTown":
			active.getActiveActions().add(new ActionDestroyTown());
			return;
		case "embark":
			active.getActiveActions().add(new ActionEmbark(con(config, 0)));
			active.getActiveActions().add(new ActionDisEmbark());
			return;
		case "sendRess":
			active.getActiveActions().add(new ActionSendRessource());
			return;
		case "firstTown":
			active.getActiveActions().add(new ActionFoundTown(true).addReq(new ReqTown(true, 0)));
			return;
		case "alchemyA":
			active.getActiveActions().add(new ActionProduceRandom(config.split(SPLIT)).addReq(new ReqAp(false, 5)));
			return;
		case "alchemyP":
			active.getPassiveActions().add(new ActionProduceRandom(config.split(SPLIT)));
			return;
		case "train":
			actionTrain(config);
			return;
		case "produce":
			actionProduce(config);
			return;
		case "gift":
			active.getActiveActions().add(new ActionGift());
			return;
		case "build":
			if (config.equals("1")) {
				active.getActiveActions().add(new ActionBuild(false));
			} else {
				active.getActiveActions().add(new ActionBuild(true, config.split("-")));
			}
			return;
		default:
			YLog.log("action type unknown", type);
		}
	}

	private void actionTrain(String config) {
		// build all?
		if (config.equals("1")) {
			active.getActiveActions().add(new ActionTrain(false));
			return;
		}

		// build only the selected
		String[] params = config.split("-");

		if (params[0].equals("not")) {
			active.getActiveActions().add(new ActionTrain(false, Arrays.copyOfRange(params, 1, params.length - 1)));
		} else {
			active.getActiveActions().add(new ActionTrain(true, params));
		}

	}

	private void passiveAction(String type, String config) {
		switch (type) {
		case "upgradeRangeLimit":
			active.getPassiveActions().add(new ActionUpgradeRangeLimit(con(config, 0)));
			return;
		case "upgradeBuildingLimit":
			active.getPassiveActions().add(new ActionUpgradeBuildingLimit(con(config, 0)));
			return;
		case "storage":
			active.getPassiveActions().add(new ActionUpgradeStorage(con(config, 0)));
			return;
		case "upgradeTownLimit":
			active.getPassiveActions().add(new ActionUpgradeTownLimit(con(config, 0)));
			return;
		case "enablePlayerFeature":
			active.getPassiveActions().add(new ActionEnablePlayerFeature(Feature.valueOf(config)));
			return;
		case "upgradeTownLevel":
			active.getPassiveActions().add(new ActionUpgradeTownLevel(con(config, 0)));
			return;

		default:
			YLog.log("passive action type unknown", type);
		}
	}

	private void actionProduce(String config) {
		String[] params = config.split("-");
		int count = con(params[1], 1);

		switch (params[0]) {
		case "wood":
			active.getActiveActions().add(
					new ActionProduce(true, false, BaseRess.WOOD, count).addReq(new ReqTerrain(false, BaseTerrain.FOREST)).addReq(new ReqAp(false, count)));
			return;
		case "leaf":
			active.getActiveActions()
					.add(new ActionProduce(true, false, "leaf", count).addReq(new ReqTerrain(false, BaseTerrain.FOREST)).addReq(new ReqAp(false, count)));
			return;
		case "food":
			active.getActiveActions()
					.add(new ActionProduce(true, false, BaseRess.FOOD, count).addReq(new ReqTerrain(false, BaseTerrain.GRASS)).addReq(new ReqAp(false, count)));
			return;
		case "stone":
			active.getActiveActions().add(new ActionProduce(true, false, BaseRess.STONE, count)
					.addReq(new ReqTerrain(false, BaseTerrain.HILL, BaseTerrain.MOUNTAIN)).addReq(new ReqAp(false, count)));
			return;
		default:
			YLog.log("action produce type unknown", config);
		}
	}
}

package de.ninenations.data;

import java.io.Serializable;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.widget.tabbedpane.TabbedPane;

import de.ninenations.core.NN;
import de.ninenations.core.NObjectMap;
import de.ninenations.data.building.BarrackBuilding;
import de.ninenations.data.building.BridgeBuilding;
import de.ninenations.data.building.CastleBuilding;
import de.ninenations.data.building.DataBuilding;
import de.ninenations.data.building.DockyardBuilding;
import de.ninenations.data.building.TownHallBuildingV1;
import de.ninenations.data.buildingunitbase.DataObject;
import de.ninenations.data.buildingunitbase.UnitBuildingParser;
import de.ninenations.data.elements.BaseElement;
import de.ninenations.data.events.BaseEvent;
import de.ninenations.data.events.ParserEvent;
import de.ninenations.data.god.BaseGod;
import de.ninenations.data.ki.BasePlayerType;
import de.ninenations.data.ki.HotSeatPlayerType;
import de.ninenations.data.ki.HumanPlayerType;
import de.ninenations.data.ki.NatureKI;
import de.ninenations.data.nations.BaseNation;
import de.ninenations.data.nations.ParserNation;
import de.ninenations.data.research.BaseResearch;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.data.ress.ParserRess;
import de.ninenations.data.terrain.BaseTerrain;
import de.ninenations.data.terrain.ParserTerrain;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.game.unit.DataUnit;
import de.ninenations.game.unit.SettlerUnit;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.util.NSettings;
import de.ninenations.util.YLog;

/**
 * Enthält die grundlegenden Spieleigenschaften, e.g. Libary
 */
public class NData implements Serializable {

	private static final long serialVersionUID = 8356632140372844393L;

	/**
	 * @return
	 */
	public NData() {}

	/**
	 * 
	 */
	private NObjectMap<BaseElement> elements;

	/**
	 * 
	 */
	private NObjectMap<BaseNation> nations;

	/**
	 * 
	 */
	private NObjectMap<DataUnit> units;

	/**
	 * 
	 */
	private NObjectMap<DataBuilding> buildings;

	/**
	 * 
	 */
	private NObjectMap<BasePlayerType> playerType;

	/**
	 * 
	 */
	private NObjectMap<BaseTerrain> terrain;

	/**
	 * 
	 */
	private NObjectMap<BaseRess> ress;

	/**
	 * 
	 */
	private NObjectMap<BaseResearch> research;

	/**
	 * 
	 */
	private NObjectMap<BaseEvent> events;

	/**
	 * 
	 */
	private Array<BaseGod> gods;

	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public void generate() {
		// add elements
		elements = new NObjectMap<>("Element");
		elements.addS(new BaseElement(BaseElement.FIRE, "Fire", YIcons.FIRE));
		elements.addS(new BaseElement(BaseElement.AIR, "Air", YIcons.AIR));
		elements.addS(new BaseElement(BaseElement.DEATH, "Death", YIcons.DEATH));
		elements.addS(new BaseElement(BaseElement.EARTH, "Earth", YIcons.EARTH));
		elements.addS(new BaseElement(BaseElement.ENERGY, "Energy", YIcons.ENERGY));
		elements.addS(new BaseElement(BaseElement.LIFE, "Life", YIcons.LIFE));
		elements.addS(new BaseElement(BaseElement.METAL, "Metal", YIcons.METAL));
		elements.addS(new BaseElement(BaseElement.WATER, "Water", YIcons.WATER));
		elements.addS(new BaseElement(BaseElement.WOOD, "Wood", YIcons.WOOD));

		// add ki
		playerType = new NObjectMap<>("ki");
		add(new NatureKI());
		add(new HumanPlayerType());
		add(new HotSeatPlayerType());

		terrain = new NObjectMap<>("Terrain");
		ress = new NObjectMap<>("Ress");
		research = new NObjectMap<>("Research");
		events = new NObjectMap<>("Events");

		// add units
		units = new NObjectMap<>("Units");
		add(new SettlerUnit());

		// add building
		buildings = new NObjectMap<>("Buildings");
		add(new TownHallBuildingV1());

		add(new DockyardBuilding());
		add(new BridgeBuilding());

		add(new CastleBuilding());
		add(new BarrackBuilding());

		nations = new NObjectMap<>("Nations");

		// add the confis
		// for (String file :
		// Gdx.files.internal("system/config/list.txt").readString().split(",")) {
		// new DataParser(this, Gdx.files.internal("system/config/" + file));
		// }

		addFiles(NN.plattform().getFolderContent("system/config", ".csv"));
		NN.mods().readnData(this);

		// add log
		YLog.log("Finish loading nData");
		for (@SuppressWarnings("rawtypes")
		NObjectMap o : new NObjectMap[] { elements, nations, units, buildings, playerType, terrain, ress, research }) {
			YLog.log(o.getType() + ": " + o.size);

			// check it
			if (NSettings.isDebug()) {
				for (String b : ((NObjectMap<BaseDisplay>) o).keys()) {
					((NObjectMap<BaseDisplay>) o).get(b).validate();
				}
			}
		}

	}

	/**
	 * Add this folder to the config
	 * 
	 * @param folder
	 */
	public void addFiles(FileHandle[] files) {
		// add the confis
		for (FileHandle file : files) {
			if (file.nameWithoutExtension().endsWith("Building") || file.nameWithoutExtension().endsWith("Unit")) {
				new UnitBuildingParser(this, file);
				continue;
			}

			if (file.nameWithoutExtension().endsWith("Ress")) {
				new ParserRess(this, file);
				continue;
			}

			if (file.nameWithoutExtension().endsWith("Nation")) {
				new ParserNation(this, file);
				continue;
			}

			if (file.nameWithoutExtension().endsWith("Event")) {
				new ParserEvent(this, file);
				continue;
			}

			if (file.nameWithoutExtension().endsWith("Terrain")) {
				new ParserTerrain(this, file);
				continue;
			}

			YLog.log("unsupport config file", file);

		}
	}

	public BaseTerrain add(BaseTerrain unit) {
		terrain.addS(unit);
		return unit;
	}

	public void add(BaseNation unit) {
		nations.addS(unit);
	}

	public void add(BaseResearch unit) {
		research.addS(unit);
	}

	public void add(BaseEvent unit) {
		events.addS(unit);
	}

	public void add(DataUnit unit) {
		units.addS(unit);
	}

	public void add(DataBuilding unit) {
		buildings.addS(unit);
	}

	public void add(BasePlayerType unit) {
		playerType.addS(unit);
	}

	public BaseRess add(BaseRess unit) {
		ress.addS(unit);
		return unit;
	}

	public void removeB(String key) {
		buildings.remove(key);
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public DataUnit getU(String type) {
		return units.getS(type);
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public DataBuilding getB(String type) {
		return buildings.getS(type);
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public BaseElement getE(String type) {
		return elements.getS(type);
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public BaseEvent getEv(String type) {
		return events.getS(type);
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public BaseEvent getRandEv() {
		return events.getRnd();
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public BaseTerrain getT(String type) {
		return terrain.getS(type);
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public BaseTerrain getT(int pos) {
		return terrain.get(pos);
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public BaseNation getN(String type) {
		return nations.getS(type);
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public BasePlayerType getK(String type) {
		return playerType.getS(type);
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public BaseRess getR(String type) {
		return ress.getS(type);
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public BaseResearch getRS(String type) {
		return research.getS(type);
	}

	/**
	 * Exist it?
	 * 
	 * @param type
	 */
	public boolean existRS(String type) {
		return research.containsKey(type);
	}

	/**
	 * Exist it?
	 * 
	 * @param type
	 */
	public boolean existEv(String type) {
		return events.containsKey(type);
	}

	/**
	 * Exist it?
	 * 
	 * @param type
	 */
	public boolean existB(String type) {
		return buildings.containsKey(type);
	}

	/**
	 * Exist it?
	 * 
	 * @param type
	 */
	public boolean existN(String type) {
		return nations.containsKey(type);
	}

	/**
	 * Exist it?
	 * 
	 * @param type
	 */
	public boolean existU(String type) {
		return units.containsKey(type);
	}

	/**
	 * Exist it?
	 * 
	 * @param type
	 */
	public boolean existT(String type) {
		return terrain.containsKey(type);
	}

	/**
	 * Get a unit
	 * 
	 * @param type
	 */
	public DataObject get(EMapData mapData, String type) {
		if (mapData == EMapData.UNIT) {
			return units.getS(type);
		} else {
			return buildings.get(type);
		}
	}

	/**
	 * Get all units keys
	 * 
	 * @param type
	 */
	public Array<String> getUnits() {
		return units.keys().toArray();
	}

	/**
	 * Get all units keys
	 * 
	 * @param type
	 */
	public Array<String> getResearch() {
		return research.keys().toArray();
	}

	/**
	 * Get all buildings keys
	 * 
	 * @param type
	 */
	public Array<String> getBuildings() {
		return buildings.keys().toArray();
	}

	/**
	 * Get all buildings keys
	 * 
	 * @param type
	 */
	public Array<String> getRess() {
		return ress.keys().toArray();
	}

	/**
	 * Get all buildings keys
	 * 
	 * @param type
	 */
	public Array<String> getTerrain() {
		return terrain.keys().toArray();
	}

	/**
	 * Get all buildings keys
	 * 
	 * @param type
	 */
	public Array<String> getKIs() {
		return playerType.keys().toArray();
	}

	/**
	 * Get all buildings keys
	 * 
	 * @param type
	 */
	public Array<String> getNations() {
		return nations.keys().toArray();
	}

	public void buildLexikon(TabbedPane tabbedPane) {
		tabbedPane.add(new LexikonTab<>(units));
		tabbedPane.add(new LexikonTab<>(buildings));
		tabbedPane.add(new LexikonTab<>(research));
		tabbedPane.add(new LexikonTab<>(ress));
		tabbedPane.add(new LexikonTab<>(terrain));
		tabbedPane.add(new LexikonTab<>(elements));
		tabbedPane.add(new LexikonTab<>(nations));
		tabbedPane.add(new LexikonTab<>(events));
		// tabbedPane.add(new LexikonTab<>(playerType));

	}

	/**
	 * @return the terrain
	 */
	public NObjectMap<BaseTerrain> getTerrainRaw() {
		return terrain;
	}

}
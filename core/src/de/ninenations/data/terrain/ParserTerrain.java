package de.ninenations.data.terrain;

import com.badlogic.gdx.files.FileHandle;

import de.ninenations.data.NData;
import de.ninenations.data.config.BaseParser;
import de.ninenations.data.terrain.BaseTerrain.NMove;
import de.ninenations.util.YLog;

public class ParserTerrain extends BaseParser<BaseTerrain> {

	public ParserTerrain(NData nData, FileHandle file) {
		super(nData, file, 1, 2);
	}

	@Override
	protected void run(String type, String mparam, String param) {
		switch (type) {
		case "icon":
			active.setIcon(Integer.parseInt(param));
			return;
		case "sound":
			active.setSound(param);
			return;
		case "autotile":
			autotile(mparam, param);
			return;
		case "move":
			moveType(mparam, param);
			return;
		case "buildtime":
			active.setBuildTime(Integer.parseInt(param));
			return;
		case "visible":
			active.setVisibleRange(Integer.parseInt(param));
			return;

		default:
			YLog.log("type unknown", type);
		}

	}

	private void moveType(String mparam, String param) {
		active.getCostRaw().put(NMove.valueOf(mparam), Integer.parseInt(param));
	}

	private void autotile(String mparam, String param) {
		if (mparam.equals("WINTER")) {
			active.setAutoTileWinter(Integer.parseInt(param));
		} else {
			active.setAutoTile(Integer.parseInt(param));
		}
	}

	@Override
	protected BaseTerrain registerType(String[][] data, int l) {
		String param = data[l][0];
		// contain?
		if (nData.existT(param)) {
			return nData.getT(param);
		}

		return nData.add(new BaseTerrain(param));
	}

	@Override
	protected void unRegisterType(String[][] data, int l) {
		active = null;

	}

}

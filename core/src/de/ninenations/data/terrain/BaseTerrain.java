package de.ninenations.data.terrain;

import java.io.Serializable;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.ObjectMap;

import de.ninenations.core.NN;
import de.ninenations.core.NSimpleObjectMap;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.TextHelper;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.YLog;
import de.ninenations.util.YSounds;

/**
 * 
 */
public class BaseTerrain extends BaseDisplay implements Serializable {

	private static final long serialVersionUID = 2576201761404142403L;

	public static final String MOUNTAIN = "mountain", DARK = "dark", DEF = "default", GRASS = "grass", WATER = "water", FOREST = "forest", HILL = "hill";

	public enum NMove {
		WALK, SWIM, FLY
	}

	private NSimpleObjectMap<NMove, Integer> cost;
	private int buildTime, visibleRange, icon;

	private int autoTile, autoTileWinter;

	private String sound;

	@SuppressWarnings("unused")
	private BaseTerrain() {
		super();
	}

	/**
	 * Default constructor
	 */
	public BaseTerrain(String type) {
		super(type, type);

		cost = new NSimpleObjectMap<>();

		icon = -1;
	}

	public int getCost(NMove move) {
		return cost.get(move, 5);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getAutotile(icon);
	}

	/**
	 * Can pass it?
	 *
	 * @param pass
	 * @return
	 */
	public boolean canPass(NMove pass) {
		return cost.containsKey(pass) && getCost(pass) >= 0;
	}

	@Override
	public YTable getInfoPanel() {
		YTable t = new YTable();

		t.addL("Name", name);
		t.addH("Movement cost");
		for (NMove m : NMove.values()) {
			t.addL(m.toString(), !canPass(m) ? "Not usable" : getCost(m) + " AP");
		}

		if (buildTime != 0 || visibleRange != 0) {
			t.addH("Modificer");
			if (buildTime != 0) {
				t.addL("Buildtime", TextHelper.plus(buildTime) + "%");
			}
			if (visibleRange != 0) {
				t.addL("Visible Range", TextHelper.plus(visibleRange) + " Fields");
			}
		}

		return t;
	}

	/**
	 * @return the buildTime
	 */
	public int getBuildTime() {
		return buildTime;
	}

	/**
	 * @return the visibleRange
	 */
	public int getVisibleRange() {
		return visibleRange;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.data.buildingunitbase.DataObject#validate()
	 */
	@Override
	public void validate() {
		super.validate();
		if (icon == -1) {
			YLog.log("Validate", type, "has no icon");
		}

	}

	/**
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(int icon) {
		this.icon = icon;
	}

	/**
	 * @return the cost
	 */
	public ObjectMap<NMove, Integer> getCostRaw() {
		return cost;
	}

	/**
	 * @param buildTime
	 *            the buildTime to set
	 */
	public void setBuildTime(int buildTime) {
		this.buildTime = buildTime;
	}

	/**
	 * @param visibleRange
	 *            the visibleRange to set
	 */
	public void setVisibleRange(int visibleRange) {
		this.visibleRange = visibleRange;
	}

	/**
	 * @return the autoTile
	 */
	public int getAutoTile() {
		return autoTile;
	}

	/**
	 * @param autoTile
	 *            the autoTile to set
	 */
	public void setAutoTile(int autoTile) {
		this.autoTile = autoTile;
	}

	/**
	 * @return the autoTileWinter
	 */
	public int getAutoTileWinter() {
		return autoTileWinter;
	}

	/**
	 * @param autoTileWinter
	 *            the autoTileWinter to set
	 */
	public void setAutoTileWinter(int autoTileWinter) {
		this.autoTileWinter = autoTileWinter;
	}

	/**
	 * Play the sound
	 */
	public void playSound() {
		if (sound == null) {
			YSounds.pClick();
		} else {
			YSounds.play(sound);
		}
	}

	/**
	 * @param sound
	 *            the sound to set
	 */
	public void setSound(String sound) {
		this.sound = sound;
	}

	/**
	 * Load the asset
	 */
	public void assetLoad() {
		if (sound != null) {
			NN.asset().load("system/sound/" + sound + ".mp3", Sound.class);
		}

	}
}
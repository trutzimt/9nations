package de.ninenations.data;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.utils.Array;

import de.ninenations.core.NObjectMap;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.elements.YSplitTab;

public class LexikonTab<T extends BaseDisplay> extends YSplitTab<T> {

	// private NObjectMap<BaseDisplay> display;

	public LexikonTab(NObjectMap<T> display) {
		super(display.getType(), "There are no " + display.getType());

		// this.display = display;

		Array<String> a = display.keys().toArray();
		a.sort();

		// add all elements
		for (String key : a) {
			addElement(display.get(key));
		}
	}

	@Override
	protected void doubleClickElement(Button btn) {}

}
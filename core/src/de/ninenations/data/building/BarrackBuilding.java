/**
 *
 */
package de.ninenations.data.building;

import de.ninenations.actions.action.ActionTrain;

/**
 * @author sven
 *
 */
public class BarrackBuilding extends DataBuilding {

	private static final long serialVersionUID = 2815814789924479883L;

	/**
	 * @param id
	 * @param x
	 * @param y
	 */
	public BarrackBuilding() {
		super("nbarrack", "Barack", 998, NCat.WAR);

		activeActions.add(new ActionTrain(false, "nship"));
	}

}

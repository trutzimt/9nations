/**
 *
 */
package de.ninenations.data.building;

import de.ninenations.actions.action.ActionProduce;
import de.ninenations.actions.req.ReqNoBuild;
import de.ninenations.data.ress.BaseRess;

/**
 * @author sven
 *
 */
public class TownHallBuildingV1 extends DataBuilding {

	private static final long serialVersionUID = -5993943430692118543L;

	/**
	 * @param id
	 * @param x
	 * @param y
	 */
	public TownHallBuildingV1() {
		super("ntownhall", "Village hall", 582, NCat.GENERAL);

		reqs.add(new ReqNoBuild());

		passiveActions.add(new ActionProduce(false, true, BaseRess.FOOD, 20, BaseRess.STONE, 8, BaseRess.WOOD, 15, BaseRess.TOOL, 10, BaseRess.WORKER, 6,
				BaseRess.WORKERMAX, 6));

	}

}
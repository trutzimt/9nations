/**
 * 
 */
package de.ninenations.data.building;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.ObjectMap;

import de.ninenations.data.buildingunitbase.DataObject;
import de.ninenations.data.terrain.BaseTerrain.NMove;
import de.ninenations.game.S;
import de.ninenations.game.map.NMapBuilding;
import de.ninenations.player.Player;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.YLog;

/**
 * @author sven
 *
 */
public class DataBuilding extends DataObject {

	private static final long serialVersionUID = -235214538545472296L;

	protected int fid;

	protected ObjectMap<NMove, Integer> ownerCost, publicCost;

	protected boolean connected;

	public DataBuilding() {
		super();
	}

	/**
	 * @param type
	 * @param name
	 */
	public DataBuilding(String type, String name, int fid, NCat category) {
		super(type, name, category);

		this.fid = fid;

		ownerCost = new ObjectMap<>();
		publicCost = new ObjectMap<>();

		ownerCost.put(NMove.WALK, -1);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getBuild(fid);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIconO() {
		return YIcons.getBuildO(fid);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getInfoPanel()
	 */
	@Override
	public YTable getInfoPanel() {
		return super.getInfoPanel();
	}

	/**
	 * @return the ownerCost
	 */
	public ObjectMap<NMove, Integer> getOwnerCost() {
		return ownerCost;
	}

	/**
	 * @return the publicCost
	 */
	public ObjectMap<NMove, Integer> getPublicCost() {
		return publicCost;
	}

	/**
	 * This method will called, if the building is connected
	 *
	 * @param mapScreen
	 * @param baseBuilding
	 * @return
	 */
	protected int getConnectedMapLook(NMapBuilding activeBuilding) {
		if (!connected) {
			throw new UnsupportedOperationException();
		}

		int erg = getConnectionID(activeBuilding);
		int l = 32;

		int baseid = fid - 32 * 3 - 2;

		switch (erg) {
		case 0:
			return fid;
		case TOP:
			return baseid + 3 + l;
		case LEFT:
			return baseid + 3;
		case RIGHT:
			return baseid + 2 + l;
		case DOWN:
			return baseid + 2;
		case TOP + LEFT:
			return baseid + 1 + l;
		case LEFT + DOWN:
			return baseid + 1;
		case DOWN + RIGHT:
			return baseid;
		case RIGHT + TOP:
			return baseid + l;
		case DOWN + TOP:
			return baseid + 3 + l * 3;
		case RIGHT + LEFT:
			return baseid + 3 + l * 2;
		case LEFT + RIGHT + DOWN:
			return baseid + l * 2;
		case TOP + RIGHT + DOWN:
			return baseid + l * 3;
		case TOP + LEFT + DOWN:
			return baseid + l * 2 + 1;
		case TOP + LEFT + RIGHT:
			return baseid + l * 3 + 1;
		case TOP + LEFT + RIGHT + DOWN:
			return baseid + l * 2 + 2;
		default:
			throw new IllegalArgumentException("Connection " + erg + " not supported");
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	public Image getIconConnected(NMapBuilding activeBuilding) {
		return YIcons.getBuildO(getConnectedMapLook(activeBuilding));
	}

	/**
	 * Helpermethod to get the neightbors
	 *
	 * @param mapScreen
	 * @param activeBuilding
	 * @return
	 */
	protected int getConnectionID(NMapBuilding activeBuilding) {
		int erg = 0;
		int x = activeBuilding.getX(), y = activeBuilding.getY();
		Player p = activeBuilding.getPlayer();

		// LEFT
		NMapBuilding a = S.build(x - 1, y);
		if (a != null && p == a.getPlayer() && type.equals(a.getType())) {
			erg += LEFT;
		}

		// TOP
		a = S.build(x, y + 1);
		if (a != null && p == a.getPlayer() && type.equals(a.getType())) {
			erg += TOP;
		}

		// RIGHT
		a = S.build(x + 1, y);
		if (a != null && p == a.getPlayer() && type.equals(a.getType())) {
			erg += RIGHT;
		}

		// DOWN
		a = S.build(x, y - 1);
		if (a != null && p == a.getPlayer() && type.equals(a.getType())) {
			erg += DOWN;
		}

		return erg;
	}

	/**
	 * @return the connected
	 */
	public boolean isConnected() {
		return connected;
	}

	/**
	 * @param fid
	 *            the fid to set
	 */
	public void setFid(int fid) {
		this.fid = fid;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.data.buildingunitbase.DataObject#validate()
	 */
	@Override
	public void validate() {
		super.validate();
		if (fid == -1) {
			YLog.log("Validate", type, "has no fid");
		}
	}

	/**
	 * @param connected
	 *            the connected to set
	 */
	public void setConnected(boolean connected) {
		this.connected = connected;
	}

}

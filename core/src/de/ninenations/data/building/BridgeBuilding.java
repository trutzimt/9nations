/**
 *
 */
package de.ninenations.data.building;

import de.ninenations.actions.req.ReqRiver;
import de.ninenations.data.terrain.BaseTerrain;
import de.ninenations.data.terrain.BaseTerrain.NMove;
import de.ninenations.game.S;
import de.ninenations.game.map.NMapBuilding;

/**
 * @author sven
 *
 */
public class BridgeBuilding extends DataBuilding {

	private static final long serialVersionUID = -3174663196190625987L;

	/**
	 * @param id
	 * @param x
	 * @param y
	 */
	public BridgeBuilding() {
		super("nbridge", "Bridge", 851, NCat.MOVE);

		reqs.add(new ReqRiver());

		ownerCost.put(NMove.WALK, 5);

		connected = true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.qossire.game.movable.building.BaseBuilding#getConnectedMapLook(de.qossire.
	 * screens.map.MapScreen, de.qossire.game.movable.building.ActiveBuilding)
	 */
	@Override
	protected int getConnectedMapLook(NMapBuilding activeBuilding) {
		int x = activeBuilding.getX();
		int y = activeBuilding.getY();

		if (x > 0 && !S.map().getTerrainID(x - 1, y).equals(BaseTerrain.WATER) && S.map().getTerrainID(x, y).equals(BaseTerrain.WATER) && x + 1 < S.width()
				&& !S.map().getTerrainID(x + 1, y).equals(BaseTerrain.WATER)) {
			return fid;
		}

		return fid + 1;
	}

}

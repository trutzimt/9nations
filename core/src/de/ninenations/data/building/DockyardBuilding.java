/**
 *
 */
package de.ninenations.data.building;

import de.ninenations.actions.action.ActionTrain;
import de.ninenations.data.terrain.BaseTerrain.NMove;

/**
 * @author sven
 *
 */
public class DockyardBuilding extends DataBuilding {

	private static final long serialVersionUID = -7925347065450143639L;

	/**
	 * @param id
	 * @param x
	 * @param y
	 */
	public DockyardBuilding() {
		super("ndockyard", "Dockyard", 932, NCat.WAR);

		activeActions.add(new ActionTrain(true, "nship"));

		ownerCost.put(NMove.SWIM, 5);

	}

}

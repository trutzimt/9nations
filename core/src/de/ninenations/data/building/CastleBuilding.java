/**
 *
 */
package de.ninenations.data.building;

import de.ninenations.actions.action.ActionDisEmbark;
import de.ninenations.actions.action.ActionEmbark;
import de.ninenations.actions.action.ActionTrain;

/**
 * @author sven
 *
 */
public class CastleBuilding extends DataBuilding {

	private static final long serialVersionUID = 9083538797422004452L;

	/**
	 * @param id
	 * @param x
	 * @param y
	 */
	public CastleBuilding() {
		super("ncastle", "Castle", 963, NCat.WAR);

		activeActions.add(new ActionTrain(false));
		activeActions.add(new ActionEmbark(3));
		activeActions.add(new ActionDisEmbark());

	}

}
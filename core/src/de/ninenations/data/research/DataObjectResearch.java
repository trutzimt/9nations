package de.ninenations.data.research;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.req.ReqTownLevel;
import de.ninenations.data.buildingunitbase.DataObject;
import de.ninenations.game.S;
import de.ninenations.game.screen.MapData.EMapData;

public class DataObjectResearch extends BaseResearch {

	private static final long serialVersionUID = -938678178555847840L;
	private EMapData mType;

	public DataObjectResearch() {
		super(null, null);
	}

	/**
	 * @param type
	 * @param title
	 */
	public DataObjectResearch(DataObject build, EMapData mType, int level, String... ress) {
		super(build.getType(), build.getName());

		this.mType = mType;

		// add the research
		for (String r : ress) {
			if (!elements.contains(r, false)) {
				elements.add(r);
			}

		}

		reqs.add(new ReqTownLevel(false, level));
	}

	@Override
	public Image getIcon() {
		return S.nData().get(mType, type).getIcon();
	}

}
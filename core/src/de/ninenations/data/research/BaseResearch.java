package de.ninenations.data.research;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.core.NArray;
import de.ninenations.data.elements.BaseElement;
import de.ninenations.game.S;
import de.ninenations.player.Player;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;

/**
 * 
 */
public abstract class BaseResearch extends BaseDisplay implements Serializable {

	private static final long serialVersionUID = 2150588184001452528L;

	/**
	 * 
	 */
	protected NArray<BaseReq> reqs;

	protected NArray<String> elements;

	public BaseResearch() {
		super();
	}

	/**
	 * Default constructor
	 */
	public BaseResearch(String type, String name) {
		super(type, name);

		elements = new NArray<>();
		reqs = new NArray<>();
	}

	/**
	 * Add a new ress cost
	 * 
	 * @param type
	 */
	protected void addCost(String type) {
		elements.add(type);
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(YIcons.RESEARCH);
	}

	@Override
	public YTable getInfoPanel() {
		YTable table = new YTable();
		table.addH("General");
		table.addL("Name", name);

		table.addH("Research Areas");

		for (String key : elements) {
			BaseElement r = S.nData().getE(key);
			table.addI(r.getIcon(), r.getName());
		}

		return table;

	}

	/**
	 * @param obj
	 */
	public boolean canPerform(Player player) {
		// check pref
		for (BaseReq req : reqs) {
			if (!req.checkReq(player, null, -1, -1)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * @return the elements
	 */
	public Array<String> getElements() {
		return elements;
	}
}
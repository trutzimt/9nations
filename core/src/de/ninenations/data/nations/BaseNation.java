package de.ninenations.data.nations;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;

import de.ninenations.core.NArray;
import de.ninenations.core.NSimpleObjectMap;
import de.ninenations.data.building.DataBuilding;
import de.ninenations.data.ress.BaseRess;
import de.ninenations.data.terrain.BaseTerrain;
import de.ninenations.game.S;
import de.ninenations.game.unit.DataUnit;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.YLog;

/**
 * 
 */
public class BaseNation extends BaseDisplay implements Serializable {

	public static final String NORTH = "north", RANGER = "ranger";

	/**
	 * TOWNLEVEL, Need a special townlevel, 0=disabled TOWNLEVELCHANGEABLE, Needed
	 * for townlevel, one worker consume it per Round, 0=disabled
	 */
	public enum NationRess {
		MARKETCOST, TOWNLEVEL, TOWNLEVELCHANGEABLE
	}

	private static final long serialVersionUID = -2237732874909090945L;

	private int icon;

	private NArray<String> buildings;
	private NArray<String> units;
	private NArray<String> research;

	private NSimpleObjectMap<String, Object> ressData;
	private NSimpleObjectMap<String, Integer> terrainCost;

	private String leader;
	private String townhall;

	private String researchElement;

	@SuppressWarnings("unused")
	private BaseNation() {
		super();
	}

	public BaseNation(String type) {
		super(type, type);

		buildings = new NArray<>();
		units = new NArray<>();
		research = new NArray<>();
		icon = -1;

		ressData = new NSimpleObjectMap<>();
		terrainCost = new NSimpleObjectMap<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getInfoPanel()
	 */
	@Override
	public YTable getInfoPanel() {
		YTable t = new YTable();
		t.addL("Name", name);
		t.addL("Research", S.nData().getE(researchElement).getName());

		if (terrainCost.size > 0) {
			t.addH("Terrain cost");
			for (String b : terrainCost.keys()) {
				BaseTerrain terr = S.nData().getT(b);
				t.addI(terr.getIcon(), terr.getName() + ": " + terrainCost.get(b) + " AP");
			}
		}

		if (buildings.size > 0) {
			t.addH("Buildings");
			for (String b : buildings) {
				DataBuilding d = S.nData().getB(b);
				t.addI(d.getIcon(), d.getName());
			}
		}

		if (units.size > 0) {
			t.addH("Units");
			for (String b : units) {
				DataUnit d = S.nData().getU(b);
				t.addI(d.getIcon(), d.getName());
			}
		}

		return t;
	}

	/**
	 * @return the buildings
	 */
	public Array<String> getBuildings() {
		return buildings;
	}

	/**
	 * @return the units
	 */
	public Array<String> getUnits() {
		return units;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(icon);
	}

	/**
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(int icon) {
		this.icon = icon;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.data.buildingunitbase.DataObject#validate()
	 */
	@Override
	public void validate() {
		super.validate();
		if (icon == -1) {
			YLog.log("Validate", type, "has no icon");
		}

		if (buildings.size == 0) {
			YLog.log("Validate", type, "has no buildings");
		}

		if (units.size == 0) {
			YLog.log("Validate", type, "has no units");
		}
	}

	/**
	 * @return the baseUnit
	 */
	public String getLeader() {
		return leader;
	}

	/**
	 * @param baseUnit
	 *            the baseUnit to set
	 */
	public void setLeader(String leader) {
		this.leader = leader;
	}

	/**
	 * @return the ressData
	 */
	public Object getRessData(String ress, NationRess type) {
		String id = ress + type;
		if (!ressData.containsKey(id)) {
			BaseRess r = S.nData().getR(ress);

			switch (type) {
			case MARKETCOST:
				ressData.put(id, r.getMarketCost());
				break;
			case TOWNLEVELCHANGEABLE:
				ressData.put(id, 0f);
				break;
			case TOWNLEVEL:
				ressData.put(id, 0);
				break;
			}

		}

		return ressData.get(ress + type);
	}

	/**
	 * 
	 * @param ress
	 * @return
	 */
	public int getRessMarketPrice(String ress) {
		return (int) getRessData(ress, NationRess.MARKETCOST);
	}

	/**
	 * @param ressData
	 *            the ressData to set
	 */
	public void setRessData(String ress, NationRess type, Object value) {
		ressData.put(ress + type, value);
	}

	/**
	 * @return the townhall
	 */
	public String getTownhall() {
		return townhall;
	}

	/**
	 * @param townhall
	 *            the townhall to set
	 */
	public void setTownhall(String townhall) {
		this.townhall = townhall;
	}

	/**
	 * @return the research
	 */
	public NArray<String> getResearch() {
		return research;
	}

	/**
	 * @return the terrainCost
	 */
	public int getTerrainCost(String terrain) {
		return terrainCost.get(terrain, 0);
	}

	/**
	 * @param terrainCost
	 *            the terrainCost to set
	 */
	public void setTerrainCost(String terrain, int cost) {
		terrainCost.put(terrain, cost);
	}

	/**
	 * @return the researchElement
	 */
	public String getResearchElement() {
		return researchElement;
	}

	/**
	 * @param researchElement
	 *            the researchElement to set
	 */
	public void setResearchElement(String researchElement) {
		this.researchElement = researchElement;
	}

}
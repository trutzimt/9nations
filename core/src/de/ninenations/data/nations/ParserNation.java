package de.ninenations.data.nations;

import com.badlogic.gdx.files.FileHandle;

import de.ninenations.data.NData;
import de.ninenations.data.config.BaseParser;
import de.ninenations.data.nations.BaseNation.NationRess;
import de.ninenations.util.YLog;

public class ParserNation extends BaseParser<BaseNation> {

	public ParserNation(NData nData, FileHandle file) {
		super(nData, file, 1, 2);
	}

	@Override
	protected void run(String type, String mparam, String param) {
		switch (type) {
		case "icon":
			active.setIcon(Integer.parseInt(param));
			return;
		case "townlevel":
			active.setRessData(param, NationRess.TOWNLEVEL, Integer.parseInt(mparam));
			return;
		case "townLevelChangeable":
			active.setRessData(mparam, NationRess.TOWNLEVELCHANGEABLE, Float.parseFloat(param));
			return;
		case "leader":
			active.setLeader(param);
			return;
		case "terrain":
			active.setTerrainCost(mparam, Integer.parseInt(param));
			return;
		case "researchElement":
			active.setResearchElement(param);
			return;
		case "townhall":
			active.setTownhall(param);
			return;
		default:
			YLog.log("type unknown", type);
		}

	}

	@Override
	protected BaseNation registerType(String[][] data, int l) {
		String param = data[l][0];
		// contain?
		if (nData.existN(param)) {
			return nData.getN(param);
		}

		BaseNation active = new BaseNation(param);
		nData.add(active);
		return active;
	}

	@Override
	protected void unRegisterType(String[][] data, int l) {
		active = null;

	}

}

package de.ninenations.data.config;

import com.badlogic.gdx.files.FileHandle;

import de.ninenations.actions.base.IReq;
import de.ninenations.actions.req.ReqDate;
import de.ninenations.actions.req.ReqDayTime;
import de.ninenations.actions.req.ReqMinMaxObj;
import de.ninenations.actions.req.ReqScenConf;
import de.ninenations.actions.req.ReqSeason;
import de.ninenations.actions.req.ReqTownLevel;
import de.ninenations.data.NData;
import de.ninenations.data.building.DataBuilding;
import de.ninenations.game.NRound.NSeason;
import de.ninenations.game.ScenConf;
import de.ninenations.game.screen.MapData.EMapData;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.util.YError;
import de.ninenations.util.YLog;

public abstract class BaseParser<T extends BaseDisplay> {

	protected NData nData;

	protected static final String SPLIT = "-";

	protected T active;

	/**
	 * 
	 */
	public BaseParser(NData nData, FileHandle file, int startColumn, int startRow) {
		try {
			this.nData = nData;
			YLog.log("Parse file", file);

			// read file
			String lines[] = file.readString().split("\\r?\\n");

			int width = lines[0].split(",").length;

			String data[][] = new String[lines.length][width];

			// read all data
			for (int i = 0; i < lines.length; i++) {
				String[] tmp = lines[i].split(",");
				for (int j = 0; j < tmp.length; j++) {
					data[i][j] = tmp[j];
				}

			}

			// run per line
			for (int l = startRow; l < data.length; l++) {

				// skip line?
				if (data[l].length == 0) {
					YLog.log("Skip line", l);
					continue;
				}

				active = registerType(data, l);
				if (active == null) {
					YLog.log("Invalid line", l);
					continue;
				}

				// perform settings
				try {
					for (int r = startColumn; r < data[l].length; r++) {
						// has a param?
						if (data[l][r] == null || data[l][r].isEmpty()) {
							continue;
						}

						baseRun(data[0][r], data[1][r], data[l][r]);
					}
				} catch (Exception e) {
					YError.error(new IllegalArgumentException("Can not read line " + l, e), false);
				}

				unRegisterType(data, l);
			}
		} catch (Exception e) {
			YError.error(e, false);
		}
	}

	protected void baseRun(String type, String mparam, String param) {

		switch (type) {
		case "name":
			active.setName(param);
			return;
		default:
			run(type, mparam, param);
		}
	}

	/**
	 * Perform the specific type to import the data
	 * 
	 * @param type,
	 *            1. line
	 * @param mparam,
	 *            2. line
	 * @param param,
	 *            actual row
	 */
	protected abstract void run(String type, String mparam, String param);

	/**
	 *
	 * @param data
	 * @param l
	 * @return the base object
	 */
	protected abstract T registerType(String[][] data, int l);

	/**
	 * Unregister and finish the type
	 * 
	 * @param data
	 * @param l
	 */
	protected abstract void unRegisterType(String[][] data, int l);

	/**
	 * Convert. If an error happen, the default value will be used and an error
	 * thrown
	 * 
	 * @param value
	 * @param def
	 * @return
	 */
	protected int parseInt(String value, int def) {
		try {
			return Integer.parseInt(value);
		} catch (Exception e) {
			YError.error(e, false);
			return def;
		}
	}

	/**
	 * Convert string to int
	 * 
	 * @param num
	 * @param def
	 * @return
	 */
	protected int con(String num, int def) {
		if (num == null || num.length() == 0) {
			return def;
		}
		return Integer.parseInt(num);
	}

	/**
	 * Add the req
	 * 
	 * @param obj
	 * @param type
	 * @param config
	 */
	@SuppressWarnings("rawtypes")
	protected void req(IReq obj, String type, String config) {
		String[] s;
		switch (type) {
		case "scenconf":
			obj.addReq(new ReqScenConf(ScenConf.valueOf(config)));
			return;
		case "min":
			s = config.split("-");
			obj.addReq(new ReqMinMaxObj(false, Integer.valueOf(s[0]), obj instanceof DataBuilding ? EMapData.BUILDING : EMapData.UNIT, s[1],
					Boolean.valueOf(s[2])));
			return;
		case "max":
			s = config.split("-");
			obj.addReq(new ReqMinMaxObj(true, Integer.valueOf(s[0]), obj instanceof DataBuilding ? EMapData.BUILDING : EMapData.UNIT, s[1],
					Boolean.valueOf(s[2])));
			return;
		case "townlevel":
			obj.addReq(new ReqTownLevel(false, con(config, 0)));
			return;
		case "season":
			boolean not = config.startsWith("not");
			String season = not ? config.split("-")[1] : config;

			obj.addReq(new ReqSeason(NSeason.valueOf(season.toUpperCase()), not));
			return;
		case "daytime":
			String[] times = config.split("-");
			obj.addReq(new ReqDayTime(Boolean.parseBoolean(times[0]), Boolean.parseBoolean(times[1]), Boolean.parseBoolean(times[2])));
			return;
		case "day":
			obj.addReq(new ReqDayTime(config));
			return;
		case "date":
			String[] days = config.split("-");
			obj.addReq(new ReqDate(Integer.parseInt(days[0]), Integer.parseInt(days[1])));
			return;
		default:
			YLog.log("req type unknown", type);
		}
	}
}

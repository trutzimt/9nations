package de.ninenations.data.events;

import com.badlogic.gdx.files.FileHandle;

import de.ninenations.actions.action.ActionPlayerOtherMulti;
import de.ninenations.actions.action.ActionPlayerRessMulti;
import de.ninenations.actions.action.ActionProduceRandom;
import de.ninenations.actions.action.ActionResearchFinish;
import de.ninenations.actions.req.ReqTown;
import de.ninenations.data.NData;
import de.ninenations.data.config.BaseParser;
import de.ninenations.player.Player.RessType;
import de.ninenations.util.YLog;

public class ParserEvent extends BaseParser<BaseEvent> {

	public ParserEvent(NData nData, FileHandle file) {
		super(nData, file, 1, 2);
	}

	@Override
	protected void run(String type, String mparam, String param) {
		switch (type) {
		case "icon":
			active.setIcon(parseInt(param, 0));
			return;
		case "desc":
			active.setDesc(param);
			return;
		case "length":
			active.setLength(parseInt(param, 1));
			return;
		case "req":
			req(active, mparam, param);
			return;
		case "produce":
		case "consume":
		case "ress":
		case "finish":
		case "playerMulti":
			action(type, mparam, param);
			return;
		default:
			YLog.log("type unknown", type);
		}

	}

	protected void action(String type, String mparam, String param) {
		switch (type) {
		case "produce":
			active.getStartActionsRaw().add(new ActionPlayerRessMulti(RessType.PRODUCE, mparam, parseInt(param, 0)));
			active.getEndActionsRaw().add(new ActionPlayerRessMulti(RessType.PRODUCE, mparam, -parseInt(param, 0)));
			return;
		case "consume":
			active.getStartActionsRaw().add(new ActionPlayerRessMulti(RessType.CONSUME, mparam, parseInt(param, 0)));
			active.getEndActionsRaw().add(new ActionPlayerRessMulti(RessType.CONSUME, mparam, -parseInt(param, 0)));
			return;
		case "ress":
			active.getStartActionsRaw().add(new ActionProduceRandom(null).setValue(parseInt(param, -1)).addReq(new ReqTown(false, 1)));
			return;
		case "finish":
			if (mparam.equals("research")) {
				active.getStartActionsRaw().add(new ActionResearchFinish());
				return;
			}
			YLog.log("type unknown", type);
			return;
		case "playerMulti":
			active.getStartActionsRaw().add(new ActionPlayerOtherMulti(mparam, parseInt(param, 0)));
			active.getEndActionsRaw().add(new ActionPlayerOtherMulti(mparam, -parseInt(param, 0)));
			return;
		default:
			YLog.log("type unknown", type);
		}
	}

	@Override
	protected BaseEvent registerType(String[][] data, int l) {
		String param = data[l][0];
		// contain?
		if (nData.existEv(param)) {
			return nData.getEv(param);
		}

		BaseEvent active = new BaseEvent(param);
		nData.add(active);
		return active;
	}

	@Override
	protected void unRegisterType(String[][] data, int l) {
		active = null;

	}

}

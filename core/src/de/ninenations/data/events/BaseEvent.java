/**
 * 
 */
package de.ninenations.data.events;

import java.io.Serializable;

import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.actions.base.BaseReq;
import de.ninenations.actions.base.GAction;
import de.ninenations.actions.base.IReq;
import de.ninenations.core.NArray;
import de.ninenations.game.map.NOnMapObject;
import de.ninenations.player.Player;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;

/**
 * @author sven
 *
 */
public class BaseEvent extends BaseDisplay implements Serializable, IReq<BaseEvent> {

	private static final long serialVersionUID = -591807974574073844L;

	private int length, icon;
	private String desc;

	private NArray<GAction> startActions, endActions;
	private NArray<BaseReq> reqs;

	@SuppressWarnings("unused")
	private BaseEvent() {}

	/**
	 * @param type
	 * @param name
	 */
	public BaseEvent(String type) {
		super(type, type);

		startActions = new NArray<>();
		endActions = new NArray<>();
		reqs = new NArray<>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		return YIcons.getIconI(icon);
	}

	/**
	 * Get the id for the icon
	 * 
	 * @return
	 */
	public int getIconID() {
		return icon;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getInfoPanel()
	 */
	@Override
	public YTable getInfoPanel() {
		YTable t = super.getInfoPanel();
		t.add(desc).colspan(2).row();
		t.addL("Length", length + " rounds");

		// add actions
		t.addH("Actions");
		for (GAction g : startActions) {
			g.addToTable(t);
		}
		return t;
	}

	/**
	 * @return the length
	 */
	public int getLength() {
		return length;
	}

	/**
	 * @param length
	 *            the length to set
	 */
	public void setLength(int length) {
		this.length = length;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc
	 *            the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}

	/**
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(int icon) {
		this.icon = icon;
	}

	/**
	 * @return the actions
	 */
	public NArray<GAction> getStartActionsRaw() {
		return startActions;
	}

	/**
	 * @return the actions
	 */
	public NArray<GAction> getEndActionsRaw() {
		return endActions;
	}

	/**
	 * @return the reqs
	 */
	public NArray<BaseReq> getReqsRaw() {
		return reqs;
	}

	/**
	 * Check if it can used
	 * 
	 * @param player
	 * @param onMap
	 * @param x
	 * @param y
	 * @return true=can used, otherwise false
	 */
	public boolean check(Player player, NOnMapObject onMap, int x, int y) {
		for (BaseReq r : reqs) {
			if (!r.checkReq(player, onMap, x, y)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Perform the start actions
	 * 
	 * @param player
	 * @param onMap
	 * @param x
	 * @param y
	 */
	public void performStart(Player player, NOnMapObject onMap, int x, int y) {
		for (GAction g : startActions) {
			g.perform(player, onMap, x, y);
		}
	}

	/**
	 * Perform the end actions
	 * 
	 * @param player
	 * @param onMap
	 * @param x
	 * @param y
	 */
	public void performEnd(Player player, NOnMapObject onMap, int x, int y) {
		for (GAction g : endActions) {
			g.perform(player, onMap, x, y);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.ninenations.actions.base.IReq#addReq(de.ninenations.actions.base.BaseReq)
	 */
	@Override
	public BaseEvent addReq(BaseReq req) {
		reqs.add(req);
		return this;
	}

}

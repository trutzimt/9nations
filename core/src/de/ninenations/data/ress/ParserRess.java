package de.ninenations.data.ress;

import com.badlogic.gdx.files.FileHandle;

import de.ninenations.data.NData;
import de.ninenations.data.config.BaseParser;
import de.ninenations.util.YLog;

public class ParserRess extends BaseParser<BaseRess> {

	public ParserRess(NData nData, FileHandle file) {
		super(nData, file, 1, 1);
	}

	@Override
	protected void run(String type, String mparam, String param) {
		switch (type) {
		case "icon":
			active.setIcon(Integer.parseInt(param));
			return;
		case "sound":
			active.setSound(param);
			return;
		case "market":
			active.setMarketCost(Integer.parseInt(param));
			return;
		case "storage":
			active.setStorageWeight(Float.parseFloat(param));
			return;
		case "rechangeatdestroy":
			active.setRechangeAtDestroy(Integer.parseInt(param));
			return;
		default:
			YLog.log("type unknown", type);
		}

	}

	@Override
	protected BaseRess registerType(String[][] data, int l) {
		String param = data[l][0];
		// contain?
		if (nData.existRS(param)) {
			return nData.getR(param);
		}

		return nData.add(new BaseRess(param));
	}

	@Override
	protected void unRegisterType(String[][] data, int l) {
		active = null;

	}

}

/**
 *
 */
package de.ninenations.data.ress;

import java.util.HashMap;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.layout.HorizontalFlowGroup;
import com.kotcrab.vis.ui.widget.Tooltip;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;

import de.ninenations.game.S;
import de.ninenations.towns.Town;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YTextButton;
import de.ninenations.ui.window.YWindow;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class SendRessWindow extends YWindow {

	private VisTable ressTable, buttonTable, townTable;

	private String send;

	private Town town, destTown;

	/**
	 * @param title
	 */
	public SendRessWindow(final Town town) {
		super("Buy & Sell");

		// load towns
		this.town = town;

		// build header
		add(new VisLabel("Send"));
		add(new VisLabel("Towns")).row();

		// build drop down list
		ressTable = new VisTable();
		updateRessPanel();

		add(ressTable).grow();

		townTable = new VisTable();
		updateTownPanel();

		VisScrollPane scrollPane = new VisScrollPane(townTable);
		scrollPane.setFadeScrollBars(false);
		scrollPane.setFlickScroll(false);
		scrollPane.setOverscroll(false, false);
		scrollPane.setScrollingDisabled(true, false);

		add(scrollPane).grow().row();

		buttonTable = new VisTable();
		add(buttonTable).colspan(2).fillX();
		updateButtons();

		addTitleIcon(YIcons.getIconI(YIcons.RESSOURCE));
		pack();
	}

	private void updateTownPanel() {
		// list the towns
		townTable.clearChildren();

		for (final Town t : S.town().getTownsByPlayer(S.actPlayer())) {
			// skip town?
			if (t == town) {
				continue;
			}

			VisImageTextButton b = new VisImageTextButton(t.getName(), t.getIcon().getDrawable());
			b.addListener(new YChangeListener(false) {

				@Override
				public void changedY(Actor actor) {
					destTown = t;
					YSounds.pClick();
					updateButtons();
					updateTownPanel();
				}
			});
			if (t == destTown) {
				b.getColor().a = 0.5f;
			}

			townTable.add(b).fillX().row();
		}
	}

	protected void updateRessPanel() {
		ressTable.clearChildren();

		// add head
		boolean found = false;

		// Add all the buildings
		Array<String> types = S.nData().getRess();

		HashMap<String, Integer> ress = new HashMap<>();

		// ress it
		for (String type : types) {

			if (S.nData().getR(type).getMarketCost() == 0) {
				continue;
			}

			if (town.getRess(type) == 0) {
				continue;
			}

			ress.put(type, town.getRess(type));
			found = true;

		}

		if (found) {
			// sell it
			WidgetGroup group = new HorizontalFlowGroup(2);
			for (final String type : ress.keySet()) {
				BaseRess r = S.nData().getR(type);
				VisImageTextButton b = new VisImageTextButton(Integer.toString(ress.get(type)), r.getIcon().getDrawable());
				b.addListener(new YChangeListener(false) {

					@Override
					public void changedY(Actor actor) {
						send = type;
						YSounds.pClick();
						updateButtons();
						updateRessPanel();
					}
				});
				if (type.equals(send)) {
					b.getColor().a = 0.5f;
				}

				new Tooltip.Builder("Sell " + r.getName()).target(b).build();

				group.addActor(b);
			}

			VisScrollPane scrollPane = new VisScrollPane(group);
			scrollPane.setFadeScrollBars(false);
			scrollPane.setFlickScroll(false);
			scrollPane.setOverscroll(false, false);
			scrollPane.setScrollingDisabled(true, false);

			ressTable.add(scrollPane).grow();
		} else {
			ressTable.add(new VisLabel("You have no ress to send"));
		}

	}

	protected void updateButtons() {
		buttonTable.clearChildren();

		buildButton(1);
		buildButton(10);
		buttonTable.row();
		buildButton(100);
		buildButton(1000);

	}

	/**
	 * Add one button
	 *
	 * @param in
	 */
	protected void buildButton(final int hIn) {

		// build stings
		String msg = "Send " + hIn + " " + (send == null ? "unknown" : S.nData().getR(send).getName()) + " to "
				+ (destTown == null ? "unknown" : destTown.getName());

		// add it
		YTextButton text = new YTextButton(msg) {

			@Override
			public void perform() {
				// TODO add time for sending more
				// sell
				town.addRess(send, -hIn);
				// buy
				destTown.addRess(send, hIn);

				YSounds.play(YSounds.BUY);

				// update
				updateRessPanel();
				updateButtons();
			}
		};
		text.setDisabled(send == null || hIn > town.getRess(send) || destTown == null);

		buttonTable.add(text).fillX();
	}

}

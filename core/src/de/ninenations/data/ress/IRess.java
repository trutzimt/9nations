/**
 *
 */
package de.ninenations.data.ress;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * @author sven
 *
 */
public interface IRess {

	/**
	 * Give the element some ress
	 *
	 * @param type
	 * @param count
	 */
	public void addRess(String type, int count);

	/**
	 * Has that specific ress?
	 *
	 * @param type
	 * @return 0 or the ress count
	 */
	public int getRess(String type);

	/**
	 * Add a new notification
	 *
	 * @param title
	 * @param desc
	 * @param icon
	 */
	public void addNotification(String title, String desc, TextureRegion icon);

	/**
	 * Get the wealth multiplicator or 0 if is disabled
	 * 
	 * @return
	 */
	public float getWealthMultiplicator();

}
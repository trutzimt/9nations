package de.ninenations.data.ress;

import java.io.Serializable;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import de.ninenations.core.NN;
import de.ninenations.ui.BaseDisplay;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.YLog;
import de.ninenations.util.YSounds;

/**
 * 
 */
public class BaseRess extends BaseDisplay implements Serializable {

	private static final long serialVersionUID = -4405398095678269719L;

	public static final String FAITH = "faith", CULTURE = "culture", SAFETY = "safety", STONE = "stone", WOOD = "wood", FOOD = "food", GOLD = "gold",
			WORKER = "worker", WORKERMAX = "workermax", RESEARCH = "research", PLANK = "plank", BRICK = "brick", ORE = "ore", TOOL = "tool", WEAPON = "weapon",
			WEALTH = "wealth";

	/**
	 * ID of the icon
	 */
	private int icon;

	/**
	 * Cost on the market. 0=not shopable
	 */
	private float marketCost;

	/**
	 * Weight in storage, 0=not shown in the ress overview
	 */
	private float storageWeight;

	/**
	 * How much procent you get back at destry
	 */
	private int rechangeAtDestroy;

	private String sound;

	@SuppressWarnings("unused")
	private BaseRess() {
		super();
	}

	public BaseRess(String type) {
		super(type, type);

		marketCost = 1;
		storageWeight = 1;
		rechangeAtDestroy = 50;
		icon = -1;
	}

	@Override
	public YTable getInfoPanel() {
		YTable table = new YTable();
		table.addL("Name", name);
		table.addL("Cost on market", marketCost + "");
		table.addL("Storage weight", storageWeight + "");

		return table;
	}

	/**
	 * @return the marketCost
	 */
	public float getMarketCost() {
		return marketCost;
	}

	/**
	 * @return the storageWeight
	 */
	public float getStorageWeight() {
		return storageWeight;
	}

	/**
	 * @return the rechangeAtDestroy
	 */
	public int getRechangeAtDestroy() {
		return rechangeAtDestroy;
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(icon);
	}

	/**
	 * @param marketCost
	 *            the marketCost to set
	 */
	public void setMarketCost(float marketCost) {
		this.marketCost = marketCost;
	}

	/**
	 * @param storageWeight
	 *            the storageWeight to set
	 */
	public void setStorageWeight(float storageWeight) {
		this.storageWeight = storageWeight;
	}

	/**
	 * @param rechangeAtDestroy
	 *            the rechangeAtDestroy to set
	 */
	public void setRechangeAtDestroy(int rechangeAtDestroy) {
		this.rechangeAtDestroy = rechangeAtDestroy;
	}

	/**
	 * @return the iID
	 */
	public int getiID() {
		return icon;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.data.buildingunitbase.DataObject#validate()
	 */
	@Override
	public void validate() {
		super.validate();
		if (icon == -1) {
			YLog.log("Validate", type, "has no icon");
		}
	}

	/**
	 * @param iID
	 *            the iID to set
	 */
	public void setIcon(int icon) {
		this.icon = icon;
	}

	/**
	 * Play the sound
	 */
	public void playSound() {
		if (sound == null) {
			YSounds.pClick();
		} else {
			YSounds.play(sound);
		}
	}

	/**
	 * @param sound
	 *            the sound to set
	 */
	public void setSound(String sound) {
		this.sound = sound;
	}

	/**
	 * Load the asset
	 */
	public void assetLoad() {
		if (sound != null) {
			NN.asset().load("system/sound/" + sound + ".mp3", Sound.class);
		}

	}

}
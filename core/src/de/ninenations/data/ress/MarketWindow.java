/**
 *
 */
package de.ninenations.data.ress;

import java.util.HashMap;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup;
import com.badlogic.gdx.utils.Array;
import com.kotcrab.vis.ui.layout.HorizontalFlowGroup;
import com.kotcrab.vis.ui.widget.Tooltip;
import com.kotcrab.vis.ui.widget.VisImageButton;
import com.kotcrab.vis.ui.widget.VisImageTextButton;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisScrollPane;
import com.kotcrab.vis.ui.widget.VisTable;

import de.ninenations.game.S;
import de.ninenations.towns.Town;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YProgressBar;
import de.ninenations.ui.elements.YTextButton;
import de.ninenations.ui.window.YWindow;
import de.ninenations.util.YSounds;

/**
 * @author sven
 *
 */
public class MarketWindow extends YWindow {

	private VisTable ressTable, buttonTable;

	private String sell, buy;

	private Town town;

	/**
	 * @param title
	 */
	public MarketWindow(final Town town) {
		super("Buy & Sell");

		// load towns
		this.town = town;

		// build drop down list

		ressTable = new VisTable();
		updateRessPanel();

		add(ressTable).grow().row();

		buttonTable = new VisTable();
		add(buttonTable).fillX();
		updateButtons();

		addTitleIcon(YIcons.getIconI(YIcons.SHOP));
		pack();
	}

	private void updateRessPanel() {
		ressTable.clearChildren();

		// add bar?
		YProgressBar y = new YProgressBar(town.getActStorage(), town.getMaxStorage());
		y.getLabel().setText("Storage " + y.getLabel().getText());
		ressTable.add(y).colspan(2).row();

		// add head
		ressTable.add(new VisLabel("Sell"));
		ressTable.add(new VisLabel("Buy")).row();

		boolean found = false;

		// Add all the buildings
		Array<String> types = S.nData().getRess();

		HashMap<String, Integer> ressSell = new HashMap<>(), ressBuy = new HashMap<>();

		// ress it
		for (String type : types) {

			if (S.nData().getR(type).getMarketCost() == 0) {
				continue;
			}

			ressBuy.put(type, 0);

			if (town.getRess(type) == 0) {
				continue;
			}

			ressSell.put(type, town.getRess(type));
			found = true;

		}

		if (found) {
			// sell it
			WidgetGroup group = new HorizontalFlowGroup(2);
			for (final String type : ressSell.keySet()) {
				BaseRess r = S.nData().getR(type);
				VisImageTextButton b = new VisImageTextButton(Integer.toString(ressSell.get(type)), r.getIcon().getDrawable());
				b.addListener(new YChangeListener(false) {

					@Override
					public void changedY(Actor actor) {
						sell = type;
						YSounds.pClick();
						updateButtons();
						updateRessPanel();
					}
				});
				if (type.equals(sell)) {
					b.getColor().a = 0.5f;
				}

				new Tooltip.Builder("Sell " + r.getName()).target(b).build();

				group.addActor(b);
			}

			VisScrollPane scrollPane = new VisScrollPane(group);
			scrollPane.setFadeScrollBars(false);
			scrollPane.setFlickScroll(false);
			scrollPane.setOverscroll(false, false);
			scrollPane.setScrollingDisabled(true, false);

			ressTable.add(scrollPane).grow();

			// buy it
			group = new HorizontalFlowGroup(2);
			for (final String type : ressBuy.keySet()) {
				BaseRess r = S.nData().getR(type);

				if (r.getMarketCost() == 0) {
					continue;
				}

				VisImageButton b = new VisImageButton(r.getIcon().getDrawable(), "Buy " + r.getName());
				b.addListener(new YChangeListener(false) {

					@Override
					public void changedY(Actor actor) {
						buy = type;
						YSounds.pClick();
						updateButtons();
						updateRessPanel();
					}
				});
				if (type.equals(buy)) {
					b.getColor().a = 0.5f;
				}

				group.addActor(b);
			}

			scrollPane = new VisScrollPane(group);
			scrollPane.setFadeScrollBars(false);
			scrollPane.setFlickScroll(false);
			scrollPane.setOverscroll(false, false);
			scrollPane.setScrollingDisabled(true, false);

			ressTable.add(scrollPane).grow().row();
		} else {
			ressTable.add(new VisLabel("You have no ress to sell"));
		}

	}

	protected void updateButtons() {
		buttonTable.clearChildren();

		buildButton(1);
		buildButton(10);
		buttonTable.row();
		buildButton(100);
		buildButton(1000);

	}

	/**
	 * Add one button
	 *
	 * @param in
	 */
	protected void buildButton(int in) {
		// calculate price?
		int out = 0;
		if (sell != null && buy != null) {
			BaseRess sR = S.nData().getR(sell);
			BaseRess bR = S.nData().getR(buy);

			out = (int) Math.floor((double) sR.getMarketCost() * in / bR.getMarketCost());
			// change in?
			if (out == 0) {
				out = 1;
				in = (int) Math.ceil((double) out * bR.getMarketCost() / sR.getMarketCost());
			}
		}

		final int hIn = in, hOut = out;

		// build stings
		String msg = in + " " + (sell == null ? "unknown" : S.nData().getR(sell).getName()) + " to " + out + " "
				+ (buy == null ? "unknown" : S.nData().getR(buy).getName());

		// add it
		YTextButton text = new YTextButton(msg) {

			@Override
			public void perform() {
				// sell
				town.addRess(sell, -hIn);
				// buy
				town.addRess(buy, hOut);

				YSounds.play(YSounds.BUY);

				// update
				updateRessPanel();
				updateButtons();
			}
		};
		text.setDisabled(sell == null || buy == null || hIn > town.getRess(sell));

		buttonTable.add(text).fillX();
	}

}

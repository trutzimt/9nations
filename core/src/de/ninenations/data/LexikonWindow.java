/**
 * 
 */
package de.ninenations.data;

import de.ninenations.game.S;
import de.ninenations.ui.window.YTabWindow;

/**
 * @author sven
 *
 */
public class LexikonWindow extends YTabWindow {

	/**
	 * @param title
	 * @param error
	 */
	public LexikonWindow() {
		super("Lexicon");

		// add tabs
		S.nData().buildLexikon(tabbedPane);
		// TODO add concept tab

		buildIt();
	}

}

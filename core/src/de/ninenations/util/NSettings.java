/**
 *
 */
package de.ninenations.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

import de.ninenations.core.NN;

/**
 * @author sven
 *
 */
public class NSettings {

	public static final double VERSION = 0.15;
	public static final String NAME = "9 Nations";

	public static final String NOTICATIONDURATION = "notdur", GUIDESIGN = "guidesign", ONEXTUPDATECHECK = "nextUpdateCheck",
			OENABLEUPDATECHECK = "enableUpdateCheck", SHOWDISABLED = "showdisabledbutton";

	private NSettings() {}

	/**
	 * Get it
	 *
	 * @return
	 */
	public static Preferences getPref() {
		return NN.pref();
	}

	/**
	 * Get it
	 *
	 * @return
	 */
	public static void save() {
		if (getPref() != null) {
			getPref().putInteger("screen.width", Gdx.graphics.getWidth());
			getPref().putInteger("screen.height", Gdx.graphics.getHeight());

			getPref().flush();

			YLog.log("Settings", "Save it");
		}
	}

	/**
	 * Return if the user select the bigger gui
	 *
	 * @return
	 */
	public static boolean isBiggerGui() {
		return getPref().getBoolean("biggerGui", true);
	}

	/**
	 * Return if the user select the bigger gui
	 *
	 * @return
	 */
	public static int getGuiScale() {
		return getPref().getInteger("guiScale", 32);
	}

	/**
	 * Return the folder for the gui scale
	 *
	 * @return
	 */
	public static String getGuiScaleFolder() {
		return getGuiScale() == 32 ? "system" : Integer.toString(getGuiScale());
	}

	/**
	 * Get the selected language
	 *
	 * @return
	 */
	@Deprecated
	public static String getLang() {
		return getPref().getString("lang", "def");
	}

	/**
	 * Get the selected length to show the notifications
	 *
	 * @return
	 */
	public static int getNoticationDuration() {
		return getPref().getInteger(NOTICATIONDURATION, 10);
	}

	/**
	 * Enabled Autosave
	 *
	 * @return
	 */
	public static boolean isAutosave() {
		return getPref().getBoolean("autosave", true);
	}

	/**
	 * Enabled Fullscreen
	 *
	 * @return
	 */
	public static boolean isFullscreen() {
		return getPref().getBoolean("fullscreen", true);
	}

	/**
	 * Get sound volume
	 *
	 * @return
	 */
	public static float getSoundVolume() {
		return getPref().getFloat("sound", 0.5f);
	}

	/**
	 * Get music volume
	 *
	 * @return
	 */
	public static float getMusicVolume() {
		return getPref().getFloat("music", 0.5f);
	}

	/**
	 * Is the game in the dev mode?
	 * 
	 * @return
	 */
	public static boolean isDebug() {
		return getPref().getBoolean("dev", false);
	}

	/**
	 * Check if the updates enabled
	 * 
	 * @return
	 */
	public static boolean isEnableUpdateCheck() {
		return getPref().getBoolean(OENABLEUPDATECHECK, true);
	}

	/**
	 * Time stamp for the next update check
	 * 
	 * @return
	 */
	public static long getNextUpdateCheck() {
		return getPref().getLong(ONEXTUPDATECHECK, 0);
	}

	/**
	 * Setting for vsync
	 * 
	 * @return
	 */
	public static boolean isVSync() {
		return getPref().getBoolean("vsync", false);
	}

	/**
	 * Get the selected username
	 *
	 * @return
	 */
	public static String getUserName() {
		return getPref().getString("playername", NN.plattform().getPlayerDefaultName());
	}

	public static boolean isCleanerGuiDesign() {
		return getPref().getBoolean(GUIDESIGN, false);
	}

	public static boolean isShowDisabled() {
		return getPref().getBoolean(SHOWDISABLED, false);
	}

}

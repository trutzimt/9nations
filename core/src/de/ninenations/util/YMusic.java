package de.ninenations.util;

import java.io.FileNotFoundException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.files.FileHandle;

import de.ninenations.core.NN;

public class YMusic {

	private int checkC;

	private Music music;
	private String actSong;
	private String newSong;
	private boolean newLoop;
	private boolean playList;

	public YMusic() {}

	/**
	 * Return a sound
	 * 
	 * @param name
	 * @return
	 */
	private static void play(String name, boolean loop) {
		if (NN.music() == null) {
			return;
		}
		NN.music().playList = false;
		NN.music().newLoop = loop;
		NN.music().newSong = name;
	}

	/**
	 * Stop the music
	 */
	public static void stop() {
		if (NN.music() == null) {
			return;
		}

		// has old music?
		if (NN.music().music != null) {
			NN.music().music.stop();
			NN.music().music.dispose();
		}
	}

	/**
	 * Stop the music
	 */
	public static void updateVol() {
		if (NN.music() == null) {
			return;
		}

		if (NSettings.getMusicVolume() == 0) {
			stop();
		}

		if (NN.music().music != null) {
			NN.music().music.setVolume(NSettings.getMusicVolume());
		}
	}

	/**
	 * Start the ingame music
	 */
	public static void startInGameMusic() {
		// has a folder?
		if (NN.music() == null) {
			return;
		}

		NN.music().playList = true;
		NN.music().generateNewSong();

	}

	/**
	 * Generate a new song
	 */
	private void generateNewSong() {
		newSong = "game/" + NGenerator.getRndA(Gdx.files.internal("music/game/list.txt").readString().split(","));
		YLog.log("next song", newSong);
		newLoop = false;
	}

	/**
	 * Play the click sound
	 */
	public static void mainmenu() {
		play("Our Tragic Kingdom", true);
	}

	/**
	 * Play the click sound
	 */
	public static void winScenario() {
		play("The Succesor", false);
	}

	/**
	 * Play the click sound
	 */
	public static void loseScenario() {
		play("Sleep and Rest", false);
	}

	/**
	 * Play the click sound
	 */
	public static void clickForOptions() {
		YSounds.play(YSounds.CLICK, NSettings.getMusicVolume());
	}

	/**
	 * @return the actSong
	 */
	public String getActSong() {
		return actSong;
	}

	/**
	 * Check the music
	 */
	public void act() {

		if (checkC >= 0) {
			checkC--;
			return;
		}
		checkC = 30;

		// ingame?
		if (playList && music != null && !music.isPlaying()) {
			generateNewSong();
		}

		// has a new song?
		if (newSong == null || newSong == actSong) {
			return;
		}

		// has old music? //fade out
		if (music != null) {
			stop();
		}

		// start new music
		if (NSettings.getMusicVolume() > 0) {
			String s = "music/" + newSong + ".mp3";
			FileHandle f = Gdx.files.internal(s);
			if (!f.exists()) {
				actSong = null;
				newSong = null;
				playList = false;
				YError.error(new FileNotFoundException(s), false);

				return;
			}

			music = Gdx.audio.newMusic(f);
			music.setLooping(newLoop);
			music.setVolume(NSettings.getMusicVolume());
			music.play();
			actSong = newSong;
			newSong = null;
		} else {
			music = null;
		}
	}

}

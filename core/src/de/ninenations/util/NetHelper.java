/**
 * 
 */
package de.ninenations.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpMethods;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.StringBuilder;

/**
 * @author sven
 *
 */
public class NetHelper {

	/**
	 * 
	 */
	private NetHelper() {}

	/**
	 * Open this url
	 * 
	 * @param url
	 * @param listener
	 */
	public static void getGETContent(String url, final NetListener listener) {
		YLog.log("Open url", url);
		// Make a GET request
		HttpRequest request = new HttpRequest(HttpMethods.GET);
		request.setTimeOut(2500);
		request.setUrl(url);

		// Send the request, listen for the response
		Gdx.net.sendHttpRequest(request, new HttpResponseListener() {
			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {

				// We're going to download the file to external storage, create the streams
				String result = httpResponse.getResultAsString();
				listener.changed(result);
			}

			@Override
			public void failed(final Throwable t) {
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						listener.error(t);
					}
				});
			}

			@Override
			public void cancelled() {
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						listener.error(null);
					}
				});
			}
		});
	}

	/**
	 * Open this url
	 * 
	 * @param url
	 * @param listener
	 */
	public static void sendPOSTContent(String url, ObjectMap<String, String> data, final NetListener listener) {
		YLog.log("Open post url", url);
		// Make a GET request
		HttpRequest request = new HttpRequest(HttpMethods.POST);

		StringBuilder build = new StringBuilder();
		// add data
		for (String key : data.keys()) {
			build.append(key);
			build.append("=");
			build.append(data.get(key));
			build.append("&");
		}
		request.setContent(build.toString());

		request.setTimeOut(2500);
		request.setUrl(url);

		// Send the request, listen for the response
		Gdx.net.sendHttpRequest(request, new HttpResponseListener() {
			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {

				// We're going to download the file to external storage, create the streams
				String result = httpResponse.getResultAsString();
				listener.changed(result);
			}

			@Override
			public void failed(final Throwable t) {
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						listener.error(t);
					}
				});
			}

			@Override
			public void cancelled() {
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						listener.error(null);
					}
				});
			}
		});
	}

}

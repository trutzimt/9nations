package de.ninenations.util;

public abstract class NetListener {

	public void changed(String result) {
		try {
			changedY(result);
		} catch (Exception e) {
			YError.error(e, false);
		}

	}

	/**
	 * 
	 * @param result
	 */
	public abstract void changedY(String result);

	/**
	 * 
	 * @param t
	 */
	public abstract void error(Throwable t);

}

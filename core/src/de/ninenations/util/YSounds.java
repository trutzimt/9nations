package de.ninenations.util;

import com.badlogic.gdx.audio.Sound;

import de.ninenations.core.NN;

public class YSounds {

	public static final String CLICK = "click3", BUZZER = "interface6", CANCEL = "interface2", RANDOM = "random3", ATTACK = "sword-unsheathe2",
			DEFEND = "swing3", BUY = "coin2", STARTGAME = "Won_", SWITCHTAB = "bookFlip1", WINDOWCLOSE = "bookClose", DESTROY = "qubodup-BangMid";

	private YSounds() {}

	/**
	 * 
	 */
	public static void loadSound() {
		// add sounds
		for (String s : new String[] { CLICK, BUZZER, CANCEL, RANDOM, ATTACK, DEFEND, BUY, STARTGAME, SWITCHTAB, WINDOWCLOSE, DESTROY }) {
			NN.asset().load("system/sound/" + s + ".mp3", Sound.class);
		}
	}

	/**
	 * Return a sound
	 *
	 * @param name
	 * @return
	 */
	private static Sound get(String name) {
		return NN.asset().get("system/sound/" + name + ".mp3", Sound.class);
	}

	/**
	 * Play the click sound
	 */
	public static void play(String name, float vol) {
		if (NSettings.getSoundVolume() > 0) {
			get(name).play(NSettings.getSoundVolume() * vol);
		}
	}

	/**
	 * Play the click sound
	 */
	public static void play(String name) {
		play(name, 1);
	}

	/**
	 * Play the click sound
	 */
	public static void pClick() {
		play(CLICK);
	}

	/**
	 * Play the click sound
	 */
	public static void flipTab() {
		// TODO play("Book2");
	}

	/**
	 * Play the click sound
	 */
	@Deprecated
	public static void pRandom() {
		play(RANDOM);
	}

	/**
	 * Play the click sound
	 */
	public static void pBuzzer() {
		play(BUZZER);
	}

	/**
	 * Play the click sound
	 */
	public static void pCancel() {
		play(CANCEL);
	}

	/**
	 * Play the click sound
	 */
	@Deprecated
	public static void buy() {
		play(BUY);
	}

	/**
	 * Play the click sound
	 */
	public static void nextDay() {
		// TODO play("Item");

	}

	/**
	 * Play the click sound
	 */
	@Deprecated
	public static void foundTown() {
		// TODO play("Item");

	}

	/**
	 * Play the click sound
	 */
	public static void die() {
		// TODO play("Item");

	}

}

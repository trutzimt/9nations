package de.ninenations.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.kotcrab.vis.ui.util.dialog.Dialogs;
import com.kotcrab.vis.ui.widget.VisCheckBox;

import de.ninenations.core.NN;
import de.ninenations.ui.NDialog;
import de.ninenations.ui.actions.YChangeListener;

public class NSettingsHelper {

	private NSettingsHelper() {}

	public static VisCheckBox generateOptionsBox(String title, final String id, boolean def, final YChangeListener action) {
		VisCheckBox box = new VisCheckBox(title, NSettings.getPref().getBoolean(id, def));
		box.addCaptureListener(new YChangeListener(true) {

			@Override
			public void changedY(Actor actor) {
				NSettings.getPref().putBoolean(id, ((VisCheckBox) actor).isChecked());
				if (action != null) {
					action.changedY(actor);
				}
			}
		});
		return box;
	}

	public static void checkforUpdates(final boolean showMessage, final Stage stage) {
		// need to check?
		if (showMessage) {} else {
			if (NSettings.isEnableUpdateCheck() && System.currentTimeMillis() > NSettings.getNextUpdateCheck()) {} else {
				return;
			}
		}

		NetHelper.getGETContent("https://9nations.de/files/update.php?ver=" + NSettings.VERSION + "&typ=" + NN.plattform().getID(), new NetListener() {

			@Override
			public void changedY(String result) {
				if (result == null) {
					Dialogs.showOKDialog(NN.screen().getStage(), "Update check error", "Can not check for updates. Check you network.");
					return;
				}

				String erg[] = result.split(";");
				boolean up = Boolean.parseBoolean(erg[0]);
				// has no update show it?
				if (!showMessage && !up) {
					NSettings.getPref().putLong(NSettings.ONEXTUPDATECHECK, System.currentTimeMillis() + 1000 * 60 * 60 * 23 * 7);
					return;
				}

				NDialog d = new NDialog(up ? "Update avaible" : "No Update avaible", erg[2]);
				if (up) {
					d.add("Download V" + erg[1], new YChangeListener() {

						@Override
						public void changedY(Actor actor) {
							Gdx.net.openURI("https://9nations.de/files/directLinks.php?typ=" + NN.plattform().getID());

						}
					});
					if (!showMessage) {
						d.add("Inform tomorrow", new YChangeListener() {

							@Override
							public void changedY(Actor actor) {

								NSettings.getPref().putLong(NSettings.ONEXTUPDATECHECK, System.currentTimeMillis() + 1000 * 60 * 60 * 23);

							}
						});
					}
				}
				d.addCancel();
				d.show(stage);

			}

			@Override
			public void error(Throwable t) {
				if (t == null) {
					Dialogs.showOKDialog(NN.screen().getStage(), "Update check error", "Can not check for updates. Check you network.");
					return;
				}
				YError.error(t, false);

			}
		});
	}
}

/**
 * 
 */
package de.ninenations.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;

import de.ninenations.core.NN;
import de.ninenations.feedback.FeedbackWindow;
import de.ninenations.game.screen.MapScreen;
import de.ninenations.saveload.SaveLoadManagement;
import de.ninenations.screen.ErrorScreen;
import de.ninenations.screen.IScreen;

/**
 * @author sven
 *
 */
public class YError {

	public static final Array<Throwable> errors = new Array<>();
	private static boolean addError = false;

	private YError() {}

	/**
	 * Show an error
	 *
	 * @param t
	 */
	public static void error(final Throwable t, boolean fatal) {
		Gdx.app.error(NSettings.NAME, t.getMessage(), t);
		errors.add(t);

		if (addError) {
			Gdx.app.error("Error in Error", t.getMessage(), t);
			return;
		}
		addError = true;

		// try to save
		if (NN.get().getScreen() instanceof MapScreen) {
			try {
				SaveLoadManagement.save("crash", true);
			} catch (Throwable e) {
				Gdx.app.error(NSettings.NAME, e.getMessage(), e);
			}
		}

		// Arrays.toString(t.getStackTrace())
		if (!fatal && NN.get().getScreen() instanceof IScreen) {
			try {
				Gdx.app.postRunnable(new Runnable() {
					@Override
					public void run() {
						showLightError(t);

					}
				});
				addError = false;
				return;
			} catch (Exception e) {
				Gdx.app.error(NSettings.NAME, e.getMessage(), e);
			}
		}

		if (NN.get().getScreen() instanceof ErrorScreen) {
			NN.plattform().showErrorDialog(t.getClass() + ":" + t.getMessage());
		} else {
			Gdx.app.postRunnable(new Runnable() {

				@SuppressWarnings("deprecation")
				@Override
				public void run() {
					NN.get().setScreen(new ErrorScreen(t, NN.get().getScreen()));
				}
			});
		}
		addError = false;
	}

	/**
	 * @param t
	 */
	private static void showLightError(final Throwable t) {
		String title = t.getClass() + " " + t.getLocalizedMessage();

		FeedbackWindow w = (FeedbackWindow) NN.windows().open(FeedbackWindow.ID);
		w.setTitle(title);
		w.setText(t.getStackTrace()[0].toString() + "\n" + t.getStackTrace()[1].toString());
		w.setThrowable(t);
		w.pack();

		// NN.windows().setException

		// if (t instanceof Exception) {
		// Dialogs.showErrorDialog(((IScreen) NN.get().getScreen()).getStage(), title,
		// (Exception) t);
		// } else {
		// // build message
		// String mess = "\n" + t.getMessage();
		// for (int i = 0; i < 5; i++) {
		// mess += "\n" + t.getStackTrace()[i];
		// }
		// Dialogs.showErrorDialog(((IScreen) NN.get().getScreen()).getStage(), title,
		// mess);
		// }
	}
}

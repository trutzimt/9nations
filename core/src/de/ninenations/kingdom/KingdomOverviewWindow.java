/**
 *
 */
package de.ninenations.kingdom;

import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.ninenations.game.S;
import de.ninenations.game.ScenConf;
import de.ninenations.player.PlayersOverviewTab;
import de.ninenations.player.event.TimeEventTab;
import de.ninenations.towns.TownOverviewTab;
import de.ninenations.towns.WealthOverviewTab;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.newWindow.NTab;
import de.ninenations.ui.newWindow.NTabWindow;

/**
 * @author sven
 *
 */
public class KingdomOverviewWindow extends NTabWindow {

	public static final String ID = "kingdom";

	/**
	 * @param title
	 */
	public KingdomOverviewWindow() {
		super(ID, "Kingdom Overview", YIcons.RESSOURCE);

		addTab(new PlayerOverviewTab());
		addTab(new TownOverviewTab());

		// add happiness
		if (S.isActive(ScenConf.WEALTH) && S.town().getTownsByPlayerCount(S.actPlayer()) >= 2) {
			addTab(new WealthOverviewTab());
		}

		if (S.players().getSize() > 2) {
			addTab(new PlayersOverviewTab());
		}

		if (S.actPlayer().getNotifications().size > 0) {
			addTab(new NotificationTab());
		}

		if (S.actPlayer().getTimeEvents().getArrayRaw().size > 0) {
			addTab(new TimeEventTab());
		}

		addTitleIcon(YIcons.getIconI(YIcons.RESSOURCE));

		buildIt();
	}

	class PlayerOverviewTab extends NTab {

		public static final String ID = "playeroverview";

		public PlayerOverviewTab() {
			super(ID, "overview", S.actPlayer().getNation().getIcon());
		}

		@Override
		public Table getContentTable() {
			return S.actPlayer().getInfoPanel().createScrollPaneInTable();
		}

	}
}

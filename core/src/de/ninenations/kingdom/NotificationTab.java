/**
 * 
 */
package de.ninenations.kingdom;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.kotcrab.vis.ui.widget.VisImageTextButton;

import de.ninenations.game.S;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.YTable;
import de.ninenations.ui.newWindow.NTab;
import de.ninenations.ui.window.YNotificationSaver;

/**
 * @author sven
 *
 */
public class NotificationTab extends NTab {

	public static final String ID = "notification";

	public NotificationTab() {
		super(ID, "Notifications", YIcons.MESSAGE);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kotcrab.vis.ui.widget.tabbedpane.Tab#getContentTable()
	 */
	@Override
	public Table getContentTable() {
		YTable t = new YTable();
		// build it
		for (YNotificationSaver s : S.actPlayer().getNotifications()) {
			VisImageTextButton b = new VisImageTextButton(s.getText(), s.getIcon().getDrawable());
			// add action?
			if (s.getAction() == null) {
				b.setTouchable(Touchable.disabled);
			} else {
				b.addCaptureListener(s.getAction());
			}
			t.add(b).growX().row();
		}

		return t.createScrollPaneInTable();
	}

}

/**
 * 
 */
package de.ninenations.desktop;

import java.net.ConnectException;
import java.util.HashMap;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.kotcrab.vis.ui.widget.VisLabel;
import com.kotcrab.vis.ui.widget.VisTextButton;
import com.kotcrab.vis.ui.widget.VisTextField;

import de.golfgl.gdxgamesvcs.GameJoltClient;
import de.golfgl.gdxgamesvcs.IGameServiceIdMapper;
import de.ninenations.stats.gameservice.BaseGameService;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.actions.YChangeListener;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.YError;
import de.ninenations.util.NSettings;

/**
 * @author sven
 *
 */
public class GameJoltGameService extends BaseGameService {

	private GameJoltClient gsc;
	private String uname, pwd;
	private VisLabel status;
	private VisTextButton sign;
	private HashMap<String, Integer> translate;

	/**
	 * 
	 */
	public GameJoltGameService() {
		super("gamejolt");

		status = new VisLabel();
		sign = new VisTextButton("?");

		sign.addListener(new YChangeListener(true) {

			@Override
			public void changedY(Actor actor) {
				if (gsc.isSessionActive()) {
					gsc.logOff();
				} else {
					if (uname.length() == 0 || pwd.length() == 0) {
						gsc.setGuestName("Guest");
					} else {
						gsc.setUserName(uname);
						gsc.setUserToken(pwd);
					}
					if (!gsc.logIn()) {
						status.setText("Cannot sign in: No credentials or session id given.");
					}

					NSettings.save();
					refreshStatusLabel();
				}

			}
		});

		leaderboardid = "370168";

		// translate it
		translate = new HashMap<>();
		translate.put("9towns", 97897);
		translate.put("1001gold", 97895);
		translate.put("42research", 97898);
		translate.put("finishtut", 97899);
		translate.put("destroytown", 97896);
		translate.put("4towns", 97823);

		// import
		uname = NSettings.getPref().getString(getBaseKey() + "name", "");
		pwd = NSettings.getPref().getString(getBaseKey() + "pwd", "");

		if (active && autoLogin) {
			init();
		}
	}

	@Override
	protected void init() {
		gsc = new GameJoltClient();
		gsc.initialize("364417", "3df888f19715d96762a9edb24e940a83");
		// login?
		if (active && autoLogin) {
			gsc.setUserName(uname);
			gsc.setUserToken(pwd);
			gsc.logIn();
		}

		igsc = gsc;

		gsc.setGjTrophyMapper(new IGameServiceIdMapper<Integer>() {
			@Override
			public Integer mapToGsId(String independantId) {
				if (translate.containsKey(independantId)) {
					return translate.get(independantId);
				}

				return null;
			}
		});
		gsc.setGjScoreTableMapper(new IGameServiceIdMapper<Integer>() {
			@Override
			public Integer mapToGsId(String independantId) {
				return Integer.parseInt(independantId);
			}
		});

		refreshStatusLabel();
	}

	@Override
	protected void logoff() {
		if (gsc == null) {
			return;
		}

		gsc.logOff();
		gsc = null;
		igsc = gsc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getIcon()
	 */
	@Override
	public Image getIcon() {
		// TODO Auto-generated method stub
		return YIcons.getIconI(YIcons.WARNING);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.ui.IDisplay#getName()
	 */
	@Override
	public String getName() {
		return "GameJolt";
	}

	@Override
	protected void buildConfigTable(YTable table) {
		VisTextField name = new VisTextField(uname);
		name.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				uname = ((VisTextField) actor).getText();
				NSettings.getPref().putString(getBaseKey() + "name", uname);

			}
		});
		VisTextField token = new VisTextField(pwd);
		token.addCaptureListener(new YChangeListener(false) {

			@Override
			public void changedY(Actor actor) {
				pwd = ((VisTextField) actor).getText();
				NSettings.getPref().putString(getBaseKey() + "pwd", pwd);

			}
		});

		table.addL("Username", name);
		table.addL("Token", token);

		// status label
		table.addL("Status", status);

		// login area
		// sign.clearListeners();
		// sign.addListener(new YChangeListener(true) {
		//
		// @Override
		// public void changedY(Actor actor) {
		// if (gsc.isSessionActive()) {
		// gsc.logOff();
		// } else {
		// if (uname.length() == 0 || pwd.length() == 0) {
		// gsc.setGuestName("Guest");
		// } else {
		// gsc.setUserName(uname);
		// gsc.setUserToken(pwd);
		// }
		// if (!gsc.logIn()) {
		// status.setText("Cannot sign in: No credentials or session id given.");
		// }
		//
		// refreshStatusLabel();
		// }
		//
		// }
		// });
		refreshStatusLabel();

		table.addL(null, sign);
	}

	private void refreshStatusLabel() {
		if (gsc == null) {
			return;
		}

		String newStatusText;

		if (gsc.isSessionActive()) {
			newStatusText = "Login as " + gsc.getPlayerDisplayName();
		} else if (gsc.isConnectionPending()) {
			newStatusText = "Connect ...";
		} else {
			newStatusText = "No Session";
		}

		status.setText(newStatusText);

		sign.setText(gsc.isSessionActive() ? "Sign out" : "Sign in");

	}

	@Override
	public void gsOnSessionActive() {
		refreshStatusLabel();

	}

	@Override
	public void gsOnSessionInactive() {
		refreshStatusLabel();

	}

	@Override
	public void gsShowErrorToUser(GsErrorType et, String msg, Throwable t) {
		ConnectException c = new ConnectException(et + " in " + getName() + ":" + msg);
		c.initCause(t);
		YError.error(c, false);

	}
}

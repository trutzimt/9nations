package de.ninenations.desktop;

import java.io.File;

import javax.swing.JOptionPane;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import de.ninenations.core.NN;

/**
 * Launches the desktop (LWJGL) application.
 */
public class DesktopLauncher {
	public static void main(String[] args) {

		try {
			new LwjglApplication(NN.init(new DesktopPlattform()), getDefaultConfiguration());
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, e.getLocalizedMessage() + ": " + e.getStackTrace()[0], e.getClass().toString(), JOptionPane.ERROR_MESSAGE);
		}
	}

	private static LwjglApplicationConfiguration getDefaultConfiguration() {
		LwjglApplicationConfiguration configuration = new LwjglApplicationConfiguration();
		configuration.title = "9 Nations";
		configuration.width = 800;
		configuration.height = 600;
		for (int size : new int[] { 512, 256, 128, 48, 32, 16 }) {
			if (new File("system/logo/logo_" + size + ".png").exists())
				configuration.addIcon("system/logo/logo_" + size + ".png", FileType.Internal);
		}
		return configuration;
	}
}

/**
 * 
 */
package de.ninenations.desktop;

import java.net.URL;
import java.net.URLClassLoader;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import javax.swing.JOptionPane;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.kotcrab.vis.ui.widget.LinkLabel;

import de.ninenations.core.BasePlattform;
import de.ninenations.core.NN;
import de.ninenations.mod.IMod;
import de.ninenations.mod.ModInfo;
import de.ninenations.ui.YIcons;
import de.ninenations.ui.elements.ITextfield;
import de.ninenations.ui.elements.YRandomField;
import de.ninenations.ui.elements.YTable;
import de.ninenations.util.NSettings;
import de.ninenations.util.NSettingsHelper;
import de.ninenations.util.YError;

/**
 * @author sven
 *
 */
public class DesktopPlattform extends BasePlattform {

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#formatDate(long)
	 */
	@Override
	public String formatDate(long date) {
		return new SimpleDateFormat().format(new Date(date));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.IPlattform#showErrorDialog(java.lang.String)
	 */
	@Override
	public void showErrorDialog(String message) {
		JOptionPane.showMessageDialog(null, message);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.base.IPlattform#startUpCode()
	 */
	@Override
	public void startUpCode() {
		Gdx.graphics.setTitle(NSettings.NAME + " V" + NSettings.VERSION);
		Gdx.graphics.setVSync(NSettings.isVSync());

		// change size?
		if (NSettings.isFullscreen()) {
			Gdx.graphics.setUndecorated(true);
			Gdx.graphics.setWindowedMode(Gdx.graphics.getDisplayMode().width, Gdx.graphics.getDisplayMode().height);
		} else {
			Gdx.graphics.setWindowedMode(NSettings.getPref().getInteger("screen.width", 800), NSettings.getPref().getInteger("screen.height", 600));
		}

		NN.get().getAchievements().addGameService(new GameJoltGameService());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.qossire.yaams.base.IPlattform#addGraphicOptions(de.qossire.yaams.ui.
	 * YTable)
	 */
	@Override
	public void addOptionsGraphic(YTable cont) {
		cont.addL(null, NSettingsHelper.generateOptionsBox("Fullscreen", "fullscreen", NSettings.isFullscreen(), null));
		cont.addL(null, NSettingsHelper.generateOptionsBox("vSnyc", "vsync", NSettings.isVSync(), null));
		cont.addI(YIcons.getIconI(YIcons.WARNING), "Please restart " + NSettings.NAME + " to see the changes");

	}

	@Override
	public void addOptionsAudio(YTable table) {
		// add music?
		if (NN.music() == null) {
			table.addL("Music", new LinkLabel("Download music addon", "https://9nations.de/download/music"));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.yaams.base.IPlattform#getAnimatedLabel(java.lang.String)
	 */
	@Override
	public Label getAnimatedLabel(String message) {
		// TypingLabel t = new TypingLabel(message, VisUI.getSkin());
		// t.setWrap(true);
		// return t;
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.yaams.base.IPlattform#getPlayerDefaultName()
	 */
	@Override
	public String getPlayerDefaultName() {
		return System.getProperties().getProperty("user.name", "Player");
	}

	@Override
	public String getSystemErrorInfo() {

		Object osv = "??", os = "??", java = "??";
		try {
			osv = System.getProperties().getProperty("os.version", "" + Gdx.app.getVersion());
			os = System.getProperties().getProperty("os.name", Gdx.app.getType().toString());
		} catch (Exception e1) {
			Gdx.app.error("Errorscreen", "Can not get OS", e1);
		}

		try {
			java = System.getProperties().getProperty("java.version", "?");
		} catch (Exception e1) {
			Gdx.app.error("Errorscreen", "Can not get java", e1);
		}

		// generate string
		return "OS:" + os + " " + osv + ", Java:" + java;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.yaams.base.IPlattform#showSystemSavePath(de.yaams.ui.YTable,
	 * com.badlogic.gdx.files.FileHandle)
	 */
	@Override
	public void showSystemSavePath(YTable table, FileHandle file) {
		table.addL("Absolut", file.file().getAbsolutePath());
	}

	@Override
	public Image getIcon() {
		return YIcons.getIconI(OsCheck.getOperatingSystemType().getIcon());
	}

	@Override
	public String getName() {
		return OsCheck.getOperatingSystemType().getName();
	}

	@Override
	public String getID() {
		return OsCheck.getOperatingSystemType().getID();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#createTextField()
	 */
	@Override
	public ITextfield createTextField(YRandomField base) {
		return new DesktopTextField(base);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#loadMod(de.ninenations.mod.ModInfo)
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "deprecation" })
	public IMod loadMod(ModInfo info) {
		try {
			FileHandle jar = info.getFolder().child(info.getType() + ".jar");
			URL[] urls = { jar.file().toURL() };

			URLClassLoader child = new URLClassLoader(urls, this.getClass().getClassLoader());
			Class classToLoad = Class.forName(info.getClassName(), true, child);
			child.close();
			return (IMod) classToLoad.newInstance();
		} catch (Exception e) {
			YError.error(e, false);
		}
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.ninenations.core.BasePlattform#getDayOfYear()
	 */
	@Override
	public int getDayOfYear() {
		return LocalDate.now().getDayOfYear();
	}

}

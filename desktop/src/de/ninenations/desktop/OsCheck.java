package de.ninenations.desktop;

/**
 * helper class to check the operating system this Java VM runs in
 *
 * please keep the notes below as a pseudo-license
 *
 * http://stackoverflow.com/questions/228477/how-do-i-programmatically-determine-operating-system-in-java
 * compare to http://svn.terracotta.org/svn/tc/dso/tags/2.6.4/code/base/common/src/com/tc/util/runtime/Os.java
 * http://www.docjar.com/html/api/org/apache/commons/lang/SystemUtils.java.html
 */
import java.util.Locale;

import de.ninenations.ui.YIcons;

public final class OsCheck {
	/**
	 * types of Operating Systems
	 */
	public enum OSType {
		WIN("Windows", YIcons.WINDOWS), MAC("Mac OS X", YIcons.MAC), LIN("Linux", YIcons.LINUX), Other("??", YIcons.LINUX);

		/**
		 * @param icon
		 * @param name
		 */
		private OSType(String name, int icon) {
			this.icon = icon;
			this.name = name;
		}

		private int icon;
		private String name;

		/**
		 * @return the icon
		 */
		public int getIcon() {
			return icon;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the name
		 */
		public String getID() {
			return toString().toLowerCase();
		}

	};

	// cached result of OS detection
	protected static OSType detectedOS;

	/**
	 * detect the operating system from the os.name System property and cache the
	 * result
	 * 
	 * @returns - the operating system detected
	 */
	public static OSType getOperatingSystemType() {
		if (detectedOS == null) {
			String OS = System.getProperty("os.name", "generic").toLowerCase(Locale.ENGLISH);
			if (OS.indexOf("mac") >= 0 || OS.indexOf("darwin") >= 0) {
				detectedOS = OSType.MAC;
			} else if (OS.indexOf("win") >= 0) {
				detectedOS = OSType.WIN;
			} else if (OS.indexOf("nux") >= 0) {
				detectedOS = OSType.LIN;
			} else {
				detectedOS = OSType.Other;
			}
		}
		return detectedOS;
	}
}